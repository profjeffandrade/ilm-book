\cleardoublepage
\chapter*{Prefácio}
\label{cha:prefacio}
\addcontentsline{toc}{chapter}{Prefácio}

\section*{Propósito e Objetivos}
\label{sec:prop-e-objet}

Este livro é uma primeira introdução à lógica formal voltada
principalmente à resolução de problemas. Ao longo dos anos ministrando
a disciplina de Lógica em cursos da área de informática, tais como
Sistemas de Informação, Análise e Desenvolvimento de Sistemas, e
Administração com Ênfase em Processamento de Dados, tenho percebido
que a forma tradicional de abordar a lógica que é usada em cursos mais
técnicos tais como Ciência da Computação, e Engenharia da Computação
tem pouca receptividade nos alunos dos cursos da área de informática,
i.e., dos cursos que trabalham a computação mais como meio do que como
fim. Para os alunos dos cursos em que a computação é a área fim, faz
sentido tratar de questões tais como demonstração do completude da
lógica proposicional, ou algoritmos de unificação, ou provas
automáticas de teoremas. Entretanto, para alunos dos cursos em que a
computação é meio, estas questões tem muito pouca relevância. Para
estes, a lógica deve ser tratada como ferramenta para a resolução de
problemas.

Assim, meu propósito ao iniciar a escrita deste livro foi preencher um
vazio que, na minha opinião, existe na bibliografia de livros
didáticos sobre lógica. Por um lado, embora exista uma boa quantidade
de títulos sobre ``raciocínio lógico'' (principalmente para
concursos), estes livros, de modo geral, não apresentam o rigor
matemático que as aplicações de lógica em computação costumam exigir
(sim, mesmo como ferramenta a lógica pede rigor), podendo ser
classificados como livros sobre lógica informal. Por outro lado, os
livros voltados para o público de nível superior, que apresentam um
grau aceitável de rigor matemático, por via de regra, tratam o estudo
da lógica como um fim em si mesmo, se atendo principalmente em provas
de propriedades sobre os sistemas dedutivos apresentados, e sobre
métodos e algoritmos de provas automáticas de teoremas. É evidente que
estes temas são importantes no estudo da lógica (eu mesmo sou
fascinado pro eles), mas não são muito relevantes ao estudante que
deseja utilizar a lógica apenas como ferramenta.

Se, após ler este livro e praticar os exercícios indicados, o
estudante tiver atingido um nível em que ele compreenda que a lógica
não apenas é um dos fundamentos da computação, mas também que a lógica
pode ser utilizada diretamente para resolver uma enorme quantidade de
problemas, e este estudante tiver confiança em sua proficiência na
resolução de alguns destes problemas utilizando os métodos indicados
neste livro, então o objetivo da existência deste livro terá sido
atingido e as muitas horas de trabalho gastas com escrita, preparação
de exercícios, revisão, correção e formatação terão sido
recompensadas.

Um objetivo secundário ao elaborar este livro, foi redigi-lo de tal
modo que ele fosse o mais autocontido possível, para minimizar a
necessidade de procurar material complementar em outras fontes. O
objetivo não é, de forma alguma, sugerir que este livro esgota o tema
ou desencorajar os estudantes a procurar outros materiais. Estudar o
mesmo tema em mais de uma fonte, ter mais de um ponto de vista e mais
de uma forma de explicar o mesmo assunto sempre é benéfico e
certamente irá ajudar a entender melhor os tópicos estudados. A
motivação para tornar o material autocontido é propiciar um fluxo de
estudo com o mínimo de interrupções possível. De modo que o estudante
possa ``atacar'' um determinado tópico do começo ao fim, e depois
disso, caso queira, poderá procurar outras fontes sobre o mesmo
tópico.


\section*{Abordagem}
\label{sec:abordagem}

Embora o propósito e o objetivo deste livro estivessem claros para mim
desde o início, a forma de atingir este objetivo não estava muito
clara -- e talvez ainda não esteja, ou talvez sempre vá estar meio
nebulosa e precisando ser redefinida. Mas não dá pra ficar parado sem
fazer nada esperando perfeição, então considerei algumas
possibilidades. Uma era a de utilizar uma abordagem totalmente baseada
em problemas, onde o problema fosse apresentado e em seguida a solução
utilizando lógica formal fosse apresentada. O problema dessa
abordagem, ao meu ver, é que uma vez que a lógica matemática exige
formalismo o aluno seria apresentado a uma solução em uma linguagem
que ele ainda não conhece e haveria pouca chance de que ele fosse
conseguir entender a solução apresentada. Além disso, depois de pensar
sobre o assunto por um tempo, não me pareceu muito humano jogar os
alunos na vastidão da lógica formal, para que eles achem seus próprios
caminhos, sem um primeiro curso de ``sobrevivência na selva''.

A outra possibilidade que considerei, e que foi aquela pela qual
acabei me decidindo, foi a de dividir o tema da lógica matemática em
``blocos aplicáveis'' (teoria dos conjuntos, cálculo proposicional,
lógica de predicados) e apresentar a fundamentação teórica de cada um
desses temas, seguido imediatamente pelo estudo de uma das aplicações
desses tópicos. Qualquer um dos tópicos da lógica matemática abordados
neste livro têm múltiplas aplicações práticas, então a escolha de qual
aplicação prática adotar para cada tópico também é um problema a se
considerar. O critério que adotei para a escolha das aplicações a
estudar foi o seguinte: uma vez que o livro é voltado para alunos de
curso na área de informática, escolhi aplicações mais próximas da
atividade de programação de computadores. Por exemplo, para o cálculo
de predicados a aplicação escolhida poderia ter sido o estudo de
circuitos lógicos, mas essa aplicação está mais próxima do contexto do
engenheiro de computação dou do engenheiro eletrônico do que do
bacharel em sistemas de informação ou do engenheiro de software, por
exemplo. Assim, para o caso do cálculo proposicional, optei pelo
estudo de prova de correção de algoritmos, que é uma atividade na qual
qualquer profissional de informática que desenvolva, ou supervisione o
desenvolvimento de sistemas deveria ter proficiência.


\section*{Público Alvo}
\label{sec:publico-alvo}

Este livro é voltado principalmente para os estudantes dos períodos
iniciais dos cursos de informática. O material não assume nenhuma
experiência prévia com lógica formal, nem com programação. Entretanto,
os alunos que já tem alguma experiência com programação provavelmente
terão mais facilidade em trabalhar os capítulos de aplicações que
lidam com as linguagens \href{https://www.python.org/}{Python} e
\href{http://www.swi-prolog.org/}{Prolog}.

A maioria dos estudantes de informática tem algum contato com
linguagens de programação imperativas e orientadas a objetos em algum
ponto dos seus cursos de graduação. A linguagem Python é uma linguagem
orientada a objetos pura, i.e., todos os valores na linguagem são
tratados como objetos, e provavelmente a maioria dos alunos dos cursos
de informática já tiveram, ou terão, algum contato com esta
linguagem. Já a linguagem Prolog, por outro lado, é uma linguagem que
pertence ao paradigma de programação lógica, que não costuma ser visto
na maioria dos cursos de informática, ou é visto de modo
superficial. Assim, mesmo que o estudante já tenha tido algum contato
com programação, é razoável assumir que ele, provavelmente, não terá
visto Prolog.

Com relação ao conhecimento prévio de matemática, o que se espera é
que o estudante tenha visto a matemática básica do ensino
médio. Principalmente no que se relaciona à aritmética e resolução de
equações moderadamente simples. Sem esse conhecimento, será difícil
acompanhar alguns dos exemplos, sobretudo quando falarmos sobre
conjuntos, relações e funções.


\section*{Visão Geral}
\label{sec:visao-geral}

O conteúdo deste livro está dividido em duas partes:
\begin{enumerate}[label={(\Roman*)}]
\item Conceitos Preliminares
\item Lógica Matemática
\end{enumerate}

Na parte destinada aos conceitos preliminares temos um capítulo
voltado par contar um pouco da história e da evolução da lógica
enquanto disciplina sistemática de estudo, um capítulo para tratar dos
conceitos de conjuntos, relações e funções, e um capítulo que
apresenta a forma como a linguagem Python lida com conjuntos e alguns
estudos de casos sobre resolução de problemas de programação usando
conjuntos. Em algumas grades curriculares, há disciplinas de
matemática discreta que já abrangem o tópico de conjuntos, muitas
vezes no contexto da análise combinatória. Se este for o caso o
professor da disciplina pode optar por omitir a parte de conceitos
preliminares e iniciar o curso diretamente na parte de lógica
matemática sem prejuízo para o curso.

A parte destinada à lógica matemática propriamente dita inicia com um
capítulo apresentando os conceitos fundamentais da lógica
formal. Conceitos como dedução e indução, verdade e validade,
raciocínio lógico e a diferença entre lógica formal e lógica informal
são discutidos neste capítulo inicial.

Em seguida temos o capítulo voltado para a lógica proposicional, que
descreve os conceitos de proposição e conectivos lógicos, valoração
lógica, tradução da linguagem natural para a forma simbólica e o
sistema de prova por dedução natural, e abordamos brevemente a
demonstração de argumentos formais.

O capítulo seguinte trata sobre demonstração de correção de
algoritmos, utilizando Lógica de Hoare. A cobertura dada ao tópico de
demonstração de correção não é completa, nem tão pouco aprofundada o
suficiente para aplicação em sistemas de grande porte. Este tipo de
aplicação exigiria o uso de assistentes de prova e/ou de provadores
automáticos de teoremas, que estão fora do escopo de uma abordagem
introdutória como a deste livro. Também não abordamos lógicas mais
sofisticadas como Lógica de Separação, por exemplo, pelo mesmo
motivo. Ao invés disso, focamos em uma lógica consideravelmente
similar à lógica proposicional e em demonstrações de algoritmos
relativamente simples. O propósito deste capítulo é o de mostrar como
é possível \textbf{provar} a correção de algoritmos através da lógica,
em contraste com meramente \emph{testar} os algoritmos.

Após o capítulo sobre demonstração de correção de algoritmos, temos um
capítulo sobre lógica de predicados de primeira ordem, ou lógica de
primeira ordem, de modo mais resumido. Assim como no capítulo sobre
lógica de predicados, neste capítulo temos as noções mais teóricas,
descrevendo o sistema lógico, os conceitos fundamentais de predicados,
variáveis, funções lógicas, quantificadores, interpretações, etc., e
ao final do capítulo falamos sobre o sistemas de provas por dedução
natural para a lógica de primeira ordem, e sobre argumentos formais.

O último e mais ``denso'' capítulo do livro trata sobre modelagem de
problemas usando lógica e da resolução destes problemas através de
programação lógica. Se você tem alguma experiência na área de
informática, já ouviu a expressão ``lógica de programação''. Não
confunda ``lógica de programação'' com ``programação lógica''.
\begin{description}
\item[``lógica de programação''] geralmente significa a solução de
  problemas através do desenvolvimento de algoritmos e a implementação
  destes algoritmos em alguma linguagem de programação.
\item[``programação lógica''] é um \emph{paradigma de programação}
  baseado na lógica formal. Um programa escrito em uma linguagem de
  programação lógica é formado por um conjunto de sentenças em forma
  lógica, expressando fatos e regras sobre algum \emph{domínio de
    problema}.
\end{description}

Se as expressões ``paradigma de programação'' e ``domínio de
problema'' não estão muito claras para você, não se preocupe, iremos
falar mais e melhor delas no último capítulo.


\section*{Agradecimentos}
\label{sec:agradecimentos}

Primeiramente, preciso agradecer à minha família. Agradeço a Karin, a
Anyssa, ao Jefferson e à Samiya, pela paciência que tiveram e
continuam tendo comigo, por não reclamarem (muito) de todo o tempo que
deveria ser livre, mas que eu gasto escrevendo, revisando e
aperfeiçoando este livro.

Este livro tem estado em constante desenvolvimento, pelo menos,
durante os últimos 3 anos. É claro que ele teve, tem e terá muitos
erros. Ao longo do tempo em que eu tenho utilizado este livro como
notas de aula, e depois como livro texto, vários alunos identificaram
incorreções e, gentilmente, me indicaram essas incorreções para que eu
pudesse corrigi-las. A estes alunos o meu muito obrigado.


\vspace{3.5cm}

\begin{flushright}
  Jefferson O.\ Andrade\\
  Vitória, 31 de outubro de 2016.
\end{flushright}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../ilm-book"
%%% ispell-local-dictionary: "brasileiro"
%%% End:
