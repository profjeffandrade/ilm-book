# -*- mode: shell-script; coding: utf-8; -*-
# Use of glossaries, extra indexes, and other images conversions
# Thanks to Herb Schulz

$pdflatex = 'lualatex -shell-escape %O %S';

# Custom dependency for glossary/glossaries package if you make custom
# glossaries you may have to add items to the @cus_dep_list and corresponding
# sub-routines
add_cus_dep( 'glo', 'gls', 0, 'glo2gls' );
sub glo2gls {
    system("xindy -L portuguese -C utf8 -I xindy -M \"$_[0]\" -t \"$_[0].glg\" -o \"$_[0].gls\" \"$_[0].glo\"");
}

# The glossaries package, with the [acronym] option, produces a .acn file when
# processed with (xe/pdf)latex and then makeindex to process the .acn into .acr
# and finally runs of (xe/pdf)latex to read in the .acr file. Unfortunately the
# glossary package does just the reverse; i.e. (xe/pdf)latex processing produces
# a .acr files and makeindex then is used to convert the .acr file to a .acn
# file which is then ... . This dependency assumes the glossaries package.
add_cus_dep( 'acn', 'acr', 0, 'acn2acr' );
sub acn2acr {
    system("xindy -L portuguese -C utf8 -I xindy -M \"$_[0]\" -t \"$_[0].alg\" -o \"$_[0].acr\" \"$_[0].acn\"");
}

# Added custom glossary type, this is for the new glossary type command
# \newglossary[slg]{symbol}{sym}{sbl}{Lista de Símbolos}
# from the glossaries package
add_cus_dep('sbl', 'sym', 0, 'sbl2sym');
sub sbl2sym {
    system("xindy -L portuguese -C utf8 -I xindy -M \"$_[0]\" -t \"$_[0].slg\" -o \"$_[0].sym\" \"$_[0].sbl\"");
}

# Custom dependency and function for nomencl package
add_cus_dep( 'nlo', 'nls', 0, 'makenlo2nls' );
sub makenlo2nls {
    system( "makeindex -s nomencl.ist -o \"$_[0].nls\" \"$_[0].nlo\"" );
}

# Custom dependecy to convert tif to png
add_cus_dep( 'tif', 'png', 0, 'tif2png' );
sub tif2png {
    system( "convert \"$_[0].tif\" \"$_[0].png\"" );
}
