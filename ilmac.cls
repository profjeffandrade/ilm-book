%%% ----------------------------------------------------------------------------
%%% Classe de de formatação do livro “Introdução à Lógica Matemática para Alunos
%%% de Computação”. Autor: Jefferson O. Andrade.
%%% ----------------------------------------------------------------------------
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{ilmac}[2022/04/25 v2.1.1 Livro ILMAC]

%%% --------------------------------------------------------------------
%%% Declaração das opções
%%% ---
%%% Esta classe estenda a classe `memoir'
%%% Lê todas as opções de `documentclass' e as passa para `memoir'.
%%% --------------------------------------------------------------------
\DeclareOption*{%
  \PassOptionsToClass{\CurrentOption}{memoir}}

\ProcessOptions\relax

\LoadClass{memoir}


%%% ----------------------------------------------------------------------------
%%% Carrega os pacotes auxiliares
%%% ----------------------------------------------------------------------------

% É preciso carregar o pacote `luatex85' para que o `xcolor' passe a aceitar o
% documento. Isso é um bug do xcolor e será corrigido (tomara).
\RequirePackage{luatex85}

\RequirePackage[svgnames,hyperref,table]{xcolor}
%\RequirePackage[luatex,dvipsnames]{color}

\RequirePackage{graphicx}

\RequirePackage{tikz}

\RequirePackage[framemethod=tikz]{mdframed}

\RequirePackage{amsmath}

\RequirePackage{amsthm}

\RequirePackage{amsfonts}

\RequirePackage{amssymb}

\RequirePackage{thmtools}

\RequirePackage{mathtools}

\RequirePackage{multicol}

\RequirePackage{setspace}

\RequirePackage[inline,shortlabels]{enumitem}

\RequirePackage{ccicons}

% \RequirePackage{answers}

\RequirePackage{minted}

\RequirePackage{subcaption}

\RequirePackage{calc}

\RequirePackage{minitoc}

%%% `hyperref' must be loaded at the end of the package list.
\RequirePackage[%
luatex,%
bookmarks,%
plainpages=false,%
colorlinks=true,%
linkcolor=blue%
]{hyperref}

%%% `glossaries' must be loaded after `hyperref'.
\RequirePackage[acronym,toc,xindy]{glossaries}


%%% --------------------------------------------------------------------
%%% Definições de macros e ambientes
%%% --------------------------------------------------------------------

\newcommand{\compl}[1]{{#1}^{\complement}}
\newcommand{\nto}{\nrightarrow}
\newcommand{\union}{\cup}
\newcommand{\intersect}{\cap}
\newcommand{\simdiff}{\triangle}
\newcommand{\abs}[1]{\left|#1\right|}
\newcommand{\valor}[1]{\ensuremath{\mathcal{V}(#1)}}
\newcommand{\lxor}{\veebar}
\newcommand{\liff}{\leftrightarrow}
\newcommand{\nullvalue}{\textsf{(null)}}

%% Definições para a lista de símbolos. Depende o pacote `glossaries'.
\newcommand{\listofsymbolsname}{Lista de Símbolos}
\newcommand{\symboltype}{symbol}

% Definição de comando para bibunits que coloca as referências no
% nível de seção.
% \newcommand{\mybibname}{Referências}
% \newcommand{\putbibex}[1][]{\section{\mybibname}
%   \renewcommand{\bibname}{}
%   \vspace{-2\baselineskip}
%   \addtocontents{toc}{\protect\setcounter{tocdepth}{-1}}
%   \ifthenelse{\isempty{#1}}{\putbib}{\putbib[#1]}
%   \addtocontents{toc}{\protect\setcounter{tocdepth}{2}}}

\setminted[python]{linenos=true,autogobble=true,breaklines=true,frame=lines}
\newcommand{\pyinline}[1]{\mintinline{python}{#1}}
\renewcommand{\listoflistingscaption}{Lista de Códigos Fonte}

\newcommand{\eoex}{Fim do Exemplo}

% Estilo de definições
\declaretheoremstyle[%
  headfont=\bfseries,
  notebraces={(}{)},
  bodyfont=\normalfont,
  headpunct={},
  postheadspace=\newline,
  postheadhook={\textcolor{green}{\rule[.6ex]{\linewidth}{0.4pt}}\\},
  spacebelow=1.5\parsep,
  spaceabove=1.5\parsep,
  % qed=\qedsymbol,
  mdframed={
    backgroundcolor=green!10,
    linecolor=green!35,
    innertopmargin=6pt,
    innerbottommargin=6pt,
    roundcorner=5pt,
    skipabove=\parsep,
    skipbelow=\parsep
  }
]{ilm-definition}
\declaretheorem[
style=ilm-definition,
name={Defini\c{c}\~ao},
numberwithin=chapter
]{defn}

% Estilo de exemplos
\declaretheoremstyle[%
  headfont=\bfseries,
  notebraces={(}{)},
  bodyfont=\normalfont,
  headpunct={},
  postheadspace=\newline,
  postheadhook={\textcolor{yellow}{\rule[.6ex]{\linewidth}{0.4pt}}\\},
  spacebelow=1.5\parsep,
  spaceabove=1.5\parsep,
  % qed=\qedsymbol,
  mdframed={
    backgroundcolor=yellow!10,
    linecolor=yellow!35,
    innertopmargin=6pt,
    innerbottommargin=6pt,
    roundcorner=5pt,
    skipabove=\parsep,
    skipbelow=\parsep
  }
]{ilm-example}
\declaretheorem[
style=ilm-example,
name={Exemplo},
numberwithin=chapter
]{exmp}

\newmdenv[
innerlinewidth=0.35pt,
roundcorner=3pt,
linecolor=black,
innerleftmargin=3pt,
innerrightmargin=3pt,
innertopmargin=3pt,
innerbottommargin=3pt
]{roundbox}

% \newcounter{example}[chapter]
% \numberwithin{example}{chapter}
% \newenvironment{exmp}[1][]{%
%   \refstepcounter{example}\par\bigskip%
%   \def\temp{#1}%
%   \ifx\temp\empty%
%   \noindent\textbf{Exemplo~\theexample.\hspace*{1ex}}\rmfamily%
%   \else%
%   \noindent\textbf{Exemplo~\theexample\; (#1).\hspace*{1ex}}\rmfamily%
%   \fi%
% }
% {
%   \hfill
%   \begin{minipage}{\widthof{\tiny{\eoex} plus 2mm}}
%     \begin{roundbox}
%       \begin{center}
%         \tiny{\eoex}
%       \end{center}
%     \end{roundbox}
%   \end{minipage}
%   \bigskip
% }

% Estilo de problemas
\declaretheoremstyle[%
  headfont=\bfseries,
  notebraces={(}{)},
  bodyfont=\normalfont,
  headpunct={},
  postheadspace=\newline,
  postheadhook={\textcolor{Red}{\rule[.6ex]{\linewidth}{0.4pt}}\\},
  spacebelow=1.5\parsep,
  spaceabove=1.5\parsep,
  % qed=\qedsymbol
  mdframed={
    backgroundcolor=Red!10,
    linecolor=Red!35,
    innertopmargin=6pt,
    innerbottommargin=6pt,
    roundcorner=5pt,
    skipabove=\parsep,
    skipbelow=\parsep
  }
]{ilm-problem}
\declaretheorem[
style=ilm-problem,
name={Problema},
numberwithin=chapter
]{problem}


%%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%%% Suporte à construção de soluções dos problemas propostos.
%%% TODO: Propagar corretamente o número do problema para a solução.
\providecommand{\thesolutionnumber}{}
\newtheorem*{solutionenv}{Problema \thesolutionnumber}

\ExplSyntaxOn
\tl_new:N \g_ilmac_solution_tl

\NewDocumentEnvironment{solution}{+b}
{
  \tl_gput_right:Nx \g_ilmac_solution_tl
  {
    \printsolution{\theproblem}{ \exp_not:n { #1 } }
  }
}
{}
\NewDocumentCommand{\printsolutions}{}
{
  \tl_use:N \g_ilmac_solution_tl
  \tl_gclear:N \g_ilmac_solution_tl
}
\NewDocumentCommand{\printsolution}{m +m}
{
  % \cs_set:Npn \thesolutionnumber { #1 }
  \cs_set_nopar:Npn \thesolutionnumber { #1 }
  \begin{solutionenv} #2 \end{solutionenv}
}
\ExplSyntaxOff


% Estilo de notas
\declaretheoremstyle[%
  headfont=\bfseries,
  notebraces={(}{)},
  bodyfont=\normalfont,
  headpunct={:},
  postheadspace={1em},
  spacebelow=1.5\parsep,
  spaceabove=1\parsep,
  mdframed={
    backgroundcolor=Purple!10,
    linecolor=Purple!35,
    innertopmargin=6pt,
    innerbottommargin=6pt,
    roundcorner=5pt,
    skipabove=\parsep,
    skipbelow=\parsep
  }
]{ilm-note}
\declaretheorem[
  style=ilm-note,
  name={Nota},
  numberwithin=chapter
]{note}

% \newtheoremstyle{ilm-exercise}% name of the style to be used
% {1em}% measure of space to leave above the theorem. E.g.: 3pt
% {1em}% measure of space to leave below the theorem. E.g.: 3pt
% {\rmfamily}% name of font to use in the body of the theorem
% {}% measure of space to indent
% {\bfseries}% name of head font
% {.}% punctuation between head and body
% {\newline}% space after theorem head; { } = normal interword space; \newline = linebreak
% {}% Manually specify head

% \theoremstyle{ilm-exercise}
% \newtheorem{exercise}{Exerc\'{i}cio}[chapter]

% Estilo de problemas
\declaretheoremstyle[%
headfont=\bfseries,
notebraces={(}{)},
bodyfont=\normalfont,
headpunct={},
postheadspace={ },
%postheadhook={\textcolor{Red}{\rule[.6ex]{\linewidth}{0.4pt}}\\},
spaceabove=1ex,
spacebelow=1ex
% qed=\qedsymbol
% mdframed={
%   backgroundcolor=Red!10,
%   linecolor=Red!35,
%   innertopmargin=6pt,
%   innerbottommargin=6pt,
%   roundcorner=5pt,
%   skipabove=\parsep,
%   skipbelow=\parsep
% }
]{ilm-exercise}
\declaretheorem[
style=ilm-exercise,
name={Exerc\'{i}cio},
numberwithin=chapter
]{exercise}


% \theoremstyle{plain}
% \newtheorem{conj}{Conjectura}[chapter]

% Estilo de problemas
\declaretheoremstyle[%
headfont=\bfseries,
notebraces={(}{)},
bodyfont=\normalfont,
headpunct={},
postheadspace=\newline,
postheadhook={\textcolor{Gray}{\rule[.6ex]{\linewidth}{0.4pt}}\\},
spaceabove=1ex,
spacebelow=1ex,
% qed=\qedsymbol
mdframed={
  backgroundcolor=Gray!10,
  linecolor=Gray!35,
  innertopmargin=6pt,
  innerbottommargin=6pt,
  roundcorner=5pt,
  skipabove=\parsep,
  skipbelow=\parsep
}
]{ilm-conjecture}
\declaretheorem[
style=ilm-conjecture,
name={Conjectura},
numberwithin=chapter
]{conjecture}

% %%% --------------------------------------------------------------------
% %%% Definição de cabelçalhos e rodapés usando os comandos do pacote
% %%% scrlayer-scrpage
% %%% --------------------------------------------------------------------
% \refoot[\footnotesize\theversion]{\footnotesize \theversion}
% \lofoot[\footnotesize\theauthor\ -- \ccbyncsa]{\footnotesize\theauthor\ -- \ccbyncsa}

% \makeevenhead{⟨style⟩}{⟨left⟩}{⟨center⟩}{⟨right⟩}
% \makeoddhead{⟨style⟩}{⟨left⟩}{⟨center⟩}{⟨right⟩}
\makeevenfoot{plain}{}{}{\footnotesize \theversion}
\makeoddfoot{plain}{\footnotesize\theauthor\ -- \ccbyncsa}{}{}

\endinput
% Local Variables:
% ispell-local-dictionary: "brasileiro"
% End:
