\section{Relações entre Conjuntos}
\label{sec:set-relations}

Assim como temos operações de comparação entre números, tais como
igualdade, maior, menor, etc., também temos operações que nos permitem
fazer comparações entre conjuntos.

A mais elementar comparação que podemos fazer entre dois conjuntos é
perguntar se esses conjuntos são iguais ou não. Dizemos que dois
conjuntos são iguais se eles contém exatamente os mesmos elementos. A
Definição~\ref{def:set-eql} formaliza este conceito.

\vspace{0.5\baselineskip}
\begin{defn}[Igualdade de Conjuntos]
  \label{def:set-eql}
  Dados dois conjuntos $A$ e $B$ são ditos conjuntos iguais,
  escrevemos $A = B$, se e somente se, $A$ e $B$ contém exatamente os
  mesmos elementos. Ou seja, se $x\in A$, então $x\in B$ e vice-versa.
\end{defn}

Utilizando a definição de igualdade de conjuntos podemos ver que
$\{1,3,2\}$ e $\{2,1,3\}$ são iguais (lembre-se que a ordem dos
elementos é irrelevante nos conjuntos), mas que $\{1,2,4\}$ e
$\{1,2,5\}$ não são.

Uma outra comparação que podemos fazer entre conjuntos é a comparação
de \emph{subconjunto}, ou de \emph{estar contindo}. Um conjunto $A$ é
um subconjunto de outro conjunto $B$, denotada por $A \subseteq B$, se
$x \in B$, sempre que $x \in A$.  Alternativamente, $A \subseteq B$ se
e somente se todos os elementos de $A$ também forem elementos de
$B$. Também podemos ler $A \subseteq B$ como ``$A$ está contido em
$B$'' ou ``$B$ contém $A$''. A Definição~\ref{def:subset} formaliza
este conceito.

\vspace{0.5\baselineskip}
\begin{defn}[Subconjunto]
  \label{def:subset}
  Dizemos que um conjunto $A$ é \emph{subconjunto} de um conjunto $B$,
  denotado por $A \subseteq B$, se e somente se para todo elemento $x
  \in A$, temos que $x \in B$. Para denotar a noção inversa, ou seja,
  que $A$ não é subconjunto de $B$, escreveremos $A \not\subseteq B$.
\end{defn}

Por definição, o conjunto vazio $\emptyset$ é subconjunto de todos os
conjuntos.

Iremos assumir a existência de um \emph{universo de discurso}, o
conjunto $\mathbb{U}$.  Todos os conjuntos que iremos considerar são
subconjuntos de $\mathbb{U}$.  Assim, por definição, temos:

\begin{enumerate}

\item $\emptyset \subseteq A$, para qualquer conjunto $A$.

\item $A \subseteq \mathbb{U}$, para qualquer conjunto $A$.

\end{enumerate}

Além da noção de subconjunto, existe uma outra noção mais restrita, a de
\emph{subconjunto próprio}. Se $A \subseteq B$, mas existe algum
elemento $x \in B$ tal que $x \notin A$, então dizemos que $A$ é um
subconjunto próprio de $B$, escrito $A \subset B$. Outra forma de
definir subconjunto próprio é dizer que $A \subset B$ sse $A \subseteq
B$ e $A \neq B$.

\vspace{0.5\baselineskip}
\begin{defn}[Subconjunto Próprio]
  \label{def:proper-subset}
  Dizemos que um conjunto $A$ é \emph{subconjunto próprio} de um conjunto
  $B$, denotado por $A \subset B$, se para todo elemento $x \in A$, temos
  que $x \in B$, mas existe ao menos um elemento $x \in B$ tal que
  $x \notin A$.
  Para denotar a noção inversa, ou seja, que $A$ não é subconjunto próprio
  de $B$, escreveremos $A \not\subset B$.
\end{defn}

A noção de \emph{continência} (relação subconjunto) nos permite dar uma outra
definição para a igualdade de conjuntos.

\vspace{0.5\baselineskip}
\begin{defn}[Igualdade de Conjuntos]
  \label{def:set-eql-alt}
  Dois conjuntos $A$ e $B$ são ditos \emph{conjuntos iguais}, denotado por
  $A = B$, se e somente se possuem exatamente os mesmos elementos. De modo
  mais formal, dizemos que $A = B$ se e somente se $A \subseteq B$ e
  $B \subseteq A$.
\end{defn}

\vspace{0.5\baselineskip}
\begin{exmp}
  As seguintes proposições são verdadeiras para os conjuntos
  indicados:\footnote{Os símbolos $\mathbb{N}$ e $\mathbb{Z}$ denotam os
  conjuntos dos naturais e inteiros, respectivamente.}

  \begin{multicols}{2}
    \begin{enumerate}

    \item $\{a, b\} \subseteq \{b, a\}$

    \item $\{a, b\} \not\subset \{b, a\}$

    \item $\{a, b\} \subseteq \{a, b, c\}$

    \item $\{a, b\} \subset \{a, b, c\}$

    \item $\{1, 2, 3\} \subseteq \mathbb{N}$

    \item $\{1, 2, 3\} \subset \mathbb{N}$

    \item $\mathbb{N} \subseteq \mathbb{Z}$

    \item $\mathbb{N} \subset \mathbb{Z}$

    \item $\mathbb{Z} \subseteq \mathbb{Z}$

    \item $\mathbb{Z} \not\subset \mathbb{Z}$

    \item $\emptyset \subseteq \{a, b, c\}$

    \item $\emptyset \subset \{a, b, c\}$

    \item $\{1, 2, 3\} = \{x \mid x \in \mathbb{N} \text{~e~} 0 < x <
      4\}$

    \item $\mathbb{N} = \{x \mid x \in \mathbb{Z} \text{~e~} x \geq 0\}$

    \item $\emptyset = \{\}$

    \item $\emptyset \neq \{\emptyset\}$

    \end{enumerate}
  \end{multicols}
\end{exmp}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../ilm-book"
%%% End:
