\section{Relações}
\label{sec:relations}


\begin{quote}
  ``Quando dois objetos, qualidades, classes ou atributos, vistos juntos pela
  mente, são vistos sob alguma conexão, essa conexão é chamada de relação.''
  
  \hfill -- Augustus De Morgan
\end{quote}


Intuitivamente, uma relação expressa a ideia de que elementos de dois ou mais
conjuntos estão associados entre si. Como exemplos de relações podemos citar,
entre outros:
\begin{enumerate*}[label=(\alph*)]
\item cidadãos brasileiros e seus CPF\footnote{Cadastro de Pessoa Física.};
\item pessoas de uma mesma família;
\item números de chapas de automóveis e de seus números de chassi;
\item ordenação entre números inteiros;
\item contratante, contratado, e objeto do contrato.
\end{enumerate*}


\subsection{Relações Binárias}
\label{sec:relacoes-binarias}

No caso mais simples, uma relação envolve elementos de dois conjuntos apenas.
Podemos dizer, então, que uma relação associa elementos de um conjunto de origem
com elementos de um conjunto de destino. Se uma relação $\rho$ associa elementos
do conjunto $A$ como elementos conjunto $B$, escrevemos $\rho: A \to B$ e
chamamos o conjunto $A$ de conjunto de origem (ou partida) e o conjunto $B$ de
conjunto de destino (ou chegada).

Numa abordagem um pouco mais formal, dizemos que uma relação $\rho$ de
$A$ em $B$ é um subconjunto de $A \times B$. Ou seja, escrever
$\rho: A \to B$ é o mesmo que escrever $\rho \subseteq A \times B$.
Se, na relação $\rho$, um elemento $a$ do conjunto de origem está
associado a um elemento $b$ do conjunto de destino, então escrevemos
que $(a,b) \in \rho$.

\begin{defn}[Relação Binária]
  \label{def:relacao-binara}

  Dizemos que $\rho$ é uma \emph{relação binária}, ou simplesmente
  \emph{relação}, do conjunto $A$, chamado conjunto de origem, para o
  conjunto $B$, chamado conjunto de destino, se e somente se $\rho$ é
  um subconjunto do produto cartesiano $A \times B$ e escrevemos
  \[
  \rho: A \to B
  \]

\end{defn}

É importante notar que, para o nosso estudo, \textbf{relações são
  conjuntos}. Isso significa que tudo o que aprendemos sobre conjuntos
também se aplica às relações. É possível, portanto, comparar se duas
relações são iguais, se uma relação é um subconjunto de outra, fazer a
união ou a interseção de duas relações, e assim por diante. Tudo o que
se aplica a conjuntos também se aplica a relações.

\begin{defn}[Domínio e Contradomínio]
  \label{def:dominio-contradominio}

  Seja $R: A \to B$ uma relação binária. O \emph{domínio de
    definição}, ou simplemente \emph{domínio}, da relação $R$,
  denotado por $\mathit{dom}(R)$, é definido por:
  \[
  \mathit{dom}(R) = \{a \mid a \in A\ \text{e}\ (a,b) \in R,\
  \text{para algum $b \in B$}\}
  \]
  O \emph{contradomínio}, ou \emph{imagem}, da relação $R$, denotado
  por $\mathit{im}(R)$, é definido por:
  \[
  \mathit{im}(R) = \{b \mid b \in B \text{~e~} (a,b) \in R,\
  \text{para algum $a \in A$}\}
  \]

\end{defn}


Também é comum usar-se a notação $a \rho b$ para representar
$(a,b) \in R$.  Neste texto, por questões de legibilidade, buscaremos
usar a notação \emph{infixa} apenas quando a relação for identificada
por um símbolo, por exemplo:
$a = b, a \leq b, a \odot b, a \rhd b, a \rho b$, etc. E usaremos a
notação de conjuntos para os demais casos, por exemplo:
$(a,b) \in R, (a,b) \in \mathsf{Ord}, (a,b) \in \Xi,$ etc.


\begin{exmp}[Relação Binária]

  Sejam $A = \{1, 2, 3, 4, 5, 6\}$ e $B = \{2, 4, 6, 8, 10\}$, e seja
  a relação binária $R: A \to B$ definida como:
  \[
  R = \{(1,2), (1,4), (1,8), (2,4), (2,8), (3,8), (5,10)\}
  \]
  Então temos:
  \begin{enumerate}

    \item O domínio de $R$ é $\mathit{dom}(R) = \{1, 2, 3, 5\}$.

    \item A imagem de $R$ é $\mathit{im}(R) = \{2, 4, 8, 10\}$.

  \end{enumerate}

\end{exmp}


Algumas vezes, para relações binárias simples, é conveniente
representar a relação graficamente. A forma mais comum de
representação gráfica de relações binárias consistem em usar círculos
ou elipses para representar os conjuntos de origem e de destino, com
os elementos dos conjuntos representados como pontos com rótulos. As
associações entre os elementos do conjunto de origem com o conjunto de
destino são representadas por setas indo do elemento de origem para o
elemento de destino. Um exemplo dessa forma de representação gráfica é
mostrada na Figura~\ref{fig:bin-rel-graph-ex}

\begin{figure}[h]
  \centering
  \includegraphics[width=0.66\textwidth]{pics/bin-rel-graph-ex-crop.pdf}
  \caption[Representação gráfica de uma relação
  binária.]{Representação gráfica de uma relação binária. O conjunto
    $A$ é o conjunto de origem e o conjunto $B$ é o conjunto de
    destino; as setas representam os pares ordenados que compõem a
    relação.}
  \label{fig:bin-rel-graph-ex}
\end{figure}


\subsection{Propriedades de Relações Binárias}
\label{sec:propr-relac-binarias}

No estudo de relações binárias, muitas vezes é útil classificar
as relações de acordo com algumas propriedades. Para relações
binárias em geral são classificadas com relação quatro propriedades:
\begin{enumerate}
\item Total
\item Funcional
\item Injetora
\item Sobrejetora
\end{enumerate}

Abaixo definimos as principais propriedades de relações binárias.

\begin{defn}[Relação Total]
  \label{def:relacao-total}

  Seja a relação binária $R: A \to B$. Dizemos que $R$ é uma relação
  \emph{total} em $A$ se e somente se $R$ relaciona todos os elementos
  de $A$ com \emph{ao menos um} elemento de $B$, ou seja, se
  $\mathit{dom}(R) = A$.

\end{defn}


\begin{figure}[h]
  \centering
  \includegraphics[width=0.6\textwidth]{pics/bin-rel-total-crop.pdf}
  \caption{Relação total do conjunto $A$ para o conjunto $B$.}
  \label{fig:bin-rel-total}
\end{figure}


\begin{defn}[Relação Funcional]
  \label{def:relacao-funcional}

  Seja a relação binária $R: A \to B$. Dizemos que $R$ é uma relação
  \emph{funcional} em $A$ se e somente se $R$ relaciona cada elemento
  de $A$ com \emph{no máximo um} elemento de $B$.

\end{defn}


\begin{figure}[H]
  \centering
  \includegraphics[width=0.6\textwidth]{pics/bin-rel-funcional-crop.pdf}
  \caption{Relação funcional do conjunto $A$ para o conjunto $B$.}
  \label{fig:bin-rel-funcional}
\end{figure}


\begin{defn}[Relação Injetora]
  \label{def:relacao-injetora}

  Seja a relação binária $R: A \to B$. Dizemos que $R$ é uma relação
  \emph{injetora} em $B$ se e somente se $R$ relaciona cada elemento
  de $B$ com \emph{no máximo um} elemento de $A$.

\end{defn}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.6\textwidth]{pics/bin-rel-injetora-crop.pdf}
  \caption{Relação injetora do conjunto $A$ para o conjunto $B$.}
  \label{fig:bin-rel-injetora}
\end{figure}


\begin{defn}[Relação Sobrejetora]
  \label{def:relação-sobrejetora}

  Seja a relação binária $R: A \to B$. Dizemos que $R$ é uma relação
  \emph{sobrejetora} em $B$ se e somente se $R$ relaciona todos os
  elementos de $B$ com \emph{ao menos um} elemento de $A$, ou seja, se
  $\mathit{im}(R) = B$.

\end{defn}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.6\textwidth]{pics/bin-rel-sobrejetora-crop.pdf}
  \caption{Relação sobrejetora do conjunto $A$ para o conjunto $B$.}
  \label{fig:bin-rel-sobrejetora}
\end{figure}


Quando uma relação é simultaneamente injetora e sobrejetora, dizemos
que esta função é \emph{bijetora}.

\begin{defn}[Relação Bijetora]
  \label{def:relacao-bijetora}

  Seja a relação binária $R: A \to B$. Dizemos que $R$ é uma relação
  \emph{bijetora} se e somente se $R$ é tanto injetora quanto
  sobrejetora.  Ou seja, se $R$ relaciona cada um e todos os elementos
  de $B$ com \emph{exatamente um} elemento de $A$.

\end{defn}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.6\textwidth]{pics/bin-rel-bijetora-crop.pdf}
  \caption{Relação bijetora do conjunto $A$ para o conjunto $B$.}
  \label{fig:bin-rel-bijetora}
\end{figure}


\begin{exmp}

  A seguir estão alguns exemplos de relações binárias familiares
  juntamente com os suas propriedades.

  \begin{enumerate}

  \item A relação $\leq$ é uma relação de $\mathbb{N}$ para
    $\mathbb{N}$, que é ao mesmo tempo total e sobrejetora em
    $\mathbb{N}$. Ou seja, tanto a imagem quanto o domínio da relação
    $\leq$ em $\mathbb{N}$ são o próprio $\mathbb{N}$.

  \item A relação binária que associa seqüências de teclas de um
    teclado de computador com os respectivos códigos ASCII de 8-bit é
    um exemplo de uma relação total.

  \item A relação binária que associa um código de caracter ISO-8859-1
    com o código correspondente de caracter Unicode é um exemplo de
    uma relação funcional e injetora.

  \end{enumerate}

\end{exmp}


\subsection{Endorrelações}
\label{sec:endorrelacoes}

Em uma relação binária $R$, quando o conjunto de origem e o conjunto de destino
são o mesmo conjunto $A$, dizemos que $R$ é uma \emph{endorrelação} em $A$, ou
ainda, que $R$ é uma \emph{auto-relação} em $A$, e escrevemos $R \subseteq A^2$.
Várias relações comumente usadas no dia-a-dia são endorrelações. Algumas
endorrelações comuns em~$\mathbb{N}$, por exemplo, são $=, \neq, <, \leq, >,
\geq$. Assim, os elementos do conjunto $\{(0, 0)$, $(0, 1)$, $(0, 2)$, $\ldots$
$(1, 1)$, $(1, 2)$, $\ldots \}$ são todos membros da relação $\leq$ que é um
subconjunto de $\mathbb{N} \times \mathbb{N}$.

De modo geral, uma relação n-ária entre os conjuntos $A_1, A_2, \ldots, A_n$ é
um subconjunto do conjunto $A_1 \times A_2 \times \ldots \times A_n$.

Como vimos na \autoref{sec:relacoes-binarias}, relações binárias
entre dois conjuntos podem ser representadas mostrando-se os dois
conjuntos como círculos ou elipses, indicando-se os elementos dentro
dos conjuntos e representando os pares ordenados da relação por setas
com origem no primeiro elemento do par ordenado e destino no segundo
elemento do par. Como mostrado na Figura~\ref{fig:bin-rel-graph-ex}.

Essa técnica pode ser utilizada também para endorrelações. Entretanto,
para endorrelações, o conjunto de origem e de destino são o mesmo, o
que torna a representação de dois conjuntos redundante. Por esta
razão, frequentemente as endorrelações são representadas por uma
técnica diferente. Desenham-se apenas os elementos do conjunto sobre o
qual a endorrelação está definida, sem representar as fronteiras do
conjunto. Os pares ordenados que compõem a relação continuam sendo
representados por setas com origem no primeiro elemento do par e
destino no segundo elemento do par. Para se conseguir uma apresentação
gráfica mais clara, costuma-se organizar os elementos em círculo, mas
isso não é obrigatório.

\vspace{\baselineskip}
\begin{exmp}
  Considere a relação $R \subseteq A$, onde $A = \{0, 1, 2, 3, 4, 5,
  6, 7, 8, 9\}$ e $R = \{(x,y) \mid y = (x+3)\mod 10\}$. Com estas
  definições temos que a representação gráfica de $R$ é a seguinte:

  \begin{center}
    \input{pics/endorrel-graph-ex.tikz}
  \end{center}
\end{exmp}

No exemplo acima, a operação $a\mod b$ representa o resto da divisão
inteira de $a$ por $b$. Por exemplo, $13\mod 10 = 3$.


\subsection{Propriedades de Endorrelações}

Uma endorelação em um conjunto $A$ pode apresentar algumas
propriedades de interesse para o nosso estudo. Considere por exemplo a
relação de \emph{igualdade} no conjunto $\mathbb{N}$. Esta relação
apresenta três propriedades importantes:

\begin{enumerate}

\item Para qualquer número $x \in \mathbb{N}$, $x = x$. Chamamos esta
  proprieade de \emph{reflexividade}.

\item Para quaisquer números $x,y \in \mathbb{N}$, $x = y$ implica que
  $y = x$. Chamamos esta propriedade de \emph{simetria}.

\item Para quaisquer números $x,y,z \in \mathbb{N}$, $x = y$ e $y = z$
  implica que $x = z$. Chamamos esta propriedade de
  \emph{transitividade}.

\end{enumerate}

Considere agora a relação \emph{menor-ou-igual} no conjunto
$\mathbb{N}$. Essa relação é reflexiva, pois para qualquer $x \in
\mathbb{N}$, $x \leq x$. Ela também é transitiva, pois para quaisquer
$x,y,z \in \mathbb{N}$, se $x \leq y$ e $y \leq z$, então $x \leq
z$. Entretanto, não podemos dizer que essa relação é simétrica, pois
se tomarmos dois número quaisquer $x,y \in \mathbb{N}$, \textbf{não}
podemos afirmar que $x \leq y$ implica em $y \leq x$. De fato, só
teremos $x \leq y$ e $y \leq x$ se $x = y$. Esta característica
descreve a propriedade que chamamos de \emph{antissimetria}.

Assim, de modo geral, endorrelações podem ser classificadas como
\emph{reflexivas}, \emph{simétricas}, \emph{antissimétricas} ou
\emph{transitivas}.


\begin{defn}[Relação Reflexiva]

  Uma endorrelação $R \subseteq A^2$ é \emph{reflexiva} se, e somente
  se, todo elemento de $A$ está relacionado a si mesmo. Ou seja,
  quando $(a,a) \in R$ para todo elemento $a \in A$.
  
\end{defn}

\begin{figure}[h]
  \centering
  \input{pics/rel-reflex.tikz}
  \caption{Diagrama da endorrelação reflexiva ``$x$ divide $y$'' sobre
    o conjunto $A=\{2, 3, 4, 5, 6, 7\}$.}
  \label{fig:rel-reflex}
\end{figure}

A Figura~\ref{fig:rel-reflex} mostra a representação gráfica da
endorrelação $R_1 \subseteq A_1$, onde $A_1=\{2,3,4,5,6,7\}$ e
$R_1 = \{(x,y) \mid x~\mbox{divide}~y\}$. Como todo número divide a si
mesmo, todos os elementos de $A_1$ aparecem com setas em laço, voltando
para si mesmos. Além disso, 2 aparece com setas para 4 e 6,
visto que 2 divide 4 e 2 divide 6; e 3 aparece com uma seta para 6,
visto que 3 divide 6. Uma vez que todo elemento do conjunto de origem,
$A$, está relacionado a si mesmo, podemos dizer que a endorrelação
$R_1$ é reflexiva.


\begin{defn}[Relação Simétrica]

  Uma endorrelação $R \subseteq A^2$ é \emph{simétrica} se, e somente
  se, sempre que $(a,b) \in R$, então $(b,a) \in R$, para quaisquer
  elementos $a,b \in A$.

\end{defn}

\begin{figure}[h]
  \centering
  \input{pics/rel-simetric.tikz}
  \caption{Diagrama da endorrelação simétrica ``$x+y=6$'' sobre
    o conjunto $A=\{1,2,3,4,5,6\}$.}
  \label{fig:rel-simetric}
\end{figure}

A Figura~\ref{fig:rel-simetric} mostra a representação gráfica da
endorrelação $R_2\subseteq A_2$, onde $A_2=\{1,2,3,4,5,6\}$ e
$R_2 = \{(x,y) \mid x+y=6\}$. Podemos ver que estão representados os
pares ordenados $(1,5)$, $(2,4)$, $(3,3)$, $(4,2)$ e $(5,1)$. Também
podemos notar que para todo par ordenado do tipo $(a,b)$ pertencente à
relação, existe o par simétrico, i.e., o par $(b,a)$. Por exemplo,
temos o par $(2,4)$ e temos o simétrico $(4,2)$. Sendo assim, podemos
dizer que a endorelação $R_2$ é simétrica.

Note também, que apesar da relação $R_2$ possuir um par ordenado onde
um elemento se relaciona a ele mesmo, \textbf{não} podemos dizer que
$R_2$ é reflexiva, pois para ser reflexiva, todos os elementos
deveriam estar relacionados a si mesmos, e não é o caso.


\begin{defn}[Relação Antissimétrica]

  Uma endorelação $R \subset A^2$ é \emph{antissimétrica} se, e somente
  se, nunca existirem ambos $(a,b) \in R$ e $(b,a) \in R$, para
  quaisquer elementos $a,b \in A$. Exceto se $a = b$.

\end{defn}

\begin{figure}[h]
  \centering
  \input{pics/rel-antissim.tikz}
  \caption{Diagrama da endorrelação antissimétrica ``$x-y=3$'' sobre
    o conjunto $A=\{1,2,3,4,5,6\}$.}
  \label{fig:rel-antissim}
\end{figure}

A Figura~\ref{fig:rel-antissim} mostra a representação gráfica da
endorrelação $R_3 \subseteq A_3$, onde $A_3 = \{1,2,3,4,5,6,7\}$, e
$R_3 = \{(x,y) \mid x-y=3\}$. Podemos observar nesse gráfico que nunca
há setas ``voltando'', ou seja, nunca vemos um par ordenado $(a,b)$ e
seu simétrico $(b,a)$. Nessa situação, podemos dizer que a
endorrelação $R_3$ é antissimétrica.


\begin{defn}[Relação Transitiva]

  Uma endorrelação $R \subseteq A^2$ é \emph{transitiva} se, e somente
  se, sempre que $(a,b) \in R$ e $(b,c) \in R$ então $(a,c) \in R$,
  para quaisquer elementos $a,b,c \in A$.

\end{defn}

\begin{figure}[h]
  \centering
  \input{pics/rel-trans.tikz}
  \caption{Diagrama da endorrelação transitiva ``$y$ é potência de $x$'' sobre
    o conjunto $A_4 = \{1,2,3,4,9,16,27,64,81\}$.}
  \label{fig:rel-trans}
\end{figure}

A Figura~\ref{fig:rel-trans} mostra a representação gráfica da
endorrelação $R_4 \subseteq A_4$, onde
$A_4 = \{1,2,3,4,8,9,16,27,64,81\}$ e
$R_4 = \{(x,y) \mid y = x^n, n \in \mathbb{N}, n > 1\}$. Podemos
observar que sempre que temos dois pares ordenados do tipo $(a,b)$ e
$(b,c)$ em $R_4$, também ocorre o par ordenado $(a,c)$. Por exemplo, tempo
$(2,4)$ e $(4,16)$ e também temos $(2,16)$; temos $(2,8)$ e $(8,64)$ e
também temos $(2,64)$; temos $(3,9)$ e $(9,81)$ e também temos
$(3,81)$; e assim por diante. Nesta situação, podemos dizer que $R_4$
é uma endorrelação transitiva.


\subsection{Relações $n$-árias}
\label{sec:relacoes-n-arias}

Há situações em que as relações envolvem mais de dois conjuntos. Considere, por
exemplo, a relação envolvendo três papéis que as pessoas podem desempenhar,
expressa em uma declaração da forma ``X acha que Y gosta de Z''. Os fatos de
uma situação concreta poderiam ser organizados em no conjunto de trios ordenado
$S$: 
\[
  \begin{split}
    S = & \{
    (\text{Alice}, \text{Beto}, \text{Denise}),
    (\text{Carlos}, \text{Alice}, \text{Beto}),
    (\text{Carlos}, \text{Carlos}, \text{Alice}), \\
    & \quad (\text{Denise}, \text{Denise}, \text{Denise})\}
  \end{split}
\]
O conjunto $S$ define uma relação ternária sobre o conjunto $P$ de pessoas em
discussão:
\[
  P = \{\text{Alice}, \text{Beto}, \text{Carlos}, \text{Denise}\}
\]

Cada $n$upla de $S$ registra um fato, i.e., faz uma afirmação do formulário ``X
acha que Y gosta de Z''. Por exemplo, o trio $(\text{Alice}, \text{Beto},
\text{Denise})$ diz, na verdade, ``Alice acha que Beto gosta de Denise''.

A relação $S$ pode ser vista como um exemplo extremamente simples de um banco de
dados relacional.


\begin{defn}[Relações $n$-árias]
  \label{def:relacoes-n-arias}

  Uma relação $L$ sobre os conjuntos $X_1,\ldots , X_n$ é um subconjunto do seu
  produto cartesiano, escrito $L \subseteq X_1 \times \cdots \times X_n$.

  As relações são classificadas de acordo com o número de conjuntos no produto
  cartesiano que as define. Por isso:

  \begin{itemize}
  \item $u \in L$ ou $L(u)$ denota uma relação ou propriedade \textbf{unária};
  \item $(u,v) \in L$, ou $L(u,v)$ ou $u \mathop{L} v$ denotam uma relação
    binária; $L$ é uma \emph{relação homogênea} quando $X_1 = X_2$ e uma
    \emph{relação heterogênea} quando $X_1 \neq X_2$.
  \item $(u,v,w) \in L$ ou $L(u,v,w)$ denota uma relação \textbf{ternária};
  \item $(u,v,w,x) \in L$ ou $L(u,v,w,x)$ denota uma relação \textbf{quaternária}.
  \end{itemize}
  
  Relações com mais de quatro termos são geralmente referidas como $n$-árias.
  
\end{defn}

As seguintes considerações se aplicam:

\begin{itemize}

\item Os conjuntos $X_j$ para $j$ de 1 a $n$ são chamados de domínios da
  relação.

\item Se todos os domínios $X_j$ são o mesmo conjunto $X$, então é mais simples
  referir-se a $L$ como uma relação $n$-ária sobre $X$.

\item Se algum dos domínios $X_j$ estiver vazio, então o produto cartesiano de
  definição é vazio, e a única relação sobre essa sequência de domínios é a
  relação vazia $L = \varnothing$. Por isso é comummente estipulado que todos os
  domínios não são vazios.

\item Se $P(x_1, x_2, \ldots, x_n)$ é um predicado, então
  \[
    \{(a1, a2, \ldots, a_n) \mid P(a_1, a_2, \ldots, a_n)~\text{é verdadeiro}\}
  \]
  é uma relação $n$-ária.

\item Se $f(x_1, x_2, \ldots, x_{n-1}) = x_n$ é uma função, então
  \[
    \{(a_1, a_2, \ldots, a_{n-1}, a_n) \mid f(a_1, a_2, \ldots, a_{n-1}) = a_n\}
  \]
  é uma relação $n$-ária.

\end{itemize}

Veremos mais sobre relações $n$-árias e bancos de dados relacionais
na~\autoref{sec:relational-model}.

% Para exemplos vide
% http://www.cs.utsa.edu/~bylander/cs3233/nary.pdf

%%% Local Variables:
%%% mode: latex
%%% ispell-local-dictionary: "brasileiro"
%%% TeX-master: "../ilm-book"
%%% End:
