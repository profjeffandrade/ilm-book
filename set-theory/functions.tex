\section{Funções}
\label{sec:functions}

Uma relação funcional também pode ser chamada de função parcial. Se
além de funcional, uma relação $f$ for também uma relação total,
dizemos que $f$ é uma \emph{função total} de A para B, usualmente
representada por $f: A \to B$.


\begin{defn}[Função Total]
  Uma \emph{função total}, ou simplesmente uma \emph{função}, $f$ de
  $A$ para $B$ é uma relação binária $f \subseteq A \times B$ tal que
  para cada elemento $a \in A$ existe um único elemento $b \in B$ de
  modo que $(a,b) \in f$ (geralmente denotada $f(a) = b$, e por vezes
  $f: a \mapsto b$).

  \begin{enumerate}

  \item Usaremos a notação $f: A \to B$ para denotar uma função $f$,
    de $A$ em $B$.

    \item O conjunto $A$ é chamado de \emph{domínio} da função $f$.

    \item O conjunto $B$ é chamado de \emph{co-domínio} (ou
    \emph{contra-domínio}) da função $f$.

  \item O \emph{intervalo} de uma função $f: A \to B$ é o conjunto
    $\{b \mid f(a) = b\}$ para algum $a \in A$.

  \end{enumerate}

  Alternativamente, podemos definir uma \emph{função total} $f$ de $A$
  em $B$, como uma relação binária funcional total de $A$ em $B$.

  Uma \emph{função parcial} $g$ de $A$ para $B$, denotada $g: A \nto
  B$ é uma função total de algum subconjunto de $A$ para o conjunto
  $B$. Evidentemente, toda função total é também uma função parcial.
\end{defn}

A Figura~\ref{fig:bin-rel-func-total} ilustra o diagrama de uma
relação funcional total, i.e., de uma função.

\begin{figure}[h]
  \centering
  \includegraphics[width=0.6\textwidth]{pics/bin-rel-func-total-crop.pdf}
  \caption[Relação funcional e total do conjunto $A$ para o conjunto
  $B$.]{Relação funcional e total do conjunto $A$ para o conjunto
    $B$. Este tipo de relação é chamado de função.}
  \label{fig:bin-rel-func-total}
\end{figure}


A palavra ``função'', salvo disposição em contrário, será usada com o sentido de
``função total'' -- quando quisermos nos referir a ``função parcial'' usaremos
explicitamente este termo.

\begin{exmp}
  Alguns exemplos familiares de funções parciais e totais são:

  \begin{enumerate}
  \item $+$ e $\cdot$ (adição e multiplicação) são funções totais do
    tipo $f: \mathbb{N} \times \mathbb{N} \to \mathbb{N}$.

  \item $-$ (subtração) é uma função parcial do tipo $f: \mathbb{N}
    \times \mathbb{N} \nto \mathbb{N}$.

  \item $\mathit{div}$ e $\mathit{mod}$ são funções totais do tipo $f:
    \mathbb{N} \times \mathbb{P} \to \mathbb{N}$. Sejam, $a,b,q,r \in
    \mathbb{N}$, se $a = q \cdot b + r$ tal que $0 \leq r < b$, então
    as funções $\mathit{div}$ e $\mathit{mod}$ são definidas como
    $\mathit{div}(a,b) = q$ e $\mathit{mod}(a,b) = r$. Nós poderemos
    escrever estas funções como $a\ \mathit{div}\ b$ e $a\
    \mathit{mod}\ b$.

    Note que $\mathrm{div}$ e $\mathrm{mod}$ também são funções
    parciais do tipo $f: \mathbb{N} \times \mathbb{N} \nto
    \mathbb{N}$.

  \item As relações binárias $=, \neq, <, \leq, >, \geq$ também pode
    ser imaginadas como funções totais do tipo $f: \mathbb{N} \times
    \mathbb{N} \to \mathbb{B}$.
  \end{enumerate}
\end{exmp}


\subsection{Operações em Relações e Funções}
\label{sec:oper-rel-fn}

Nesta seção vamos considerar algumas operações sobre relações binárias
em geral, incluindo funções.

Em algumas situação é útil ter uma relação que relaciona cada elemento
de um conjunto exclusivamente a si mesmo. Essas relações são chamadas
de \emph{função identidade}, e sua formalização é dada na
Definição~\ref{def:fn-id}.

\begin{defn}[Função Identidade]
  \label{def:fn-id}
  Dado um conjunto $A$, a função identidade sobre A, denotada por
  $id_A: A \to A$, é o conjunto $\{(a,a) \mid a \in A\}$.
\end{defn}

O exemplo mais comum de função identidade é a igualdade.

\begin{exmp}
  Seja o conjunto $A = \{2,3,5,7,11\}$. A função identidade do
  conjunto $A$ é dada por
  $id_A = \{(2, 2), (3, 3), (5, 5), (7, 7), (11,11)\}$.
\end{exmp}

Outra situação bastante comum, é quando temos uma relação de um
conjunto $A$ para um conjunto $B$, por exemplo, do conjunto de pessoas
para o conjunto de números de CPF, e queremos a relação que faz o
inverso, que relacione os elementos de $B$ com seus respectivos pares
em $A$. Continuando nosso exemplo, essa relação inversa levaria de
números de CPF para pessoas. O conceito de \emph{relação inversa} é
formalizado na Definição~\ref{def:rel-inv}.

\begin{defn}[Relação/Função Inversa]
  \label{def:rel-inv}
  Dada uma relação binária $R$ de $A$ em $B$, a relação inversa de R,
  denotada por $R^{-1}$ é a relação de $B$ em $A$ definido como
  $R^{-1} = \{(b,a) \mid (a,b) \in R\}$.
\end{defn}

Note que, dada uma relação $R$, sempre existirá a relação inversa,
$R^{-1}$. Entretanto, dada uma função $f$, nem sempre existirá a
\textbf{função} inversa, $f^{-1}$. Observe os exemplos abaixo.

\begin{exmp}
  \label{exm:rel-inv-1}
  Seja $A = \{2,3,5,7,11,13\}$, e seja a relação $R:A\to A$ definida como %
  $R = \{(x,y) \mid {x < y}\}$. Então temos que: $R = \{(2, 3)$, $(2, 5)$, $(2,
  7)$, $(2, 11)$, $(2, 13)$, $(3, 5)$, $(3,7)$, $(3, 11)$, $(3, 13)$, $(5, 7)$,
  $(5, 11)$, $(5, 13)$, $(7, 11)$, $(7, 13)$, $(11,13)\}$.

  Sendo assim, a inversa de $R$ é dada por: $R^{-1} = \{(3, 2)$, $(5, 2)$, $(7,
  2)$, $(11, 2)$, $(13, 2)$, $(5, 3)$, $(7, 3)$, $(11, 3)$, $(13, 3)$, $(7, 5)$,
  $(11, 5)$, $(13, 5)$, $(11, 7)$, $(13, 7)$, $(13,11)\}$.
\end{exmp}

\begin{exmp}
  \label{exm:rel-inv-2}
  Seja $B = \{0,1,2,3,4,5,6,7,8,9\}$, e seja $f:B\to B$ definida como $f(x) =
  (x+3)\mod 10$. Então temos que: $f = \{(0, 3)$, $(1, 4)$, $(2, 5)$, $(3, 6)$,
  $(4, 7)$, $(5, 8)$, $(6, 9)$, $(7, 0)$, $(8, 1)$, $(9, 2)\}$.

  Sendo assim, a inversa de $f$ é dada por: $f^{-1} = \{(3, 0)$, $(4, 1)$, $(5,
  2)$, $(6, 3)$, $(7, 4)$, $(8, 5)$, $(9, 6)$, $(0, 7)$, $(1, 8)$, $(2, 9)\}$.
\end{exmp}

\begin{exmp}
  \label{exm:rel-inv-3}
  Seja $C = \{-1,0,1\}$, e seja $g:C\to C$ definida como
  $g(x) = x^2$. então temos que $g = \{(-1, 1), (0, 0), (1, 1)\}$.

  Sendo assim, a inversa de $g$ é dada por:
  $g^{-1} = \{(1, {-1}), (0, 0), (1, 1)\}$.
\end{exmp}

Note que nos três exemplos acima, foi possível determinar a relação
inversa. Entretanto, devemos observar a diferença entre os dois
últimos exemplos. No caso do Exemplo~\ref{exm:rel-inv-2}, tanto a
relação original, $f$, quanto sua inversa, $f^{-1}$, são
funções. Entretanto, no caso do Exemplo~\ref{exm:rel-inv-3}, embora a
relação original, $g$, seja uma função, a sua inversa, $g^{-1}$, não é
função.


\begin{defn}[Composição de Relações]
  Dadas duas relações binárias $R$ de $A$ em $B$ e $S$ de $B$ em $C$,
  a composição de $R$ com $S$, denotada por $S \circ R$, é definida
  como:
  \[
    S \circ R = \{(a,c) \mid (a,b) \in R\ \text{e}\ (b,c) \in S,\
    \text{para algum}\ b \in B\}
  \]
\end{defn}


A notação $S \circ R$ deve ser lida como ``composta de $R$ com $S$''
ou ``$S$-composta-$R$''. Se $R$ é uma relação de $A$ em $B$, e $S$ é
uma relação de $B$ em $C$, então $S \circ R$ será uma relação de $A$
em $C$, ou seja, do conjunto de partida da primeira relação para o
conjunto de chegada da segunda relação. Para, para que seja possível
compor duas relações é necessário que o conjunto de chegada da
primeira relação seja o mesmo que o conjunto de partida da segunda
relação.


\begin{defn}[Composição de Funções]

  Dadas duas funções (parciais ou totais) $f: A \nto B$ e $g: B \nto
  C$, sua composição é a função $g \circ f: A \to C$ definida
  simplesmente como a composição relacional das duas funções.
  \[
  (g \circ f) = \{(a,c) \mid f(a) = b\ \text{e}\ g(b) = c,\
  \text{para algum}\ b \in B\}
  \]
  Assim, $(g \circ f)(a) = g(f(a))$.

\end{defn}


\vspace{3mm}\noindent\textbf{Importante}: Note que, embora digamos ``função
composta de $f$ e $g$'', a notação usada, $g \circ f$, coloca mais à esquerda a
função que é aplicada por último (ou mais externa), i.e., em $g(f(x))$ $g$ é
aplicada por último, ``sobre'' o resultado de $f$, logo, $g$ deve aparecer à
equerda.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../ilm-book"
%%% End:
