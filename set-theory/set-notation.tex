%%% Introdução à teoria dos conjuntos
\section{Introdução}
\label{sec:introducao-conjuntos}

Esta seção introduz os conceitos básicos relativos à \emph{Teoria do
  Conjuntos} bem como algumas notações que serão utilizadas no restante
do curso. O objetivo não é dar um tratamento formal completo à teoria
dos conjuntos, mas apenas definir os conceitos essenciais que serão
necessários para acompanhar os próximos capítulos.
%
A versão de teoria dos conjuntos que veremos neste capítulo é muitas
vezes chamada de \emph{Teoria Simples dos Conjuntos}\footnote{A tradução
  literal do termo em inglês \emph{``naïve set theory''} seria algo como
  ``teoria ingênua dos conjuntos''.}, em contraste com a versão mais
formal (e mais avançada) que é chamada de \emph{Teoria Axiomática dos
  Conjuntos}.
%
O conceito de conjunto é fundamental para o nosso estudo, pois
praticamente todos os conceitos que veremos mais adiante são baseados em
conjuntos e construções sobre conjuntos.

\vspace{5mm}

\begin{defn}[Conjunto]\label{def:set}
  Um \emph{conjunto} é uma coleção de zero ou mais \emph{elementos}
  distintos sem qualquer ordem específica associada e que não contém a
  si mesma.
\end{defn}

Usaremos a notação $\{a, b, c\}$ para indicar o conjunto cujos elementos
são $a$, $b$ e $c$.

Vamos analisar as partes da Definição~\ref{def:set}.
\begin{itemize}

\item Um conjunto é uma coleção de elementos distintos, ou seja, sem
  repetições. O termo ``elemento'' é usado aqui de modo bem amplo.
  Qualquer coisa pode ser um elemento de um conjunto, até mesmo outros
  conjuntos. Consideraremos ``elemento'' como um conceito fundamental,
  i.e., um conceito que não precisa (ou não pode) ser definido
  formalmente.

\item Um conjunto não possui qualquer ordem específica associada aos
  seus elementos. Isso significa, entre outras coisas, que não faz
  sentido falar em primeiro elemento do conjunto, ou segundo elemento,
  etc. É claro que, ao escrever o conjunto precisaremos escolher alguma
  ordem para a escrita, mas esta ordem é apenas um ``mal necessário''
  para que possamos escrever o conjunto e não se deve considerar que a
  ordem faça parte do conjunto. Por exemplo, se temos um conjunto com os
  elementos $a$, $b$ e $c$, podemos escrever este conjunto como $\{a, b,
  c\}$, $\{a, c, b\}$, $\{b, a, c\}$, $\{b, c, a\}$, $\{c, a, b\}$ ou
  $\{c, b, a\}$; todas estas formas de escrever representam o mesmo
  conjunto.

\item Um conjunto não contém a si mesmo. Conjuntos podem conter
  quaisquer tipos de elementos, inclusive outros conjuntos. Entretanto,
  se permitirmos que um conjunto contenha a si mesmo, corremos o risco
  de permitir \emph{paradoxos} em nosso sistema.\footnote{Um
    \emph{paradoxo} é uma sentença ou conjunto de sentenças que leva à
    uma contradição lógica ou a uma situação de desafia a lógica e a
    razão.}
  %
  Para evitar o surgimento de paradoxos, impomos a condição de
  que um conjunto não possa conter a si mesmo. Infelizmente, uma
  explicação completa de paradoxos e dos problemas relacionados à
  autorreferência está fora do escopo deste texto.
  % O tema de autorreferência será tratado (de forma simplificada) na
  % Seção~\ref{sec:selfref}.

\end{itemize}

\vspace{5mm}

\begin{exmp}
  São conjuntos:

  \begin{enumerate}

    \item As vogais: \textsf{a, e, i, o, u}.

    \item Os dígitos: \textsf{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}.

    \item Os números pares: \textsf{0, 2, 4, \ldots}

    \item Todos os brasileiros.

    \item Os jogadores da seleção brasileira de futebol.

    \item $A = \{\{a, b, c\}, d\}$. O conjunto $A$ contém dois
      elementos: $\{a, b, c\}$ e $d$.

  \end{enumerate}
\end{exmp}


Podemos descrever um conjunto qualquer enumerando todos os elementos do conjunto
ou especificando as propriedades que caracterizam exclusivamente os elementos do
conjunto. Assim, o conjunto de todos os inteiros pares não superiores a 10 pode
ser descrita como $S = \{2, 4, 6, 8, 10\}$ ou, equivalentemente, como $S = \{x
\mid x\ \text{é um inteiro positivo par}, x \leq 10\}$.

A definição de um conjunto listando todos os seus elementos é chamada de
\emph{definição por extensão}, por exemplo:
\[
\mathsf{Vogais} = \{a, e, i, o, u\}
\]
que deve ser lida como ``\textsf{Vogais} é o conjunto cujos elementos são
\textsf{a, e, i, o, u}''.

A definição de um conjunto especificando propriedades é chamada de
\emph{definição por intensão} ou \emph{definição por compreensão}, por
exemplo:
\[
\mathsf{Pares} = \{n \mid n\ \text{é inteiro par}\}
\]
que deve ser lida como ``\textsf{Pares} é o conjunto de todos os
elementos $n$ tais que $n$ é um inteiro par''.

Usaremos a notação $x \in S$ para indicar que $x$ é um elemento do (ou
pertence ao) conjunto $S$. Para indicar o inverso, ou seja, que $x$
\textbf{não} pertence ao conjunto $S$, usaremos a notação $x \notin S$.

Um \emph{conjunto vazio} é aquele que não contém elementos e vamos denotá-lo
com o símbolo $\emptyset$.
Por exemplo, seja $S$ o conjunto de todos os alunos que reprovarem neste curso.
$S$ pode vir a ser vazio (se todos estudarem muito).

\vspace{5mm}

\begin{exmp}
  As seguintes sentença sobre pertinência de elementos a conjunto são
  todas verdadeiras.

  \begin{multicols}{2}
    \begin{enumerate}

      \item $a \in \{a, b, c, d\}$

      \item $a \in \mathsf{Vogais}$

      \item $\{a\} \notin \mathsf{Vogais}$

      \item $0 \in \{0, 1, 2, 3, 4, 5, 6, 7, 8, 9\}$

      \item $\{0\} \notin \{0, 1, 2, 3, 4, 5, 6, 7, 8, 9\}$

      \item $\emptyset \notin \{0, 1, 2, 3, 4, 5, 6, 7, 8, 9\}$

      \item $f \in \{\{a, b, c\}, \{d, e\}, f\}$

      \item $a \notin \{\{a, b, c\}, \{d, e\}, f\}$

      \item $e \notin \{\{a, b, c\}, \{d, e\}, f\}$

      \item $\{d, e\} \in \{\{a, b, c\}, \{d, e\}, f\}$

      \item $\{e, d\} \in \{\{a, b, c\}, \{d, e\}, f\}$

      \item $\{a, b\} \notin \{\{a, b, c\}, \{d, e\}, f\}$

      \item $\{c, a, b\} \in \{\{a, b, c\}, \{d, e\}, f\}$

      \item $\{f\} \notin \{\{a, b, c\}, \{d, e\}, f\}$

      \item $\{\{e, d\}\} \notin \{\{a, b, c\}, \{d, e\}, f\}$

      \item $\emptyset \notin \emptyset$

    \end{enumerate}
  \end{multicols}
\end{exmp}


\subsection{Alguns Conjuntos Importantes}

Usaremos a seguinte notação para denotar alguns conjuntos de padrão:

\begin{itemize}

\item O conjunto vazio: $\emptyset$

\item O conjunto Universo: $\mathbb{U}$

\item O conjunto dos números naturais\footnote{Embora existam alguns
    autores que não considerem o zero como um número natural, nós iremos
    ficar do lado dos autores que consideram o zero um número natural,
    afinal é bastante natural tirar zero em uma prova, por exemplo.}:
  $\mathbb{N} = \{0, 1, 2, \ldots\}$

\item O conjunto dos números inteiros: $\mathbb{Z} = \{\ldots, -2, -1,
  0, 1, 2, \ldots\}$ --- também é comum vermos o uso de $\mathbb{Z}^*$
  para representar os inteiros não-negativos, i.e. $\mathbb{Z}^* =
  \mathbb{N}$ e de $\mathbb{Z}^+$ para representar os inteiros
  positivos, i.e., $\mathbb{Z}^+ = \{1, 2, 3, 4, \ldots\}$.

  \item O conjunto dos números reais: $\mathbb{R}$

  \item O conjunto dos números racionais: $\mathbb{Q}$

  \item O conjunto dos números irracionais: $\mathbb{I}$ ou
  $\mathbb{R} \setminus \mathbb{Q}$

  \item O conjunto dos valores lógicos\footnote{Também chamados de valores
  \emph{Booleanos}.}: $\mathbb{B} = \{\mathit{falso}, \mathit{verdadeiro}\}$

  \item O conjunto de todos os pontos do \emph{plano cartesiano}:
  $\mathbb{R}^2$

\end{itemize}


\subsection{Diagramas de Venn}
\label{ssec:venn}

Em muitos casos é util usar diagramas para representar relações e
operações sobre conjuntos. No final do século XIX, o matemático inglês
John Venn inventou uma forma de construir estes diagramas, que desde
então são chamados \emph{Diagramas de Venn} \cite{Venn1880}. Os
diagramas de Venn são usados no ensino da teoria básica de conjuntos,
bem como para ilustrar relações simples entre conjuntos no estudo de
probabilidades, lógica, estatística, linguistica e ciência da
computação.

Construir diagramas de Venn para dois ou três conjuntos é bastante
fácil, entretanto, construir diagramas de Venn para mais do que três
conjuntos passa a ser cada vez mais difícil à medida que o número de
conjuntos aumenta, e é ainda mais difícil se tentarmos usar apenas
formas geométricas ``bem comportadas'' como círculos ou elipses
\cite{Ruskey2006}. A Figura~\ref{fig:venn:2-3sets} mostra exemplos de
diagramas para 2 e 3 conjuntos.

\begin{figure}[h]
  \centering
  \begin{subfigure}{0.45\textwidth}
    \begin{tikzpicture}[scale=0.75, transform shape]
      \tikzset{venn circle/.style={draw,circle,minimum width=5cm,fill=#1,opacity=0.6,thick}}

      \node [venn circle = red] (A) at (0,0) {$A$};
      \node [venn circle = green] (B) at (0:3cm) {$B$};
      \node[below] at (barycentric cs:A=1/2,B=1/2 ) {$AB$};
      \vspace*{3mm}
    \end{tikzpicture}
    \caption{Diagrama para 2 conjuntos}
  \end{subfigure}
  \qquad
  \begin{subfigure}{0.45\textwidth}
    \begin{tikzpicture}[scale=0.6, transform shape]
      \tikzset{venn circle/.style={draw,circle,minimum width=5cm,fill=#1,opacity=0.6,thick}}
      \node [venn circle = red] (A) at (0,0) {$A$};
      \node [venn circle = blue] (B) at (60:3cm) {$B$};
      \node [venn circle = green] (C) at (0:3cm) {$C$};
      \node[left] at (barycentric cs:A=1/2,B=1/2 ) {$AB$};
      \node[below] at (barycentric cs:A=1/2,C=1/2 ) {$AC$};
      \node[right] at (barycentric cs:B=1/2,C=1/2 ) {$BC$};
      \node[below] at (barycentric cs:A=1/3,B=1/3,C=1/3 ){$ABC$};
    \end{tikzpicture}
    \caption{Diagrama para 3 conjuntos}
  \end{subfigure}
  \caption{Diagramas de Venn para 2 e 3 conjuntos.}
  \label{fig:venn:2-3sets}
\end{figure}

O que torna difícil a construção de diagramas de Venn para mais de três
conjuntos é o fato de que um diagrama de Venn deve necessariamente
mostrar \textbf{todas} as possíveis interações (interferências) entre os
conjuntos envolvidos.  Por exemplo, para dois conjuntos --- $A$ e $B$
--- devemos ter no diagrama uma parte que mostre os elementos que são
apenas de $A$, outra que mostre os elementos que são apenas de $B$ e
outra que mostre os elementos que pertencem ao mesmo tempo a $A$ e a
$B$. Assim temos que ter três regiões distintas. Para três conjuntos ---
$A$, $B$ e $C$ --- precisaremos ter sete regiões: $A$, $B$, $C$, $AB$,
$AC$, $BC$ e $ABC$. De modo geral, um diagrama de Venn para $n$
conjuntos precisará de $2^n - 1$ regiões distintas, o que significa que
para cada novo conjunto o número de regiões do diagrama de Venn
aproximadamente dobra. A Figura~\ref{fig:venn:nsets} mostra diagramas de
Venn para 4 e 5 conjuntos.

\begin{figure}[h]
  \begin{subfigure}{0.45\textwidth}
    \centering
    \includegraphics[width=0.95\textwidth]{pics/Venn_diagram_4-sets.pdf}
    \caption{Diagrama para 4 conjuntos}
  \end{subfigure}
  \qquad
  \begin{subfigure}{0.45\textwidth}
    \centering
    \includegraphics[width=0.95\textwidth]{pics/Venn_diagram_5-sets.pdf}
    \caption{Diagrama para 5 conjuntos}
  \end{subfigure}
  \caption[Diagramas de Venn para 4 e 5 conjuntos.]{Diagramas de Venn
    para 4 e 5 conjuntos. As regiões estão identificadas com os nomes
    dos conjuntos que estão interagindo na região.}
  \label{fig:venn:nsets}
\end{figure}


%%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
\subsection{Notação para Descrever e Definir Conjuntos}
\label{sec:notac-descr-conj}

Como vimos, é possível representar graficamente os conjuntos através dos
diagramas de Venn. Mas para poder trabalhar com eles, é necessário também
representá-los em linguagem matemática.

Usamos as chaves, $\{\}$, para representar e definir os conjuntos, e no seu
interior estão os elementos que o formam separados por vírgula.

Por exemplo, se quisermos escrever o conjunto $\mathcal{F}$ formado pelos
elementos $1$, $p$, $z$, e $3$ podemos usar a seguinte forma:
\[
  \mathcal{F} = \{1, p, z, 3\}
\]


\subsubsection{Descrição dos Conjuntos por Extensão}

Para descrever os elementos de um determinado conjunto podemos mencioná-los um a
um, o que é chamado de \textbf{descrição por extensão}. Definimos como o $\mathcal{Q}$
conjunto formado pelas cores do arco íris, desta forma vamos descrever o
conjunto $\mathcal{Q}$ por extensão dessa forma:
\[
  \mathcal{Q} = \{\text{vermelho}, \text{laranja}, \text{amarelo}, \text{verde},
  \text{azul}, \text{índigo}, \text{violeta}\}
\]

Se um conjunto tem muitos elementos, podemos usar \textbf{reticências}, i.e.,
três pontos, para descrevê-lo. Por exemplo, o conjunto $\mathcal{W}$ é formado
pelos cem primeiros números inteiros positivos e podemos representá-lo da
seguinte forma:
\[
  \mathcal{W} = \{1, 2, 3, \ldots, 98, 99, 100\}
\]

Neste caso não mostramos os cem elementos que formam o conjunto, contudo, as
reticências representam todos os elementos que não escrevemos.


\subsubsection{Conjuntos por Compreensão}

Em alguns casos, os conjuntos podem ter uma grande variedade de elementos e a
descrição por extensão fica muito difícil. O que podemos fazer é descrever os
conjuntos mencionando as características comuns dos elementos que o forma. Por
exemplo, se $\mathcal{C}$ é o conjunto formado por todos os países do mundo, podemos
escrevê-lo assim:
\[
  \mathcal{C} = \{x \mid x~\text{é um país}\}
\]

A barra ($\mid$) é lida como ``tal que''. Assim a expressão acima pode ser lida
da forma: ``$\mathcal{C}$ é o conjunto dos elementos $x$, tais que $x$ é um
país''. Neste caso o símbolo $x$ é usado simplesmente para representar os
elementos do conjunto $\mathcal{C}$.


\subsubsection{Conectivos}

Algumas vezes os elementos que formam um conjunto devem satisfazer mais de uma
condição, ou pelo menos uma de várias. Nestes casos usamos os conectivos
disjunção e conjunção para criar condições compostas.


\subsubsection{A Disjunção}

Observe o seguinte exemplo: 
\[
  \mathcal{A} = \{a \mid a~\text{é um animal mamífero ou voador}\}
\]

Neste caso há duas condições para os animais que formam o conjunto: Ser mamífero
\emph{ou} ser voar. A disjunção é a palavra \textbf{``ou''} que os conecta e
significa que os elementos que formam o conjunto \textbf{devem atender alguma
  destas condicionais ou as }.

Neste caso por exemplo, a abelha atende à condição de voar, e por isso deve
pertencer ao conjunto. O gato por sua vez, atende à condição de ser mamífero e
por isso também pertence ao conjunto $\mathcal{A}$. O morcego atende às duas
condições, já que é um mamífero que voa e por isto também pertence a
$\mathcal{A}$.


\subsubsection{A Conjunção}

Considere agora uma variação do conjunto definido acima:
\[
  \mathcal{B} = \{b \mid b~\text{é um animal mamífero e voador}\}
\]

Neste caso também existem duas condições mas estão unidas pela conjunção
\textbf{``e''}. Isto significa que os elementos que pertencem ao conjunto
\textbf{devem atender às duas condições ao mesmo tempo}.

No caso acima, nem o gato, nem a abelha fariam parte do conjunto $\mathcal{B}$
pois cada um deles só atende a uma das condições; entretanto, o morcego faria
parte do conjunto $\mathcal{B}$ pois atende às duas condições simultaneamente,
i.e., é mamífero e voa.

Consideremos outro exemplo. Definimos o conjunto $\mathcal{P}$ assim:
\[
  \mathcal{P} = \{p \mid p~\text{é um número maior que zero e menor que zero}\}
\]

Como não existem números que atendem as duas condições ao mesmo tempo,
concluimos que o conjunto $\mathcal{P}$ não tem elementos, ou seja, \textbf{é
  vazio}.

Também é possível combinar os conectivos anteriores para estabelecer as
condições que devem cumprir os elementos de um determinado conjunto. Por
exemplo:
\[
  \mathcal{K} = \{k \mid k~\text{é um número maior ou igual a 4 e menor que 8}\}
\]

Na definição dos elementos do conjunto há duas condições: ``ser maior ou igual a
4'' e ``ser menor que 8'', como estas condições estão unidas por uma conjunção,
i.e., ``e'', ambas devem ser cumpridas. Entretanto, a condição ``ser maior ou
igual a 4'' está composta por duas condições menores unidas por uma disjunção,
i.e., ``ou'', e isso significa que a atendem os números que são maiores que 4 ou
iguais a 4.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../ilm-book"
%%% ispell-local-dictionary: "brasileiro"
%%% End:
