\section{Relações e Bancos de Dados}
\label{sec:relations-db}

Um banco de dados é um depósito de informações associadas. O usuário de um banco
de dados pode recuperar qualquer fato específico armazenado no banco de dados. Mas
um banco de dados bem projetado é mais do que simplesmente uma lista de fatos. O
usuário pode executar consultas no banco de dados para recuperar informações não
contidas em um único fato. O todo se torna mais do que a soma de suas partes.

Para projetar um banco de dados informatizado útil e eficiente, é necessário
modelar o sistema do qual o banco de dados trata. Um \textbf{modelo conceitual}
tenta capturar os recursos e trabalhos importantes do sistema. Pode ser
necessária considerável interação com aqueles que estão familiarizados com o
sistema real para obter todas as informações necessárias para formular o modelo.


\subsection{Modelo Entidades-Relacionamentos}
\label{sec:entity-relat-model}

Uma representação em alto nível de sistemas é o \textbf{modelo
  entidade-relacionamento} (modelo ER). Neste modelo, objetos importantes no
sistemas, ou entidades, são identificados juntamente com suas propriedades
importantes, ou atributos. Os relacionamentos entre estas várias entidades
também é identificado.
%
Uma outra definição possível é dizer que um modelo ER descreve elementos de
interesse inter-relacionados em um domínio de conhecimento específico.
%
Estas informações são representadas graficamente por um \textbf{diagrama
  entidade-relacionamento}, ou diagrama ER. Em um diagrama ER, retângulos
representam conjuntos de \emph{entidades}, elipses representam \emph{atributos}
e losangos representam \emph{relacionamentos}.

\newcommand{\pkey}[1]{\underline{#1}}

\begin{figure}
  \centering
  \begin{tikzpicture}[node distance=7em, scale=0.85, transform shape]
    \node[entity] (person) {Pessoa};
    \node[attribute] (pid) [left of=person] {\pkey{ID}} edge (person);
    \node[attribute] (name) [above left of=person] {Nome} edge (person);
    \node[multi attribute] (phone) [above of= person] {Telefone} edge (person);
    \node[attribute] (address) [above right of=person] {Endereço} edge (person);
    \node[attribute] (street) [above right of=address] {Logradouro} edge (address);
    \node[attribute] (city) [right of=address] {Cidade} edge (address);
    \node[derived attribute] (age) [right of=person] {Idade} edge (person);
    \node[relationship] (uses) [below of=person] {Usa} edge (person);
    \node[entity] (tool) [below of=uses] {Ferramenta} edge[total] (uses);
    \node[attribute] (tid) [left of=tool] {\pkey{ID}} edge (tool);
    \node[attribute] (tname) [right of=tool] {Nome} edge (tool);
  \end{tikzpicture}
  \caption{Exemplo de diagrama ER}
  \label{fig:er-diagram}
\end{figure}


A \autoref{fig:er-diagram} mostra um exemplo de diagrama ER para um domínio
bastante simples. Temos um conjunto de entidades Pessoal que se relaciona com um
conjunto de entidades Ferramenta através de um relacionamento Usa. Podemos
interpretar esta modelagem como significando ``pessoas usam ferramentas''.
Também são mostrados os atributos da entidade Pessoa, i.e., ID, Nome, Telefone,
Endereço, e Idade; e os atributos da entidade Ferramenta, i.e., ID, e Nome.
Alguns elementos do diagrama merecem atenção especial:
\begin{itemize}
\item Os atributos que aparecem sublinhados (ID nas duas entidades) são
  \textbf{chaves}, i.e., servem para identificar unicamente instâncias de
  entidades no conjunto de entidades.

\item A borda dupla no atributo Telefone, na entidade Pessoa, indica que este é
  um \textbf{atributo múltiplo}, ou seja, que uma pessoa pode ter vários
  telefones.

\item O atributo Endereço, na entidade Pessoa, é um \textbf{atributo composto}.
  Ele se divide em dois outros subatributos: Logradouro e Cidade.

\item A linha tracejada no atributo Idade, na entidade Pessoa, indica que este é
  um \textbf{atributo derivado}, i.e., que seu valor não é armazenado mas
  calculado de alguma forma, possivelmente à partir dos valores de outros
  atributos.

\item A linha dupla entre o atributo Usa e a entidade Ferramenta indica que a
  participação do conjunto entidade Ferramenta no relacionamento é
  \textbf{total}, ou seja, toda instância de Ferramenta estará associada a ao
  menos uma instância de Pessoa.
\end{itemize}


\begin{exmp}

  A \emph{Associação Brasileira dos Amigos do Bichinhos de Estimação} (ABABE)
  que desenvolver uma base de dados. A ABABE adquiriu listas de e-mail de fontes
  comerciais, e está interessada nas pessoas que possuem animais de estimação e
  em algumas informações básicas sobre estes animais, tais como nome, tipo do
  animal (cachorro, gato, etc.), e raça (caso seja aplicável).

  A \autoref{fig:er-ababe} mostra o diagrama ER para o domínio da ABABE. Este
  diagrama diz que pessoas e animais são entidades. Pessoas tem os atributos
  \emph{Nome}, \emph{Endereço} (rua, nº), \emph{Cidade}, e \emph{UF} (estado).
  Animais tem os atributos \emph{NomePet}, \emph{Tipo}, e \emph{Raça}. O
  diagrama também mostra que pessoas possuem animais. Pensando em termos de
  conjuntos de entidades, i.e., o conjuto Pessoas e o conjunto Animais, o
  relacionamento ``Possui'' é uma relação binária de Pessoa para Animal -- a
  relação de propriedade é capturada pelo para ordenado (pessoa, animal). O
  ``1'' e o ``N'' na linhas do relacionamento indicam que esta relação binária é
  do tipo ``um-para-muitos'' ou ``um-para-N''; ou seja, neste domínio em
  particular, uma pessoa pode possuir vários animais, mas nenhum animal pode ter
  múltiplos donos. (Animais com múltiplos donos resultariam em um relacionamento
  ``muitos-para-muitos''.) Além disso, neste exemplo, uma pessoa pode não
  possuir animais, um animal pode não ter dono.

  O fato de que nenhum animal pode ter múltiplos donos é uma das ``regras de
  negócio'' deste domínio. Identificar as regras de negócio é muito importante,
  pois elas irão determinar várias características da nossa base de dados, como
  veremos adiante.
  
\end{exmp}

\begin{figure}
  \centering
  \begin{tikzpicture}[node distance=7em, scale=0.8, transform shape]
    \node[entity] (person) {Pessoa};
    \node[attribute] (address) [above left of=person] {Endereço} edge (person);
    \node[attribute] (name) [left of=address] {\pkey{Nome}} edge (person);
    \node[multi attribute] (city) [above right of=person] {Cidade} edge (person);
    \node[attribute] (state) [right of=city] {UF} edge (person);
    \node[relationship] (own) [below of=person] {Possui} edge node[left] {1} (person);
    \node[entity] (pet) [below of=own] {Animal} edge node[left] {N} (own);
    \node[attribute] (pettype) [below of=pet] {Tipo} edge (pet);
    \node[attribute] (petname) [left of=pettype] {\pkey{NomePet}} edge (pet);
    \node[attribute] (breed) [right of=pettype] {Raça} edge (pet);
  \end{tikzpicture}
  \caption{Exemplo de diagrama ER}
  \label{fig:er-ababe}
\end{figure}



\subsection{Modelo Relacional}
\label{sec:relational-model}

Um outra forma de representar as informações de um domínio de conhecimento
específico é através do \textbf{modelo relacional}, que pode ser desenvolvido à
partir do modelo ER. No modelo relacional tanto as entidades quanto os
relacionamentos do modelo ER se tornam \emph{relações} (no sentido matemático do
termo). As relações são representadas como tabelas. Um \textbf{banco de dados
  relacional} consiste em uma coleção desta tabelas.

Uma tabela é criada para cada conjunto entidade. Cada linha na tabela contém os
valores dos $n$ atributos de cada instância específica do conjunto entidade.
Deste modo a tabela pode ser considerada como um conjunto de $n$-uplas (linhas).
Assim como em um conjunto, assume-se que não existem tuplas duplicadas nas
tabelas do modelo relacional, e não se assume nenhuma ordenação específica das
tuplas na tabela. A ordem dos atributos não é importante, exceto pelo fato de
que deve-se manter consistência, ou seja, cada coluna em uma tabela contém
valores para um atributo específico em todas as tuplas. O número de atributos
(colunas) da tabela é chamado de \textbf{grau} da relação. O número de tuplas
(linhas) é chamado de \textbf{cardinalidade} da relação.

Mais formalmente, uma relação de banco de dados é um subconjunto de $D_1\times
D_2\times\cdots\times D_n$, onde $D_i$ é o domínio do qual o atributo $A_i$ toma
seus valores. Isso significa que o modelo relacional usa o termo \emph{relação}
de forma consistem com a definição de relação $n$-aria dada em
\autoref{sec:relacoes-n-arias}.


\begin{exmp}

  A relação \emph{Pessoa} no banco de dados ABABE pode conter dados como os
  mostrados abaixo.

  \begin{center}
    \begin{tabular}{|l|l|l|l|l|}
      \hline
      \multicolumn{4}{|c|}{\textbf{Pessoa}}                                                          \\
      \hline
      \pkey{\textbf{Nome}} & \textbf{Sexo} & \textbf{Endereço}       & \textbf{Cidade} & \textbf{UF} \\
      \hline
      Ana Souza            & F             & Rua Quintino, 123       & Vitória         & ES          \\ \hline
      Bruno Trevis         & M             & Av. dos Desolados, 1021 & Linhares        & ES          \\ \hline
      Carlos Umbre         & M             & Rua Don Quixote, 357    & Niterói         & RJ          \\ \hline
      Diana Vicentino      & F             & Rua Brooklyn, 99        & Santos          & SP          \\ \hline
      Elvira Walker        & F             & Trav. do Viradouro, 56  & Itabira         & MG          \\ \hline
      Fausto Xylander      & M             & Rua João da Cruz, 101   & Serra           & ES          \\ \hline
      Gilda Yankov         & F             & Rua da Revolução, 1917  & Vila Velha      & ES          \\ \hline
      Heitor Zoz           & M             & Av. Antônio Carlos, 67  & Rio de Janeiro  & RJ          \\ \hline
    \end{tabular}
  \end{center}

  A relação \emph{Animal} poderia ser como a seguir.

  \begin{center}
    \begin{tabular}{|l|l|l|}
      \hline
      \multicolumn{3}{|c|}{Animal}                            \\
      \hline
      \pkey{\textbf{NomePet}} & \textbf{Tipo} & \textbf{Raça} \\
      \hline
      Mancha                  & cão           & cão de caça   \\ \hline
      Princesa                & gato          & siamês        \\ \hline
      Garotão                 & cão           & collie        \\ \hline
      Lassie                  & cão           & collie        \\ \hline
      Moicano                 & peixe         & zanclidae     \\ \hline
      Piu-piu                 & pássaro       & canário       \\ \hline
      Tigre                   & gato          & pêlo curto    \\ \hline
    \end{tabular}
  \end{center}
  
\end{exmp}


Como não há duplicação de tuplas em uma relação, indicar o valor de todos os $n$
atributos de uma tupla claramente a distingue de todas as outras. Entretanto,
podem existir muitos subconjuntos mínimos de atributos que possam ser usados
para distinguir cada tupla. Esses subconjuntos de atributos, caso existam, são
chamados de \textbf{chave primária}. Se o subconjunto for formado por um único
atributo, ele é dito uma \emph{chave primária simples}; se o subconjunto
consistir de mais de um atributo, ele é uma \emph{chave primária composta}.
Tipicamente, na tabela que descreve a relação, os nomes dos atributos que compõem
a chave primária aparecem sublinhados.

Uma outra regra de negócios do domínio da ABABE é que pessoas tem nomes únicos,
portanto o atributo \emph{Nome} é suficiente para identificar unicamente cada
tupla e foi escolhido como chave primária da relação \emph{Pessoa}. De modo
semelhante, o atributo \emph{NomePet} foi escolhido como chave primária na
relação \emph{Animal} o que indica deve haver uma regra de negócios que garante
que não haverá repetição nos nomes de animais de estimação.

Estas regras de negócio que determinam que não haverá duplicação de nomes são,
certamente, simplistas e usadas apenas para propósitos didáticos. Em domínios de
conhecimento reais, tipicamente utiliza-se atributos numéricos tais como CPF,
número de passaporte, ou algum outro para identificar pessoas pois há garantia
de que não haverá repetição. No caso dos animais de estimação não há um
identificador como estes, assim, em um cenário mais realista, seria necessário
criar um atributo ``artificial'' único para cada animal de estimação para ser
usado como chave primária. Esta chave primária não teria nenhum correspondente
no domínio de conhecimento real associado ao sistema, assim o usuário do banco
de dados nunca precisaria tomar conhecimento dela; este tipo de chave é chamada
de \emph{chave
  substituta}.\footnote{\url{https://en.wikipedia.org/wiki/Surrogate_key}}

Tipicamente, as implementações de bancos de dados relacionais assumem que cada
domínio $D_i$ de atributo em uma base de dados relacional contém -- além de seus
valores ``reais'' -- um valor especial \emph{nulo}, de modo que uma dada tupla
pode ter um ou mais de seus atributos com valor nulo (vazio). Entretanto, nenhum
atributo da chave primária pode ser nulo. Essa restrição de \textbf{integridade
  de entidade} meramente confirma que cada tupla deve ter um valor de chave
primária que a distingua de todas as outras tuplas, e que todos os atributos da
chave primária são necessários para identificar a tupla unicamente.

Um atributo em uma relação (chamada relação ``filha'') pode ter o mesmo domínio
que o atributo de chave primária em outra relação (chamada relação ``pai'').
Este atributo é chamado de \textbf{chave estrangeira} (da relação pai) na
relação filha. A relação (no modelo relacional) que representa um relacionamento
entre duas entidades (no modelo entidade-relacionamento) usa chaves estrangeiras
para estabelecer conexões entre estas entidades. Haverá uma chave estrangeira na
relação de relacionamento pra cada entidade que participe do relacionamento.


\begin{exmp}

  A base de dados da ABABE identificou as seguintes instâncias para o
  relacionamento \emph{Possui}. O atributo \emph{Nome} em \emph{Possui} é uma
  chave estrangeira da relação \emph{Pessoa}; o atributo \emph{NomePet} em
  \emph{Possui} é uma chave estrangeira de \emph{Animal}.

  \begin{center}
    \begin{tabular}{|l|l|}
      \hline
      \multicolumn{2}{|c|}{\textbf{Possui}}      \\
      \hline
      \textbf{NomePet} & \pkey{\textbf{NomePet}} \\
      \hline
      Fausto Xylander  & Mancha                  \\ \hline
      Gilda Yankov     & Princesa                \\ \hline
      Ana Souza        & Garotão                 \\ \hline
      Heitor Zoz       & Lassie                  \\ \hline
      Bruno Trevis     & Piu-Piu                 \\ \hline
      Carlos Umbre     & Tigre                   \\ \hline
    \end{tabular}
  \end{center}

  A primeira tupla estabelece o relacionamento \emph{Possui} entre Fausto
  Xylander e Mancha; ou seja, ela indica que Fausto Xylander é dono de Mancha.

  Pessoas que não possuem animais de estimação não são representadas em
  \emph{Possui}, nem são representados os animais de estimação que não tem dono.
  A chave primária da relação \emph{Possui} foi definida como sendo o atributo
  \emph{NomePet}. Recorde a regra de negócio que estabelece que um animal de
  estimação não pode ter múltiplos donos. Se fosse possível um animal de
  estimação ter múltiplos donos precisaríamos usar uma chave primária composta
  com \emph{Nome} e \emph{NomePet} para identificar as tuplas da relação
  \emph{Possui}.
  
\end{exmp}


Algumas vezes não é necessário definir uma tabela separada para representar um
relacionamento, como fizemos no exemplo anterior. A tabela separada para o
relacionamento nunca é necessária se o relacionamento for do tipo um-para-um, e
ela pode frequentemente ser removida em relacionamentos do tipo um-para-muitos
como o do nosso exemplo.


\begin{exmp}

  Como cada animal de estimação só pode ter um dono (ou nenhum), podemos guardar
  o nome do dono diretamente na tupla que já armazena as outras informações do
  animal. Desta forma não seria necessário criar uma tabela separada para
  representar o relacionamento, ele já estaria representado na relação que
  representa o animal. Vamos chamar a relação que contém as informação do animal
  e o nome do dono de \emph{AnimalDono}.

  \begin{center}
    \begin{tabular}{|l|l|l|l|}
      \hline
      \multicolumn{4}{|c|}{\textbf{AnimalDono}} \\
      \hline
      \textbf{Nome}   & \pkey{\textbf{NomePet}} & \textbf{Tipo} & \textbf{Raça} \\
      \hline
      Fausto Xylander & Mancha                  & cão           & cão de caça   \\ \hline
      Gilda Yankov    & Princesa                & gato          & siamês        \\ \hline
      Ana Souza       & Garotão                 & cão           & collie        \\ \hline
      Heitor Zoz      & Lassie                  & cão           & collie        \\ \hline
      \nullvalue\     & Moicano                 & peixe         & zanclidae     \\ \hline
      Bruno Trevis    & Piu-piu                 & pássaro       & canário       \\ \hline
      Carlos Umbre    & Tigre                   & gato          & pêlo curto    \\ \hline
    \end{tabular}
  \end{center}

  A relação \emph{AnimalDono} substitui tanto a relação \emph{Animal} quanto a
  relação \emph{Possui} sem perda de informação. Note que a relação
  \emph{AnimalDono} possui uma tupla com o valor \nullvalue\ no atributo
  \emph{Nome}. Isto permite que animais sem dono sejam representados na relação
  e não viola a integridade da entidade pois o atributo \emph{Nome} não faz
  parte da chave primaria.
  
\end{exmp}


\subsection{Álgebra Relacional}

A \textbf{álgebra relacional} é uma álgebra derivada da álgebra de conjuntos e
da lógica de primeira ordem. Ela é uma álgebra com uma semântica (significado)
bem fundamentada usada para modelar dados armazenados em bases de dados
relacionais, e para definir consultas nestas bases de dados. A principal
aplicação da álgebra relaciona é fornecer a fundamentação teórica para as
\href{https://pt.wikipedia.org/wiki/Banco_de_dados_relacional}{bases de dados
  relacionais}, particularmente para as linguagens de consulta, dentre as quais
a principal é a linguagem SQL.

A álgebra relacional foi descrita pela primeira vez por E. F. Codd em 1970,
enquanto ele trabalhava para a IBM. As cinco operações primitivas da álgebra
relacional descrita por Codd são a \emph{seleção}, a \emph{projeção}, o
\emph{produto cartesiano}, a \emph{união}, e \emph{diferença} de conjuntos. Mais
tarde também foi acrescentada a operação de \emph{renomeação}. Há outras
operações possíveis, e bastante úteis, tais como \emph{junção natural},
\emph{junção-$\theta$}, \emph{equijunção}, \emph{semijunção} e \emph{divisão}.
Mas estas podem ser derivadas à partir das operações primitivas, então nós não
iremos discutí-las (exceto pela junção-$\theta$, como falaremos adiante).

Para uma abordagem mais detalhada sobre a álgebra relacional e suas operações,
veja a página \href{%
  https://pt.wikipedia.org/wiki/\%C3\%81lgebra_relacional}{%
  Álgebra relacional} da Wikipédia.

A seguir teremos uma breve visão da álgebra relacional, de suas operações
primitivas. Também falaremos da operação de junção-$\theta$.


\subsubsection{Operadores de Conjuntos}

A álgebra relacional usa as operações de união, diferença e produto cartesiano
da teoria dos conjuntos, mas adiciona algumas restrições a essas operações.

Para união e a diferença de conjuntos, só é possível realizar as operações se as
duas relações envolvidas forem \textbf{compatíveis para união} -- isto é, as duas
relações devem ter os mesmos atributos, na mesma ordem. Uma vez que, na álgebra
relacional, a interseção de conjuntos é definida em termos da união e da
diferença; na interseção também é necessário que as duas relações envolvidas
sejam compatíveis.

Para definir o produto cartesiano, as duas relações envolvidas devem ter
cabeçalhos disjuntos -- isto é, as duas relações não podem ter atributos com os
mesmos nomes.

Na álgebra relacional o produto cartesiano é definido de maneira ligeiramente
diferente da forma como ele é definido em teoria dos conjuntos.

\begin{itemize}
\item Na teoria dos conjuntos, os elementos do dois conjuntos envolvidos no
  produto formam um par ordenado.
\item Na álgebra relacional, os dois conjuntos envolvidos no produto são
  relações e seus elementos são tuplas. Ao efetuar o produto, ao invés de parear
  as tuplas elas são \textbf{concatenadas}.
\end{itemize}

Isto é, o produto cartesiano de um conjunto de $n$-uplas com um conjunto de
$m$-uplas resulta um conjunto $(n+m)$-uplas. Mais formalmente, $R\times S$ é
definido como segue:
\[
R \times S = \{
(r_1, r_2, \ldots, r_n, s_1, s_2, \ldots, s_m)
\mid (r_1, r_2, \ldots, r_n) \in R, (s_1, s_2, \ldots, s_m) \in S
\}
\]

A cardinalidade do produto Cartesiano é o produto das cardinalidades dos seus
fatores, i.e., $|R\times S| = |R|\times |S|$.

O produto cartesiano é, tipicamente, utilizando em conjunto com as operações de
seleção e projeção.


\subsubsection{Projeção ($\pi$)}

A projeção é uma operação unária escrita como $\pi_{[a_1, \dots, a_n]}(R)$ onde
$a_1, \dots, a_n$ é um conjunto de nomes de atributos. O resultado de tal
projeção é definida como o conjunto que é obtido quando todas as tuplas em $R$
são restritas ao conjunto de atributos $a_1, \dots, a_n$.

Isto especifica o subconjunto de colunas (atributos de cada tupla) a ser
selecionado. Por exemplo, para obter os nomes e cidades da relação
\emph{Pessoa}, a projeção poderia ser escrita
$\pi_{[\text{Nome},\text{Cidade}]}(\text{Pessoa})$. O resultado de tal projeção
seria uma relação com apenas os atributos \emph{Nome} e \emph{Cidade} para
cada entrada única em \emph{Pessoa}.

Note que ao fazer a projeção, duas ou mais tuplas que originalmente eram
distintas podem se tornar iguais (pode ser que os atributos que continham
valores diferentes sejam removidos na projeção). Caso isso acontece, como o
resultado das operações é sempre um conjunto, as duplicatas serão removidas e
apenas uma instância permanecerá no resultado.


\subsubsection{Seleção ($\sigma$)}

Uma seleção é uma operação unária escrita como $\sigma_\varphi(R)$
onde $\varphi$ é uma fórmula proposicional que consiste de átomos como
é permitido na seleção normal e operadores os lógicos $\land$ (e),
$\lor$ (ou) e $\lnot$ (não). Esta seleção seleciona todas as tuplas em
$R$ para as quais o valor de $\varphi$ é verdadeiro.

Por exemplo, para obter a lista de todas as pessoas do sexo feminino que
moram no Espírito Santo, a seleção poderia ser escrita como:
\[
\sigma_{\text{Sexo} = \text{F} \land \text{UF} = \text{ES}}(\text{Pessoa})
\]
O resultado seria uma relação que tinha todos atributos de todos os
registo únicos onde \emph{Sexo} tem valor \textsf{F} ou onde o estado é igual
a \emph{ES}.

No artigo acadêmico de Codd de 1970, a seleção é chamada de \emph{restrição}.


\subsubsection{Renomeação ($\rho$)}

Renomear é uma operação unária escrita como $\rho_{a / b}(R)$ onde o resultado é
um conjunto idêntico ao conjunto $R$ exceto pelo fato de que o atributo $b$ em
todas as tuplas é renomeado para um atributo $a$. Isto é usado para mudar o nome
do atributo de uma relação ou da própria relação.

Para renomear o atributo \textsf{Tipo} para \textsf{Espécie} na
relação \emph{Animal}, podemos escrever a expressão:
\[
  \rho_{\text{Espécie}/\text{Tipo}}(\text{Animal})
\]
Se mais do que uma renomeação for necessária, elas podem ser separadas por
vírgulas:
\[
  \rho_{\text{Espécie}/\text{Tipo},\text{Variedade}/\text{Raça}}(\text{Animal})
\]

O operador de renomeação também pode ser usado para mudar o nome da relação.
Isso frequentemente é feito quando se precisa usar duas ``cópias'' da mesma
relação em uma expressão. A forma como o operador $\rho$ é usado para mudar o
nome das relações é a seguinte:
\[
  \rho_{\text{Pet}}(\text{Animal})
\]


\subsubsection{Junção Natural ($\bowtie$)}

A junção natural ($\bowtie$) é uma operação binária escrito como $R \bowtie S$,
onde $R$ e $S$ são relações. O resultado da junção natural é o conjunto de todas
as combinações de tuplas em $R$ e $S$ que são iguais em seus atributos comuns,
i.e., atributos de mesmo nome. Para um exemplo, considere as tabelas
\emph{Funcionário} e \emph{Departamento} e sua junção natural:


\begin{center}
  \begin{tabular}[t]{|l|l|l|}
    \multicolumn{3}{c}{\textbf{Funcionário}}                  \\
    \hline
    \textbf{Nome}      & \textbf{FuncId} & \textbf{NomeDepto} \\
    \hline
    Horácio            & 3415            & Finanças           \\
    \hline
    Sônia              & 2241            & Vendas             \\
    \hline
    Jorge              & 3401            & Finanças           \\
    \hline
    Helena             & 2202            & Vendas             \\
    \hline
  \end{tabular}
  \quad
  \begin{tabular}[t]{|l|l|}
    \multicolumn{2}{c}{\textbf{Departamento}}                 \\
    \hline
    \textbf{NomeDepto} & \textbf{Gerente}                     \\
    \hline
    Finanças           & Jorge                                \\
    \hline
    Vendas             & Helena                               \\
    \hline
    Produção           & Carlos                               \\
    \hline
  \end{tabular}
\end{center}

\begin{center}
  \begin{tabular}[t]{|l|l|l|l|}
    \multicolumn{4}{c}{$\text{Funcionário}\bowtie\text{Departamento}$} \\
    \hline
    \textbf{Nome}      & \textbf{FuncId} & \textbf{NomeDepto} & \textbf{Gerente} \\
    \hline
    Horácio            & 3415            & Finanças           & Jorge            \\
    \hline
    Sônia              & 2241            & Vendas             & Helena           \\
    \hline
    Jorge              & 3401            & Finanças           & Jorge            \\
    \hline
    Helena             & 2202            & Vendas             & Helena           \\
    \hline  
  \end{tabular}
\end{center}

A junção natural é, provavelmente, um dos operadores mais importantes, pois é a
contraparte relacional da conjunção (``e'') lógico. Observe cuidadosamente que
se a mesma variável aparecer em cada um dos dois predicados conectados por um
``e'', então essa variável representa a mesma coisa e ambas as ocorrências devem
sempre ser substituídas pelo mesmo valor. Em particular, a junção natural
permite a combinação de relações associadas por uma \emph{chave estrangeira}.
Por exemplo, no exemplo acima, provavelmente há uma uma chave estrangeira entre
os campos \emph{Funcionário.NomeDepto} e \emph{Departamento.NomeDepto}. A junção
natural de \emph{Funcionário} e \emph{Departamento} combina todos os
funcionários com seus departamentos. Isso funciona porque a chave estrangeira é
válida entre os atributos com o mesmo nome. Se esse não for o caso, como na
chave estrangeira de \emph{Departamento.Gerente} para \emph{Funcionário.Nome},
temos que renomear essas colunas antes de tomarmos a junção natural. Tal junção
é às vezes também referida como uma equjunção (veja junção-$\theta$).


\subsubsection{Junção-$\theta$ ($\bowtie_\theta$)}

A junção-$\theta$ entre duas relações $R$ e $S$, que é escrita $R \bowtie_\theta
S$, pode ser vista como uma operação que combina o produto cartesiano e a
seleção, da seguinte forma:
\[
  R \bowtie_\theta S \equiv \delta_\theta(R\times S)
\]
Onde $\theta$ representa um critério de seleção do tipo $a\odot b$ ou $a\odot v$
em que $\odot$ é um dos operadores relacionais ($<, \leq, =, \neq, \geq, >$);
$a$ e $b$ são atributos do produto cartesiano $R\times S$, e $v$ é um valor
constante.

Como exemplo, considere a expressão abaixo:\footnote{Note que estamos igualando
  atributos de mesmo nome, logo, poderíamos ter usado uma junção natural ao
  invés da junção-$\theta$.}
\[
  \rho_P(\text{Pessoa}) \bowtie_{\text{P.nome} = \text{A.nome}} \rho_A(\text{AnimalDono})
\]
Essa expressão renomeia a relação \emph{Pessoa} para \textsf{P} e a relação
\emph{AnimalDono} para \textsf{A} (apenas para ``encurtar'' a expressão). Em
seguida realiza uma junção-$\theta$ entre as relações \textsf{P} e \textsf{A}
com critério de seleção \(\text{P.nome} = \text{A.nome}\). Ou seja, será feito
um produto cartesiano entre \emph{Pessoa} e \emph{AnimalDono} e, sobre o
resultado do produto cartesiano, será aplicado um filtro para selecionar apenas
as tuplas onde o \emph{Nome} que ``veio'' de \emph{AnimalDono} seja igual ao
\emph{Nome} que ``veio'' de \emph{Pessoa}.

O mesmo resultado poderia ser obtido pela expressão abaixo:
\[
  \delta_{\text{P.nome} = \text{A.nome}}(\rho_P(\text{Pessoa})\times\rho_A(\text{AnimalDono}))
\]
Onde primeiramente é feito, explicitamente, o produto cartesiano e em seguida é
aplicada, também explicitamente, a seleção.


\subsection{Integridade de Bancos de Dados}
\label{sec:db-integrity}


\subsection{Exercícios}
\label{sec:exercicios-relat-algebra}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../ilm-book"
%%% ispell-local-dictionary: "brasileiro"
%%% End:
