\section{Operações Aritméticas na Base $b$}

Os algoritmos convencionais para adição, subtração, multiplicação e
divisão que são ensinados no ensino fundamental para o sistema de
numeração posicional de base 10 também funcionam para qualquer outra
base. Basta que se ajuste adequadamente a forma de representar os
números.

\subsection{Adição}

Adição é uma das operações básicas da álgebra. Na sua forma mais
simples, adição combina dois números (termos, somandos ou parcelas), em
um único número, a soma. Adicionar mais números corresponde a repetir a
operação. Uma adição é representada por:
\begin{equation}
  \label{eq:addition}
  a + b = c
\end{equation}

Caso os númerais $a$ e $b$ na Equação~\eqref{eq:addition} possuam apenas
um dígito, a adição será feita seguindo o procedimento ``padrão'' de
acrescentar $b$ unidades ao numeral $a$. Por exemplo, na base
hexadecimal temos $\mathrm{A} + 9 = 13$, ou seja, foram acrescentadas
$9_{16}$ unidades, ao numeral $A_{16}$, e a soma foi $13_{16}$. Por
exemplo:
\begin{multicols}{3}
  \begin{itemize}
  \item $1_2 + 1_2 = 11_2$
  \item $3_4 + 1_4 = 10_4$
  \item $6_8 + 5_8 = 13_8$
  \item $7_8 + 1_8 = 10_8$
  \item $A_{12} + 7_{12} = 15_{12}$
  \item $B_{12} + 9_{12} = 18_{12}$
  \item $E_{16} + A_{16} = 18_{16}$
  \item $F_{16} + 7_{16} = 16_{16}$
  \item $H_{20} + A_{20} = 17_{20}$
\end{itemize}
\end{multicols}

Para adições envolvendo numerais com mais do que um dígito, utilizamos o
algoritmo de adição manual -- que é o mesmo aprendido no ensino
fundamental --, que consiste em escrever as duas parcelas alinhadas à
direita de modo que os dígitos das posições correspondentes dos dois
numerais fiquem alinhados. Em seguida, realiza-se a soma dos dígitos
dois-a-dois das duas parcelas, iniciando-se com os dígitos da posição
zero, em seguida com os da posição um, e assim por diante. Caso a soma
de algum par de dígitos resulte em um numeral com mais do que um dígito,
o dígito de mais baixa ordem da soma é escrito no resultado e os demais
digitos são promovidos para a próxima etapa da soma.

Note que só faz sentido efetuar o algoritmo de adição se os dois
numerais sendo somados estiverem na mesma base.

\begin{exmp}[Adição dos numerais $442_8$ e $5473_8$]~
  \begin{enumerate}
  \item Escrevemos as duas parcelas alinhadas à direita:
    \\
    \begin{tabular}{ccccc}
        &   & 4 & 4 & 2 \\
      + & 5 & 4 & 7 & 3 \\
      \hline
        &   &   &   &   \\
    \end{tabular}
  \item Somamos os dígitos da posição 0; na base 8: $2 + 3 = 5$
    \\
    \begin{tabular}{ccccc}
        &   & 4 & 4 & 2 \\
      + & 5 & 4 & 7 & 3 \\
      \hline
        &   &   &   & 5 \\
    \end{tabular}
  \item Somamos os dígitos da posição 1; na base 8: $4 + 7 = 13$ --- uma
    vez que a soma possui mais de um dígito, escrevemos o dígito de mais
    baixa ordem (3) no resultado e promovemos os demais dígito para a
    próxima etapa da soma.
    \\
    \begin{tabular}{ccccc}
        &   & $\overset{1}{\mathsf{4}}$ & 4 & 2 \\
      + & 5 & 4 & 7 & 3 \\
      \hline
        &   &   & 3 & 5 \\
    \end{tabular}
  \item Somamos os dígitos da posição 2 (incluindo os promovidos da
    etapa anterior); na base 8: $1 + 4 = 5$ e $5 + 4 = 11$ ---
    novamente, escrevemos o dígito de mais baixa ordem e promovemos os
    demais para a próxima etapa.
    \\
    \begin{tabular}{ccccc}
        & $\overset{1}{}$ & $\overset{1}{\mathsf{4}}$ & 4 & 2 \\
      + & 5 & 4 & 7 & 3 \\
      \hline
        &   & 1  & 3 & 5 \\
      \end{tabular}
  \item Somamos os dígitos da posição 3; na base 8: $1 + 5 = 6$
    \\
    \begin{tabular}{ccccc}
        & $\overset{1}{}$ & $\overset{1}{\mathsf{4}}$ & 4 & 2 \\
      + & 5 & 4 & 7 & 3 \\
      \hline
        & 6  & 1  & 3 & 5 \\
      \end{tabular}
    \item Portanto, $442_8 + 5473_8 = 6135_8$.
    \end{enumerate}
\end{exmp}

\begin{exmp}[Adição dos numerais $\mathrm{F}\mathrm{A}84_{16}$ e
  $1\mathrm{A}\mathrm{D}\mathrm{C}7_{16}$]~
  \begin{enumerate}
  \item Escrevemos as duas parcelas alinhadas à direita:
    \\
    \begin{tabular}{cccccc}
    ~ & 1 & $\mathrm{A}$ & $\mathrm{D}$ & $\mathrm{C}$ & 7 \\
    + & ~ & $\mathrm{F}$ & $\mathrm{A}$ & 8 & 4 \\
    \hline
    ~ & ~ & ~ & ~ & ~ & ~ \\
    \end{tabular}
  \item Somamos os dígitos da posição 0; na base 16: $7 + 4 = B$
    \\
    \begin{tabular}{cccccc}
    ~ & 1 & $\mathrm{A}$ & $\mathrm{D}$ & $\mathrm{C}$ & 7 \\
    + & ~ & $\mathrm{F}$ & $\mathrm{A}$ & 8 & 4 \\
    \hline
    ~ & ~ & ~ & ~ & ~ & $\mathrm{B}$ \\
    \end{tabular}
  \item Somamos os dígitos da posição 1; na base 16: $\mathrm{C} + 8 =
    14$ --- escrevemos o $4$ e promovemos o $1$.
    \\
    \begin{tabular}{cccccc}
    ~ & 1 & $\mathrm{A}$ & $\overset{1}{\mathrm{D}}$ & $\mathrm{C}$ & 7 \\
    + & ~ & $\mathrm{F}$ & $\mathrm{A}$ & 8 & 4 \\
    \hline
    ~ & ~ & ~ & ~ & 4 & $\mathrm{B}$ \\
    \end{tabular}
  \item Somamos os dígitos da posição 2; na base 16: $1 + \mathrm{D} =
    \mathrm{E}$ e $\mathrm{E} + \mathrm{A} = 18$ --- escrevemos o $8$ e
    promovemos o $1$.
    \\
    \begin{tabular}{cccccc}
    ~ & 1 & $\overset{1}{\mathrm{A}}$ & $\overset{1}{\mathrm{D}}$ & $\mathrm{C}$ & 7 \\
    + & ~ & $\mathrm{F}$ & $\mathrm{A}$ & 8 & 4 \\
    \hline
    ~ & ~ & ~ & 8 & 4 & $\mathrm{B}$ \\
    \end{tabular}
  \item Somamos os dígitos da posição 3; na base 16: $1 + \mathrm{A} =
    \mathrm{B}$ e $\mathrm{B} + \mathrm{F} = 1\mathrm{A}$ --- escrevemos
    o $\mathrm{A}$ e promovemos o $1$.
    \\
    \begin{tabular}{cccccc}
    ~ & $\overset{1}{1}$ & $\overset{1}{\mathrm{A}}$ & $\overset{1}{\mathrm{D}}$ & $\mathrm{C}$ & 7 \\
    + & ~ & $\mathrm{F}$ & $\mathrm{A}$ & 8 & 4 \\
    \hline
    ~ & ~ & $\mathrm{A}$ & 8 & 4 & $\mathrm{B}$ \\
    \end{tabular}
  \item Somamos os dígitos da posição 4; na base 16: $1 + 1 = 2$.
    \\
    \begin{tabular}{cccccc}
    ~ & $\overset{1}{\mathsf{1}}$ & $\overset{1}{\mathrm{A}}$ & $\overset{1}{\mathrm{D}}$ & $\mathrm{C}$ & 7 \\
    + & ~ & $\mathrm{F}$ & $\mathrm{A}$ & 8 & 4 \\
    \hline
    ~ & 2 & $\mathrm{A}$ & 8 & 4 & $\mathrm{B}$ \\
    \end{tabular}
  \end{enumerate}
\end{exmp}


\subsection{Subtração}
\newcommand{\corta}[1]{\ensuremath{\mathrlap{\diagdown}{#1}}}

Subtração é uma operação matemática que indica quanto é um valor
numérico resultante se, de um valor inicial (minuendo), for removido
outro valor numérico (subtraendo). Uma subtração é representada por:
\begin{equation}
  \label{eq:subtraction}
  a - b = c
\end{equation}

Caso os numerais $a$ e $b$ na Equação~\eqref{eq:subtraction} possuam
apenas um dígito e o numeral $a$ repesente um número maior do que $b$,
então a subtração $a - b$ poderá ser calculada simplesmente
``contando-se'' quanta unidades $a$ possui a mais do que $b$. Por
exemplo:
\begin{multicols}{3}
  \begin{itemize}
  \item $1_2 - 0_2 = 1_2$
  \item $3_4 - 1_4 = 2_4$
  \item $6_8 - 5_8 = 1_8$
  \item $7_8 - 1_8 = 6_8$
  \item $A_{12} - 7_{12} = 3_{12}$
  \item $B_{12} - 5_{12} = 6_{12}$
  \item $E_{16} - A_{16} = 4_{16}$
  \item $F_{16} - 9_{16} = 6_{16}$
  \item $H_{20} - 8_{20} = A_{20}$
\end{itemize}
\end{multicols}

Entretanto, se o minuendo e/ou o subtaendo forem compostos por mais de
um dígito, deve-se utilizar o algoritmos manual de subtração. Este
algoritmo é, em essência, o mesmo que é aprendido no ensino fundamental,
exceto pelo fato de que as subtrações dígito-a-dígito devem ser
efetuadas na mesma base do minuando e do subtraendo. (Note que só faz
sentido subtrair dois númerais se eles estiverem na mesma base.)

O algoritmo de subtração manual pode ser resumido da seguinte forma:
\begin{enumerate}

\item Escreve-se o minuendo e abaixo dele, alinhados à direita, o
  subtraendo. Ambos os numerais devem ser escrito de tal modo que o
  dígito na posição zero do minuendo esteja diretamente acima do dígito
  da posição zero do subtraendo, o dígito da posição um do minuendo
  esteja diretamente acima do dígito da posição um do subtraendo, e
  assim por diante.

\item Cada dígito do minuendo e do subtraendo constituem uma etapa da
  subtração, assim o dígito zero corresponderá à etapa zero, o dígito um
  à etapa um, etc.

\item Para cada etapa da subtração:
  \begin{enumerate}

  \item Se o valor do minuendo para esta etapa for maior do que o valor
    do subtraendo, então realize a subtração simples entre os dois
    valores e escreva o resultado.

  \item Se o valor do minuendo para esta etapa for menor do que o valor
    do subtraendo, então uma unidade de potência deverá ser subtraída da
    etapa porterior (do dígito à esquerda) e adicionada ao valor desta
    etapa, após adicionar esta unidade de potência deve-se fazer a
    subtração simples e escrever o resultado.

  \end{enumerate}

\end{enumerate}


\begin{exmp}[Subração dos numerais $1337_8$ e $235_8$]~
  \begin{enumerate}
  \item Escrever o minuendo e o subtraendo alinhados à direita.
    \\
    \begin{tabular}{ccccc}
       ~  & 1 & 3 & 3 & 7 \\
      $-$ & ~ & 2 & 3 & 5 \\
      \hline
      ~ & ~ & ~ & ~ & ~ \\
    \end{tabular}
  \item Subtrair os valores da etapa 0; na base 8: $7 - 5 = 2$.
    \\
    \begin{tabular}{ccccc}
      ~  & 1 & 3 & 3 & 7 \\
      $-$ & ~ & 2 & 3 & 5 \\
      \hline
      ~  & ~ & ~ & ~ & 2 \\
    \end{tabular}
  \item Subtrair os valores da etapa 1; na base 8: $3 - 3 = 0$.
    \\
    \begin{tabular}{ccccc}
      ~  & 1 & 3 & 3 & 7 \\
      $-$ & ~ & 2 & 3 & 5 \\
      \hline
      ~  & ~ & ~ & 0 & 2 \\
    \end{tabular}
  \item Subtrair os valores da etapa 2; na base 8: $3 - 2 = 1$.
    \\
    \begin{tabular}{ccccc}
      ~  & 1 & 3 & 3 & 7 \\
      $-$ & ~ & 2 & 3 & 5 \\
      \hline
      ~  & ~ & 1 & 0 & 2 \\
    \end{tabular}
  \item Subtrair os valores da etapa 3; na base 8: $1 - 0 = 1$. Note que
    a ausência de dígitos à esquerda é interpretada como zero.
    \\
    \begin{tabular}{ccccc}
      ~  & 1 & 3 & 3 & 7 \\
      $-$ & ~ & 2 & 3 & 5 \\
      \hline
      ~  & 1 & 1 & 0 & 2 \\
    \end{tabular}
  \end{enumerate}
\end{exmp}

\begin{exmp}[Subração dos numerais $1A7CD_{16}$ e $38F5_{16}$]~
  \begin{enumerate}
  \item Escrever o minuendo e o subtraendo alinhados à direita.
    \\
    \begin{tabular}{cccccc}
       ~  & 1 & A & 7 & C & D \\
      $-$ & ~ & 3 & 8 & F & 5 \\
      \hline
      ~ & ~ & ~ & ~ & ~ & ~ \\
    \end{tabular}
  \item Subtrair os valores da etapa 0; na base 16: $D - 5 = 8$.
    \\
    \begin{tabular}{cccccc}
       ~  & 1 & A & 7 & C & D \\
      $-$ & ~ & 3 & 8 & F & 5 \\
      \hline
      ~ & ~ & ~ & ~ & ~ & 8 \\
    \end{tabular}
  \item Subtrair os valores da etapa 1; em hexadecimal $C < F$ portanto
    não é possível fazer a subtração simples nesta etapa; é necessário
    subtrair uma unidade de potência do dígito da etapa à esquerda, ou
    seja, é preciso ``pegar emprestado'' uma unidade do dígito $7$ na
    posição 2 e adicionar esta unidade de potência ao dígito $C$ na
    posição 1. Só depois podemos fazer a subtração em hexadecimal $1C -
    F = D$.
    \\
    \begin{tabular}{cccccc}
       ~  & 1 & A & $\corta{7}^6$ & ${}^1$C & D \\
      $-$ & ~ & 3 & 8 & F & 5 \\
      \hline
      ~ & ~ & ~ & ~ & D & 8 \\
    \end{tabular}
  \item Subtrair os valores da etapa 2; uma vez que $6 < 8$, não é
    possível fazer a subtração simples; é necessário ``pegar
    emprestado'' uma unidade do dígito $A$ na posição 3 e adicionar esta
    unidade de potência ao dígito $6$ na posição 2. Só depois podemos
    fazer a subtração em hexadecimal $16 - 8 = E$.
    \\
    \begin{tabular}{cccccc}
       ~  & 1 & $\corta{\mathsf{A}}^9$ & $\corta{7}^{16}$ & ${}^1$C & D \\
      $-$ & ~ & 3 & 8 & F & 5 \\
      \hline
      ~ & ~ & ~ & E & D & 8 \\
    \end{tabular}
  \item Subtrair os valores da etapa 3; em hexadecimal: $9 - 3 = 6$.
    \\
    \begin{tabular}{cccccc}
       ~  & 1 & $\corta{\mathsf{A}}^9$ & $\corta{7}^{16}$ & ${}^1$C & D \\
      $-$ & ~ & 3 & 8 & F & 5 \\
      \hline
      ~ & ~ & 6 & E & D & 8 \\
    \end{tabular}
  \item Subtrair os valores da etapa 3; em hexadecimal: $1 - 0 = 1$.
    \\
    \begin{tabular}{cccccc}
       ~  & 1 & $\corta{\mathsf{A}}^9$ & $\corta{7}^{16}$ & ${}^1$C & D \\
      $-$ & ~ & 3 & 8 & F & 5 \\
      \hline
      ~ & 1 & 6 & E & D & 8 \\
    \end{tabular}
  \end{enumerate}
\end{exmp}


\subsection{Multiplicação}

Na sua forma mais simples a multiplicação é uma forma simples de se
adicionar uma quantidade finita de números iguais. O resultado da
multiplicação de dois números é chamado produto. Os números sendo
multiplicados são chamados de coeficientes, fatores ou operandos, e
individualmente de multiplicando e multiplicador. A multiplicação é
representada por:
\[
a\cdot b = c
\quad
\text{ou}
\quad
a\times b = c
\]


\subsection{Divisão}

Divisão é a operação matemática inversa da multiplicação. O ato de
dividir por um elemento de um conjunto só faz sentido quando a
multiplicação por aquele elemento for uma função bijetora. No conjunto
dos número inteiro a propriedade de bijetividade não é satisfeita para o
zero (0), assim não se define divisão por zero. A divisão é representada
por:
\[
a\div b = c
\quad
\text{ou}
\quad
\frac{a}{b} = c
\]

\subsection{Exercícios}

\begin{exercise}
  Efetue as adições indicadas abaixo com os número em suas respectivas
  bases.
  \begin{multicols}{2}
    \begin{enumerate}[label=({\alph*})]
    \item $01010110_2 + 1000110_2$
    \item $1110000_2 + 100111_2$
    \item $100110000_2 + 100111101_2$
    \item $000001001_2 + 111001_2$
    \item $1211030_4 + 32000_4$
    \item $1331331_4 + 3301122_4$
    \item $3120322_4 + 1121000_4$
    \item $23301_4 + 1231030_4$
    \item $505052_8 + 4034_8$
    \item $4306_8 + 77166_8$
    \item $43254_8 + 132042_8$
    \item $327104_8 + 373277_8$
    \item $\mathrm{6A41}_{12} + \mathrm{310}_{12}$
    \item $\mathrm{6A5B50}_{12} + \mathrm{BA342}_{12}$
    \item $\mathrm{75A6B4}_{12} + \mathrm{786625}_{12}$
    \item $\mathrm{798A2}_{12} + \mathrm{749220}_{12}$
    \item $\mathrm{4DE058}_{16} + \mathrm{564DF}_{16}$
    \item $\mathrm{85DE}_{16} + \mathrm{F1365E}_{16}$
    \item $\mathrm{E2C1}_{16} + \mathrm{A123}_{16}$
    \item $\mathrm{64FA4D}_{16} + \mathrm{E5AFFE}_{16}$
    \end{enumerate}
  \end{multicols}
\end{exercise}

\begin{exercise}
  Efetue as subtrações indicadas abaixo com os número em suas
  respectivas bases.
  \begin{multicols}{2}
    \begin{enumerate}[label=({\alph*})]
    \item $100101101_2 - 10110010_2$
    \item $10011_2 - 101110_2$
    \item $1100100_2 - 1011_2$
    \item $110000110_2 - 10111000_2$
    \item $3121020_4 - 1212131_4$
    \item $123020_4 - 122302_4$
    \item $3132320_4 - 2202122_4$
    \item $232021_4 - 230001_4$
    \item $515377_8 - 3550213_8$
    \item $2204420_8 - 57743_8$
    \item $5512_8 - 601411_8$
    \item $4560407_8 - 252111_8$
    \item $\mathrm{6870B}_{12} - \mathrm{B2B917}_{12}$
    \item $\mathrm{86712}_{12} - \mathrm{1423}_{12}$
    \item $\mathrm{41308A}_{12} - \mathrm{96599}_{12}$
    \item $\mathrm{9BA29B}_{12} - \mathrm{48A50}_{12}$
    \item $\mathrm{66BA94}_{16} - \mathrm{49FEC}_{16}$
    \item $\mathrm{A27A42}_{16} - \mathrm{A7FD72}_{16}$
    \item $\mathrm{C7C83}_{16} - \mathrm{1855BC}_{16}$
    \item $\mathrm{9337C}_{16} - \mathrm{30BDE4}_{16}$
    \end{enumerate}
  \end{multicols}
\end{exercise}

\begin{exercise}
  Efetue as multiplicações indicadas abaixo com os número em suas
  respectivas bases.
  \begin{multicols}{2}
    \begin{enumerate}[label=({\alph*})]
    \item $1100_2 \cdot 11101_2$
    \item $1011010_2 \cdot 10100_2$
    \item $10010_2 \cdot 110100_2$
    \item $101001_2 \cdot 10100_2$
    \item $32210_4 \cdot 12033_4$
    \item $30101_4 \cdot 20121_4$
    \item $3212_4 \cdot 13013_4$
    \item $1033_4 \cdot 21132_4$
    \item $562_8 \cdot 6606_8$
    \item $73042_8 \cdot 10764_8$
    \item $7140_8 \cdot 3071_8$
    \item $7422_8 \cdot 145_8$
    \item $\mathrm{4564}_{12} \cdot \mathrm{2B4}_{12}$
    \item $\mathrm{942}_{12} \cdot \mathrm{B10}_{12}$
    \item $\mathrm{7B5}_{12} \cdot \mathrm{278}_{12}$
    \item $\mathrm{17A2}_{12} \cdot \mathrm{A35}_{12}$
    \item $\mathrm{9A}_{16} \cdot \mathrm{20}_{16}$
    \item $\mathrm{89}_{16} \cdot \mathrm{109}_{16}$
    \item $\mathrm{0787}_{16} \cdot \mathrm{36}_{16}$
    \item $\mathrm{5B0}_{16} \cdot \mathrm{B9}_{16}$
    \end{enumerate}
  \end{multicols}    
\end{exercise}

\begin{exercise}
  Para cada uma das equações abaixo, determine qual é a base $b_i$ na
  qual os numerais estão representados.
  \begin{enumerate}[label=({\alph*})]
  \item $\mathrm{3B8}_{b_1} + \mathrm{608}_{b_1} = \mathrm{9C1}_{b_1}$
  \item $\mathrm{93B23}_{b_2} + \mathrm{9A2F8}_{b_2} = \mathrm{10DDHB}_{b_2}$
  \item $134140_{b_3} + 3124410_{b_3} = 3314100_{b_3}$
  \item $6124536_{b_4} + 2253134_{b_4} = 11411003_{b_4}$
  \end{enumerate}
\end{exercise}


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../Intro_Log_Mat"
%%% ispell-local-dictionary: "brasileiro"
%%% End: 
