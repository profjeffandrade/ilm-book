\section{Sistemas de Numeração Posicionais}

Sistemas de numeração posicionais, ou notação posicional, ou notação
posição-valor, é um método de representação ou codificação e números.  A
notação posicional é diferente das outras notações por utilizar o
mesmo símbolo para diferentes ordens de magnitude (as unidades, as
dezenas, as centenas, os milhares, etc.). Esta notação simplificou
bastante a forma de executar as operações aritiméticas, o que levou à
sua popularização relativamente rápida por todo o mundo.

Com o uso de um \emph{ponto raiz}\footnote{Também chamado de
  \emph{ponto de origem}. Em língua portuguesa, espanhol, alemão e
  outras, o ``ponto'' é na verdade uma vírgula.} (vírgula decimal),
esta notação pode ser extendida para incluir números fracionários,
como as expansões numéricas dos números reais. O sistema de numeração
Hindo-Arábico é um exemplo de um sistema de numeração posicional
baseado no número 10.

\subsection{Base do Sistema de Numeração}

Uma ``base de numeração'' é um conjunto de símbolos utilizado para
escrever numerais. Em princípio qualquer conjunto finito de símbolos
pode ser utilizado como base de numeração, entretanto, usualmente os
dígitos do sistema de numeração hindo-arábicos e as letras do alfabeto
latino são utilizadas. A ``base'' ou ``radical'' é o número de
dígitos, incluíndo o zero, que um sistema posicional de numeração usa
para representar números. Por exemplo, no sistema decimal a base é 10
porque ele usa 10 dígitos, de 0 a 9. 

O símbolo mais alto de um sistema de numeração posicional tem o valor
da base menos um. Quando o número que se quer representar é maior do
que o mais alto dígito da base são acrescentados mais dígitos. Assim,
na base 10, o número seguinte ao 9 é escrito colocando-se o ``1''
seguido do ``0'', que formam o número dez. Em binário a base é 2,
assim o número que se segue ao 1 (o dígito mais alto da base 2) também
é formado colocando-se o ``1'' seguido do ``0'' para formar o número
dois.

Na notação posicional de base 10 (decimal), existem 10 dígitos e
temos:
\[
2506 = 2 \cdot 10^3 + 5 \cdot 10^2 + 0 \cdot 10^1 + 6 \cdot 10^0
\]

Na notação posicional de base 16 (hexadecimal), existem 16 dígitos e
temos:
\[
171\mathrm{B} = 1 \cdot 16^3 + 7 \cdot 16^2 + 1 \cdot 16^1 + \mathrm{B} \cdot 16^0
\]
onde o $\mathrm{B}$ representa o valor onze.

De modo geral, em um sistema posicional de base $b$, há $b$ dígitos e
temos:
\[
a_k\!\ldots\!a_2a_1a_0 = a_k\cdot b^k + \ldots + a_2\cdot b^2 + a_1\cdot b^1 + a_0\cdot b^0
\]
Note que $a_ka_{k-1}\!\ldots\!a_2a_1a_0$ representa a sequência dos
dígitos $a_i$ e não a sua multiplicação.

As vezes o número da base aparece subescrito após o número
representado. Por exemplo, $23_8$ indica que o número $23$ está expresso
na base $8$; $1111011_2$ indica que o número $1111011$ está na base 2;
$173_{12}$ está na base $12$; e $7\mathrm{B}_{16}$ está na base
$16$. Esta é a notação que usaremos neste texto. Caso a base não seja
explicitamente indicada, assumiremos que a base é 10.

Ao descrever bases em notação matemática, a letra $b$ é geralmente
usada como um símbolo para representar a base, assim, para um sistema
binário $b = 2$. Evidentemente, a base $b$ também pode ser indicada
pela fraze ``base $b$''. Assim números binário são ``base 2'', números
octais são ``base 8'', número decimais são ``base 10'', e assim por
diante.

As bases númericas mais comumente utilizadas são:\footnote{Note que
  alguns altores não incluem o ``I'' na base vigesimal, alegando que o
  ``I'' é muito parecido com o ``1''; esses altores consideram que os
  dígitos da base vigesimal são: 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, A, B, C,
  D, E, F, G, H, J e K.}

\begin{tabular}{lclcl}
  \textbf{Base} & --- & \textbf{Nomenclatura} & --- & \textbf{Dígitos}
  \\
  Base 2  & --- & binário     & --- & 0 1
  \\
  Base 8  & --- & octal       & --- & 0 1 2 3 4 5 6 7
  \\
  Base 10 & --- & decimal     & --- & 0 1 2 3 4 5 6 7 8 9
  \\
  Base 12 & --- & duodecimal  & --- & 0 1 2 3 4 5 6 7 8 9 A B
  \\
  Base 16 & --- & hexadecimal & --- & 0 1 2 3 4 5 6 7 8 9 A B C D E F
  \\
  Base 20 & --- & vigesimal   & --- & 0 1 2 3 4 5 6 7 8 9 A B C D E F G H I J
\end{tabular}


\subsection{Exponenciação}

Sistemas de numeração posicionais funcionam baseados em exponenciação
da base. O valor de um dígito é obtido multiplicando-se o valor do
algarismo pelo valor de sua posição. O valor da posição é dado pelo
valor da base multiplicado pela $n$-ésima potência, onde $n$ é o
número de outros dígitos entre um dado dígito e o ponto raiz.  Se um
determinado dígito está à esquerda do ponto raiz, então o valor de $n$
é positivo ou zero; se o dígito está à esquerda do ponto raiz, então
$n$ é negativo.

Como um exemplo, considere o número $465_b$ (note que $b \geq 7$, pois
o algarismo mais alto do número é $6$), assim:
\[
465_b = 4\cdot b^2 + 6\cdot b^1 + 5\cdot b^0
\]

Se $b = 10$, então o valor seria:
\begin{align*}
  465_{10} 
  &= 4\cdot 10^2 + 6\cdot 10^1 + 5\cdot 10^0 \\
  &= 400 + 60 + 5 \\
  &= 465_{10}
\end{align*}

Entretanto, se o número estiver na base $7$, então:
\begin{align*}
  465_7
  &= 4\cdot 7^2 + 6\cdot 7^1 + 5\cdot 7^0 \\
  &= 4\cdot 49 + 6\cdot 7 + 5\cdot 1 \\
  &= 196 + 42 + 5 \\
  &= 243_{10}
\end{align*}

Para qualquer base $b$, $10_b = b$. Isso pode ser demonstrado pelo
fato de que $10_b = 1\cdot b^1 + 0\cdot b^0$, logo $10_b = 1\cdot
b$.

Números que não são inteiros possuem dígitos à direita do ponto
raiz. Para cada posição além (à direita) deste ponto, a potência $n$
diminui de 1. Por exemplo, o número $12,35_b$ é igual a:
\[
12,35_b = 1\cdot b^1 + 2\cdot b^0 + 3\cdot b^{-1} + 5\cdot b^{-2}
\]

Assim, se $b = 8$ temos:
\begin{align*}
  12,35_8
  &= 1\cdot 8^1 + 2\cdot 8^0 + 3\cdot 8^{-1} + 5\cdot 8^{-2} \\
  &= 1\cdot 8 + 2\cdot 1 + 3\cdot\frac{1}{8} + 5\cdot\frac{1}{64} \\
  &= 8 + 2 + \frac{3}{8} + \frac{5}{64} \\
  &= 10,453125_{10}
\end{align*}

Mas, se $b = 12$ temos:
\begin{align*}
  12,35_{12} 
  &= 1\cdot 12^1 + 2\cdot 12^0 + 3\cdot 12^{-1} + 5\cdot 12^{-2} \\
  &= 1\cdot 12 + 2\cdot 1 + 3\cdot\frac{1}{12} + 5\cdot\frac{1}{144} \\
  &= 12 + 2 + \frac{3}{12} + \frac{5}{144} \\
  &= 14,28472222\!\ldots_{10}
\end{align*}


\subsection{Dígitos e Numerais}

Um dígito é o que é usado como uma posição na notação posição-valor, e
um númeral é uma sequência de um ou mais dígitos. A distinção entre um
digito e um numeral é mais pronunciada ao se considerar diferentes
bases de numeração.

Um numeral não-zero com mais de uma posição de dígito representará
números diferentes em bases de numeração diferentes, mas de modo
geral, os dígitos manterão o mesmo significado. Por exemplo, considere
o numeral ``$23$''; na base 8 este numeral representa o número
dezenove, ao passo que na base 10 eles representam o número vinte e
três. Entretanto, tanto na base 8 quanto na base 10 o dígito ``$2$''
representa ``dois'' e o dígito ``3'' representa ``três''.

O sistema de numeração mais comum nos sistemas de computação atuais é
o sistema binário (base 2), mas também se utilizam os sistemas octal
(base 8) e hexadecimal (base 16) uma vez que estes sistemas são
congruentes ao sistema de numeração binário. Em binário, apenas os
dígitos ``$0$'' e ``$1$'' são usados. Em octal, os dígitos de ``$0$''
a ``$7$'' são usados. Em hexadecimal, dezesseis dígitos são usados; os
dígitos de ``$0$'' a ``$9$'' e de ``A'' a ``F'', com ``A''
representando dez, ``B'' representando onze, e assim por diante.


\subsection{Exercícios}

\begin{exercise}
  Escreva em ordem crescente os primeiros 25 números em cada uma das
  bases indicadas abaixo. Escreva os números na forma de uma tabela
  usando as bases como cabeçalhos das colunas.
  \begin{multicols}{2}
    \begin{enumerate}[label=({\alph*})]
    \item decimal
    \item binário
    \item quinário
    \item octal
    \item undecimal
    \item duodecimal
    \item hexadecimal
    \item vigesimal
    \end{enumerate}
  \end{multicols}
\end{exercise}

\begin{exercise}
  Quantos números diferentes de no máximo três (3) dígitos é possível
  escrever em cada uma das bases indicadas abaixo:
  \begin{multicols}{2}
    \begin{enumerate}[label=({\alph*})]
    \item Base 2: \rule{50mm}{0.5pt}
    \item Base 4: \rule{50mm}{0.5pt}
    \item Base 5: \rule{50mm}{0.5pt}
    \item Base 8: \rule{50mm}{0.5pt}
    \item Base 10: \rule{50mm}{0.5pt}
    \item Base 12: \rule{50mm}{0.5pt}
    \item Base 16: \rule{50mm}{0.5pt}
    \item Base 20: \rule{50mm}{0.5pt}
    \end{enumerate}
  \end{multicols}
\end{exercise}

\begin{exercise}
  Quanto dígitos são necessários para se escrever o numero $1111$ em
  cada uma das bases abaixo? 
  \begin{multicols}{2}
    \begin{enumerate}[label=({\alph*})]
    \item Base 2: \rule{50mm}{0.5pt}
    \item Base 4: \rule{50mm}{0.5pt}
    \item Base 5: \rule{50mm}{0.5pt}
    \item Base 8: \rule{50mm}{0.5pt}
    \item Base 10: \rule{50mm}{0.5pt}
    \item Base 12: \rule{50mm}{0.5pt}
    \item Base 16: \rule{50mm}{0.5pt}
    \item Base 20: \rule{50mm}{0.5pt}
    \end{enumerate}
  \end{multicols}
\end{exercise}

\begin{exercise}
  Indique qual a menor base possível para os numerais indicados
  abaixo. Por exemplo, para $547_b$, temos $b \geq 8$; e para $5A7_b$,
  temos $b \geq 11$.
  \begin{multicols}{2}
    \begin{enumerate}[label=({\alph*})]
    \item $0_b$
    \item $1101_b$
    \item $777_b$
    \item $1024_b$
    \item $666_b$
    \item $1EE7_b$
    \item $A5A_b$
    \item $B1A_b$
    \end{enumerate}
  \end{multicols}
\end{exercise}

\begin{exercise}
  Considere os seguintes símbolos: $\Box, \Diamond, \triangle, \star,
  \bowtie, \odot$. Considerando que esta sequência de símbolos forme uma
  base numérica, e que os símbolos foram dados em ordem aritmética, ou
  seja, o primeiro símbolo tem valor zero, o segundo tem valor um, etc.
  Determine o valor de cada um dos numerais abaixo (na base definida
  acima) usando o método de expoentes:
  \begin{multicols}{2}
    \begin{enumerate}[label=({\alph*})]
    \item $\Diamond\Box\star$
    \item $\star\Box\Box\odot$
    \item $\bowtie\Diamond\star\Box$
    \item $\odot\Box\triangle\triangle$
    \end{enumerate}
  \end{multicols}
\end{exercise}

\begin{exercise}
  Considere a base $\mathcal{L}$ formada pelas 26 letras do alfabeto
  latino ocidental, sendo $Z = 0, A = 1, B = 2, C = 3, \ldots, X = 24$ e
  $Y = 25$.  Determine o valor de cada um dos numerais abaixo (na base
  $\mathcal{L}$) usando o método de expoentes:
  \begin{multicols}{2}
    \begin{enumerate}[label=({\alph*})]
    \item $YOU$
    \item $SHALL$
    \item $NOT$
    \item $PASS$
    \end{enumerate}
  \end{multicols}
\end{exercise}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../Intro_Log_Mat"
%%% End: 
