\section{Números em Complemento de Dois}

Em computação, a expressão \emph{complemento para dois} ou
\emph{complemento de dois} pode significar tanto uma operação matemática
sobre números em binário quanto um tipo de representação de números em
binário com sinal baseada nesta operação. A notação de complemento de
dois é amplamente usada nas arquiteturas dos dispositivos computacionais
modernos. Nesta representação dígito mais significativo, i.e., o dígito
mais à esquerda, é o que informa o sinal do número. Se este dígito for
$0$ o número é positivo, e se for $1$ é negativo.

O complemento de dois de um número $x$ de $n$-bits\footnote{Um digito em
  binário é chamado de um \emph{bit}. Uma sequência de 4 bits é chamada
  de um \emph{nibble}. Uma sequência de 8 bits é chamada de um
  \emph{byte} ou \emph{octeto}.} é definido como o complemento em
relação à $2^n$; em outras palavras, é o resultado de subtrair $x$ de
$2^n$. O mesmo resultado pode ser conseguido tomando-se o
\emph{complemento de um} do número $x$, escrito como $\bar{x}$, e em
seguida somando-se 1 a $\bar{x}$. O \emph{complemento de um} de um
número é calculado invertendo-se todos os bits de um número, ou seja,
trocando-se todos os 0's por 1's e vice-versa. O complemento de dois de
um número $x$ se comporta como o negativo de $x$ na maioria das
operações aritiméticas, de modo que números positivos e negativos podem
coexistir de modo natural, sem a necessidade de sinais.

Na notação de complemento de dois, números negativos são representados
pelo complemento de dois de seu valor absoluto. Este sistema é o método
mais comum de representar números inteiros com sinal em sistemas
digitais. Um número de $n$-bits na notação de complemento de dois pode
representar todos os inteiros na faixa de $-(2^{n-1})$ até
$+(2^{n-1}-1)$.

O sistema de complemento de complemento de dois tem a vantagem de que as
operações aritméticas fundamentais de adição, subtração e mutiplicação
são idênticas àquelas para números binários sem sinal, desde que todos
os números envolvidos sejam representados com a mesma quantidade de bits
e que qualquer \emph{overflow}\footnote{O termo \emph{overflow} descreve
  a condição que ocorre quando uma operação aritmética produz um
  resultado que tem magnitude maior do que aquela que um dado
  registrador ou espaço de armazenamento pode armazenar, i.e., o
  resultado exigiria mais bits do que estão disponíveis no espaço de
  armazenamento.} ou \emph{underflow}\footnote{O termo \emph{underflow}
  descreve uma condição na qual o resultado de um cálculo é menor do que
  pode ser armazenado no espaço de armazenamento disponível; é uma
  condição análoga ao \emph{overflow}, porém para valores negativos.}
que exceda estes bits seja descartado. Esta propriedade torna o sistema
ao mesmo tempo mais simples de implementar e capaz de manipular
facilmente aritmética de alta precisão.

Para identificar que um número está representado em complemento de dois,
anexaremos o subscrito $\bar{2}$ (o número dois com uma barra acima) ao
número. Por exemplo: $00101101_{\bar{2}}$, $10010110_{\bar{2}}$, etc.
Desta forma, poderemos diferenciar entre a notação em complemento de
dois e a notação binária comum.


\subsection{Conversão em Complemento de Dois}

Podemos falar de conversões em complemento de dois em quatro situações
mais comuns:
\begin{enumerate}
\item Conversão de complemento de dois para decimal;
\item Conversão de decimal para complemento de dois;
\item Conversão de binário (com sinal) para complemento de dois;
\item Conversão de complemento de dois para binário (com sinal);
\end{enumerate}
Falaremos sobre estas conversões nas seções abaixo.

\subsubsection{Conversão de Complemento de Dois para Decimal}

O sistema numérico de complemento de dois codifica números negativos e
positivos como números binários. O ``peso'' de cada bit é uma potência
de dois exceto para o bit mais significante (o mais a esquerda), cujo
peso é o negativo da potência de dois correspondente.

No sistema numérico de complemento de dois, o valor $w$ de um inteiro de
$n$ bits representado por $a_{n-1}a_{n-2}\ldots{}a_0$ (onde cada $a_i$,
para $i$ de $n-1$ até $0$, representa um bit) é dado pela fórmula
abaixo:

\begin{equation}
  \label{eq:compl2val}
  w = -a_{n-1}\cdot 2^{n-1} + \sum_{i=0}^{n-2} a_i\cdot 2^i
\end{equation}

Desta forma, o bit mais significante determina o sinal do número e é
comumente chamado de ``bit de sinal''. Diferentemente do representação
``sinal-e-magnitude'', o bit de sinal também tem um ``peso'' associado
que a potência $2^{n-1}$ mostrada na Equação~\eqref{eq:compl2val}, porém
este peso é sempre negativo.  Usando $n$ bits em complemento de dois é
possível representar todos os números inteiros de $-(2^{n-1})$ até
$2^{n-1}-1$.

\begin{exmp}[Converter $00101101_{\bar{2}}$ para decimal]~\\
  Seguindo a Equação~\eqref{eq:compl2val}:
  \begin{align*}
    00101101_{\bar{2}}
    &= -0\cdot 2^7 + 0\cdot 2^6 + 1\cdot 2^5 + 0\cdot 2^4 + 1\cdot 2^3 + 1\cdot 2^2 + 0\cdot 2^1 + 1\cdot 2^0 \\
    &= -0\cdot 128 + 0\cdot 64 + 1\cdot 32 + 0\cdot 16 + 1\cdot 8 + 1\cdot 4 + 0\cdot 2 + 1\cdot 1 \\
    &= -0 + 0 + 32 + 0 + 8 + 4 + 0 + 1 \\
    &= 45
  \end{align*}
  $\therefore 00101101_{\bar{2}} = 45_{10}$
\end{exmp}

\begin{exmp}[Converter $11101001_{\bar{2}}$ para decimal]~\\
  Seguindo a Equação~\eqref{eq:compl2val}:
  \begin{align*}
    11101001_{\bar{2}}
    &= -1\cdot 2^7 + 1\cdot 2^6 + 1\cdot 2^5 + 0\cdot 2^4 + 1\cdot 2^3 + 0\cdot 2^2 + 0\cdot 2^1 + 1\cdot 2^0 \\
    &= -1\cdot 128 + 1\cdot 64 + 1\cdot 32 + 0\cdot 16 + 1\cdot 8 + 0\cdot 4 + 0\cdot 2 + 1\cdot 1 \\
    &= -128 + 64 + 32 + 0 + 8 + 0 + 0 + 1 \\
    &= -23
  \end{align*}
  $\therefore 11101001_{\bar{2}} = -23_{10}$
\end{exmp}



\subsubsection{Conversão de Decimal para Complemento de Dois}

Não se conhece um algoritmo para conversão direta de números em decimal
com sinal para complemento de dois. O processo normalmente utilizado
para se realizar a conversão manualmente é primeiro converter o número
em decimal para binário com sinal e em seguida converter o número
binário com sinal para complemento de dois.


\subsubsection{Conversão para Complemento de Dois}

Em notação de complemento de dois, um número inteiro não negativo é
representado simplesmente por sua notação em binária; neste caso o bit
mais significativo é zero (0). Entretanto, deve-se prestar atenção para
o fato de que a faixa de valores representada não é a mesma que na
notação em binário. Por exemplo, um número de 8 bits em binário pode
representar os valores com magnitude de 0 a 255 ($11111111_2$).
Entretanto, um número de 8 bits em complemento de dois pode representar
apenas os número não-negativos de 0 a 127 ($0111111_2$), porque todas as
outras combinações de bits, com o bit mais significativo como $1$,
representam os valores negativos de $-1$ a $-128$.

A operação de complemento de dois é \emph{elemento
  inverso}\footnote{\emph{Elemento inverso} é aquele cuja utilização
  numa operação binária matemática bem definida resulta no elemento
  neutro específico dessa operação.}, ou \emph{oposto
  aditivo}\footnote{\emph{Oposto aditivo} de um número $x$ é o número
  $y$ tal que $x+y=0$. Em notação convencional, o oposto aditivo de um
  número $x$ é escrito $-x$.}, de modo que os números negativos são
representados pelo complemento de dois do valor absoluto do número.
Como dito, a operação de complemento de dois de um número de $n$ bits é
definida como o complemento em relação a $2^n$, o complemento de dois de
um número binário negativo pode ser obtido subtraindo-se o valor
absoluto deste número de $2^n$ em binário, ou seja, um número binário de
$n+1$ bits, com o primeiro bit 1 e os demais 0.


\begin{exmp}[Converter $-101001_2$ para complemento de dois com $8$ bits]~\\
  O valor absoluto do número a ser convertido é $101001_2$. \\
  Este valor será subtraído de $2^8$ (i.e., $100000000_2$).\\[3mm]
  \begin{tabular}{cccccccccc}
    ~ & $\corta{1}^0$ & $\corta{0}^1$ & $\corta{0}^1$ & $\corta{0}^1$ & $\corta{0}^1$ & $\corta{0}^1$ & $\corta{0}^1$ & $\corta{0}^1$ & ${}^10$ \\
    - & ~ & ~ & ~ & $1$ & $0$ & $1$ & $0$ & $0$ & $1$ \\
    \hline
    ~ & $0$ & $1$ & $1$ & $0$ & $1$ & $0$ & $1$ & $1$ & $1$
  \end{tabular}
  \\[2mm]
  Portanto, a representação em complemento de dois com 8 bits do número
  $-101001_2$ é $11010111_{\bar{2}}$
\end{exmp}


\begin{exmp}[Converter $-1110100_2$ para complemento de dois com $8$ bits]~\\
  O valor absoluto do número a ser convertido é $1110100_2$. \\
  Este valor será subtraído de $2^8$ (i.e., $100000000_2$).\\[3mm]
  \begin{tabular}{cccccccccc}
    ~ & $\corta{1}^0$ & $\corta{0}^1$ & $\corta{0}^1$ & $\corta{0}^1$ & $\corta{0}^1$ & $\corta{0}^1$ & ${}^10$ & $0$ & $0$ \\
    - & ~ & ~ & $1$ & $1$ & $1$ & $0$ & $1$ & $0$ & $0$ \\
    \hline
    ~ & $0$ & $1$ & $0$ & $0$ & $0$ & $1$ & $1$ & $0$ & $0$
  \end{tabular}
  \\[2mm]
  Portanto, a representação em complemento de dois com 8 bits do número
  $-101001_2$ é $11010111_{\bar{2}}$
\end{exmp}


\subsubsection{Conversão para Complemento de Dois via Complemento de Um}

Uma forma alternativa de realizar a conversão de um número em binário
para complemento de dois é utilizar uma conversão intermediária para
\emph{complemento de um}. O processo para obtenção do complemento de
dois com $n$ bits de um número binário $x$ via complemento de um é
descrito abaixo:

\begin{enumerate}

\item Toma-se o valor absoluto de $x$ denotado por $\abs{x}$.
  
\item Caso $\abs{x}$ possua menos do que $n$ bits, $\abs{x}$ deve ser
  reescrito como $x'$, acrescentando-se zeros à esquerda até que $x'$
  fique com $n$ bits.

\item Calcula-se o complemento de um de $x'$, que será denotado por
  $\bar{x'}$.
  \begin{enumerate}
  \item O complemento de um de $x'$ é calculado invertendo-se
    (negando-se) cada um dos bits de $x'$.
  \end{enumerate}

\item Soma-se $1$ ao valor de $\bar{x'}$, despresando-se qualquer
  possível bit de \emph{overflow} resultante da soma. Os $n$ bits
  obtidos representam o complemento de dois de $x$.

\end{enumerate}


\begin{exmp}[Converter $-101001_2$ para complemento de dois com $8$ bits]~\\
  \begin{enumerate}
  \item O valor absoluto do número é $101001$;
  \item Ajustando para 8 bits temos: $00101001$;
  \item Tomando o complemento de um: $11010110$;
  \item Somando $1$ ao complemento de um:\\[3mm]
    \begin{tabular}{ccccccccc}
      ~ & $1$ & $1$ & $0$ & $1$ & $0$ & $1$ & $1$ & $0$ \\
      + &     &     &     &     &     &     &     & $1$ \\
      \hline
      ~ & $1$ & $1$ & $0$ & $1$ & $0$ & $1$ & $1$ & $1$ \\      
    \end{tabular}
  \end{enumerate}
  Portanto, a representação em complemento de dois com 8 bits do número
  $-101001_2$ é $11010111_{\bar{2}}$
\end{exmp}


\begin{exmp}[Converter $-1110100_2$ para complemento de dois com $8$ bits]~\\
  \begin{enumerate}
  \item O valor absoluto do número é $1110100$;
  \item Ajustando para 8 bits temos: $01110100$;
  \item Tomando o complemento de um: $10001011$;
  \item Somando $1$ ao complemento de um:\\[3mm]
    \begin{tabular}{ccccccccc}
      ~ & $1$ & $0$ & $0$ & $0$ & $1$ & $\overset{1}{0}$ & $\overset{1}{1}$ & $1$ \\
      + &     &     &     &     &     &     &     & $1$ \\
      \hline
      ~ & $1$ & $0$ & $0$ & $0$ & $1$ & $1$ & $0$ & $0$ \\      
    \end{tabular}
  \end{enumerate}
  Portanto, a representação em complemento de dois com 8 bits do número
  $-1110100_2$ é $10001100_{\bar{2}}$
\end{exmp}


\subsection{Operações Aritméticas em Complemento de Dois}

\subsubsection{Adição}

A adição de dois números em complemento de dois não requer qualquer
pocessamento especial, mesmo que os operando tenham sinais opostos: o
sinal do resultado é determinado autommaticamente como resultado da
própria operação de adição.

\begin{exmp}[Adicionando $15$ e $-5$]~\\
  \label{ex:compl2add1}
  Considerando a notação de complemento de dois com 8 bits, temos:\\
  $15_{10} = 0000 1111_{\bar{2}}$ \\
  $-5_{10} = 1111 1011_{\bar{2}}$ \\
  Somando os dois termos:\\[2mm]
  \begin{tabular}{ccccccccc}
    $\scriptstyle{\corta{1}}$
    & $\scriptstyle{1}$
    & $\scriptstyle{1}$
    & $\scriptstyle{1}$
    & $\scriptstyle{1}$
    & $\scriptstyle{1}$
    & $\scriptstyle{1}$
    & $\scriptstyle{1}$
    & ~ \\
    ~   & $0$ & $0$ & $0$ & $0$ & $1$ & $1$ & $1$ & $1$ \\
    $+$ & $1$ & $1$ & $1$ & $1$ & $1$ & $0$ & $1$ & $1$ \\
    \hline
    ~   & $0$ & $0$ & $0$ & $0$ & $1$ & $0$ & $1$ & $0$ \\
  \end{tabular}
  \\
  Temos, como esperado, que: $0000 1010_{\bar{2}} = 10_{10}$.
\end{exmp}

Este processo impõe a restrição de que o número seja limitado a 8
bits. Devido a essa restrição o vai-um para o 9\textordmasculine~bit
do Exemplo~\ref{ex:compl2add1} foi ignorado, o que resultou na soma
correta. Caso a restrição não existisse o 9\textordmasculine~bit teria
sido considerado e o resultado seria um valor negativo (incorreto).

Os dois últimos bits da linha de vai-um (da direita para a esquerda)
contém informações de grande importância: eles determinam se ocorreu ou
não \emph{overflow}, i.e., se o resultado foi um número grande demais
para ser armazenado na quantidade de bits disponível. Se os dois últimos
bits da linha de vai-um foram diferentes entre si, ocorreu
\emph{overflow}.

Em outras palavras, se os dois últimos bits da linha de vai-um forem
$11$ ou $00$\footnote{Dizer que os bits de vai-um são $00$ é o mesmo que
  dizer que não houve ``vai-um'' nestes bits.}, o resultado é válido;
porém, se os dois últimos bits forem $01$ ou $10$, então ocorreu
\emph{overflow}. Considere o exemplo abaixo:

\begin{exmp}[Adicionando $7$ e $3$ em complemento de dois com 4 bits]~\\
  \label{ex:compl2add2}
  Considerando a notação de complemento de dois com 4 bits, temos:\\
  $7_{10} = 0111_{\bar{2}}$ \\
  $3_{10} = 0011_{\bar{2}}$ \\
  Somando os dois termos:\\[2mm]
  \begin{tabular}{ccccc}
    $\scriptstyle{\corta{0}}$
    & $\scriptstyle{1}$
    & $\scriptstyle{1}$
    & $\scriptstyle{1}$
    & ~ \\
    ~   & $0$ & $1$ & $1$ & $1$ \\
    $+$ & $0$ & $0$ & $1$ & $1$ \\
    \hline
    ~   & $1$ & $0$ & $1$ & $0$ \\
  \end{tabular}
  \\
  Temos: $1010_{\bar{2}} = -6_{10}$, que é um resultado inválido.
\end{exmp}

No caso ilustrado no Exemplo~\ref{ex:compl2add2}, os dois bits de vai-um
mais a esquerda são $01$, o que significa que houve um \emph{overflow}
na adição, ou seja, o resultado da adição $7+3$ está fora da faixa
permitida para número em complemento de doi de quatro bits, que é de
$-8$ a $7$.


\subsubsection{Subtração}

Utilizando-se a notação de complemento de dois é possível simplificar o
cálculo da operação de subtração transformando-a em uma adição. O
procedimento de baseia na identidade mostrada abaixo:
\[
x - y \equiv x + (-y)
\]
Assim, ao invés de efetuarmos a subtração $x - y$, efetuamos a soma dos
números $x$ e $-y$, ambos expressos em complemento de dois.

\begin{exmp}[Subtração $15 - 5$ em complemento de dois com 8 bits]~\\
  A subtração $15 - 5$ é transformada na adição $15 + (-5)$;\\
  $15_{10} = 0000 1111_{\bar{2}}$\\
  $-5_{10} = 1111 1011_{\bar{2}}$\\
  Efetuando a soma:\\[2mm]
  \begin{tabular}{ccccccccc}
    $\scriptstyle{\corta{1}}$
    & $\scriptstyle{1}$
    & $\scriptstyle{1}$
    & $\scriptstyle{1}$
    & $\scriptstyle{1}$
    & $\scriptstyle{1}$
    & $\scriptstyle{1}$
    & $\scriptstyle{1}$
    & ~ \\
    ~   & $0$ & $0$ & $0$ & $0$ & $1$ & $1$ & $1$ & $1$ \\
    $+$ & $1$ & $1$ & $1$ & $1$ & $1$ & $0$ & $1$ & $1$ \\
    \hline
    ~   & $0$ & $0$ & $0$ & $0$ & $1$ & $0$ & $1$ & $0$ \\
  \end{tabular}
  \\
  Temos então o resultado: $00001010_{\bar{2}} = 10_{10}$.
\end{exmp}


\begin{exmp}[Subtração $15 - (-5)$ em complemento de dois com 8 bits]~\\
  A subtração $15 - 5$ é transformada na adição $15 + (+5)$;\\
  $15_{10} = 00001111_{\bar{2}}$\\
  $5_{10}  = 00000101_{\bar{2}}$\\
  Efetuando a soma:\\[2mm]
  \begin{tabular}{ccccccccc}
    ~
    & ~
    & ~
    & ~
    & $\scriptstyle{1}$
    & $\scriptstyle{1}$
    & $\scriptstyle{1}$
    & $\scriptstyle{1}$
    & ~ \\
    ~   & $0$ & $0$ & $0$ & $0$ & $1$ & $1$ & $1$ & $1$ \\
    $+$ & $0$ & $0$ & $0$ & $0$ & $0$ & $1$ & $0$ & $1$ \\
    \hline
    ~   & $0$ & $0$ & $0$ & $1$ & $0$ & $1$ & $0$ & $0$ \\
  \end{tabular}
  \\
  Temos então o resultado: $00010100_{\bar{2}} = 20_{10}$.
\end{exmp}


\begin{exmp}[Subtração $15 - 35$ em complemento de dois com 8 bits]~\\
  A subtração $15 - 5$ é transformada na adição $15 + (-35)$;\\
  $15_{10}  = 00001111_{\bar{2}}$\\
  $-35_{10} = 11011101_{\bar{2}}$\\
  Efetuando a soma:\\[2mm]
  \begin{tabular}{ccccccccc}
    ~
    & ~
    & ~
    & $\scriptstyle{1}$
    & $\scriptstyle{1}$
    & $\scriptstyle{1}$
    & $\scriptstyle{1}$
    & $\scriptstyle{1}$
    & ~ \\
    ~   & $0$ & $0$ & $0$ & $0$ & $1$ & $1$ & $1$ & $1$ \\
    $+$ & $1$ & $1$ & $0$ & $1$ & $1$ & $1$ & $0$ & $1$ \\
    \hline
    ~   & $1$ & $1$ & $1$ & $0$ & $1$ & $1$ & $0$ & $0$ \\
  \end{tabular}
  \\
  Temos então o resultado: $11101100_{\bar{2}} = -20_{10}$.
\end{exmp}


% \subsubsection{Multiplicação}

% \subsubsection{Comparação}


\subsection{Exercícios}

\begin{exercise}
  Converta os números abaixo de decimal para complemento de dois com 5 bits.
  \begin{multicols}{3}
    \begin{enumerate}[label=({\alph*})]
    \item $12$
    \item $10$
    \item $-7$
    \item $-10$
    \item $3$
    \item $-9$
    \item $15$
    \item $-4$
    \item $-15$
    \item $-3$
    \item $11$
    \item $-1$
    \end{enumerate}
  \end{multicols}
\end{exercise}

\begin{exercise}
  Complete a tabela abaixo com o número em complemento de dois com 4
  bits equivalentes aos número decimais.
  \begin{center}
    \begin{tabular}{|c|c|}
      \hline
      \textbf{Decimal} & \textbf{Complemento de 2} \\
      \hline
      $7$ & ~ \\ \hline
      $6$ & ~ \\ \hline
      $5$ & ~ \\ \hline
      $4$ & ~ \\ \hline
      $3$ & ~ \\ \hline
      $2$ & ~ \\ \hline
      $1$ & ~ \\ \hline
      $0$ & ~ \\ \hline
    \end{tabular}
    ~
    \begin{tabular}{|c|c|}
      \hline
      \textbf{Decimal} & \textbf{Complemento de 2} \\
      \hline
      $-1$ & ~ \\ \hline
      $-2$ & ~ \\ \hline
      $-3$ & ~ \\ \hline
      $-4$ & ~ \\ \hline
      $-5$ & ~ \\ \hline
      $-6$ & ~ \\ \hline
      $-7$ & ~ \\ \hline
      $-8$ & ~ \\ \hline      
    \end{tabular}
  \end{center}
\end{exercise}

\begin{exercise}
  Converta os número em binário abaixo para complemento de dois em 12
  bits.
  \begin{multicols}{2}
    \begin{enumerate}[label=({\alph*})]
    \item $-1011101_2$
    \item $-11010111_2$
    \item $-1011101111_2$
    \item $-10101001100_2$
    \item $-1011110111_2$
    \item $-11011011_2$
    \end{enumerate}
  \end{multicols}
\end{exercise}

\begin{exercise}
  Converta os número em complemento de dois de 10 bit abaixo para
  binário.
  \begin{multicols}{3}
    \begin{enumerate}[label=({\alph*})]
    \item $1101110111_{\bar{2}}$
    \item $0101000001_{\bar{2}}$
    \item $0101101001_{\bar{2}}$
    \item $1101001110_{\bar{2}}$
    \item $1001101001_{\bar{2}}$
    \item $0111101000_{\bar{2}}$
    \item $1101111101_{\bar{2}}$
    \item $1010000000_{\bar{2}}$
    \item $0010101101_{\bar{2}}$
    \item $0110001110_{\bar{2}}$
    \item $1011001010_{\bar{2}}$
    \item $0010100000_{\bar{2}}$
    \end{enumerate}
  \end{multicols}
\end{exercise}

\begin{exercise}
  Converta os número em complemento de dois de 10 bit abaixo para
  decimal.
  \begin{multicols}{3}
    \begin{enumerate}[label=({\alph*})]
    \item $0010100101_{\bar{2}}$
    \item $0010101100_{\bar{2}}$
    \item $1110110101_{\bar{2}}$
    \item $0000001011_{\bar{2}}$
    \item $1000010011_{\bar{2}}$
    \item $1100111010_{\bar{2}}$
    \item $1110101010_{\bar{2}}$
    \item $1000110111_{\bar{2}}$
    \item $1000110011_{\bar{2}}$
    \item $1011111000_{\bar{2}}$
    \item $0111101110_{\bar{2}}$
    \item $0101010011_{\bar{2}}$
    \end{enumerate}
  \end{multicols}
\end{exercise}

\begin{exercise}
  Efetue as operações aritméticas indicadas abaixo em complemento de
  dois de 8 bits.
  \begin{multicols}{2}
    \begin{enumerate}[label=({\alph*})]
    \item $101111_2 + 1000_2$
    \item $1101001_2 - 11100_2$
    \item $001_2 - 101111_2$
    \item $110_2 + 100_2$
    \item $100_2 - 11_2$
    \item $10000_2 + 11101_2$
    \item $111111_2 + 1111111_2$
    \item $1001_2 - 10000_2$
    \item $110101_2 - 101000_2$
    \item $101101_2 + 101101_2$
    \end{enumerate}
  \end{multicols}
\end{exercise}

\begin{exercise}
  Efetue as operações aritméticas indicadas abaixo em complemento de
  dois de 8 bits.
  \begin{multicols}{2}
    \begin{enumerate}[label=({\alph*})]
      \item $-103_{10} - 118_{10}$
      \item $-98_{10} + 25_{10}$
      \item $36_{10} - 124_{10}$
      \item $104_{10} - 82_{10}$
      \item $-26_{10} - (-63_{10})$
      \item $-2_{10} + 78_{10}$
      \item $83_{10} - 29_{10}$
      \item $41_{10} + 110_{10}$
      \item $118_{10} - 113_{10}$
      \item $-42_{10} - 12_{10}$
    \end{enumerate}
  \end{multicols}
\end{exercise}


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../Intro_Log_Mat"
%%% End: 
