\section{Conversão entre Bases Numéricas}

É possível converter numeros de uma base de numeração $b_1$ para outra
base $b_2$ diretamente, entretanto essa conversão pode ser bastante
difícil se a pessoa realizando a conversão não estiver acostumada a
realizar operações aritiméticas diretamente nas bases $b_1$ e $b_2$.
Como a maioria das pessoas está mais acostumada a lidar com a base
$10$, é mais simples converter da base $b_1$ para a base $10$ e da
base 10 para a base $b_2$. 
%
A exceção a esta regra é quando as bases envolvidas são congruentes,
ou seja, quando ambas são potências de uma base comum, como é o caso
da conversão de base $8$ para a base $16$, por exemplo, pois ambas são
potências de $2$ (i.e., $8 = 2^3$ e $16 = 2^4$).

Um ponto importante a se ter em mente é a distinção entre a
representação (o numeral) e o número. Não confunda a representação com o
número. Por exemplo, cada um dos exemplos abaixo é uma representação
diferente para o mesmo número:
\begin{multicols}{3}
  \begin{itemize}
  \item $11111111_2$
  \item $100110_3$
  \item $3333_4$
  \item $2010_5$
  \item $377_8$
  \item $255_{10}$
  \item $193_{12}$
  \item $FF_{16}$
  \item $CF_{20}$
  \end{itemize}
\end{multicols}


\subsection{Conversão de Base $b$ para Base 10}

De modo geral, para um número expresso na base $b$ por um numeral de $n$
dígitos, i.e., para um númeral na forma $a_n\!\ldots{}a_2a_1a_0$ na base
$b$, o valor do número na base 10 pode ser calculado pela fórmula
abaixo:

\begin{equation}
  \label{eq:conv10}
  \left[ a_n\!\ldots{}a_2a_1a_0 \right]_b 
  = \left[ \sum_{i = 0}^n a_i\cdot b^i \right]_{10}
\end{equation}

Ou seja, cada dígito $a_i$ é multiplicado pela base $b$ elevada à
posição $i$ do dígito. As posições são atribuídas da direita para a
esquerda, iniciando em zero (0) para o dígito mais à direita. Expandindo
o lado direito da equação~\eqref{eq:conv10} obtemos a equação na forma abaixo:

\begin{align}
  \left[ a_n\!\ldots{}a_2a_1a_0 \right]_b
  &= \left[ \sum_{i = 0}^n a_i\cdot b^i \right]_{10} \nonumber\\ 
  &= \left[ a_n\cdot b^n + \ldots + a_2\cdot b^2 + a_1\cdot b^1 + a_0\cdot b^0 \right]_{10} 
  \label{eq:conv10x}
\end{align}

Abaixo temos alguns exemplos do uso da equação~\eqref{eq:conv10x}.\vspace{1em}

\begin{exmp}[Conversão de $241_5$ para base 10]
  \vspace{-2\baselineskip}
  \begin{align*}
    241_5
    &= 2\cdot 5^2 + 4\cdot 5^1 + 1\cdot 5^0 \\
    &= 2\cdot 25 + 4\cdot 5 + 1\cdot 1 \\
    &= 50 + 20 + 1 \\
    &= 71_{10}
  \end{align*}
\end{exmp}

\begin{exmp}[Conversão de $193_{12}$ para base 10]
  \vspace{-2\baselineskip}
  \begin{align*}
    193_{12}
    &= 1\cdot 12^2 + 9\cdot 12^1 + 3\cdot 12^0 \\
    &= 1\cdot 144 + 9\cdot 12 + 3\cdot 1 \\
    &= 144 + 108 + 3 \\
    &= 255_{10}
  \end{align*}
\end{exmp}

\begin{exmp}[Conversão de $23333_4$ para base 10]
  \vspace{-2\baselineskip}
  \begin{align*}
    23333_4
    &= 2\cdot 4^4 + 3\cdot 4^3 + 3\cdot 4^2 + 3\cdot 4^1 + 3\cdot 4^0 \\
    &= 2\cdot 256 + 3\cdot 64 + 3\cdot 16 + 3\cdot 4 + 3\cdot 1\\
    &= 512 + 192 + 48 + 12 + 3 \\
    &= 767
  \end{align*}
\end{exmp}

\begin{exmp}[Conversão de $1337_8$ para base 10]
  \vspace{-2\baselineskip}
  \begin{align*}
    1337_8
    &= 1\cdot 8^3 + 3\cdot 8^2 + 3\cdot 8^1 + 7\cdot 8^0 \\
    &= 1\cdot 512 + 3\cdot 64 + 3\cdot 8 + 7\cdot 1 \\
    &= 512 + 192 + 24 + 7 \\
    &= 735_{10}
  \end{align*}
\end{exmp}


\subsection{Conversão de Base 10 para Base $b$}

Como seria de se esperar, o processo de conversão de um numeral na base
10 para uma base $b$ qualquer, segue a mecânica inversa à da conversão
da base $b$ para a base 10, ou seja, ao invés de efetuarmos uma série de
multiplicações pelas diversas potências da base, fazemos uma série de
divisões pela base.

O processo se baseia no fato de que, quando fazemos a divisão inteira de
um número $a$ por um número $b$, obtendo um quociente $q$ e um resto
$r$, podemos escrever a seguinte igualdade:

\begin{equation}
  \label{eq:intdiv}
  a = b\cdot q + r
\end{equation}

A equação~\eqref{eq:intdiv} nos permite visualizar um algoritmo para
converter número em base 10 para números na base $b$ através de divisões
sucessivas. 

Assim, se tomarmos um número $a$ na base 10 e o dividirmos pela base
$b$, obteremos um quociente $q_0$ que representa a quantidade de vezes
que o número $a$ ``repete'' a base $b$ e um resto $r_0$ que representa
a quantidade de ``unidades'' de $a$ que sobraram ``dentro'' de uma
repetição de $b$.

Se repetirmos o processo utilizando $q_0$ como dividendo, então
obteremos um novo quociente $q_1$ que representará quantas vezes o
número $q_0$ repete a base $b$. Como cada unidade de $q_0$ já
representava grupos de $b$ elementos, cada unidade de $q_1$ representará
grupos de $b^2$ elementos.

\begin{figure}
  \centering
  \begin{subfigure}{0.475\textwidth}
    \centering
    \includegraphics[width=0.98\textwidth]{pics/base-conv-grps10-crop.pdf}
    \caption{Elementos agrupados em base 10.}
    \label{fig:base-conv-grps10}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.475\textwidth}
    \centering
    \includegraphics[width=0.98\textwidth]{pics/base-conv-grps8-crop.pdf}
    \caption{Elementos agrupados em base 8.}
    \label{fig:base-conv-grps8}
  \end{subfigure}
  \caption[Coleção de 247 elementos em grupos de 10 e 8.]{Coleção de
    247 elementos organizados em grupos (bases) de 10 e de 8
    elementos.}
  \label{fig:base-conv-grps}
\end{figure}

Cada divisão sucessiva aumenta a base em uma potência, formando grupos
cada vez maiores, e em cada divisão sucessiva, o resto indica a
quantidade de elementos ou grupos que sobraram do agrupamento. A
Figura~\ref{fig:base-conv-grps} ilustra este processo de reagrupamento
das unidades que compõem o número, mostrando o mesmo número de unidade
arranjado em grupos (bases) de 10 e de 8 elementos.

Se repetirmos o processo de divisões sucessivas pela base, até atigirmos
uma divisão em que o quociente seja zero (0), os restos destas diviões
nos indicarão quantos grupos de cada potência sucessiva da base podemos
formar. Os exemplos abaixo ilustram este processo.

\begin{exmp}[Conversão de $247_{10}$ para base 8]
  \vspace{-2\baselineskip}
  \begin{center}
    \begin{tabular}{rccc}
      \textbf{\#} & \textbf{base} & \textbf{quociente} & \textbf{resto}
      \\
      247 & 8 & 30 & 7 
      \\
      30 & 8 & 3 & 6
      \\
      3 & 8 & 0 & 3
    \end{tabular}
  \end{center}
  Tomando os restos sucessivos, da última divisão para a primeira temos:
  $247_{10} = 376_8$.
\end{exmp}

\begin{exmp}[Conversão de $255_{10}$ para base 12]
  \vspace{-2\baselineskip}
  \begin{center}
    \begin{tabular}{rccc}
      \textbf{\#} & \textbf{base} & \textbf{quociente} & \textbf{resto}
      \\
      255 & 12 & 21 & 3
      \\
      21 & 12 & 1 & 9
      \\
      1 & 12 & 0 & 1
    \end{tabular}
  \end{center}
  $\therefore 255_{10} = 193_{12}$
\end{exmp}

\begin{exmp}[Conversão de $777_{10}$ para base 5]~
  \vspace{-2\baselineskip}
  \begin{center}
    \begin{tabular}{rccc}
      \textbf{\#} & \textbf{base} & \textbf{quociente} & \textbf{resto}
      \\
      777 & 5 & 155 & 2
      \\
      155 & 5 & 31 & 0
      \\
      31 & 5 & 6 & 1
      \\
      6 & 5 & 1 & 1
      \\
      1 & 5 & 0 & 1
    \end{tabular}
  \end{center}
  $\therefore 777_{10} = 11102_5$
\end{exmp}

\begin{exmp}[Conversão de $1775_{10}$ para base 16]~
  \vspace{-2\baselineskip}
  \begin{center}
    \begin{tabular}{rccc}
      \textbf{\#} & \textbf{base} & \textbf{quociente} & \textbf{resto}
      \\
      1775 & 16 & 110 & $15 \to F$
      \\
      110 & 16 & 6 & $14 \to E$
      \\
      6 & 16 & 0 & 6
    \end{tabular}
  \end{center}
  $\therefore 1775_{10} = 6EF_{16}$
\end{exmp}


\subsection{Conversão entre Bases Congruentes}

Neste texto, usaremos o termo \emph{bases congruentes} para designar
bases, $b_1$ e $b_2$ sempre que a base $b_2$ for uma potência da base
$b_1$, por exemplo: as bases 2 e 4 ($2^2$); 2 e 8 ($2^3$); 3 e 9
($3^2$); 4 e 16 ($4^2$); e 5 e 25 ($5^2$) são todas congruentes duas a
duas.

Uma característica interessante das bases congruentes é que sempre é possível
converter um dígito da base mais alta em um número inteiro de dígitos da base
mais baixa. Por exemplo:
\begin{itemize}

\item cada dígito em um numeral em base 8 corresponde a exatamente três
  digitos em um numeral em base 2;

\item cada dígito em um numeral em base 16 corresponde a exatamente dois
  dígitos em um numeral em base 4;

\item cada dígito em um numeral em base 16 corresponde a exatamente
  quatro dígitos em um numeral em base 2;

\item cada dígito em um numeral em base 25 correspondem a exatamente
  dois dígitos em um numeral em base 5;

\end{itemize}

De modo geral, se $b_2 = b_1^n$, então cada dígito da base $b_2$
corresponderá a exatamente $n$ dígitos na base $b_1$. Desta forma o
processo de conversão da base $b_2$ para a base $b_1$ se torna bastante
simples, basta converter diretamente cada dígito do numeral na base
$b_2$ para $n$ dígitos no numeral na base $b_1$.

\begin{exmp}[Conversão de base congruente maior para menor]
  \vspace{-2.5\baselineskip}
  \begin{multicols}{2}
    \begin{itemize}
    \item $777_8 = {\overbrace{111}^7\overbrace{111}^7\overbrace{111}^7}_2$
    \item $753_8 = {\overbrace{111}^7\overbrace{101}^5\overbrace{011}^3}_2$
    \item $46_8 = {\overbrace{100}^4\overbrace{110}^6}_2$
    \item $\mathrm{B}4\mathrm{D}_{16} = {\overbrace{1011}^\mathrm{B}\overbrace{0100}^4\overbrace{1101}^\mathrm{D}}_4$
    \item $8567_9 = {\overbrace{22}^8\overbrace{12}^5\overbrace{20}^6\overbrace{21}^7}_3$
    \item $\mathrm{C}4\mathrm{F}3_{16} = {\overbrace{30}^\mathrm{C}\overbrace{10}^4\overbrace{33}^\mathrm{F}\overbrace{03}^3}_4$
    \end{itemize}
  \end{multicols}
\end{exmp}

\begin{table}
  \centering
  \caption{Conversões entre as bases congruentes 2--8, 3--9, 4--16.}
  \label{tab:conv-congr}
  \begin{subtable}[t]{0.25\textwidth}
    \centering
    \caption{Base~8 vs. Base~2}
    \begin{tabular}{cc}
      \textbf{B.8} & \textbf{B.2} \\
      0 & 000 \\
      1 & 001 \\
      2 & 010 \\
      3 & 011 \\
      4 & 100 \\
      5 & 101 \\
      6 & 110 \\
      7 & 111
    \end{tabular}
  \end{subtable}
  ~
  \begin{subtable}[t]{0.25\textwidth}
    \centering
    \caption{Base~9 vs. Base~3}
    \begin{tabular}{cc}
      \textbf{B.9} & \textbf{B.3} \\
      0 & 00 \\
      1 & 01 \\
      2 & 02 \\
      3 & 10 \\
      4 & 11 \\
      5 & 12 \\
      6 & 20 \\
      7 & 21 \\
      8 & 22
    \end{tabular}
  \end{subtable}
  ~
  \begin{subtable}[t]{0.4\textwidth}
    \centering
    \caption{Base~16 vs. Base~4}
    \begin{tabular}{cc|cc}
      \textbf{B.16} & \textbf{B.4} & \textbf{B.16} & \textbf{B.4} \\
      0 & 00 & 8 & 20 \\
      1 & 01 & 9 & 21 \\
      2 & 02 & A & 22 \\
      3 & 03 & B & 23 \\
      4 & 10 & C & 30 \\
      5 & 11 & D & 31 \\
      6 & 12 & E & 32 \\
      7 & 13 & F & 33
    \end{tabular}
  \end{subtable}
\end{table}

Para efeito de demonstração, a Tabela~\ref{tab:conv-congr} mostra
conversões entre dígitos de algumas bases congruentes. Tabelas
semelhantes podem ser construídas entre outras bases congruentes
como 2 e 16, ou 5 e 25, por exemplo.

Evidentemente a operação inversa, ou seja, dados dois númerais nas
bases $b_1$ e $b_2$, onde $b_2 = b_1^n$, é possível converter o
numeral da base $b_1$ ``diretamente'' para a base $b_2$, formando-se
grupos de $n$ dígitos (da direita para a esquerda) do numeral na base
$b_1$ e convertando cada grupo para o dígito correspondente na base
$b_2$.\vspace{1em}

\begin{exmp}[Conversão de base congruente menor para maior]
  \vspace{-2.5\baselineskip}
  \begin{multicols}{2}
    \begin{itemize}
    \item ${10 110 011 101}_2 = {\overbrace{2}^{10}\overbrace{6}^{110}\overbrace{3}^{011}\overbrace{5}^{101}}_8$
    \item ${0100 1111 1110}_2 = {\overbrace{4}^{0100}\overbrace{\mathrm{F}}^{1111}\overbrace{\mathrm{E}}^{1110}}_{16}$
    \item ${13 31 22 11}_4 = {\overbrace{7}^{13}\overbrace{\mathrm{D}}^{31}\overbrace{\mathrm{A}}^{22}\overbrace{5}^{11}}_{16}$
    \item ${2 12 21 11}_3 = {\overbrace{2}^{2}\overbrace{5}^{12}\overbrace{7}^{21}\overbrace{4}^{11}}_9$
    \item ${11 110}_2 = {\overbrace{3}^{11}\overbrace{6}^{110}}_8$
    \item ${12 212 101}_3 = {\overbrace{\mathrm{E}}^{12}\overbrace{\mathrm{W}}^{212}\overbrace{\mathrm{J}}^{101}}_{27}$
    \end{itemize}
  \end{multicols}
  Note que a base 27, usada acima, utiliza o dígito $0$ e as letras de A
  até Z, sendo A = 1, B = 2, C = 3, \ldots, Z = 26.
\end{exmp}


\subsection{Exercícios}

\begin{exercise}
  Converta os numerais em binário dados abaixo para a base 10.
  \begin{multicols}{3}
    \begin{enumerate}[label=({\alph*})]
      \item $1101_2$
      \item $1010_2$
      \item $10101_2$
      \item $11010_2$
      \item $110011_2$
      \item $101010_2$
      \item $1111011_2$
      \item $1101111_2$
      \item $1001101_2$
      \item $10011100_2$
      \item $01001010_2$
      \item $11010110_2$
    \end{enumerate}  
  \end{multicols}
\end{exercise}

\begin{exercise}
  Converta os numerais em quaternário dados abaixo para a base 10.
  \begin{multicols}{3}
    \begin{enumerate}[label=({\alph*})]
    \item $32_4$
    \item $13_4$
    \item $123_4$
    \item $232_4$
    \item $1332_4$
    \item $3113_4$
    \end{enumerate}
  \end{multicols}
\end{exercise}

\begin{exercise}
  Converta os numerais em quinário dados abaixo para a base 10.
  \begin{multicols}{3}
    \begin{enumerate}[label=({\alph*})]
    \item $32_5$
    \item $14_5$
    \item $143_5$
    \item $234_5$
    \item $4332_5$
    \item $3413_5$
    \end{enumerate}
  \end{multicols}
\end{exercise}

\begin{exercise}
  Converta os numerais em octal dados abaixo para a base 10.
  \begin{multicols}{3}
    \begin{enumerate}[label=({\alph*})]
    \item $32_8$
    \item $73_8$
    \item $543_8$
    \item $274_8$
    \item $1337_8$
    \item $3456_8$
    \end{enumerate}
  \end{multicols}
\end{exercise}

\begin{exercise}
  Converta os numerais em duodecimal dados abaixo para a base 10.
  \begin{multicols}{3}
    \begin{enumerate}[label=({\alph*})]
    \item $\mathrm{32}_{12}$
    \item $\mathrm{A3}_{12}$
    \item $\mathrm{B43}_{12}$
    \item $\mathrm{2AB}_{12}$
    \item $\mathrm{1BB7}_{12}$
    \item $\mathrm{357B}_{12}$
    \end{enumerate}
  \end{multicols}
\end{exercise}

\begin{exercise}
  Converta os numerais em hexadecimal dados abaixo para a base 10.
  \begin{multicols}{3}
    \begin{enumerate}[label=({\alph*})]
    \item $32_{16}$
    \item $\mathrm{AE}_{16}$
    \item $\mathrm{BA3}_{16}$
    \item $\mathrm{2AB}_{16}$
    \item $\mathrm{1BBF}_{16}$
    \item $\mathrm{3D7B}_{16}$
    \end{enumerate}
  \end{multicols}
\end{exercise}

\begin{exercise}
  Converta os numerais dados abaixo de suas respectivas bases para a
  base 10.
  \begin{multicols}{3}
    \begin{enumerate}[label=({\alph*})]
    \item $101010_2$
    \item $101010_3$
    \item $1021_4$
    \item $1025_6$
    \item $2165_8$
    \item $\mathrm{1FA2}_{16}$
    \item $\mathrm{E1A}_{16}$
    \item $707_8$
    \item $100110000_2$
    \item $11011_5$
    \item $\mathrm{1BA2}_{12}$
    \item $\mathrm{C1A}_{20}$
    \end{enumerate}
  \end{multicols}
\end{exercise}

\begin{exercise}
  Converta os numerais abaixo da base 10 para a binário, octal e hexadecimal.
  \begin{multicols}{3}
    \begin{enumerate}[label=({\alph*})]
    \item $10$
    \item $31$
    \item $100$
    \item $120$
    \item $127$
    \item $254$
    \item $1025$
    \item $4095$
    \item $65535$
    \end{enumerate}
  \end{multicols}
\end{exercise}

\begin{exercise}
  Converta os numerais abaixo da base 10 para as bases 5 e 7.
  \begin{multicols}{3}
    \begin{enumerate}[label=({\alph*})]
    \item $15$
    \item $45$
    \item $153$
    \item $211$
    \item $272$
    \item $549$
    \item $1023$
    \item $4949$
    \item $65535$
    \end{enumerate}
  \end{multicols}
\end{exercise}

\begin{exercise}
  Converta os numerais abaixo da base 10 para hexadecimal.
  \begin{multicols}{3}
    \begin{enumerate}[label=({\alph*})]
    \item $365$
    \item $5267$
    \item $4612$
    \item $170742$
    \item $161051$
    \item $465$
    \item $3420$
    \item $6341$
    \item $32575$
    \end{enumerate}
  \end{multicols}
\end{exercise}

\begin{exercise}
  Converta os números abaixo de binário para octal.
  \begin{multicols}{3}
    \begin{enumerate}[label=({\alph*})]
    \item $110101010_2$
    \item $100100010_2$
    \item $10100000_2$
    \item $101001_2$
    \item $10100101000_2$
    \item $111101001111_2$
    \item $1100100110_2$
    \item $10110100100_2$
    \item $111001111110_2$
    \end{enumerate}
  \end{multicols}
\end{exercise}

\begin{exercise}
  Converta os números abaixo de binário para hexadecimal.
  \begin{multicols}{3}
    \begin{enumerate}[label=({\alph*})]
    \item $1110100001010_2$
    \item $1010110010001001_2$
    \item $11100111111_2$
    \item $0010100101111_2$
    \item $00010101111_2$
    \item $1111100110100_2$
    \item $1111010111_2$
    \item $1011111010_2$
    \item $110010110_2$
    \end{enumerate}
  \end{multicols}
\end{exercise}

\begin{exercise}
  Converta os números abaixo de octal para hexadecimal.
  \begin{multicols}{3}
    \begin{enumerate}[label=({\alph*})]
      \item $50533_8$
      \item $44772_8$
      \item $7675_8$
      \item $761_8$
      \item $3530_8$
      \item $43160_8$
      \item $5571_8$
      \item $552_8$
      \item $174_8$
    \end{enumerate}
  \end{multicols}
\end{exercise}

\begin{exercise}
  Converta os números abaixo de hexadecimal para octal.
  \begin{multicols}{3}
    \begin{enumerate}[label=({\alph*})]
    \item $\mathrm{BB7}_{16}$
    \item $\mathrm{FE4}_{16}$
    \item $\mathrm{2C5}_{16}$
    \item $\mathrm{1B37E}_{16}$
    \item $\mathrm{062FC}_{16}$
    \item $\mathrm{5E68}_{16}$
    \item $\mathrm{A2}_{16}$
    \item $\mathrm{DA572}_{16}$
    \item $\mathrm{540F}_{16}$
    \end{enumerate}
  \end{multicols}
\end{exercise}


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../ilm-book"
%%% End: 
