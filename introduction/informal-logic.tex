\section{Lógica Informal}
\label{sec:informal-logic}

Como vimos na \autoref{sec:intro-history}, durante séculos o estudo da lógica
inspirou a idéia de que seus métodos poderiam ser aproveitados nos esforços para
entender e melhorar o pensamento, o raciocínio, e a argumentação na forma como
eles ocorrem no dia a dia: em debates e discussões públicas; e no direito, na
medicina, na filosofia, e em outras profissões. A \emph{lógica informal} é um
esforço de construir uma lógica apropriada para este propósito. Ela combina o
estudo da argumentação, de evidências, de provas, e de demonstrações, com uma
perspectiva que contribui para enfatizar sua utilidade na análise de argumentos
da vida real.

A lógica informal, como campo de estudo moderno, emergiu na segunda metade do
século XX, quando muitos filósofos e logicistas começaram a voltar sua atenção
para a análise, avaliação, e aperfeiçoamento de argumentos da vida real.
Entretanto, a lógica informal deve ser entendida como uma continuação de muitas
tentativas antigas de filósofos que propuseram métodos para entender e avaliar
argumentos reais.

Ao desenvolver uma explicação sobre argumentação, os logicistas estudaram a
inferência e muitos outros tópicos relevantes, dentre os quais temos:
explicações concorrentes sobre a natureza dos argumentos; critérios para
avaliação de argumentos; esquemas de argumentos; falácias; modelos de inferência
dedutivos, indutivos, condutores e abdutivos; abordagens retóricas e dialéticas
à argumentação; ônus e responsabilidade da prova; o estudo empírico da
argumentação; diagramação (ou ``mapeamento'') de argumentos; Predisposições
cognitivas; a história da análise de argumentos; o papel da emoção no argumento;
e as regras que regem a troca argumentativa em diferentes contextos de
comunicação.

De acordo com \textcite{Blair2015}, existem duas tarefas principais para a
lógica informal: (a) desenvolver formas de identificar e extrair argumentos do
discurso em linguagem natural; e (b) desenvolver métodos e diretrizes para
avaliar a força de convicção (ou irrefutabilidade) dos argumentos. Ambas as
tarefas assumem alguma definição do que conta como um argumento.


\subsection{Argumentos Informais}
\label{sec:informal-arguments}

A lógica informal entende o termo ``argumento'' como uma tentativa de oferecer
evidência em favor de algum ponto de vista. Neste sentido, argumentar é um ato
intencional, um ato de fala ou de comunicação, funcionando como uma tentativa de
resolver algum conflito ou discordância.

No sentido comprovativo, a lógica informal entende argumentos como coleções de
premissas e conclusões. As premissas fornecem as evidências que dão suporte às
conclusões. Essa ideia é apresentada no exemplo abaixo.

\begin{exmp}
  \label{exm:1}
  ``Uma superfície cinza parece vermelha se tivermos olhado para um
  azul-esverdeado; O papel plano parece liso se nós estivermos sentido uma lixa
  ou áspero se nós tivermos sentido uma placa de vidro; e água da torneira tem
  gosto doce se tivermos comido alcachofras. Alguma parte do que chamamos de
  vermelho ou suave ou doce deve, portanto, estar nos olhos, ou nas pontas dos
  dedos, ou na língua do observador, do apalpador, ou do provador.''
  \cite[p.~103]{Skinner1971}

  Podemos analisar este argumento da seguinte forma:

  \begin{center}
    \begin{tabular}{lm{12cm}}
      Premissa:  & Uma superfície cinza parece vermelha se tivermos olhado para
                   um azul-esverdeado.                                           \\
      Premissa:  & O papel plano parece liso se nós estivermos sentido uma lixa. \\
      Premissa:  & O papel plano parece áspero se nós tivermos sentido uma
                   placa de vidro.                                               \\
      Premissa:  & Água da torneira tem gosto doce se tivermos comido
                   alcachofras.                                                  \\
      Conclusão: & Alguma parte do que chamamos de vermelho ou suave ou doce
                   deve, portanto, estar nos olhos, ou nas pontas dos dedos,
                   ou na língua do observador, do apalpador, ou do provador.     \\
    \end{tabular}
  \end{center}
\end{exmp}

Os próximos exemplos mostram que identificar e extrair argumentos de seus
contextos nativos pode apresentar alguns desafios.

\begin{exmp}
  \label{exm:2}
  ``O advogado do réu afirmou que a sentença proferida pelo juiz não era
  proporcional ao crime porque tinha uma duração sem precedentes.''

  O argumento pode ser resumido como:

  \begin{center}
    \begin{tabular}{lm{12cm}}
      Premissa:  & A sentença (proferida pelo juiz) tinha uma duração sem
                 precedentes.                                \\
      Conclusão: & A sentença não era proporcional ao crime. \\
    \end{tabular}
  \end{center}
  Note que neste caso, a premissa aparece no texto após a conclusão. A palavra
  ``porque'' funciona como um indicador de premissa, neste caso.
\end{exmp}

\begin{exmp}
  \label{exm:3}
  ``Um coordenador da Sociedade Humanista apoiou uma sentença de prisão,
  alegando que as penas menores normalmente associadas a condenações por
  contravenção não são um elemento suficientemente desencorajante neste caso.''

  Este argumento pode ser resumido como:

  \begin{center}
    \begin{tabular}{lm{12cm}}
      Premissa:  & as penas menores normalmente associadas a condenações por
                   contravenção não são um elemento suficientemente
                   desencorajante neste caso.            \\
      Conclusão: & Uma sentença de prisão é necessária.  \\
    \end{tabular}
  \end{center}

  Novamente temos a premissa ocorrendo após a conclusão no texto. O indicador de
  premissa, neste exemplo, foi a expressão ``alegando que'', que serviu para
  indicar uma explicação ou justificativa da conclusão.
\end{exmp}

\begin{exmp}
  \label{exm:4}
  ``As pequenas empresas são as que mais contribuem para a economia. Elas
  representam mais de dois terços das oportunidades de emprego.''

  Resumidamente:

  \begin{center}
    \begin{tabular}{lm{12cm}}
      Premissa:  & As pequenas empresa representam mais de dois terços das
                   oportunidades de emprego. \\
      Conclusão: & As pequenas empresas são as que mais contribuem para a
                   economia.                 \\
    \end{tabular}
  \end{center}
\end{exmp}

Muitas vezes, os componentes de um argumento são intercalados com digressões e
observações repetitivas que não são relevantes para o argumento ou sua
avaliação. No processo de identificação e extração de um argumento de seu
contexto, isso pode significar que devemos reconhecer o que é implícito, mas
relevante para o argumento, ao mesmo tempo que descartamos o que é explícito,
mas redundante ou irrelevante.

Nos Exemplos~\ref{exm:2} e \ref{exm:3}, os argumentos dependem de alegações
implícitas que devem ser consideradas na avaliação dos argumentos. No processo
de identificação dos componentes de um argumento, tais alegações são
identificadas como premissas (ou conclusões) \emph{implícitas}. Em nossos dois
exemplos, isso significa que podemos identificar os componentes dos argumentos
como:

\begin{exmp}[Continuação do \autoref{exm:2}]\label{exm:5}
  \vspace*{-4ex}
  \begin{center}
    \begin{tabular}{lm{11cm}}
      Premissa:           & A sentença (proferida pelo juiz) tinha uma duração sem
                            precedentes.                              \\
      Premissa Implícita: & As sentenças por crimes não devem ter uma
                            duração sem precedentes.                  \\
      Conclusão:          & A sentença não era proporcional ao crime. \\
    \end{tabular}
  \end{center}
\end{exmp}

\begin{exmp}[Continuação do \autoref{exm:3}]\label{exm:6}
  \vspace*{-4ex}
  \begin{center}
    \begin{tabular}{lm{11cm}}
      Premissa:           & As penas menores normalmente associadas a condenações por
                            contravenção não são um elemento suficientemente
                            desencorajador neste caso.                            \\
      Premissa Implícita: & As sanções penais devem ter um efeito desencorajador. \\
      Conclusão:          & Uma sentença de prisão é necessária.                  \\
    \end{tabular}
  \end{center}
\end{exmp}

É importante identificar premissas e conclusões implícitas porque são elementos
que precisam ser analisados e avaliados em qualquer tentativa de avaliar os
argumentos que os contêm. No \autoref{exm:3}, por exemplo, a questão de saber se
as punições devem ser atribuídas considerando seus efeitos desencorajadores é
uma questão fundamental que deve ser considerada para decidir se o argumento
fornece evidência convincente para sua conclusão.


\subsection{Preparação de Argumentos}
\label{sec:dressing-arguments}

Quando analisamos argumentos da vida real, nossa primeira tarefa é a
identificação de suas partes componentes. Esta tarefa às vezes é chamado de
``preparar'' o argumento. Ou seja, tomar os argumentos como eles aparecem em
seus contextos de vida real, e identificar e isolar seus componentes chaves de
uma maneira que prepara o caminho para a avaliação dos argumentos.


\subsubsection{Premissas e Conclusões}
\label{sec:premises-conclusions}

A tarefa mais elementar na preparação de um argumento é identificar suas
premissas e conclusões. Tanto a lógica formal quanto a lógica informal entendem
premissas e conclusões como os componentes centrais de um argumento. Em casos
simples, eles são claramente e explicitamente indicados e facilmente
identificados.

Preparar argumentos é um aspecto chave da análise de argumentos que ocorrem no
discurso cotidiano porque tais argumentos são frequentemente obscuros e/ou
abertos à interpretação. Ao isolar suas premissas e conclusões, isso significa
que podemos ter que realizar uma ou mais das tarefas abaixo.

\begin{itemize}
\item Descartar digressões irrelevantes e comentários extemporâneos (``ruído'');
\item Eliminar questões retóricas e outros dispositivos estilísticos que
  obscurecem seu significado;
\item Escolher entre formas alternativas de expressá-los;
\item Resolver as questões levantadas por declarações e declarações incompletas,
  vagas ou ambíguas;
\item Identificar elementos visuais, auditivos, olfativos e outros tipos de
  premissas que fornecem evidência para uma conclusão.
\end{itemize}


\subsubsection{Premissas e Conclusões Implícitas}
\label{sec:implicit-premises-conclusions}

A possibilidade de premissas ou conclusões implícitas já era reconhecida desde a
antiguidade. Alguns exemplos de premissas implícitas já foram apresentados
anteriormente. As premissas implícitas identificam a ligação entre a premissa
explícita e a conclusão. Ao preparar o argumento essas premissas precisam ser
explicitadas porque precisam ser aferidas em qualquer tentativa de avaliar o
argumento.

%% TODO: Procurar um ou dois bons exemplos de argumentos que usem premissas
%% implícitas.

A tarefa de identificar premissas ou conclusões implícitas levanta questões
teóricas porque há muitas circunstâncias em que diferentes premissas ou
conclusões implícitas podem ser atribuídas a um argumento. Um princípio comum
usado em tais circunstâncias é o ``Princípio da Caridade'', que nos leva a
buscar uma premissa implícita que torna o argumento o mais forte possível. Uma
abordagem alternativa atribui a um argumento o ``mínimo lógico'' -- isto é, a
premissa mais fraca necessária para ligar as premissas do argumento à sua
conclusão.


\subsubsection{Tipos de Diálogos}
\label{sec:dialogue-kinds}

Um componente externo dos argumentos é o tipo de diálogo no qual eles estão
inseridos. Em um diálogo de investigação, por exemplo, os argumentos são usados
como ferramentas na tentativa de estabelecer o que é verdadeiro. Entendidos
dentro deste contexto, os argumentos devem aderir a padrões rígidos que
determinam o que conta como evidência ou como contra-evidência para algum ponto
de vista. Em um diálogo de negociação, os argumentos funcionam de maneira
diferente. O objetivo aqui é estabelecer um acordo entre duas partes que têm
interesses conflitantes e pontos de vista diferentes, possivelmente
inconciliáveis. Pode-se resumir essas diferenças dizendo que as expectativas,
normas e procedimentos para argumentar dependem do tipo de diálogo no qual um
argumento é proposto.

Podemos entender um diálogo como uma troca composta de uma fase de abertura, uma
fase de argumentação e uma fase de encerramento. Na fase de abertura, os
argumentadores no diálogo concordam em participar. As regras para o diálogo
definem que tipos de movimentos são permitidos, que tipos de perguntas e
respostas são permitidas, e que normas devem ser aderidas. Segundo
\textcite{Walton2007}, há sete tipos de diálogos que podem ser resumidos como
mostra a \autoref{tab:dialogue-types}.

\begin{table}[h]
  \centering
  \rowcolors{2}{gray!20}{white}
  \begin{tabular}{lm{3cm}m{4cm}m{4cm}}
    \textbf{Tipo} & \textbf{Situação}         & \textbf{Objetivo do Argumentador} & \textbf{Objetivo do Dialogo} \\
    \hline
    Persuasão     & Conflito de opiniões      & Persuadir a outra parte           & Resolver problemas           \\
    Investigação  & Precisa haver prova       & Verificar evidências              & Provar uma hipótese          \\
    Descoberta    & Necessidade de explicação & Encontrar uma hipótese            & Sustentar uma hipótese       \\
    Negociação    & Conflito de interesses    & Assegurar seus interesses         & Resolver a questão           \\
    Informação    & Necessidade de informação & Adquirir informação               & Troca de informações         \\
    Deliberação   & Escolha prática           & Ajustar metas e ações             & Decidir o que fazer          \\
    Erístico      & Conflito pessoal          & Atacar o oponente                 & Revelar conflito intenso     \\
  \end{tabular}
  \caption{Tipos de diálogos, segundo a classificação de \textcite{Walton2007}.}
  \label{tab:dialogue-types}
\end{table}

Dentro destas categorias gerais, tipos mais específicos de diálogo podem ser
governados por regras estritas. Uma das subespécies do diálogo negocial é, por
exemplo, a negociação coletiva, que proíbe legalmente a "negociação de má-fé" -
isto é, a negociação que ocorre fora da mesa de negociação. Neste tipo de
negociação, o uso de ameaças (para atacar ou bloquear os funcionários) é uma
parte fundamental do processo. Em contraste nítido, as ameaças não são
aceitáveis na investigação crítica, onde são classificadas como exemplos da
falácia\footnote{Uma \emph{falácia} é o uso de raciocínio inválido ou
  defeituoso, ou ``movimentos errados'' na construção de um argumento. Algumas
  falácias são cometidas intencionalmente para manipular ou persuadir por
  engano, enquanto outros são cometidos involuntariamente devido à negligência
  ou ignorância.} \emph{ad baculum}.\footnote{\emph{Argumentum ad baculum},
  também conhecido como ``apelo à força'', é um argumento em que a força, a
  coerção ou a ameaça de força é dada como justificativa.}

A questão de saber se há outros tipos de diálogo que ainda precisam ser
reconhecidos permanece aberta, bem como a questão de saber se existem tipos de
argumentação que não podem ser categorizados em termos de qualquer das formas
padrão de diálogo (porque eles são um híbrido de diferentes tipos de diálogo, ou
alguma nova forma de diálogo que não esteja claramente definida). Reconhecer um
diálogo no qual um argumento está embutido é uma parte importante da preparação
do argumento quando isso impõe padrões de troca argumentativa aos quais os
argumentadores podem não ter aderido adequadamente.


\subsubsection{Da Preparação à Avaliação}
\label{sec:from-dressing-to-assessment}

Preparar argumentos é um precursor para a avaliação do argumento. A preparação
pode ser um empreendimento complexo que requer o descarte de aspectos
irrelevantes da comunicação, uma interpretação de uma situação em que as
premissas e as conclusões não são claramente demarcadas, a identificação de
premissas ou conclusões implícitas. Em muitos casos, requer o reconhecimento de
que está embutido em um determinado tipo de diálogo, faz parte de uma troca com
algum oponente argumentativo identificável ou é endereçado a um público
específico que um argumentador está tentando convencer. Desta forma, uma
preparação completa de um argumento requer uma descrição detalhada de suas
características internas e externas. É uma atividade que requer treino e
atenção. Não é, de modo algum, uma tarefa trivial.


\subsection{Avaliação de Argumentos}
\label{sec:asessing-arguments}

O objetivo maior da lógica informal é normativo. Sua meta é desenvolver uma
teoria de argumentação que possa ser usada para decidir quando os argumentos são
fortes ou fracos; bons ou maus; e plausíveis e implausíveis. Os logicistas
informais preparam argumentos para a avaliação. Em vista disso, muito da lógica
informal é uma tentativa de desenvolver padrões, critérios e processos para
julgar argumentos. Argumentos particulares podem ser avaliados em termos de
critérios gerais de boa argumentação; ou como um exemplo de uma falácia ou de um
esquema de argumentos.

Uma maneira de avaliar os argumentos no discurso cotidiano é traduzindo-os em
uma linguagem formal e avaliando-os adequadamente. Este é um método que os
logicistas informais usam algumas vezes, especialmente nas discussões sobre
inteligência artificial, abordagens baseadas em teoria dos jogos para o diálogo
e relatos formais de vários tipos de raciocínio. Métodos mais informais têm sido
enfatizados no ensino de argumentação e raciocínio. No uso da lógica informal no
discurso público (argumentar a favor ou contra, ou na análise de pontos de vista
particulares) e no estudo de argumentos que não são facilmente acomodados pelos
métodos formais.


\subsubsection{Critérios AV e ARS}
\label{sec:av-ars-criteria}

Na lógica clássica, um argumento é (dedutivamente) válido se sua conclusão
deriva de suas premissas, i.e., se é impossível que suas premissas sejam
verdadeiras e sua conclusão seja falsa. O objetivo final de argumentar é um
argumento ``sólido'': um argumento válido com premissas verdadeiras. Muitas
questões surgem quando se tenta aplicar esses critérios a argumentos informais
sem adaptá-los de várias maneiras, mas eles apontam de forma útil que a força de
um argumento é função de duas coisas: (i) a viabilidade de suas premissas, e
(ii) a força da inferência dessas premissas para sua conclusão.

Dentro da lógica informal, os critérios mais simples para julgar os argumentos
são um análogo informal da ``solidez''. Este critério exige que as premissas de
um argumento sejam aceitáveis e que sua conclusão resulte dessas premissas.
Podemos chamar esta última de validade ``informal'' e estes dois critérios são
os critérios ``AV'' (aceitabilidade e validade) para avaliar argumentos.

Muitos logicistas informais entendem a validade informal em termos de relevância
e suficiência, tornando os critérios para bons argumentos \emph{aceitabilidade,
  relevância e suficiência} (os critérios ``ARS''). As premissas de um argumento
contam como relevantes para sua conclusão quando elas fornecem algum suporte
para a conclusão e suficiente quando elas fornecem apoio suficiente para
estabelecer a conclusão como plausível.

A lógica informal favorece a aceitabilidade sobre a verdade como critério para
julgar premissas. Ambos os critérios AV e ARS avaliar premissas como aceitável
ou inaceitável em vez de verdadeiro ou falso. Isso é feito por várias razões.
Porque na vida real discussões tendem a ocorrer em contextos caracterizados pela
incerteza, o que tornam difícil fazer julgamentos de verdade ou falsidade.
Porque esses argumentos frequentemente giram em torno de julgamentos éticos e
estéticos que não são facilmente categorizados como verdadeiros ou falsos.
Porque há contextos (por exemplo, negociação) em que bons argumentos não
dependem de premissas que podem claramente ser categorizadas como verdadeiras ou
falsas. Porque existem outros contextos (em lidar com públicos específicos, por
exemplo) nos quais a verdade não pode, por si só, tornar uma premissa aceitável
para um argumento informal. E, mais geralmente, porque discussões filosóficas
sobre a ``verdade'' que levantaram muitas perguntas sobre o que realmente conta
como verdadeiro e falso.

Uma situação possível em que as premissas de um argumento são verdadeiras e sua
conclusão não é verdadeira, é chamada de \emph{contraexemplo} do argumento. Uma
outra forma de definir seria simplesmente dizendo que um argumento válido é um
que não possui contraexemplos.

Quando falamos de situações possíveis, o termo `possível' deve ser entendido em
um sentido bastante amplo. Para ser possível, uma situação não precisa ser algo
que possamos realizar; nem sequer tem que obedecer às leis da física. Ela só tem
de ser algo que podemos conceber coerentemente -- ou seja, sem auto-contradição.
Um contra-exemplo não precisa ser uma situação real, embora possa ser; basta que
a situação seja conceitualmente possível.

Uma descrição bem definida de um contraexemplo deve conter três elementos:

\begin{enumerate}
\item Afirmações de todas as premissas do argumento.
\item Uma negação da conclusão do argumento.
\item Uma explicação de como isso pode acontecer, isto é, como a conclusão pode
  ser falsa enquanto as premissas são todas verdadeiras.
\end{enumerate}

Pode haver muitos, ou até infinitos, contraexemplos para um determinado
argumento. Nós não precisamos encontrar todos eles. Basta achar um contraexemplo
para podermos afirmar que o argumento não é válido.

\begin{exmp}
  \vspace*{-4ex}
  \begin{quote}
    ``Eles disseram no rádio que hoje será um dia lindo. Então, hoje será um dia
    lindo.''
  \end{quote}

  \emph{Contraexemplo:} ``Eles disseram no rádio que hoje será um dia lindo. Mas
  eles estão errados. Uma frente fria está se movendo inesperadamente e trará
  chuva em vez de um dia lindo.''

  Todos os três elementos estão presentes. A primeira frase afirma a premissa. A
  segunda nega a conclusão. A terceiro explica como a conclusão pode ser falsa
  mesmo que a premissa seja verdadeira.
\end{exmp}

Observe novamente que o contraexemplo não precisa ser uma situação real. É
apenas uma história, um cenário, uma ficção.


\subsubsection{Dedutivismo em Linguagem Natural}
\label{sec:natural-lang-deductivism}

Na avaliação dos argumentos, uma das principais questões levantadas pela lógica
informal é a questão de como devemos entender a validade informal, que é um
componente chave de argumentos fortes. Embora os logicistas informais comumente
distinguam entre a validade dedutiva e vários tipos alternativos de validade,
alguns assumem uma visão alternativa, que é chamado de ``Dedutivismo da
Linguagem Natural'' (DLN). O DLN sustenta que todos os argumentos informais
podem ser melhor interpretados como tentativas de criar inferências
dedutivamente válidas, e devem ser analisados e avaliados de acordo.

De modo geral, uma conclusão dedutiva é tão certa quanto as premissas em que se
baseia. Este é um ponto importante em contextos informais, onde os argumentos se
baseiam em premissas que são razoáveis ou plausíveis ao invés de certas. Nesses
contextos, os argumentos dedutivos produzem conclusões que não são certas, mas
razoáveis ou plausíveis. Considere o exemplo abaixo.

\begin{exmp}
  \label{exm:7}
  ``A população do mundo crescerá de 7 para 10 bilhões nos próximos 30 anos, por
  isso, se quisermos fornecer comida suficiente para todos, precisamos de uma
  maneira de alimentar 3 bilhões de pessoas adicionais.''
\end{exmp}

Este é um argumento obviamente dedutivo, mas depende de uma premissa que não é
certa, mas razoável -- porque é apoiada por outros raciocínios que extrapolam as
tendências demográficas estabelecidas. O argumento é dedutivamente válido, mas
sua conclusão é razoável e não certa.

Ao lidar com argumentos que não são explicitamente dedutivos, a abordagem DLN
interpreta um argumento como um argumento dedutivo atribuindo premissas
implícitas que o tornam dedutivo. Por exemplo:

\begin{exmp}
  \label{exm:8}
  ``Paulo Freire disse, então deve ser verdade.''
\end{exmp}

O Exemplo~\ref{exm:8} é um padrão de raciocínio comum frequentemente criticado
que apela para a autoridade de um personalidade famosa para justificar a
conclusão. Se julgarmos apenas pela premissa explícita (\emph{Paulo Freire
  disse}), este não é um argumento dedutivamente válido pois a premissa pode ser
verdadeira e a conclusão falsa (para horror de muitos, \emph{Paulo Freire pode
  estar errado}). A DLN contorna essa questão atribuindo ao argumento uma
premissa implícita que o torna dedutivo. Assim o argumento pode ser resumido
como:

\begin{exmp}[Continuação do \autoref{exm:8}]\label{exm:9}
  \vspace*{-4ex}
  \begin{center}
    \begin{tabular}{lm{11cm}}
      Premissa:           & Paulo Freire disse isso.                           \\
      Premissa Implícita: & Se Paulo Freire disse isso, isso deve ser verdade. \\
      Conclusão:          & Isso deve ser verdade.                             \\
    \end{tabular}
  \end{center}
\end{exmp}

O Exemplo~\ref{exm:9} é uma reconstrução plausível do argumento apresentado no
Exemplo~\ref{exm:8}. Pois não podemos extrair a conclusão a partir da premissa
declarada sem aceitar a premissa implícita. Uma vez tornado explícito, o
argumento é claramente dedutivo. Isso não o torna um bom argumento e, em vez
disso, sugere que devemos avaliar sua força, não avaliando sua validade, mas
avaliando a plausibilidade de suas premissas. Como a premissa implícita é
implausível uma análise da DLN implica a conclusão (correta) de que o argumento
é fraco.

A DLN é uma abordagem de avaliação de argumentos que torna explícitas algumas
das alegações e dos pressupostos chaves de que os argumentos individuais
dependem. Uma vez que a DLN analisa todos os argumentos como instâncias de uma
forma de inferência bem compreendida, ela elimina a necessidade de distinguir
entre argumentos que são dedutivos, indutivos, condutores, abdutivos, etc. --
uma distinção que pode ser difícil de fazer ao lidar com argumentos informais.
Aristóteles foi uma figura chave que adota uma abordagem dedutivista.


\subsubsection{Teoria das Falácias}
\label{sec:fallacy-theory}

Na busca de maneiras diferenciadas de lidar com a lógica informal, a pesquisa
nesta área inicialmente se voltou para a teoria das falácias. Em seu tratamento
das falácias, reviveu uma tradição que pode ser atribuída a Aristóteles. Na
história da lógica e da filosofia, seu significado se reflete nos escritos de
filósofos como John Locke, Richard Whately e John Stuart Mill. Hoje, esta
tradição aparece nos manuais e nos sites web que tentam ensinar raciocínio
informal ensinando estudantes como detectar as falácias mais comuns.

As discussões teóricas sobre falácias nuca produziram uma taxonomia
universalmente aceita, mas há um conjunto comum de falácias que são
frequentemente usadas na análise de argumentos informais. Incluindo falácias
formais como afirmar o consequente e negar o antecedente; e falacias informais
como \emph{ad hominem} (``contra a pessoa''), declive escorregadio, \emph{ad
  bacculum} (``apelo à força''), \emph{ad misericordiam}, ``generalização
precipitada'' e ``dois erros''. Nos livros de lógica informal, os autores podem
elaborar sua própria nomenclatura para destacar as propriedades de casos
particulares de argumentos falaciosos.

Embora alguns problemas com o estudo das falácias tradicionais tenham diminuído
seu significado na teoria da argumentação, eles continuam a ser uma maneira
popular de analisar o raciocínio e o argumento cotidianos. Uma boa introdução ao
estudo das falácias é a obra clássica ``\citetitle{Schopenhauer2003}''
\cite{Schopenhauer2003}.


\subsubsection{Esquemas de Argumentos}
\label{sec:argument-schemes}

Os esquemas de argumento são padrões recorrentes de argumento. Uma vez
identificados, eles podem ser usados para avaliar uma instância de argumentação,
ou como receitas que nos dizem como construir argumentos fortes.

Uma abordagem padrão aos esquemas de argumentos combina um padrão particular de
argumento com um conjunto de ``questões críticas'' que levanta. Considere o
esquema ``Apelo à Autoridade'' (também chamado de ``Apelo à Opinião de
Perito''), que pode ser resumido como segue.

\begin{exmp}[Apelo à Autoridade]\label{exm:10}
  \vspace*{-4ex}
  \begin{itemize}
  \item[] $A$ é uma autoridade no domínio $D$.
  \item[] $A$ diz que $T$ é verdade.
  \item[] $T$ está dentro de $D$.
  \item[] (Portanto) $T$ é verdadeira.
  \end{itemize}

  \textit{Questões críticas:}
  \begin{enumerate}
  \item\label{item:1} Como é credível $A$?
  \item\label{item:2} $A$ é uma autoridade no domínio $D$?
  \item\label{item:3} O que $A$ afirma que implica $T$?
  \item\label{item:4} $A$ é alguém que pode ser confiável?
  \item\label{item:5} $T$ é consistente com o que outros especialistas afirmam?
  \item\label{item:6} A afirmação de $A$ sobre $T$ é baseada em evidências?
  \end{enumerate}
\end{exmp}

O comentário abaixo é um exemplo de uso do um apelo de autoridade.

\begin{exmp}
  \label{exm:11}
  ``Não devemos armazenar armas nucleares. Precisamos dar ouvidos à advertência
  de Einstein, após o bombardeio de Hiroshima, de que a proliferação de bombas
  atômicas levaria inevitavelmente `à destruição ainda mais terrível do que a
  atual destruição da vida'.''
\end{exmp}

Podemos resumir o argumento do Exemplo~\ref{exm:11} como uma instância do
argumento do esquema de Apelo à Autoridade, onde $A$ = ``Einstein'', $D$ =
``política'' (de armas nucleares), $T$ = ``a proliferação de armas nucleares vai
levar à destruição terrível''. Podemos então avaliar o argumento perguntando se
existem respostas satisfatórias às questões críticas do argumento. Neste caso,
as questões de particular importância são a questão~\ref{item:2} (``Einstein é
uma autoridade sobre a política de armas nucleares?'') e a questão~\ref{item:5}
(``A alegação de que a proliferação de armas nucleares vai levar a uma
destruição terrível é consistente com o que outros especialistas em política de
armas nucleares afirmam?''). Uma vez que não é claro que qualquer das duas
questões tem uma resposta positiva, o Exemplo~\ref{exm:11} é um argumento fraco.

Pode-se entender um esquema como uma espécie de regra de inferência, algumas
questões críticas garantem a verdade das premissas, outras asseguram que o
contexto da inferência é apropriado, etc. Os esquemas dedutivos são comumente
codificados em regras padrão de inferência como \emph{modus ponens}, negação
dupla, modus tollens, etc.

Para uma visão mais profunda deste tema, \textcite*{Walton2008} fornecem um
coletânea de 96 esquemas de argumento.


\subsection{Outros Tópicos}
\label{sec:other-topics-informal}

Como qualquer campo de estudo, a lógica informal está em constante evolução. Sua
tentativa de entender os argumentos e como eles ocorrem em uma ampla gama de
situações da vida real continua a empurrá-la na direção de visões mais amplas
dos argumentos que reconhecem novas maneiras em que a argumentação pode ocorrer
e como e quando um argumento deve ser considerado bem sucedido. A seguir falamos
brevemente de alguns poucos novos tópicos nessa área.


\subsubsection{Mapas de Argumentos}
\label{sec:argument-maps}

Quando se entende um argumento como uma coleção de premissas e uma conclusão com
uma estrutura de inferência pode-se esclarecer e analisar esta estrutura com
diagramas de argumento ou ``mapas''. Este tipo de diagrama teve seus primeiros
usos nos séculos XIX e XX.

À medida que o interesse na análise do raciocínio informal se intensificou, esse
interesse foi acompanhado por um renovado interesse no mapeamento de argumentos.
De certa forma isso reviveu a prática de mapeamento, mas de uma nova maneira que
permite abordagens muito mais complexas e sofisticadas para o mapeamento. Mais
significativamente, tem sido acompanhado pelo desenvolvimento de softwares tais
como:\footnote{Os links apresentados foram acessados em 2016-11-15. Não existe
  qualquer garantia de que eles continuarão ativos no futuro.}

\begin{itemize}
\item \href{https://sourceforge.net/projects/argumentative/}{Argumentative};
\item \href{http://www.argunet.org/editor/}{argunet};
\item \href{http://ova.arg-tech.org/}{Online Visualisation of Argument (OVA)};
\item \href{http://www.cs.ie.niigata-u.ac.jp/Research/PIRIKA/PIRIKA.html}{PIRIKA
  (PIlot for the RIght Knowledge and Argument)};
\item \href{http://www.reasoninglab.com/}{Rationale© da ReasoningLab};
\item \href{http://www.compendiuminstitute.org/}{Compendium};
\item \href{http://www.skymark.com/Theseus/overview.asp}{Theseus}.
\end{itemize}

Além disso, existem ferramentas on-line para construção colaboração colaborativa
de mapas de argumentos.

\begin{itemize}
\item \href{http://agora.gatech.edu/}{AGORA-net};
\item \href{https://www.bcisiveonline.com/}{bCisive};
\item \href{http://carneades.github.io/}{Carneades};
\item \href{http://debategraph.org/home}{DebateGraph};
\end{itemize}

Todas essas ferramentas são desenhadas para auxiliar no mapeamento de argumentos
da vida real. Porém elas não substituem o trabalho de preparação e análise do
logicista.


\subsubsection{Emoções em Argumentos}
\label{sec:emotions-in-arguments}

Tradicionalmente, apelos emocionais têm sido vistos como elementos falaciosos em
argumentos. Esta é uma visão muito simples do papel da emoção no discurso
informal, onde os apelos à emoção desempenham um papel significativo que não
pode ser rejeitado de imediato. Em uma discussão da política nuclear, a emoção
inerente a uma descrição das consequências da guerra nuclear (digamos, a queda
da bomba em Hiroshima) pode desempenhar um papel legítimo no argumento. Nos
processos judiciais, há espaço explícito para o relato emocional da vítima sobre
o impacto de um crime, o que geralmente é considerado na sentença.

Alguns filósofos e logicistas tem estudado os argumentos emocionais,
investigando o papel que diferentes tipos de emoção e expressões de emoção
desempenham em argumentos da vida real, onde eles frequentemente funcionam como
uma maneira de convencer um público de um ponto de vista particular. Quando um
aluno, por exemplo, chora no escritório de um professor enquanto pede uma nota
mais alta,\footnote{Este é apenas um exemplo didático, sem qualquer conexão com
  o autor deste livro ou o professor da disciplina.} sua efusão emocional é um
componente chave de sua tentativa de convencer o professor de que sua nota deve
ser mudada. No intercâmbio real, esses apelos emocionais são frequentemente
bem-sucedidos. Ao distinguir entre apelos que devem ser aceitos e rejeitados, os
logicistas sugerem uma reconcepção da argumentação que reconhece os argumentos
emocionais como um gênero único de argumento que precisa ser avaliado de uma
maneira diferente da que as abordagens tradicionais sugerem.


\subsubsection{Inteligêcia Artificial}
\label{sec:informal-ai}

Como a lógica informal, o desenvolvimento da inteligência artificial requer
modelos teóricos que podem explicar a argumentação informal em contextos
amplamente diversos. Dessa forma, modelos de lógica informal influenciaram a
tentativa de modelar a argumentação entre agentes em sistemas multiagentes que
imitam ou auxiliam o raciocínio humano. Ferramentas computacionais têm sido
aplicadas a redes de argumentos interconectados de larga escala, e a raciocínio
sobre decisões médicas, questões legais, propriedades químicas e outros sistemas
complexos. Assessoramento automatizado de argumentos funciona como um auxílio
computacional que pode ajudar na construção de um argumento. O trabalho de
\textcite{Eemeren2014} fornece uma boa visão geral destas questões.

Na medida em que a lógica informal busca desenvolver uma lógica acessível ao
raciocínio cotidiano, ela e a modelagem computacional continuarão a ser
empreendimentos distintos. Mas ambos assumem uma compreensão teórica da maneira
como o raciocínio informal funciona e deve ser avaliado. A longo prazo, a
modelagem formal que a inteligência artificial requer pode restabelecer laços
mais fortes entre lógica formal e a informal. Os resultados podem fomentar o
desenvolvimento da lógica informal dentro de uma lógica (ou teoria da
argumentação) mais integrada que reconhece as diferenças entre a lógica formal e
informal, mas reconhece um modelo de argumentação abrangente que explica ambos.


\subsection{Lógica Informal e Filosofia}
\label{sec:informal-logic-and-philosophy}

Até certo ponto, a relação entre lógica informal e filosofia flui em ambos os
sentidos. A prática da filosofia assume inevitavelmente (e muitas vezes
desenvolve) algum noção de argumento enquanto reúne evidências para diferentes
perspectivas filosóficas. A lógica informal assume (e muitas vezes desenvolve)
alguma visão da natureza da razão, da racionalidade e do que conta como
evidência e conhecimento, à medida que desenvolve sua teoria de argumentação. As
questões filosóficas em jogo estão ligadas a questões complexas e incertas sobre
conhecimento e evidência.

Até certo ponto, a relevância da lógica informal para a filosofia não reside na
sua influência sobre as principais disciplinas filosóficas, tanto quanto na sua
influência sobre ela. A lógica informal é um campo no qual os filósofos aplicam
suas teorias de argumentação (racionalidade, conhecimento, etc.) ao argumento
cotidiano. Em consonância com isso, os filósofos continuam a ser os principais
contribuintes para a lógica informal, e os departamentos de filosofia nas
faculdades e universidades são o departamento central que ensinam os cursos que
são o foco da lógica informal.

Fora da filosofia, a lógica informal tem sido cada vez mais influenciada pela
retórica, estudos de comunicação, análise do discurso, semiótica, linguística e
inteligência artificial. Dentro e fora da filosofia, sua tentativa de construir
uma teoria abrangente de raciocínio e argumento tem importantes implicações para
nossa visão da racionalidade, a natureza da mente e seus processos eo papel
social, político e epistemológico do raciocínio e do argumento. Neste ponto, as
explorações de suas implicações para a filosofia da mente, ética e epistemologia
permanecem raras e seu foco continua a ser o desenvolvimento de uma ampla teoria
da argumentação que pode ser uma base para a análise e avaliação de argumentos
em um leque muito mais amplo de Contextos que aqueles que caracterizaram a
lógica tradicional.

Para uma visão mais geral sobre a filosofia, que é um tema fascinante por si só,
uma excelente fonte é o livro ``História da Filosofia Ocidental'' do filósofo e
logicista Bertrand Russell \cite{Russell2015}. Uma obra excelente, que além de
introduzir os principais conceitos da filosófica, também os coloca em seu
contexto histórico facilitando o entendimento de como foram desenvolvidos.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../ilm-book"
%%% ispell-local-dictionary: "brasileiro"
%%% End:
