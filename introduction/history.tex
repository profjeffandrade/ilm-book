\section{História da Lógica}
\label{sec:intro-history}

A lógica formal se desenvolveu na antiguidade na China, na Índia e na Grécia. A
lógica grega, principalmente a lógica aristotélica, encontrou grande aceitação e
aplicação na filosofia, nas ciências e na matemática.

A lógica de Aristóteles teve seu desenvolvimento aprofundado posteriormente por
filósofos cristãos e islâmicos na Idade Média, atingindo o seu ponto auto em
meados do século XIV. O período compreendido entre o século XIV e o início do
século XIX foi, em sua maior parte, de declínio e abandono da lógica.

A lógica formal foi revivida em meados do século XIX, o início de um período
revolucionário quando este assunto se desenvolveu em uma disciplina rigorosa e
produziu os métodos de prova utilizados na matemática. O desenvolvimento da
lógica matemática simbólica foi o maior desenvolvimento nos mais de dois mil
anos de história da lógica e é, possivelmente, um dos mais memoráveis eventos na
história intelectual humana.

O progresso na lógica matemática na primeiras décadas do século XX,
principalmente vindo dos trabalhos de Gödel e Tarski, teve um impacto
significante na filosofia analítica e na lógica filosófica, principalmente a
partir dos anos de 1950, em assuntos como lógicas modais, lógicas temporais,
lógicas deonticas, lógicas de relevância e lógicas paraconsistentes.


\subsection{Pré-história da Lógica}

O raciocínio válido tem sido empregado em todos os períodos da história humana.
Entretanto, a lógica estuda os \emph{princípios}, ou regras, do raciocínio
válido, das inferências e das demonstrações. É provavel que a necessidade de
demonstrar uma conclusão tenha aparecido inicialmente ligada à geometria, que
originalmente significava o mesmo que ``medição de terra''. Em particular os
egipcios descobriram empiricamente alguns resultados geométricos como a fórmula
do volume de uma piramide truncada.

Outra origem possível pode ser observada na Babilônia. O \emph{Manual de
  Diagnóstico} médico de Esagil-kin-aplin, do século XI BCE, era baseado em um
sistema lógico de axiomas e suposições. Além disso, astrônomos babilônicos dos
séculos VIII e VII BCE empregaram uma lógica interna dentro de seus sistemas
planetários preditivos, uma importante contribuição à filosofia da ciência.


\subsection{Lógica na Ásia}

Embora a lógica tenha sido estudada formalmente tanto na China quanto na Índia,
essas tradições não chegaram até a cultura ocidental, ou tiveram pouca
influência nesta parte do mundo. Em particular, o uso da lógica na matemática e
na ciência da computação sofreu muito pouca influência dos desenvolvimentos da
lógica na Ásia. Devido à relativa falta de informações históricas sobre a lógica
nesses países, são apresentados apenas alguns comentários breves sobre eles.

\subsubsection{Lógica na China}

Mozi, um contemporâneo de Confúcio, é creditado como o fundador da escola
Mohista, cujos ensinamentos lidavam com os problemas relacionados com a
inferência e com as condições das conclusões corretas. Em particular, uma das
escolas que nasceu do Mohismo, a ``Escola dos Nomes'', é creditada por alguns
estudiosos como sendo uma das primeiras escolas a investigar a lógica formal.
Infelizmente, em função da violenta imposição da severa regra do
\emph{legalismo}\footnote{O legalismo foi uma filosofia que enfatizava a
  obediência rigoroza ao sistema legal. Foi uma filosofia política utilitarista
  que não buscava responder questões mais elevadas como o propósito ou a
  natureza da vida. Pode significar, de uma maneira geral, ``filosofia política
  que sustenta o poder da lei''.} durante a dinastia Qin, essa linha de
investigação desapareceu da China até a introdução da filosofia indiana pelos
Budistas.

\subsubsection{Lógica na Índia}

Os sutras Nyaya de Akasapada Gautama -- cerca do século II ACE -- são os textos
centrais da escola da Nyaya, uma das seis escolas ortodoxas da filosofia Hindu.
Esta escola criou um rígido esquema de cinco elementos de inferência envolvendo
uma premissa inicial, uma razão, um exemplo, uma aplicação e uma conclusão.
%
A filosofia budista do idealismo se tornou a principal escola concorrente dos
Naiyayikas. Nagarjuna, o fundador da Madhyamika, o ``caminho do meio'',
desenvolveu uma análise conhecida como ``catuskoti'' ou tetralema. Essa análise
sistematicamente examinava a afirmação de uma proposição, sua negação, sua
afirmação e negação conjunta, e finalmente, a negação de sua afirmação e negação
conjuntas. De modo semelhante ao visto, posteriormente, na lógica multivalorada
de Gödel.

Entretanto foi com Dignaga e o seu sucessor, Dharmakirti, que a lógica budista
atingiu seu ápice. A base da analise deste pensadores é a definição da
necessidade de uma dedução lógica, ``vyapti'', também conhecida como
\emph{concomitância invariável} ou \emph{ubiquidade}. Para esse fim uma doutrina
chamada ``apoha'' ou diferenciação foi desenvolvida. As dificuldades envolvidas
neste sistema, em parte, estimularam a escola dos neo-escolásticos de
Navya-Nyaya, que introduziu a análise formal da inferência no século XVI.

A escola Navya-Nyaya desenvolveu teorias que guardam semelhanças com a lógica
moderna, tais como a ``distinção entre sentido e referência dos nomes próprios''
de Gottlob Frege e sua definição de número, bem como a teoria Navya-Nyaya de
``condições restritivas para os universais'' que antecipou alguns
desenvolvimentos da teoria de conjuntos moderna.

\subsection{Lógica na Grécia}

Na Grécia, duas importantes tradições emergiram. A Lógica estóica com as suas
raízes em Euclides de Megara, um pupilo de Sócrates, que é baseada na lógica
proposicional que talvez foi a mais próxima da lógica moderna. Entretanto, a
tradição que sobreviveu para mais tarde influenciar outras culturas foi a lógica
aristotélica, o primeiro tratado grego sobre a sistematização da lógica. Na
inspeção de Aristóteles sobre os silogismo há quem diga que existe uma
interessante comparação com o esquema de inferência dos indianos e com a menos
rígida discussão chinesa.

Difundida através de suas traduções para o latim, na Europa, e outras línguas
mais ao oeste, como árabe e armênio, a tradição aristotélica era considerada uma
codificação superior das leis do raciocínio. Somente no século XIX, com uma
maior familiaridade com a cultura clássica indiana e um conhecimento mais
profundo da China é que essa percepção mudou no ocidente.

\subsection{Lógica Medieval}

A ``lógica medieval'' (também conhecida como lógica escolástica) é a lógica
aristotélica desenvolvida na era medieval no período de 1200--1600 ACE. Esta
tradição foi fundamentada através de textos como o ``Tractatus'' de Pedro da
Espanha (século XIII), cuja verdadeira identidade é desconhecida. Tomás de
Aquino foi o filósofo que ousou mudar a antiga concepção tradicional, baseada em
Platão e Agostinho, concebendo uma visão aristotélica, e desenvolvendo a
escolástica tomista.

Essa antiga tradição também recebeu diversas considerações diferentes no século
XIV com as obras de Guilherme de Ockham (1287-1347) e Jean Buridan.

As últimas obras dessa tradição são ``Lógica'' de John Poinsot (1589-1644,
também conhecido como John de St Thomas), e o ``Discussões Metafísicas'' de
Francisco Suarez (1548-1617).

\subsection{Lógica Tradicional}

Esta tradição começou com o livro \emph{Lógica, ou a arte do pensamento} ou
\emph{Lógica de Port-Royal} de Antoine Arnauld e Pierre Nicole. Publicado em
1662, esse livro foi a mais influente introdução em lógica até o inicio do
século XX. O Lógica de Port-Royal apresenta ao leitor uma doutrina cartesiana
com uma estrutura que deriva da lógica aristotélica e medieval. O livro teve
oito edições entre 1664 e 1700. Ele foi reimpresso em inglês ate o fim do século
XIX.

A descrição das proposições que o filósofo John Locke faz na obra \emph{Uma Tese
  a Respeito do Entendimento Humano} é a mesma do Port-Royal. ``Proposições
verbais, que são palavras, os sinais de nossa ideia reunidas ou separadas em
sentenças afirmativas ou negativas. Pro tal meio de afirmar ou separar, esses
sinais formados de sons são, como se fossem, reunidos ou separados entre si. De
sorte que as proposições consistem em reunir ou separar sinais; e a verdade
consiste em reunir ou separar estes sinais, de acordo com as coisas que eles
significam para concordar ou discordar.'' \cite[Livro~IV, Capítulo~V,
pág.~244]{Locke1999}

Obras que se enquadram nessa tradição incluem ``Lógica: Ou, o Correto Uso da
Razão'' de Isaac Watts (2025), ``Lógica'' de Richard Wately (1826), e uma das
últimas grande obras dessa tradição ``Sistema de Lógica Dedutiva e Indutiva'' de
John Stuart Mill (1843), que além de formalizar os princípios do raciocínio
indutivo, também teve grande importância na filosofia da ciência e influenciou
diretamente vários cientístas, mais notadamente Paul Dirac, ganhador do Prêmio
Nobel de Física por seus trabalhos em física quântica.

\subsection{A Lógica Moderna}

René Descartes foi, provavelmente, o primeiro filósofo a utilizar as técnicas
algébricas como meio de exploração científica. A idéia de um ``cálculo do
raciocínio'' também foi cultivada por Gottfried Wilhelm Leibniz.

Durante o século XIX a lógica simbólica começou a se tornar popular entre os
filósofos e matemáticos porque eles estavam interessados no conceito do que
constitui uma prova correta em matemática. Um marco no desenvolvimento da lógica
formal ocorreu em meados do século XIX com a publicação de \emph{``Uma
  Investigação sobre as Leis do Pensamento''} pelo matemático e filósofo inglês
George Boole \cite{Boole1958}.

Gottlob Frege no \emph{Begriffschrift}, ou Ideografia, criou um sistema de
representação simbólica para representar formalmente a estrutura dos enunciados
lógicos e suas relações, e a invenção do cálculo dos predicados. Esta parte da
decomposição funcional da estrutura interna das frases (substituindo a velha
dicotomia analítica sujeito--predicado, herdada da tradição lógica aristotélica,
pela oposição matemática função--argumento) e da articulação do conceito de
quantificação (implícito na lógica clássica da generalidade), tornando assim
possível a sua manipulação em regras de dedução formal. Os enunciados ``para
todo o $x$'' e ``existe um $x$'' que denotam operações de quantificação sobre
variáveis lógicas têm a sua origem no seu trabalho fundador, ex: ``Todos os
humanos são mortais'' se torna ``Todos os $x$ são tais que, se $x$ é um humano
então $x$ é mortal''.

Ao contrário de Aristóteles, e mesmo de Boole, que procuravam identificar as
formas válidas de argumento, a preocupação básica de Frege era a sistematização
do raciocínio matemático, ou dito de outra maneira, encontrar uma caracterização
precisa do que é uma ``demonstração matemática''. Frege havia notado que os
matemáticos da época frequentemente cometiam erros em suas demonstrações,
supondo assim que certos teoremas estavam demonstrados, quando na verdade não
estavam. Para corrigir isso, Frege procurou formalizar as regras de
demonstração, iniciando com regras elementares, bem simples, sobre cuja
aplicação não houvesse dúvidas. O resultado que revolucionou a lógica, foi a
criação do cálculo de predicados (ou lógica de predicados).

Em 1889 Giuseppe Peano publicou seus nove axiomas sobre números naturais. Mais
tarde cinco destes vieram a ser conhecido com axiomas de Peano e, destes cinco,
um veio a ser a formalização do princípio da indução matemática.

Os lógicos do século XIX e início do século XX esperavam estabelecer a lógica
formal como o fundamento para a matemática. Embora os fundamentos da lógica
matemática tenham se desenvolvido nesta época e trabalhos importantes como o
\emph{``Begriffsschrift''} de Frege \cite{Frege1879} e \emph{``Principia
  Matematica''}, de Whitehead e Russell \cite{Principia1925}, tenham avançado
muito o entendimento sobre lógica e a formalização da matemática, o objetivo de
``logicização'' de toda a matemática nunca se concretizou por
completo\footnote{Devido principalmente ao resultado do \emph{Teorema da
    Incompletude}, de Göedel.}. Apesar disso, a lógica permitiu aos matemáticos
apontar o motivo pelo qual uma suposta prova estava errada, ou em que parte da
prova o raciocínio foi falho.

O trabalho de Frege nos fundamentos da lógica e a formulação da teoria dos
conjuntos de Peano, abriram para o enorme desenvolvimento da lógica matemática
que se seguiu no século XX. Nomes como o de Bertrand Russell, Alan Turing e Kurt
Gödel, trilharam o caminho aberto por Frege e Peano, e mudaram para sempre o
panorama da lógica, da matemática e da computação.

Uma grande parte do crédito por esse feito deve-se ao fato de que, escrevendo os
argumentos na linguagem simbólica ao invés de escreve-los em linguagem natural
(que é repleta de ambiguidade), verificar a corretude de uma prova se torna uma
tarefa muito mais viável.

\begin{figure}
  \centering
  \setlength{\BGthickness}{1pt}
  \setlength{\BGlinewidth}{0.2\textwidth}
  \[
    \BGassert\BGnot\BGquant{\mathfrak{F}}
    \BGconditional{
      \BGquant{\mathfrak{c}}
      \BGquant{\mathfrak{d}}
      \BGconditional{
        \BGnot\BGconditional{
          \BGterm{A(\mathfrak{c},\mathfrak{d})}
        }
        {
          \BGnot\BGterm{\mathfrak{F}(\mathfrak{c})}
        }
      }
      {
        \BGnot\BGconditional{
          \BGterm{\mathfrak{F}(\mathfrak{d})}
        }
        {
          \BGterm{\mathfrak{c}=\mathfrak{d}}
        }
      }
    }
    {
      \BGconditional{
        \BGnot\BGquant{\mathfrak{b}}
        \BGnot\BGterm{\mathfrak{F}(\mathfrak{b})}
      }
      {
        \BGterm{f\BGbracket{
            \BGquant{\mathfrak{a}}
            \BGconditional{
              \mathfrak{F}(\mathfrak{a})
            }
            {
              C(\mathfrak{a})
            }
          }
        }
      }
    }
  \]
  \caption{Notação do \emph{Begriffschrift} para a Sentença de Geach-Kaplan.}
  \label{fig:bs-geach-kaplan}
\end{figure}

Apenas a título de exemplo, a \autoref{fig:bs-geach-kaplan} mostra a formulação
em \emph{ideografia} para a Sentença de Geach-Kaplan. Esta sentença é um exemplo
bem conhecido de uso de lógica de alta ordem e, em português, a sentença de
Geach-Kaplan é:

\begin{quotation}
  \textit{Alguns críticos admiram somente uns aos outros.}
\end{quotation}

\begin{figure}
  \[
    \exists{F}:\left[
      \exists{a}:\exists{b}:\left(
        F(a) \land F(b) \land A(a,b)
      \right)
      \land
      \exists{x}:\neg F(x)
      \land
      \forall{c}:\forall{d}:\left(
        F(c) \land A(c,d) \to F(d)
      \right)
    \right]
  \]
  \caption{Notação da lógica de segunda ordem para a Sentença de Geach-Kaplan.}
  \label{fig:2ol-geach-kaplan}
\end{figure}

Também com exemplo, a \autoref{fig:2ol-geach-kaplan} mostra a mesma sentença
representada na notação moderna da Lógica de Predicados de 2ª Ordem. Não se
preocupe se você não entender as representações apresentadas nas
Figuras~\ref{fig:bs-geach-kaplan} e \ref{fig:2ol-geach-kaplan}. Elas estão
postas aqui apenas para dar um vislumbre do que é trabalhar com lógica moderna.
Tanto a \emph{ideografia} quanto as lógicas de alta ordem (como a de 2ª ordem,
por exemplo) estão fora do escopo de um curso introdutório de lógica. A
\emph{ideografia} faz parte apenas da história, e as lógicas de alta ordem
pertencem a cursos mais avançados.



%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../ilm-book"
%%% End:
