\chapter{Indução Matemática}
\label{chap:induc}

\minitoc

\emph{Indução matemática} é o raciocínio segundo o qual se estende uma
dada propriedade a todos os elementos de um conjunto, permitindo, deste
modo, demonstrar a verdade de um número possivelmente infinito de
proposições. É o método por excelência do raciocínio lógico-matemático.

A idéia central desse método consiste em ($i$) provar que o enunciado é
verdadeiro para um valor inicial, e então ($ii$) provar que o processo
usado para ir de um valor para o próximo é valido. Se estas duas coisas
são provadas, então a prova para qualquer elemento do conjunto pode ser
obtida através da repetição desse processo.

Para entender por que os dois passos descritos acima são suficientes, é
útil pensar no efeito dominó. Se você tem uma longa fila de dominós em
pé e você puder assegurar que:
%
\begin{enumerate}

\item O primeiro dominó cairá.

\item Sempre que um dominó cair, seu próximo vizinho também cairá.

\end{enumerate}
%
Então, podemos concluir que todos os dominós cairão.


Indução matemática não deve ser interpretada como uma forma de
\emph{raciocínio indutivo}, que é considerado não-rigoroso em
matemática. A indução matemática é uma forma rigorosa de raciocínio
dedutivo. O método de indução pode ser estendido para provar afirmações
sobre \emph{estruturas bem-fundadas} mais gerais, tais como listas e
árvores, por exemplo. Essa generalização, conhecida como \emph{indução
  estrutural}, é amplamente usada na lógica matemática e em ciência da
computação. Indução matemática, nesse sentido estendido, está
intimamente relacionada com \emph{recursividade}, um dos conceitos
básicos de teoria da computação.

Recursão, também conhecida como \emph{estratégia de dividir para
  conquistar}, é um método que quebra um problema grande (difícil) em
parte menores e geralmente mais simples de resolver. Se você puder
demonstrar que qualquer problema grande pode ser subdividido em outros
menores, e que os menores problemas possíveis (i.e., aqueles que não
podem mais ser subdivididos) podem ser resolvidos, você tem um método
para resolver problemas de qualquer tamanho. Evidentemente, nós podemos
provar isso usando indução.

Vejamos um exemplo simples. Suponha que lhe foram dadas as coordenadas
dos vértices de um polígono simples, i.e., um polígono cujos vértices
são distintos e cujos lados não se intercruzam, e você quer dividir o
polígono em triângulos.  Se você puder escrever um programa que quebra
qualquer polígono grande (i.e., com quatro lados ou mais) em dois
polígonos menores, então você sabe que pode triangular o polígono
todo. Divide-se o polígono original (grande) em dois menores, e então
repetidamente aplica-se o processo para os polígonos menores obtidos no
passo anterior. Uma aplicação deste problema é o algoritmo para
``renderização'' de objetos em computação gráfica.

\section{Primeiro Princípio de Indução --- Indução Fraca}

A forma mais simples e mais comum de indução matemática prova que um
enunciado vale para todos os números naturais $n$ e consiste de dois
passos:

\begin{enumerate}

\item O \emph{caso base}: mostrar que o enunciado vale para $n = 1$.

\item O \emph{passo indutivo}: mostrar que, \textbf{se} o enunciado vale
  para $n = k1$, \textbf{então} o mesmo enunciado vale para $n = k + 1$.

\end{enumerate}

A suposição, na etapa indutiva, de que o enunciado vale para algum $k$ é
chamado a \emph{hipótese de indução}. Para executar o passo indutivo,
deve-se assumir a hipótese de indução e, em seguida, usa essa suposição
para provar a afirmação para $k + 1$.


\begin{defn}[Princípio de Indução Matemática Fraca]

  Seja $P$ uma propriedade arbitrária para os números
  naturais. Escrevemos $P(n)$ para representar que a propriedade $P$ é
  verdadeira para o número $n$.

  O \emph{princípio de indução fraca}\footnote{Também chamado ``Primeiro
    Princípio de Indução''.} diz que se pudermos provar que:

  \begin{enumerate}

  \item $P(0)$ é verdadeiro; e

  \item Supondo-se $P(k)$, podemos mostrar que $P(k + 1)$ é verdadeiro.

  \end{enumerate}

  Então podemos afirmar que $P(n)$ é verdadeiro para todo $n \in
  \mathbb{N}$.

\end{defn}


\begin{exmp}[Soma de pares]

  Podemos usar o princípio de indução fraca para provar que a sentença
  abaixo é verdadeira para todo número natural $n$.
  \begin{equation}
    P(n):\; 0 + 2 + 4 + \cdots + 2n = n^2 + n
  \end{equation}
  A prova de que a senteça é verdadeira para todo $n \in \mathbb{N}$ é
  dada abaixo e segue a forma geral de demonstrações por indução, i.e.,
  primeiro provaremos o caso base e, em seguida, o caso indutivo.

  \begin{enumerate}

  \item \emph{Caso base:} $n = 0$. Substituindo $n$ por 0 em $P(n)$
    temos:
    \begin{align*}
      P(0):\; 0 & = 0^2 + 0 \\
      \therefore\; 0 & = 0
    \end{align*}

  \item \emph{Caso indutivo:} Supomos que $P$ é verdadeiro para algum
    valor $k \geq 0$, i.e., maior ou igual ao caso base, e a partir
    desta suposição (i.e., da \emph{hipótese indutiva}) devemos
    demonstrar que $P$ também é verdadeiro para $k+1$. Ou seja, queremos
    mostrar que \textbf{se} $P(k)$ for verdadeiro, \textbf{então}
    $P(k+1)$ também será. Logo, supomos

    \begin{equation}
      \label{eq:pif:ex:sum1:pk}
      P(k):\; 0 + 2 + 4 + \cdots + 2k = k^2 + k
    \end{equation}

    e devemos provar que

    \begin{equation}
      \label{eq:pif:ex:sum1:pk1}
      P(k+1):\; 0 + 2 + 4 + \cdots + 2k + 2(k+1) = (k+1)^2 + (k+1)
    \end{equation}

    também é verdadeiro.

    \[
    \begin{array}{rcl}
      \underbrace{0 + 2 + 4 + \cdots + 2k}_{\text{lado esq. de
      \eqref{eq:pif:ex:sum1:pk}}} + 2(k+1) & = & (k+1)^2 + (k+1)
      \\
      (k^2 + k) + 2(k+1)
      & =
      & (k^2+ 2k + 1) + (k+1)
      \qquad\triangleright\;\text{substituindo \eqref{eq:pif:ex:sum1:pk}}
      \\
      k^2 + 3k + 2
      & =
      & k^2 + 3k + 2
    \end{array}
    \]

  \end{enumerate}

  \qed

\end{exmp}


\begin{exmp}[Soma dos $n$ primeiros números naturais]

  Indução matemática pode ser usada para provar que a conjectura abaixo vale
  para todos os números naturais $n$.

  \begin{equation}
    \label{eq:sumpa}
    0 + 1 + 2 + \cdots + n = \frac{n (n + 1)}{2}
  \end{equation}

  Essa conjectura estabelece uma fórmula para a soma dos números naturais
  menores ou iguais ao número $n$. A prova de que a conjectura é verdadeira
  para todos $n \in \mathbb{N}$ é dada abaixo.

  Chamemos a conjectura de $P(n)$, ou seja, diremos que $P(n)$ é verdadeiro
  se a igualdade na Equação \eqref{eq:sumpa} for verdadeira.

  \begin{enumerate}

  \item \emph{Caso base}: Mostrar que o enunciado vale para $n = 0$.

    $P(0)$ representa a declaração: \[ 0 = \frac{0 \cdot (0 + 1)}{2} \]

    No lado esquerdo da equação, o único termo é $0$.

    No lado direito da equação temos $\displaystyle\frac{0 \cdot (0 + 1)}{2}$
    que também reduz a $0$.

    Os dois lados são iguais, então a afirmação é verdadeira para $n = 0$, o que
    prova o caso base.

  \item \emph{Passo indutivo}: Mostrar que \emph{se} $P(k)$ é verdadeiro,
  então, $P(k+1)$ também será verdadeiro, para qualquer $k$. Isso pode ser feito
  da seguinte forma.

    Supomos que $P(k)$ é verdadeiro (por algum valor não especificado de $k$, ou
    seja, para qualquer $k$). Em seguida, devemos mostrar que $P(k + 1)$ é
    verdadeiro, ou seja:

    Supomos que
    \begin{equation}
      \label{eq:pif1ex1}
      0 + 1 + 2 + \cdots + k = \frac{n(n+1)}{2}
    \end{equation}
    é verdadeiro e, em seguida, tentamos mostrar que
    \begin{equation}
      \label{eq:pif1ex2}
      0 + 1 + 2 + \cdots + k + (n + 1) = \frac{(n +1) ((n + 1) + 1)}{2}
    \end{equation}
    também é verdadeiro.

    Usando a hipótese de indução de que $P(k)$ é verdadeiro, podemos
    reescrever a Equação \eqref{eq:pif1ex2} da seguinte forma:

    \begin{equation}
      \label{eq:pif1ex3}
      \frac{n(n + 1)}{2} + (n +1) = \frac{(n +1) ((n + 1) + 1)}{2}
    \end{equation}

    Aplicando sucessivas simplificações algébricas à Equação \eqref{eq:pif1ex3},
    temos:

    \begin{align*}
      \frac{n(n + 1) + 2(n + 1)}{2} & = \frac{(n + 1)(n + 2)}{2}
      \\
      n(n + 1) + 2(n + 1) & = (n + 1)(n + 2)
      \\
      n^2 + n + 2n + 2 & = n^2 + 2n + n + 2
      \\
      n^2 + 3n + 2 & = n^2 + 3n + 2
    \end{align*}

    Logo, podemos verificar que os dois lados da Equação
    \eqref{eq:pif1ex3} são iguais, demonstrando assim que $P(k+1)$ é
    verdadeiro.

    Uma vez que tanto o caso base quanto o passo indutivo foram provados,
    podemos afirmar que foi provado por indução matemática que $P(n)$ vale para
    todos os números naturais $n$.

  \end{enumerate}

  \qed\footnote{O símbolo \qedsymbol, muitas vezes escrito como ``Q.E.D.'',
  significa \emph{``Quod erat demonstrandum''} -- é uma expressão em latim que
  significa \emph{``como se queria demonstrar''}.}
\end{exmp}


A parte em que se faz a prova do passo indutivo pode variar bastante,
dependendo da natureza exata da propriedade a ser provada, mas a
estrutura básica da prova por indução é sempre a mesma.


\subsection{Iniciando em $n_0 \neq 0$}

A escolha entre $n = 0$, $n = 1$, ou outro valor, no caso base é
específico para cada contexto. Se $0$ é considerado um número natural,
como é comum nas áreas de análise combinatória e lógica matemática,
então $n = 0$. Se, por outro lado, $1$ é tomado como o primeiro número
natural, então o caso base é dada por $n = 1$.  Também deve ser
considerada a prova em questão. Pode ser que a conjectura que se deseja
provar somente seja válida para valores de $n \geq n_0$, neste caso o
caso base passa a ser $n = n_0$.

Se nós queremos provar uma dada conjectura, não para todos os números
naturais, mas apenas para todos os números maiores ou iguais a um
determinado número $n_0$, então:

\begin{enumerate}

\item Mostrando que o enunciado vale quando $n = n_0$.

\item Mostrando que, \emph{se} o enunciado vale para $n = k$, onde $k
  \geq b$, então o mesmo enunciado vale para $n = k + 1$.

\end{enumerate}

Poderemos afirmar que $P(n)$ é verdadeiro para todo $n \in \mathbb{N}, 
n \geq b$.


\begin{exmp}[$n^2 > 2n, n \geq 3$]


  Podemos usar esta variação da indução fraca para para mostrar que 
  $n^2 > 2n$ para todo $n \geq 3$.

  Definiremos a propriedade a ser demonstrada como $P(n): n^2 > 2n$.

  \begin{enumerate}

  \item \emph{Caso base}: Mostrar que o enunciado vale para $n = n_0$,
    i.e., mostrar que $P(n_0)$ é verdadeiro.

    Como $n_0 = 3$, $P(3)$ eleva a:
    \begin{eqnarray*}
      3^2           & > & 2 \cdot 3  \\
      \therefore\ 9 & > & 6
    \end{eqnarray*}

    Como a inequação é verdadeira, podemos afirmar que $P(3)$ é
    verdadeiro, prova o caso base.

  \item \emph{Passo indutivo}: Mostrar que \emph{se} $P(k)$ é verdadeiro
    para algum valor de $k \geq n_0$, então, $P(k+1)$ também será
    verdadeiro.

    Supomos que $P(k)$ é verdadeiro. Em seguida, devemos mostrar que
    $P(k + 1)$ é verdadeiro, ou seja:

    Supomos que
    \begin{equation}
      \label{eq:pif1ex2a}
      P(k): k^2 > 2k
    \end{equation}
    é verdadeiro e, em seguida, tentamos mostrar que
    \begin{equation}
      \label{eq:pif1ex2b}
      P(k+1): (k+1)^2 > 2(k+1)
    \end{equation}
    também é.

    Desenvolvendo a Equação \eqref{eq:pif1ex2b}, temos:
    \[
    \begin{array}{rcll}
      (k+1)^2
      & =
      & k^2 + 2k + 1
      & \hspace{1cm}\text{(expandindo o termo à esquerda)}
      \\
      (k+1)^2
      & >
      & 2k + 2k + 1
      & \hspace{1cm}\text{(pela hipótese de indução, i.e., $k^2 > 2k$)}
      \\
      (k+1)^2
      & >
      & 2k + 6 + 1
      & \hspace{1cm}\text{(já que $k \geq 3$)}
      \\
      (k+1)^2
      & >
      & 2k + 2
      & \hspace{1cm}\text{(pois se $2k + 7 > 2k + 2$)}
      \\
      (k+1)^2
      & >
      & 2(k + 1)
      &
    \end{array}
    \]

  \end{enumerate}

  Uma vez que tanto o caso base quanto o passo indutivo foram provados,
  podemos afirmar que $P(n)$ vale para todo $n \geq 3$.

  \qed
\end{exmp}

O próximo exemplo apresenta uma demonstração que se destaca por dois
aspectios.  Primeiramente, ela é uma demonstração menos numérica do que
as apresentadas anteriormente. Em segundo lugar porque ela é uma
demonstração \emph{por casos} ou seja, uma demonstração que analisa
quais situações diferentes podem ocorrer e demonstra, para cada uma das
possíveis situações diferentes, que a propriedade desejada é verdadeira.

\begin{exmp}[Caixa Eletrônico]

  Suponha que um caixa eletrônico tem apenas notas de dois e cinco
  Reais. Você pode digitar o valor que quer, e o caixa eletrônico vai
  descobrir como as dividir o valor em nos números adequados de notas de
  dois e cinco Reais.

  \noindent\emph{Conjectura}: o caixa eletrônico pode gerar qualquer
  valor $n \geq 4$ reais.

  Representaremos a propriedade ``o caixa eletrônico pode gerar o valor
  $n$'' por $G(n)$. Portanto, queremos demonstrar que $G(n)$ é
  verdadeiro para todo valor $n \geq 4$.

  \begin{enumerate}

  \item \emph{Caso Base}: $n = 4$. Podem ser geradas com 2 notas de R\$
    2,00. Portanto, $G(4)$ é verdadeiro.

  \item \emph{Passo Indutivo}: Como \emph{hipótese indutiva} supomos que
    $G(k)$ é verdadeiro para $k \geq 4$. Devemos demonstrar que $G(k+1)$
    segue da hipótese indutiva. Efetuaremos a demonstração supondo dois
    casos distintos:

    \begin{enumerate}

    \item A saída para $k$ contém ao menos uma nota de R\$ 5,00.

      Neste caso, para gerar $k+1$ basta substituir a nota de R\$ 5,00
      por 3 notas de R\$ 2,00 (i.e., R\$ 6,00).

    \item A saída não contém nenhuma nota de R\$ 5,00.

      Neste caso, já que $k \geq 4$, devem haver ao menos 2 notas de R\$
      2,00. Para gerar $k + 1$ basta substituir 2 notas de R\$ 2,00
      (i.e., R\$ 4,00) por uma nota de R\$ 5,00.

    \end{enumerate}

    Como $G(k+1)$ é verdadeira nos dois casos possíveis, podemos afirmar
    que $G(k+1)$ é verdadeira sempre que $G(k)$ for verdadeira.

  \end{enumerate}

  Uma vez que tanto o caso base quanto o passo indutivo foram provados,
  podemos afirmar que $G(n)$ vale para todo $n \geq 4$.

  \qed
\end{exmp}


\section{Segundo Princípio de Indução --- Indução Forte}

O Princípio da Indução Matemática afirma que se $P(n_0)$ é verdadeiro e,
para $k \geq n_0$, $P(k)$ é verdadeiro, então $P(k+1)$ é verdadeira. No
entanto, às vezes é preciso ``voltar'' mais do que um passo para obter
$P(k+1)$. Por exemplo, pode ser que o valor de $P(k+1)$ seja promado
pela combinação dos valores de $P(k)$ e $P(k-1)$. Neste tipo de situação
é onde a forma forte da indução matemática é útil.

\emph{Indução forte}, ou \emph{indução completa}, ou \emph{segundo
  princípio de indução}, diz que na segunda etapa pode-se supor que não
só o enunciado vale para $n = k$, mas também que ela é verdadeira para
todo $n$ menor ou igual a $k$.


\begin{defn}[Princípio da Indução Matemática Forte]

  Sejam $a$ e $b$ números naturais tais que $a \leq b$, e seja $P(n)$ um
  predicado (propriedade) definido sobre todos os números naturais 
  $n \geq a$.

  Suponha que as seguintes afirmações são verdadeiras:
  \begin{enumerate}

  \item $P(a), P(a+1), \ldots, P(b)$ são todas afirmações verdadeiras.
    (\emph{Caso Base})

  \item Para qualquer número natural $k > b$, \textbf{se} $P(i)$ é
    verdadeira para todos os números naturais $i$, tais que $a \leq i <
    k$, então $P(k)$ é verdadeira. (\emph{Passo Indutivo})

  \end{enumerate}

  Então a afirmação $P(n)$ é verdadeira para todos os números naturais
  $n \geq a$.

\end{defn}


\begin{exmp}[Postagem com selos de 4 e 5 centavos]

  \emph{Conjectura}: qualquer valor de postagem acima de 12 centavos
  pode ser gerado apenas com selos de 4 e 5 centavos.

  Representaremos a propriedade ``é possível gerar, apenas com selos de
  4 e 5 centavos, o valor de postagem de $n$ centavos'' por
  $G(n)$. Desejamos provar que $G(n)$ é verdadeiro para todo $n \geq
  12$.

  \begin{enumerate}

  \item \emph{Caso Base}: Consideraremos como casos bases $n = 12$, $n =
    13$, $n = 14$ e $n = 15$.
    \begin{enumerate}
    \item $n = 12$, gerado por 3 selos de 4 centavos.
    \item $n = 13$, gerado por 2 selos de 4 centavos e 1 selo de 5
      centavos.
    \item $n = 14$, gerado por 1 selo de 4 centavos e 2 selos de 5
      centavos.
    \item $n = 15$, gerado por 3 selos de 5 centavos.
    \end{enumerate}
    Portanto, $G(n)$ é verdadeiro para todos os casos base.

  \item \emph{Passo Indutivo}: Seja $k \geq 15$.

    Assumiremos que $G(r)$ é verdadeiro para todo $r$, tal que $12 \leq
    r \leq k$. (\emph{Hipótese Indutiva})

    Para formar o valor de postagem $k+1$, ou seja, para demonstrar que
    $G(k+1)$ é verdadeiro, gera-se o valor de postagem para $k-3$ (que
    pode se gerado, de acordo com a hipótese de indução) e acrescenta-se
    um selo de 4 centavos.

  \end{enumerate}

  \qed
\end{exmp}


\begin{exmp}[Enésimo Número de Fibonacci]
  \label{exmp:fib}

  Os \emph{Números de Fibonacci}, ou Sequência de Fibonacci, são uma
  sequência definida recursivamente pela fórmula abaixo:
  \begin{equation}
    F(n) =
    \left\{
      \begin{array}{ll}
        0 & \text{se}\ n = 0  \\
        1 & \text{se}\ n = 1  \\
        F(n-1)+F(n-2) & \text{se}\ n > 1
      \end{array}
    \right.
    \label{eq:fib}
  \end{equation}
  E tem aplicações em biologia, arquitetura, análise combinatória,
  música e outras áreas. Os primeiros 11 números de Fibonacci, i.e., $n
  = 0$ até $n = 10$, são: 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55.

  A \emph{proporção áurea} é uma constante irracional que é
  frequentemente encontrada em campos tão diversos quanto artes e
  biologia. A proporção áurea é denotada pela letra grega $\varphi$
  (\emph{phi}), com valor aproximado de $1,618$:
  \begin{equation}
    \varphi = \frac{1+\sqrt{5}}{2}
    \label{eq:auphi}
  \end{equation}

  Interessantemente, existe uma relação entre os Números de Fibonacci e a
  Proporção Áurea.

  \vspace{2mm}\noindent\emph{Conjectura}: Para todo número natural $n$:
  \begin{equation}
    F(n) =
      \frac{
        \varphi^n - \left(\frac{-1}{\varphi}\right)^n
      }{\sqrt{5}}
    \label{eq:fib-au}
  \end{equation}

  Chamando \eqref{eq:fib-au} de $A(n)$, queremos demonstrar que $A(n)$ é
  verdadeiro para todo $n \in \mathbb{N}$.

  \begin{enumerate}

    \item \emph{Caso Base.} $n = 0$ e $n = 1$.
      \begin{enumerate}

      \item
        $\displaystyle F(0) =
          \frac{
            \varphi^0 - \left(\frac{-1}{\varphi}\right)^0
          }{
            \sqrt{5}
          }
          =
          \frac{1 - 1}{\sqrt{5}}
          =
          0$
          \vspace{3mm}

      \item $\displaystyle
        F(1) =
          \frac{
            \varphi^1 - \left(\frac{-1}{\varphi}\right)^1
          }{
            \sqrt{5}
          }
          =
          \frac{\varphi - \frac{-1}{\varphi}}{\sqrt{5}}
          =
          \frac{\varphi^2 + 1}{\varphi\sqrt{5}}$

        \vspace{2mm}
        Substituindo \eqref{eq:auphi}, temos:
        \begin{eqnarray*}
          F(1) & = &
          \frac{
            \left(\frac{1+\sqrt{5}}{2}\right)^2 + 1
          }{
            \left(\frac{1+\sqrt{5}}{2}\right)\sqrt{5}
          }
          \; = \;
          \frac{
            \frac{1+2\sqrt{5}+\sqrt{5}^2}{2^2} + 1
          }{
            \frac{\sqrt{5}+\sqrt{5}\sqrt{5}}{2}
          }
          \; = \;
          \frac{
            \frac{1+2\sqrt{5}+5}{4} + 1
          }{
            \frac{\sqrt{5}+5}{2}
          }
          \\
        F(1) & = &
          \frac{
            \frac{1+2\sqrt{5}+5+4}{4}
          }{
            \frac{\sqrt{5}+5}{2}
          }
          \; = \;
          \frac{
            \frac{2\sqrt{5}+10}{4}
          }{
            \frac{\sqrt{5}+5}{2}
          }
          \; = \;
          \frac{
            \frac{\sqrt{5}+5}{2}
          }{
            \frac{\sqrt{5}+5}{2}
          }
          \; = \; 1
        \end{eqnarray*}

      \end{enumerate}

      Portanto, $A(n)$ é verdadeiro para os dois casos base.


    \item \emph{Passo Indutivo.} Seja $k > 1$.

      Assumiremos que $A(r)$ é verdadeiro para todo $r$, tal que $0 \leq r \leq
      k$. (\emph{Hipótese Indutiva})

      Devemos provar que $A(k+1)$ é verdadeiro.
      \begin{equation}
        F(k+1) =
        \frac{
          \varphi^{(k+1)} - \left(\frac{-1}{\varphi}\right)^{(k+1)}
        }{
          \sqrt{5}
        }
        \label{eq:fibau:kp1}
      \end{equation}
      Mas de \eqref{eq:fib} temos que:
      \begin{equation}
        F(k+1) = F(k) + F(k-1)
        \label{eq:fib:kp1}
      \end{equation}
      Pela hipótese indutiva, \eqref{eq:fib:kp1} pode ser desenvolvida para:
      \begin{eqnarray}
        F(k+1) & =
        & \frac{
          \varphi^k - \left(\frac{-1}{\varphi}\right)^k
        }{
          \sqrt{5}
        }
        +
        \frac{
          \varphi^{(k-1)} - \left(\frac{-1}{\varphi}\right)^{(k-1)}
        }{
          \sqrt{5}
        }
        \nonumber
        \\
        F(k+1) & =
        & \frac{
          \varphi^k - \left(\frac{-1}{\varphi}\right)^k
          +
          \varphi^{(k-1)} - \left(\frac{-1}{\varphi}\right)^{(k-1)}
        }{
          \sqrt{5}
        }
        \nonumber
        \\
        F(k+1) & =
        & \frac{
          \left(
            \varphi^k
            + \varphi^{(k-1)}
          \right)
          -
          \left[
            \left(\frac{-1}{\varphi}\right)^k
            + \left(\frac{-1}{\varphi}\right)^{(k-1)}
          \right]
        }{
          \sqrt{5}
        }
        \nonumber
        \\
        F(k+1) & =
        & \frac{
          \varphi^{(k-1)}(\varphi + 1)
          - {
            \left(\frac{-1}{\varphi}\right)^{(k-1)}
            \left(\frac{-1}{\varphi} + 1\right)
          }
        }{
          \sqrt{5}
        }
        \label{eq:fib:kp1b}
      \end{eqnarray}

      Para tornar o lado equerdo de \eqref{eq:fib:kp1b} igual ao lado esquerdo
      de \eqref{eq:fibau:kp1} devemos demonstrar que $\varphi + 1 = \varphi^2$
      e que $\frac{-1}{\varphi} + 1 = \left(\frac{-1}{\varphi}\right)^2$.

      \begin{eqnarray}
        \varphi^2 & = & \left(\frac{1+\sqrt{5}}{2}\right)^2
        \; = \; \frac{\left(1+\sqrt{5}\right)^2}{2^2}
        \; = \; \frac{1+2\sqrt{5}+\sqrt{5}^2}{2^2}
        \nonumber
        \\
        \varphi^2 & = & \frac{6+2\sqrt{5}}{4}
        \; = \; \frac{3+\sqrt{5}}{2}
        \nonumber
        \\
        \varphi^2 & = & \frac{1+\sqrt{5}}{2} + 1
        \; = \; \varphi + 1
        \label{eq:phi2}
      \end{eqnarray}

      \begin{eqnarray}
        \left(\frac{-1}{\varphi}\right)^2
        & = & \frac{1}{\varphi^2}
        \; \left(\!\times\frac{\varphi - 1}{\varphi - 1}\right)
        \; = \; \frac{\varphi - 1}{\varphi^2(\varphi - 1)}
        \nonumber
        \\
        \left(\frac{-1}{\varphi}\right)^2
        & = & \frac{\varphi - 1}{(\varphi + 1)(\varphi - 1)}
        \; = \; \frac{\varphi - 1}{\varphi^2 - 1}
        \nonumber
        \\
        \left(\frac{-1}{\varphi}\right)^2
        & = & \frac{\varphi - 1}{(\varphi + 1) - 1}
        \; = \; \frac{\varphi - 1}{\varphi}
        \; = \; \frac{-1}{\varphi} + 1
        \label{eq:m1phi2}
      \end{eqnarray}

      Substituindo \eqref{eq:phi2} e \eqref{eq:m1phi2} na equação
      \eqref{eq:fib:kp1b}, temos:
      \begin{eqnarray}
        F(k+1) & =
        & \frac{
          \varphi^{(k-1)}\varphi^2
          - {
            \left(\frac{-1}{\varphi}\right)^{(k-1)}
            \left(\frac{-1}{\varphi}\right)^2
          }
        }{
          \sqrt{5}
        }
        \nonumber
        \\
        F(k+1) & =
        & \frac{
          \varphi^{(k+1)} - \left(\frac{-1}{\varphi}\right)^{(k+1)}
        }{
          \sqrt{5}
        }
        \label{eq:fib:kp1c}
      \end{eqnarray}

      O que prova o passo indutivo.

  \end{enumerate}

  Tanto os casos base quanto o passo indutivo foram provados, portanto podemos
  afirmar que $A(n)$ vale para todo $n \in \mathbb{N}$.

  \qed
\end{exmp}


\subsection{Indução Transfinita}

Em matemática, e em especial na teoria dos conjuntos, a indução transfinita é
uma técnica matemática rigorosa que permite provar propriedades para todos
números ordinais\footnote{Os números ordinais são números usados para assinalar
uma posição numa sequência ordenada: primeiro, segundo, terceiro, quarto,
quinto, sexto etc. Em matemática, os números ordinais são uma extensão dos
números naturais criada para incluir sequências infinitas por Georg Cantor em
1897.} -- ou, de forma mais geral, para qualquer conjunto bem ordenado -- a
partir de etapas finitas. É uma generalização da indução finita.

Seja $P(\alpha)$ uma propriedade definida para todos os ordinais $\alpha$.
Suponha que sempre que $P(\beta)$ é verdadeiro para todos $\beta < \alpha$,
então $P(\alpha)$ também é verdadeiro. Então, a indução transfinita nos diz que
$P$ é verdadeiro para todos os ordinais.

Ou seja, se $P(\alpha)$ é verdadeira sempre que $P(\beta)$ é verdadeiro para
todo $\beta < \alpha$, então $P(\alpha)$ é verdadeira para todos os $\alpha$.
Ou, em termos mais práticos: para provar a propriedade $P$ para todos os
ordinais $\alpha$, pode-se supor que $P$ já é conhecido para todos os
$\beta < \alpha$.

Normalmente a prova é dividida em três casos:

\begin{enumerate}

  \item \emph{Caso Zero}: Prove que $P(0)$ é verdadeira.

  \item \emph{Caso Sucessor}: Prove que, para qualquer sucessor ordinal
  $\alpha+1$, $P(\alpha + 1)$ decorre de $P(\alpha)$ e, se necessário, de
  $P(\beta)$ para todo $\beta < \alpha$.

  \item \emph{Caso Limite}: Prove que, para qualquer ordinal limite $\gamma$,
  $P(\gamma)$ segue a partir de $P(\beta)$ para todo $\beta < \gamma$.

\end{enumerate}

Observe que o segundo e terceiro casos são idênticas, exceto para o tipo de
ordinal considerado. Eles não precisam ser formalmente provados em separado,
mas, na prática, as provas são geralmente tão diferentes que exigem
apresentações separadas.

Observe também que a rigor, não é necessário provar o \emph{caso zero} (i.e.,
a base na indução transfinita), porque este é um caso especial \emph{vazio} da
proposição de que se $P(\beta)$ é verdadeiro para todo $\beta < \alpha$, então
$P$ é verdadeiro para $\alpha$. Isto é \emph{trivialmente verdadeiro}, porque
não há valores para $\beta < \alpha$ que poderiam servir como contra-exemplos.
Entretanto, na prática, é comum apresentar a prova para o \emph{caso zero} como
uma forma de enfatizar a corretude da prova.


\section{Exercícios}

\begin{exercise}[Aquecimento]

  Mostre que as equações abaixo são verdadeiras para todo $n \in \mathbb{N}$.

  \begin{enumerate}

    \item \(\displaystyle 0^1 + 1^1 + 2^1 + 3^1 + \cdots + (n - 1)^1 =
    \frac{n^2}{2} - \frac{n}{2}\)

    \item \(\displaystyle 0^2 + 1^2 + 2^2 + 3^2 + \cdots + (n - 1)^2 =
    \frac{n^3}{3} - \frac{n^2}{2} + \frac{n}{6}\)

    \item \(\displaystyle 0^3 + 1^3 + 2^3 + 3^3 + \cdots + (n - 1)^3 =
    \frac{n^4}{4} - \frac{n^3}{2} + \frac{n^2}{4}\)

    \item \(\displaystyle 0^4 + 1^4 + 2^4 + 3^4 + \cdots + (n - 1)^4 =
    \frac{n^5}{5} - \frac{n^4}{2} + \frac{n^3}{3} - \frac{n}{30}\)

    \item \(\displaystyle 0^2 + 1^2 + 2^2 + \cdots + n^2 =
    \frac{n(n + 1)(2n + 1)}{6}\)

  \end{enumerate}

\end{exercise}


\begin{exercise}

  Mostre que as equações abaixo são verdadeiras para todo $n \in \mathbb{N},
  n \geq 1$.

  \begin{enumerate}

    \item \(\displaystyle 1 + 3 + \cdots + (2n - 1) = n^2\)

    \item \(\displaystyle \frac{1}{1\cdot 2} + \frac{1}{2\cdot 3} + \cdots
    + \frac{1}{n(n+1)} = \frac{n}{n+1} \)

    \item \(\displaystyle 1^3 + 2^3 + \cdots + n^3 =
    \left[ \frac{n(n+1)}{2} \right]^2 \)

    \item \(\displaystyle 1 - 2^2 + 3^2 - 4^2 + \cdots + (-1)^{n-1}n^2 =
    (-1)^{n-1}\cdot\frac{n(n+1)}{2} \)

    \item \(\displaystyle 1^2 + 3^2 + \cdots + (2n - 1)^2 =
    \frac{1}{3}\left[ n(2n - 1)(2n + 1) \right] \)

  \end{enumerate}

\end{exercise}


\begin{exercise}

  Mostre que a soma de três números naturais consecutivos é sempre divisível
  por 3.

  \emph{Sugestão:} Considere a sentença aberta
  \[
    P(n):\; n + (n + 1) + (n + 2) \;\text{é divisível por 3},
  \]
  e mostre, por indução, que ela é verdadeira para todo $n \in \mathbb{N}$.

\end{exercise}


\begin{exercise}

  Mostre que a soma dos cubos de três números naturais consecutivos é sempre
  divisível por 9.

\end{exercise}


\begin{exercise}

  Dada a sentença aberta em $\mathbb{N}$:
  \[
    P(n):\; 1 + 2 + \cdots + n = \frac{n(n + 1)}{2} + 1,
  \]
  mostre que:
  \begin{enumerate}

    \item Qualquer que seja $n \in \mathbb{N}$, \textbf{se} $P(n)$ é verdadeira,
    \textbf{então} P(n + 1) é verdadeira.

    \item $P(n)$ não é verdadeira para nenhum valor de $n \in \mathbb{N}$.

  \end{enumerate}

\end{exercise}


\begin{exercise}

  Mostre, por indução, a validade das fórmulas abaixo:
  \begin{enumerate}

    \item \(\displaystyle 1\cdot 2^0 + 2\cdot 2^1 + 3\cdot 2^2 + \cdots
    + n\cdot 2^{n-1} = 1 + (n - 1)2^n \)

    \item \(\displaystyle \left(1 + \frac{1}{1}\right)
    \left(1 + \frac{1}{2}\right) \left(1 + \frac{1}{3}\right)^2
    \cdots \left(1 + \frac{1}{n - 1}\right)^{n - 1}
    = \frac{n^{n - 1}}{(n - 1)!} \)

    \item \(\displaystyle 1\cdot 1! + 2\cdot 2! + 3\cdot 3! + \cdots
    + n\cdot n! = (n + 1)! \)

  \end{enumerate}

\end{exercise}


\begin{exercise}

  Mostre que a equação abaixo é verdadeira para todo $n \in \mathbb{N},
  n \geq 1$.
  \[
    1 + \frac{1}{\sqrt{2}} + \frac{1}{\sqrt{3}} + \cdots + \frac{1}{\sqrt{n}}
    \leq 2\sqrt{n}
  \]

\end{exercise}


\begin{exercise}

  Mostre que a equação abaixo é verdadeira para todo $n \in \mathbb{N}, n \geq
  1$.
  \[
    2!\cdot 4!\cdot 6!\cdots (2n)!
    \geq ((n+1)!)^n
  \]
  Onde $x!$ denota a fatorial de $x$.

\end{exercise}


\begin{exercise}

  Mostre que a equação abaixo é verdadeira para todo $n \in \mathbb{N}, n \geq
  1$.
  \[
    \underbrace{
      \sqrt{2 + \sqrt{2 + \sqrt{2 + \cdots \sqrt{2}}}}
    }_{\times n}
    = 2\cos\frac{\pi}{2^{n+1}}
  \]

\end{exercise}


\begin{exercise}

  Demonstre que qualquer valor postal maior ou igual a dois centavos pode ser
  obtido usando-se somente selos com valor de 2 e 3 centavos.

\end{exercise}


\begin{exercise}

  Demonstre, usando o princípio de indução forte, que todo número natural $n
  \geq 2$ é um número primo ou pode ser expresso como o produto de números
  primos.

\end{exercise}


\begin{exercise}

  Em qualquer grupo de $n$ pessoas, $n \in \mathbb{Z}^+$, cada uma deve apertar
  a mão de todas as outras pessoas. Encontre uma fórmula que forneça o número de
  apertos de mão, e demonstre que a fórmula é verdadeira, usando indução.

\end{exercise}


\begin{exercise}

  Escreva os cinco primeiros valores para as sequências (definidas por recursão)
  abaixo.
  \begin{enumerate}

    \item
    \(A(1) = 5\) \\
    \(A(n) = A(n - 1) + 5\), para $n > 1$

    \item
    \(B(1) = 1\) \\
    \(B(n) = \frac{1}{B(n - 1)}\), para $n > 1$

    \item
    \(C(1) = 1\) \\
    \(C(n) = C(n - 1) + n^2\), para $n > 1$

    \item
    \(S(1) = 1\) \\
    \(S(n) = S(n - 1) + \frac{1}{n}\), para $n > 1$

    \item
    \(T(1) = 1\) \\
    \(T(n) = n\cdot T(n - 1)\), para $n > 1$

    \item
    \(P(1) = 1\) \\
    \(P(n) = n^2 \cdot P(n - 1) + (n - 1)\), para $n > 1$

    \item
    \(M(1) = 2\) \\
    \(M(2) = 2\) \\
    \(M(n) = 2 M(n - 1) + M(n - 2)\), para $n > 2$

    \item
    \(D(1) = 3\) \\
    \(D(2) = 5\) \\
    \(D(n) = (n-1)D(n-1) + (n-2)D(n-2)\), para $n > 2$

    \item
    \(W(1) = 2\) \\
    \(W(2) = 3\) \\
    \(W(n) = W(n-1)\cdot W(n-2)\), para $n > 2$

    \item
    \(R(1) = 1\) \\
    \(R(2) = 2\) \\
    \(R(3) = 3\) \\
    \(R(n) = R(n-1) + 2R(n-2) + 3R(n-3)\), para $n > 3$

  \end{enumerate}

\end{exercise}


\begin{exercise}

  Suponha que a exponenciação é definida pela equação
  \[
    x^j \cdot x = x^{j+1}
  \]
  para qualquer $j \in \mathbb{Z}^+$. Use indução para provar que $x^n \cdot x^m =
  x^{n+m}$ para $m, n \in \mathbb{Z}^+$

\end{exercise}


\begin{exercise}

  Seja $F(n)$ o n-ésimo termo da série de Fibonacci, conforme definido no
  Exemplo~\ref{exmp:fib}. Mostre que as equações abaixo são verdadeiras para
  todo $n \in \mathbb{N}, n \geq 1$.

  \begin{enumerate}

    \item \(\displaystyle F(n-1)\cdot F(n+1) = {F(n)}^2 + (-1)^n\)

    \item \(\displaystyle {\sum_{i=0}^n F(i)^2} = F(n)\cdot F(n+1)\)

    \item \(\displaystyle F(1) + F(2) + \cdots + F(n) = F(n+2) - 1\)

    \item \(\displaystyle F(2) + F(4) + \cdots + F(2n) = F(2n+1) - 1\)

    \item \(\displaystyle F(n+3) = 2F(n+1) + F(n)\)

  \end{enumerate}

\end{exercise}


\begin{exercise}

  Uma cadeia de $0$s e $1$s deve ser processada e convertida para uma cadeia de
  paridade par acrescentando-se um bit de paridade no final da cadeia. O bit de
  paridade é um bit que faça com que o número total de bits $1$ da cadeia final
  seja par. Por exemplo:
  \begin{itemize}

    \item Para a cadeia $0010101$ o bit de paridade seira $1$, e a cadeia final
    seria $0010101\mathbf{1}$, com 4 bits $1$.

    \item Para a cadeia $0010010$ o bit de paridade seria $0$, e a cadeia final
    seria $0010010\mathbf{0}$, com 2 bits $1$.

  \end{itemize}
  O bit de paridade é inicialmente $0$. Quando um caracter $0$ é processado, o
  bit de paridade permanece inalterado. Quando um caracter $1$ é processado, o
  bit de paridade muda de $0$ para $1$ ou de $1$ para $0$.

  Prove que o número de $1$s numa cadeia final, incluindo o bit de paridade, é
  sempre par.\\
  (\emph{Sugestão:} considere várias possibilidades.)

\end{exercise}


\begin{exercise}
\label{exer:ind:t1}

  Sejam o consjunto $T = \{a, b\}$, e a linguagem $T_1$ sobre $T$ definida do
  seguinte modo:
  \begin{enumerate}[label={\roman*}.]

    \item $a \in T_1$

    \item Se $u,v \in T_1$, então $ubv \in T_1$.

  \end{enumerate}

  Considerando as definições acima, responda os itens abaixo:

  \begin{enumerate}

    \item Escreva 5 cadeias que pertencem a $T_1$.

    \item Se $w \in T_1$ é possível ter dois $a$'s consecutivos em $w$?
    Demonstre a sua resposta usando indução.

    \item Demonstre que se $w \in T_1$ o número de $a$'s em $w$ é igual ao
    número de $b$'s mais 1.

  \end{enumerate}

\end{exercise}


\begin{exercise}

  Sejam o consjunto $T = \{a, b\}$, e a linguagem $T_2$ sobre $T$ definida do
  seguinte modo:
  \begin{enumerate}[label=\roman{*}.]

    \item $a \in T_2$

    \item Se $w \in T_2$, então $wbw \in T_2$.

  \end{enumerate}

  Considerando as definições acima, responda os itens abaixo:

  \begin{enumerate}

    \item Escreva 4 cadeias que pertencem a $T_2$.

    \item Dê 3 exemplos de cadeias que pertencem a $T_1$
    (Exercício~\ref{exer:ind:t1} mas não pertencem a $T_2$.

    \item Se $w \in T_2$ é possível ter dois $b$'s consecutivos em $w$?
    Demonstre a sua resposta usando indução.

    \item Demonstre que, se $w \in T_2$, o número de $a$'s em $w$ é igual ao
    número de $b$'s mais 1.

    \item Demonstre que, se $w \in T_2$, então o número de $a$'s em $w$ é igual
    a $2^n$ para algum número natural $n$.

  \end{enumerate}

\end{exercise}


%%% Local Variables:
%%% coding: utf-8
%%% mode: latex
%%% TeX-command-default: "LaTeX"
%%% TeX-master: "../ilm-book"
%%% End:
