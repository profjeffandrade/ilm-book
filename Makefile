# ----------------------------------------------------------------------------
# Makefile for “Introdução à Lógica Matemática para Alunos de Informática”
.PHONY: FORCE_MAKE

SHELL=/bin/bash
VC=./vc -f -m
MAIN=ilm-book.tex
BOOK=$(MAIN:.tex=.pdf)
TARGETS=vc.tex ilm-book.pdf ilm-cover.pdf
LATEXMK=latexmk	-pdflatex=lualatex -pdf -shell-escape
OPTS=-bibtex

# -----------------------------------------------------------------------------
# Rules

all:	$(TARGETS)

%.pdf: %.tex FORCE_MAKE
	$(LATEXMK) $(OPTS) $<

vc.tex: vc vc-git.awk .git/HEAD
	$(VC)

view:	preview

preview: ${BOOK}
	latexmk -pv -view=pdf ${MAIN}

clean:
	latexmk -c

cleanall:
	latexmk -C
