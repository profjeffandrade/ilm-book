\section{Dedução no Cálculo de Predicados}
\label{sec:pl-deduction}

Da mesma forma que ocorre com o cálculo proposicional, o objetivo do cálculo de
predicados é permitir que argumentos sejam representados de modo formal, através
da linguagem simbólica da lógica. Como vimos na \autoref{sec:pred-intro}, o
cálculo de predicados busca trazer mais poder de expressividade para a lógica,
de modo que conceitos e relações que não podem (ou que são muito difíceis de)
ser representados no cálculo proposicional possam passar a ser representados.

Apesar de estarmos lidando com um tipo diferente de lógica, com um poder
expressivo maior, o conceito de dedução, ou de modo mais geral, de
\emph{argumeto} não muda em relação ao que foi apresentado para o cálculo
proposicional:

\begin{quotation}
  A forma geral de um argumento em uma linguagem natural consistem em apresentar
  premissas para fundamentar uma conclusão. Em um argumento dedutivo típico, as
  premissas se destinam fornecer uma garantia da validade da conclusão, enquanto
  em argumentos indutivos se considera que elas fornecem razões para apoiar a
  provável validade da conclusão.
\end{quotation}

Assim, o que queremos é entender que regras, ou que condições, são suficientes
para termos certeza que um determinado conjunto de premissas justifica a nossa
confiança de que a conclusão é válida.


\subsection{Argumentos Válidos}
\label{sec:pl-valid-argument}

O conceito de \emph{consequência lógica} para o cálculo de predicados, mantém a
mesma ideia que tem para o cálculo proposicional, ou seja, uma conclusão é
consequência lógica de um conjunto de premissas, se esse conjunto de premissas
acarreta essa conclusão, ou seja, se o conjunto de premissas fornece ``motivos''
suficientes para justificar a validade da conclusão.

Entretanto, no cálculo de predicados não falamos mais em funções de valoração, e
sim em interpretações, por isso a definição formal de consequência lógica
precisa ser atualizada.

\vspace{0.75\baselineskip}

\begin{defn}[Consequência Lógica para Cálculo de Predicados]
  \label{def:conseq-logica-calc-pred}
  Dizemos que uma \emph{fórmula bem formada} $\beta$ do cálculo de predicados é
  uma \textbf{consequência lógica} de um conjunto de fórmulas $\Gamma =
  \{\alpha_1, \alpha_2, \ldots, \alpha_n\}$, quando o conjunto de premissas
  $\Gamma$ \textbf{acarreta} a conclusão $\beta$. Existem dois tipos de
  consequência lógica:

  \begin{description}
  \item[Consequência Semântica] Dizemos que a fórmula $\beta$ é uma
    \emph{consequência semântica} do conjunto de fórmulas $\Gamma = \{\alpha_1,
    \alpha_2, \ldots, \alpha_n\}$, escrito
    \[
      \alpha_1, \alpha_2, \ldots, \alpha_n \vDash \beta
    \] se e somente se não existir nenhuma \textbf{interpretação} para as que
    faça todas as fórmulas $\alpha_i \in \Gamma$ serem verdadeiras mas que faça
    $\beta$ ser falsa.

  \item[Consequência Sintática] Dizemos que a fórmula $\beta$ é uma
    \emph{consequência sintática} do conjunto de fórmulas $\Gamma = \{\alpha_1,
    \alpha_2, \ldots, \alpha_n\}$, escrito
    \[
      \alpha_1, \alpha_2, \ldots, \alpha_n \vdash \beta
    \] se e somente se não existir existir uma ``prova'' (por algum sistema de
    prova) de que dadas as premissas $\alpha_1, \alpha_2, \ldots, \alpha_n$ é
    possível derivar a fórmula $\beta$.
  \end{description}
\end{defn}

Uma vez que o número de interpretações para um dado argumento formal é infinito,
não é trivial demonstrar que uma dada conclusão é consequência semântica das
premissas. Por esse motivo, o estudo de consequência lógica na lógica de
predicados de primeira ordem costuma se focar principalmente no estudo de
consequências sintáticas. O sistema de provas que estudaremos para o cálculo de
predicado será, a exemplo do que fizemos para o cálculo proposicional, o Sistema
de Dedução Natural.


\subsection{Dedução Natural}
\label{sec:pl-natdec}

Uma forma possível de se pensar as proposições da lógica proposicional é
visualizá-las como predicados de aridade zero na lógica de primeira ordem. Se
assumirmos essa visão, não é difícil verificar que toda a lógica proposicional
pode ser representada através da lógica de primeira ordem. Assim, se o nosso
sistema de prova deve preservar validade, tudo o que era válido para o cálculo
proposicional deve continuar válido para o cálculo de predicados.

Mais especificamente, no sistema de dedução natural para o cálculo de predicados
continuam válidas as regras de inferência básicas apresentadas
na~\autoref{tab:basic-inf-rules}, ou seja, \emph{modus ponens}, \emph{modus
  tolens}, \emph{conjunção}, \emph{simplificação}, \emph{adição} e
\emph{reapresentação}, bem como todas as regras de inferência derivadas
apresentadas na~\autoref{tab:deriv-inf-rules}. Também continuam válidos o uso de
\emph{sub-provas} e do \emph{método dedutivo} (ou exportação).

Entretanto, apenas repetir as regras de inferência do sistema de dedução natural
do cálculo proposicional não é suficiente para construir um sistema de dedução
natural para o cálculo de predicados. Precisamos de regras de inferência que
sejam capazes de lidar com quantificadores, regras que sejam capazes de inserir
ou de remover quantificadores nas fórmulas. Evidentemente, essas regras não
podem permitir a inserção ou remoção indiscriminada dos quantificadores, haverá
algumas restrições, mas vamos ver as restrições quando falarmos de cada regra.
Existem quatro regras para lidar com quantificadores:

\begin{description}
\item[Particularização Universal:] permite remover o quantificador universal de
  uma fórmula.
\item[Generalização Universal:] permite inserir o quantificador universal à
  frente de uma fórmula.
\item[Generalização Existencial:] permite inserir o quantificador existencial à
  frente de uma fórmula.
\item[Particularização Existencial:] permite remover o quantificar existencial
  de uma fórmula.
\end{description}

Discutiremos mais detalhadamente estas regras nas seções abaixo.


\subsubsection{Particularização Universal}

Nós podemos pensar no quantificador universal ($\forall$) como uma generalização
da conjunção ($\land$). Imagine que nosso domínio seja o conjunto $\{1, 2, 3,
4\}$. Considerando este domínio, a fórmula $\forall x.P(x)$ tem exatamente o
mesmo significado que $P(1) \land P(2) \land P(3) \land P(4)$. De modo
semelhante, podemos considerar a regra de inferência da dedução natural para
remover o quantificador universal, i.e., a \emph{particularização universal},
como uma generalização da regra de inferência para remover a conjunção.

Na regra de inferência de simplificação, se temos uma fórmula no formato $\alpha
\lor \beta$, escolhemos uma das duas partes e a introduzimos como parte do
argumento. Na regra de inferência de \emph{particularização universal} faremos
algo parecido. Se temos uma fórmula no formato $\forall x.P(x)$, iremos
``escolher'' qualquer termo $t$ válido no nosso domínio (que seja livre em $x$)
e introduzir a fórmula $P(t)$, como parte do argumento. Esta regra é
representada baixo:
\[
  \frac{\forall x.\phi}{\phi[t/x]} \qquad \text{(pu)}
\]
A notação ``$\phi[t/x]$'' significa a fórmula obtida substituindo-se todas as
ocorrências livres de $x$, na fórmula $\phi$, por $t$.


\begin{exmp}
  Demonstre o argumento formal %
  \(\displaystyle \forall_x.(H(x) \to M(x)), H(s) \vdash M(s)\).

  \begin{math}
    \begin{nd}
      \hypo {1} {\forall x.(H(x) \to M(x))} \by{hip}{}
      \hypo {2} {H(s)}                      \by{hip}{}
      \have {3} {H(s) \to M(s)}             \by{pu [s/x]}{1}
      \have {4} {M(s)}                      \by{mp}{2,3}
    \end{nd}
  \end{math}\\
  $\qed$
\end{exmp}


\subsubsection{Generalização Universal}

Inserir o quantificador universal em uma fórmula não é tão simples quanto
removê-lo. Se usarmos uma estratégia análoga à que usamos para a
particularização universal e tentarmos buscar uma generalização da regra de
inferência que insere conjunções, teremos um problema. A regra de inferência que
insere conjunções é a seguinte:
\[
  \frac{\alpha, \beta}{\alpha\land\beta} \qquad \text{(conj)}
\]
Ou seja, generalizando esta regra de inferência, teríamos que ter $P(x)$ provado
para todos os valores de $x$ possíveis para que pudéssemos escrever
$\forall_x.P(x)$. Como o número de interpretações possíveis é infinito e os
domínios dessas interpretações também podem ser infinitos, é impossível provar
explicitamente uma fórmula para todos os valores do domínio.

Mas nem tudo está perdido. Existem outras formas pelas quais nós realizamos
prova que são válidas para todos os valores. A título de exemplo, considere o
teorema abaixo e sua prova.

\begin{description}
\item[Teorema:] Todo número inteiro positivo par é a soma de dois números
  inteiros positivos ímpares cuja diferença é no máximo 2.

\item[Prova:] Seja $n$ um número inteiro positivo par qualquer. Como $n$ é par,
  então $n = 2k$ para algum $k$ inteiro positivo.
  \begin{itemize}
  \item Se $k$ é ímpar, então $n = k + k$, e os 2 $k$'s satisfazem ao teorema.
  \item Se $k$ é par, então $n = (k-1) + (k+1)$, e os números $k-1$ e $k+1$
    satisfazem ao teorema.
  \end{itemize}
  $\qed$
\end{description}

O teorema acima, e a sua prova, são válidos para todos os números inteiros
positivos pares, mesmo que nós não tenhamos realizado a prova para todos eles
--- o que seria impossível.

Se olharmos para esta prova, ela considera um número inteiro positivo par
arbitrário $n$, e demonstra que ele pode ser representado como a soma de dois
números inteiros positivos ímpares cuja diferença é no máximo 2. O que torna a
prova genérica é o fato de não haver \textbf{nenhuma} propriedade especial sobre
$n$, isto é, ele não aparece na declaração do teorema ou em qualquer outro lugar
fora da prova. Não fizemos nenhuma suposição sobre $n$ -- exceto a de que ele é
um valor de interesse para o teorema (inteiro positivo par). Como não existe
nenhuma propriedade ou característica que torne $n$ diferente de qualquer outro
número, a prova que construímos para $n$ pode ser considerada válida para
qualquer, e todos, os números.

A estratégia de prova exemplificada acima sugere que para provar uma fórmula da
forma $\forall_x.\phi$, podemos provar a fórmula $\phi$ para alguma variável
arbitrária, porém \textbf{nova}, $x_0$ no lugar de $x$. Ou seja, provamos a
fórmula $\phi[x_0/x]$, onde $x_0$ é uma variável nova. A representação da regra
de \emph{generalização universal} é dada abaixo:

\[
  \frac{
    \begin{minipage}{3cm}
      \begin{math}
        \begin{nd}
          \open[x_0]
          \have[~] {1} {~}
          \have[~] {2} {\vdots}
          \have[~] {3} {\phi[x_0/x]}
          \close
        \end{nd}
      \end{math}
    \end{minipage}
  }{\forall x.\phi} \qquad \text{(gu)}
\]

A expressão ``variável nova'' significa que é uma variável que não foi utilizada
em nenhum ponto anterior da prova, mas também que ela não será usada depois que
$\phi[x_0/x]$ tiver sido provado. Ela é uma variável ``local'' para esta parte
da prova. Para evidenciar o uso local dessas variáveis usaremos o mesmo tipo de
marcação que usamos em sub-provas, porém com o nome da variável escrito no
início do bloco da sub-prova.

\begin{exmp}
  Demonstre o argumento: %
  \(\forall x.(P(x) \lor Q(x)), \forall x.(\neg P(x)) \vdash \forall x.Q(x)\).

  \begin{math}
    \begin{nd}
      \hypo {1} {\forall x.(P(x) \lor Q(x))} \by{hip}{}
      \hypo {2} {\forall x.(\neg P(x))}      \by{hip}{}
      \open[x_0]
      \have {3} {P(x_0) \lor Q(x_0)} \by{pu [\(x_0/x\)]}{1}
      \have {4} {\neg P(x_0)}        \by{pu [\(x_0/x\)]}{2}
      \have {5} {Q(x_0)}             \by{sd}{3,4}
      \close
      \have {6} {\forall x.Q(x)}     \by{ge [\(x/x_0\)]}{3-5}
    \end{nd}
  \end{math}\\
  $\qed$
\end{exmp}


\subsubsection{Generalização Existencial}

Assim como existe um paralelo entre o quantificador universal e a conjunção,
também existe um paralelo entre o quantificador existencial ($\exists$) e a
disjunção ($\lor$). Consideremos novamente como exemplo o domínio $\{1, 2, 3,
4\}$. Neste domínio a fórmula $\exists x.P(x)$ tem exatamente o mesmo
significado de $P(1) \lor P(2) \lor P(3) \lor P(4)$. Ou seja, o quantificador
existencial funciona como se fosse uma ``grande disjunção'' abrangendo todos os
valores do domínio.

Para introduzir o operador de disjunção no sistema de dedução natural da lógica
proposicional, temos a seguinte regra de inferência:
\[
  \frac{\phi}{\phi\lor\psi}
  \qquad
  -~\text{ou}~-
  \qquad
  \frac{\psi}{\phi\lor\psi}
\]
Ou seja, dado que provamos uma fórmula, $\phi$, por exemplo, podemos fazer a
disjunção dela com qualquer outra. Isso nos leva a concluir que, se temos que
uma fórmula é verdadeira para um dos valores do domínio, podemos simplesmente
inserir o quantificador existencial, pois o efeito é o mesmo que o de uma
``grande disjunção''. O que nos leva à seguinte regra para a \emph{generalização
  existencial}:
\[
  \frac{\phi[t/x]}{\exists x.\phi} \qquad \text{(ge)}
\]

\begin{exmp}
  Demonstre o argumento: %
  \(A(s) \land B(s), \forall x.(A(x) \land B(x) \to C(x)) %
  \vdash \exists x.(A(x) \land C(x))\)

  \begin{math}
    \begin{nd}
      \hypo {1} {A(s) \land B(s)} \by{hip}{}
      \hypo {2} {\forall x.(A(x) \land B(x) \to C(x))} \by{hip}{}
      \have {3} {A(s) \land B(s) \to C(s)} \by{pu [s/x]}{2}
      \have {4} {C(s)} \by{mp}{1,3}
      \have {5} {A(s)} \by{simp}{1}
      \have {6} {A(s) \land C(s)} \by{conj}{5,4}
      \have {7} {\exists x.(A(x) \land C(x))} \by{ge [x/s]}{6}
    \end{nd}
  \end{math}\\
  $\qed$
\end{exmp}


\subsubsection{Particularização Existencial}

Para remover o quantificador existencial, entretanto, temos um pouco mais de
dificuldade. O fato da fórmula $\exists x.P(x)$ ser verdadeira, garante que
existe algum valor de $x$ que faz $P(x)$ ser verdadeiro. Entretanto, não sabemos
qual é esse valor e não podemos arbitrar um valor qualquer que já tenha
aparecido na prova como sendo ``o escolhido''.

O que fazemos então é criar uma sub-prova onde ``instanciamos'' a fórmula, ou
seja, para a fórmula $\exists x.\phi$, criaremos uma variável \textbf{nova}
$x_0$, e abriremos uma sub-prova com a fórmula $\phi[x_0/x]$. Novamente, a
variável $x_0$ será uma variável local à sub-prova, e o desenvolvimento da
sub-prova não deve ser ``vazado'' para fora dela, mas a conclusão da sub-prova,
i.e., a última fórmula acrescentada à sub-prova -- e que não deve conter a
variável $x_0$, pode ser usada no restante da prova. Esse mecanismo constitui a
regra de inferência de \emph{particularização existencial}, e é representado
abaixo:

\[
  \frac{\exists x.\phi,
    \begin{minipage}{3cm}
      \begin{math}
        \begin{nd}
          \open[x_0]
          \have[~] {1} {\phi[x_0/x]}
          \have[~] {2} {\vdots}
          \have[~] {3} {\Theta}
          \close
        \end{nd}
      \end{math}
    \end{minipage}
  }{\Theta} \qquad \text{(pu)}
\]

\begin{exmp}
  Demonstre o argumento: %
  \(\forall x.(P(x) \lor Q(x)), \exists x.(\neg P(x)) %
  \vdash \exists x.Q(x)\)

  \begin{math}
    \begin{nd}
      \hypo {1} {\forall x.(P(x) \lor Q(x))} \by{hip}{}
      \hypo {2} {\exists x.(\neg P(x))} \by{hip}{}
      \open[x_0]
      \hypo {3} {\neg P(x_0)} \by{sup}{}
      \have {4} {P(x_0) \lor Q(x_0)} \by{pu [\(x_0/x\)]}{1}
      \have {5} {Q(x_0)} \by{sd}{3,4}
      \have {6} {\exists x.Q(x)} \by{ge [\(x/x_0\)]}{5}
      \close
      \have {7} {\exists x.Q(x)} \by{pe}{2,3-6}
    \end{nd}
  \end{math}
\end{exmp}


\input{pred-logic/fol-deduction-exe.tex}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../ilm-book"
%%% ispell-local-dictionary: "brasileiro"
%%% TeX-engine: luatex
%%% End:
