\section{Raciocínio na Lógica de Predicados}
\label{sec:pl-reasoning}

Que tipo de racínio a lógica de predicados deve permitir? Para
ter uma ideia de como responder a esta pergunta vamos considerar o
argumento a seguir.

\begin{equation}
  \label{arg:2}
  \parbox{\textwidth-2cm}{\textit{Nenhum livro é líquido. Dicinários são
      livros. Portanto, nenhum dicionário é líquido.}}
\end{equation}

Os predicados que nós escolhemos são:

\begin{description}
\item[$L(x)$:] $x$ é um livro
\item[$Q(x)$:] $x$ é líquido
\item[$D(x)$:] $x$ é um dicionário.
\end{description}

Com os predicados acima é possível traduzir o argumento~\eqref{arg:2}
como:
\[
  \neg\exists x\left(L(x) \land Q(x)\right), 
  \forall x\left(D(x) \to L(x)\right)
  \vdash \neg\exists x\left(D(x) \land Q(x)\right)
\]

Evidentemente, nós precisamos construir uma teoria de provas que nos
permita demonstrar a validade do argumento formal acima. Ou seja,
verificar que o argumento formal realmente expressa o
argumento~\eqref{arg:2} em uma forma simbólica. 

A lógica de predicados estende a lógica proposicional não apenas com
quantificadores, mas também com o conceito de \emph{símbolos
  funcionais}. Considere a seguinte sentença:

\begin{equation}
  \label{sttm:3}
  \parbox{\textwidth-2cm}{\textit{Toda criança é mais jovem do que sua mãe.}}
\end{equation}

Usando os predicados
\begin{description}
\item[$C(x)$:] $x$ é uma criança
\item[$M(x,y)$:] $x$ é a mãe de $y$
\item[$J(x,y)$:] $x$ é mais jovem que $y$
\end{description}
podemos traduzir a sentença~\eqref{sttm:3} como
\[
  \forall x\left(
    C(x) \to \forall y \left(
      M(y,x) \to J(x,y)
    \right)
  \right)
\]
Que, numa tradução reversa literal, significa ``Para todo $x$, se $x$
é uma criança, então para todo $y$ se $y$ é a mãe de $x$, então $x$ é
mais jovem do que $y$.'' Não é muito elegante dizer ``para todas as
mães de $x$'', uma vez que nós sabemos que cada indivíduo possui uma e
apenas uma mãe.\footnote{Estamos assumindo que a sentença fala de mãe
  biológica, não mãe adotiva, madrasta, etc.} A deselegância de se
codificar `mãe' como um predicado se torna ainda mais evidente se
considerarmos a seguinte sentença:

\begin{equation}
  \label{eq:2}
  \parbox{\linewidth-2cm}{\textit{Ana e Paulo possuem a mesma avó materna.}}
\end{equation}

Assumindo as constantes $a$ para Ana e $p$ para Paulo, e os mesmos
predicados da sentença~\eqref{sttm:3}, podemos traduzir a
sentença~\eqref{eq:2} como:
\[
  \forall x\forall y\forall u\forall v\left( 
    M(x,y) \land M(y,a) \land M(u,v) \land M(v,p) \to x = u
  \right)
\]

Esta formula diz que, se $y$ é a mãe de Ana e $x$ é a mãe da mãe de
Ana, e se $v$ é a mãe de Paulo e se $u$ é a mãe da mãe de Paulo, então
$x$ e $u$ são a mesma pessoa. Note que nós usamos um predicado
especial da lógica de primeira ordem, a \emph{igualdade}; que é um
predicado binário e é escrito como o símbolo ${}={}$. Diferentemente
do outros predicados, a igualdade é escrita entre os argumento, então
escrevemos $x = y$ ao invés de escrever $=\!\!(x,y)$.

Os símbolos funcionais da lógica de predicados oferecem uma forma de
evitar essa codificação deselegante, uma vez que eles permitem
representar a mãe de $y$ de forma mais direta. Ao invés de escrever
$M(x,y)$ para representar que $x$ é a mãe de $y$, nós simplesmente
escrevemos $m(y)$ para designar a mãe de $y$. O símbolo $m$ é um
símbolo funcional: ele recebe um argumento e retorna a mãe deste
argumento. Usando o símbolo $m$, as sentenças \eqref{sttm:3} e
\eqref{eq:2} possuem codificações bem mais simples do que as vistas
anteriormente. A sentença ``Toda criança é mais jovem que sua mãe.''
agora pode ser codificada como:
\[
  \forall x \left(C(x) \to J(x,m(x)) \right)
\]
Note que foi necessária apenas uma variável (e um quantificador) ao
invés de dois. E a sentença ``Ana e Paulo possuem a mesma avó
materna'', recebe uma codificação ainda mais simples, podendo ser
escrita como:
\[
  m(m(a)) = m(m(p))
\]

Sempre é possível fazer as codificações sem recorrer a símbolos
funcionais, utilizando variáveis e predicados como visto
anteriormente. Entretanto, o uso de símbolos variáveis geralmente leva
a formas mais limpas e sucintas de codificação. Apesar disso, só podem
ser usados em situações onde nós queremos designar uma única entidade
(elemento) do domínio. Acima, foi possível usar o símbolo $m$ para
designar ``mãe'' devido ao fato de todo indivíduo possuir uma mãe
unicamente identificada, de modo que podemos falar da mãe de $y$ sem
correr o risco de gerar qualquer ambiguidade. Por esta razão, não
podemos ter um símbolo funcional $i(\cdot)$ para ``irmão''. Pode não
fazer sentido falar do irmão de $x$, pois $x$ pode não ter irmãos ou
ter vários irmãos. Assim, ``irmão'' tem que ser codificado como um
predicado binário.

Para exemplificar melhor este ponto, se Maria tem vários irmão, então
a sentença ``Ana gosta do irmão de Maria'' é ambígua pois não
especifica de qual dos irmão de Maria estamos falando. Por outro lado,
a sentença ``Ana gosta de um dos irmãos de Maria'' está correta e pode
ser codificada como:
\[
  \exists x \left( I(x,m) \land G(a,x) \right)
\]
onde, $I(x,y)$ significa ``$x$ é irmão de $y$'', $G(x,h)$ significa
``$x$ gosta de $y$'', e $a$ e $m$ significam Ana e Maria,
respectivamente. Alternativamente, a sentença ``Ana gosta de todos os
irmão de Maria'', pode ser codificada como:
\[
  \forall x \left( I(x,m) \to G(a,x) \right)
\]

Diferentes símbolos funcionais podem ter diferentes números de
argumentos. Funções podem inclusive ter zero argumentos, e neste caso
são chamadas de \textbf{constantes}. Os símbolos $a$ e $m$ nos
exemplos acima são constantes representando Ana e Maria,
respectivamente. Em um domínio envolvendo estudantes, cursos e notas,
podemos ter uma função binária $n(x,y)$ que designa a nota obtida pelo
estudante $x$ no curso $y$.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../ilm-book"
%%% ispell-local-dictionary: "brasileiro"
%%% End:
