# Introdução à Lógica Matemática para Alunos de Informática


## Dependências de Compilação 

Existe um arquivo `Makefile` que automatiza o processo de compilação do arquivo.
Além disso, para resolver algumas dependências de geração dos arquivos
auxiliares, existe um arquivo chamado `latexmkrc`, que define estas referências
e como tratá-las.

Para que o processo `make` possa ser executado com sucesso são necessário que os
seguintes programas estejam instalados no sistema:
- make (Dann...)
- Latexmk
- xindy
- bzr

<!-- Local Variables: -->
<!-- ispell-local-dictionary: "brasileiro" -->
<!-- coding: utf-8-unix -->
<!-- End: -->
