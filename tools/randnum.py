# Conjunto de funções em Python para geração de número randômicos
# Jefferson O. Andrade, 2013-11-18.

from functools import reduce
from random import randint, choice

def mkstr(lst):
    elems = [str(x) for x in lst]
    lstr = reduce(lambda a,b: a+b, elems)
    return lstr

def randseq(charList, size):
    digs = [choice(charList) for _ in range(size)]
    seq = mkstr(digs)
    return seq

def randnum(digits, minsize, maxsize):
    size = randint(minsize, maxsize)
    num = randseq(digits, size)
    return num

def randbin(minsize, maxsize):
    return randnum(['0', '1'], minsize, maxsize)

def randquad(minsize, maxsize):
    return randnum(['0', '1', '2', '3'], minsize, maxsize)

def randoctal(minsize, maxsize):
    octdigits = ['0', '1', '2', '3', '4', '5', '6', '7']
    return randnum(octdigits, minsize, maxsize)

def randduodec(minsize, maxsize):
    hexdigits = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B']
    return randnum(hexdigits, minsize, maxsize)

def randhexa(minsize, maxsize):
    hexdigits = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F']
    return randnum(hexdigits, minsize, maxsize)
