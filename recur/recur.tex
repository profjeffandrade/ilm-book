\chapter{Definições Recursivas e Indução Estrutural}
\label{cha:recursive}

\minitoc

Se é difícil definir um objeto explicitamente, pode ser fácil definir
esse objeto em termos de si mesmo, i.e., o termo corrente poderia ser
definido a partir dos termos anteriores). Este processo é chamado de
recursão.

A recursão, também conhecida como \emph{estratégia de dividir para
  conquistar}, é um método que quebra um problema grande (difícil) em
parte menores e geralmente mais simples de resolver. Se você puder
demonstrar que qualquer problema grande pode ser subdividido em outros
menores, e que os menores problemas possíveis (i.e., aqueles que não
podem mais ser subdivididos) podem ser resolvidos, você tem um método
para resolver problemas de qualquer tamanho. Evidentemente, nós podemos
provar isso usando indução. A recursão pode ser usada para definir
seqüências, conjuntos, relações, funções, e vários outros tipos de
estruturas.

Vejamos um exemplo simples. Suponha que lhe foram dadas as coordenadas
dos vértices de um polígono simples, i.e., um polígono cujos vértices
são distintos e cujos lados não se intercruzam, e você quer dividir o
polígono em triângulos.  Se você puder escrever um programa que quebra
qualquer polígono grande (i.e., com quatro lados ou mais) em dois
polígonos menores, então você sabe que pode triangular o polígono
todo. Divide-se o polígono original (grande) em dois menores, e então
repetidamente aplica-se o processo para os polígonos menores obtidos no
passo anterior. Uma aplicação deste problema é o algoritmo para
``renderização'' de objetos em computação gráfica.


\section{Funções Definidas Recursivamente}

Seja $f$ uma função com o domínio $\mathbb{N}$. Para definir a função $f$ usamos
duas etapas:

\begin{enumerate}

  \item \emph{Caso Base}: Especifique o valor da função a zero, $f(0) = a$.
  (Para algum valor $a$.)

  \item \emph{Caso Recursivo}: Defina uma regra para encontrar o valor da função
  $f(n)$ a partir de $f(0)$, $f(1)$, \ldots, $f(n-1)$.

\end{enumerate}

Essa definição é chamada de \emph{definição recursiva} ou \emph{definição
indutiva}.


\begin{exmp}

  Se $f$ é definida recursivamente por
  \begin{eqnarray*}
    f(0)    & = & 3, \\
    f(n)  & = & 2f(n-1) + 3,\ \text{para $n > 0$}
  \end{eqnarray*}
  encontre $f(1)$, $f(2)$, $f(3)$ e $f(4)$.

  \noindent\textbf{Solução:} Pela definição de $f$.
  \begin{eqnarray*}
    f(1) & = & 2f(0) + 3 = {2 \cdot 3} + 3 = 6 + 3 = 9 \\
    f(2) & = & 2f(1) + 3 = {2 \cdot 9} + 3 = 18 + 3 = 21 \\
    f(3) & = & 2f(2) + 3 = {2 \cdot 21} + 3 = 42 + 3 = 45 \\
    f(4) & = & 2f(3) + 3 = {2 \cdot 45} + 3 = 90 + 3 = 93
  \end{eqnarray*}

\end{exmp}


\begin{exmp}

  Dê uma definição recursiva para $a^n$, onde $a \in \mathbb{R}$, $a \neq 0$ e
  $n \in \mathbb{N}$.

  \noindent\textbf{Solução:} Especificando as duas etapas de uma definição
  recursiva:
  \begin{enumerate}

    \item \emph{Caso Base}: Especificando o valor da função $a^n$ em zero:
    $a^0 = 1$.

    \item \emph{Caso Recursivo}: Definindo uma regra para calcular o valor da
    função em $n$, à partir dos valores para $0, 1, 2, \ldots, n-1$. A regra
    é $a^n = a \cdot a^{n-1}$.

  \end{enumerate}

  Poderíamos ter escrito o mesmo de modo mais compacto usando a notação abaixo:
  \[
    a^n =
    \left\{
      \begin{array}{ll}
        1,
        & \text{se $n = 0$}
        \\
        a \cdot a^{n-1},
        & \text{se $n > 0$}
      \end{array}
    \right.
  \]

\end{exmp}


\begin{exmp}

  Dar uma definição recursiva de $\displaystyle\sum_{k = 0}^n a^k$

  \noindent\textbf{Solução:} Usando as duas etapas:

  \begin{enumerate}

    \item \emph{Caso Base}: Especifique o valor da função em zero, $S(0) = a^0$.

    \item \emph{Caso Indutivo}: Defina a regra para encontrar o valor da função
    $S(n)$ a partir de $S(0)$, $S(1)$, \ldots, $S(n-1)$. A regra é $S(n) =
    S(n-1) + a^{n +1}$, para $n > 0$.

  \end{enumerate}

\end{exmp}


%%% --------------------------------------------------------------------
\input{recur/set-languages}


\section{Definição Indutiva de Conjuntos e Estruturas}

Conjuntos e outras estruturas, como listas e árvores, por exemplo, também podem
ser definidos recursivamente.

Para conjuntos, no caso base uma coleção inicial de elementos é especificada.
No caso recursivo  são dadas as regras para a formação de novos elementos a
partir dos antigos (aqueles que já se sabe estarem no conjunto).


\begin{exmp}

  Considere o conjunto $S \subset \mathbb{Z}$ definido por:

  \begin{enumerate}

    \item \emph{Caso Base}: $3 \in S$.

    \item \emph{Caso Indutivo}: Se $x \in S$ e $y \in S$, então $x + y \in S$.

  \end{enumerate}

  Assim, os elementos de $S$ são $3$, $6$, $9$, $12$, $15$, \ldots. Pois:
  \begin{eqnarray*}
    3 & \Leftarrow & \text{caso base}
    \\
    6 & \Leftarrow & 3 +3
    \\
    9 & \Leftarrow & 6+3, 3+6
    \\
    12 & \Leftarrow & 6+6, 3+9, 9+3
    \\
    15 & \Leftarrow & 3+12, 6+9, 9+6, 12+3
    \\
    \ldots
  \end{eqnarray*}

\end{exmp}


Seja $\Sigma$ um alfabeto, conforme Definição~\ref{def:alfabeto}.
Iremos agora dar uma definição indutiva de $\Sigma^{*}$ (o conjunto de todas
as palavras sobre $\Sigma$).


\begin{defn}[Conjunto $\Sigma^{*}$]
\label{def:sigma-star}

  Seja $\Sigma$ um alfabeto. O conjunto $\Sigma^{*}$, de todas as cadeias sobre
  $\Sigma$, é recursivamente definido por:

  \begin{enumerate}

    \item \emph{Caso base}: $\varepsilon \in \Sigma^{*}$, onde $\varepsilon$ é
    a cadeia vazia.

    \item \emph{Caso indutivo}: Seja $w$ uma palavra tal que $w \in \Sigma^{*}$
    e seja $x$ um símbolo tal que $x \in \Sigma$, então $wx \in \Sigma^{*}$.
    Onde $wx$ denota a cadeia formada acrescentando-se o símbolo $x$ ao final da
    palavra $w$.

  \end{enumerate}

\end{defn}


\begin{exmp}

  Se $\Sigma = \{0, 1\}$, então após a primeira aplicação da etapa indutiva, os
  elementos 0, 1 de $\Sigma^{*}$ são formadas. Pela aplicação da etapa indutiva
  uma vez mais, temos 00, 01, 10, 11 como os elementos de $\Sigma^{*}$.
  Aplicando a etapa indutiva uma terceira vez temos 000, 001, 010, 011, 100,
  101, 110 e 111, e assim por diante.

\end{exmp}


\begin{exmp}

  Define-se um conjunto $E$, recursivamente, como se segue:

  \begin{enumerate}

    \item\label{it:e1} $0 \in E$

    \item\label{it:e2} se $n \in E$, então $n + 2 \in E$

    \item\label{it:e3} se $n \in E$, então $-n \in E$

  \end{enumerate}

  Usando esta definição, podemos ver que uma vez que $0 \in E$ por
  \ref{it:e1}, segue-se a partir de \ref{it:e2} que $0 + 2 = 2 \in E$ e
  logo $2 + 2 = 4 \in E$, $4 + 2 = 6 \in E$, \ldots, na verdade qualquer número
  par não negativas pertence a E. Além disso, por \ref{it:e3}, $-2, -4, -6,
  \ldots \in E$.

  Existe alguma coisa mais em $E$? A definição não diz isso explicitamente, mas
  uma condição \emph{implícita} de uma definição recursiva é que a única maneira
  de as coisas pertencerem a $E$ é como consequência de \ref{it:e1},
  \ref{it:e2} ou \ref{it:e3}. Então, podemos afirmar que $E$ é
  exatamente o conjunto dos números inteiros pares.

\end{exmp}


\begin{exmp}

  Define-se um conjunto $S$, de cadeias de $a$'s e $b$'s (i.e., $S \subseteq
  \{a,b\}^{*}$) recursivamente como se segue:

  \begin{enumerate}

    \item\label{it:ab1} $\varepsilon \in S$, onde $\varepsilon$ é a string
    vazia,

    \item\label{it:ab2} se $u \in S$, então $aub \in S$,

    \item\label{it:ab3} se $u \in S$, então $bua \in S$,

    \item\label{it:ab4} se $u,v \in S$, então $u.v \in S$, onde $u.v$ denota
    a concatenação das cadeias $u$ e $v$.

  \end{enumerate}

  Usando esta definição, podemos ver que, uma vez que $\varepsilon \in S$ por
  \ref{it:ab1}, segue-se, a partir de \ref{it:ab2}, que $a \varepsilon b
  = ab \in S$, então $aabb \in S$, também por \ref{it:ab2}, e $baba \in S$
  por \ref{it:ab3}. Da mesma forma, $b \varepsilon a = ba \in S$ por
  \ref{it:ab3}, assim $abab \in S$ por \ref{it:ab2} e $bbaa \in S$ por
  \ref{it:ab3}. Além disso, uma vez que $ab \in S$ e $ba \in S$, temos $abba
  \in S$, bem como a $baab \in S$ por \ref{it:ab4}.

  Observe que cada cadeia em $S$ tem um número igual de $a$'s e $b$'s. É
  possível provar esta propriedade por indução sobre as regras que definem
  cadeias que pertencem a $S$.

\end{exmp}


Duas palavras (cadeias) podem ser combinadas pela operação de
\emph{concatenação}. A operação de concatenação é denotada por $u.v = w$, onde
$u$, $v$ e $w$ são palavras sobre um mesmo alfabeto $\Sigma$, ou seja, $u, v, w
\in \Sigma^{*}$, e esta operação indica que a palavra $w$ é formada por todos os
símbolos da palavra $u$ seguidos por todos os símbolos da palavra $v$. Por
exemplo, $acaca.ababa = acacaababa$.


\begin{defn}[Concatenação de Palavras]
\label{def:string-concat}

  Seja $\Sigma$ um conjunto de símbolos. Podemos definir a concatenação dos duas
  palavras recursivamente como se segue.

  \begin{enumerate}

    \item \emph{Caso base}: Se $w \in \Sigma^{*}$, então $w.\varepsilon = w$,
    onde $\varepsilon$ é a palavra vazia.

    \item \emph{Caso indutivo}: Se $w_1 \in \Sigma^{*}$, $w_2 \in \Sigma^{*}$,
    e $x \in \Sigma$, então $w_1.(w_2 x) = (w_1 . w_2)x$.

  \end{enumerate}

\end{defn}


\begin{exmp}

  Seja $\Sigma = \{a, b, c\}$, e sejam $w_1 = babaca$ e $w_2 = acaca$, então a
  concatenação das palavras $w_1$ e $w_2$, denotada por $w_1 . w_2$ pode ser
  determinada pela aplicação sucessiva da definição (indutiva) de concatenação
  de cadeias como se seque:
  \begin{eqnarray*}
  w_1 . w_2
    & = & babaca . acaca        \\
    & = & babaca . (acaca)      \\
    & = & (babaca . acac)a      \\
    & = & ((babaca . aca)c)a    \\
    & = & (((babaca . ac)a)c)a  \\
    & = & ((((babaca . a)c)a)c)a  \\
    & = & (((((babaca . \varepsilon)a)c)a)c)a \\
    & = & (((((babaca)a)c)a)c)a \\
    & = & ((((babacaa)c)a)c)a \\
    & = & (((babacaac)a)c)a \\
    & = & ((babacaaca)c)a \\
    & = & (babacaacac)a \\
    & = & babacaacaca
  \end{eqnarray*}

\end{exmp}


Uma propriedade de cadeias (palavras) que aparece de modo bastante intuitivo é
a propriedade de \emph{comprimento} da cadeia. O aluno provavelmente responderia
que o comprimento de $\varepsilon$ (cadeia vazia) é zero, e que o comprimento
da cadeia $abc$ é 3. Iremos agora formalizar esta propriedade através de uma
definição recursiva.


\newcommand{\strlen}{\,\mathrm{l}}

\begin{defn}[Comprimento de Palavra]
\label{def:string-length}

  Sejam $\Sigma$ um alfabeto. A função $\strlen: \Sigma^{*}~\to~\mathbb{N}$
  que mapeia cada palavra em $\Sigma^{*}$ no seu respectivo comprimento pode ser
  definida como:
  \begin{enumerate}

    \item \emph{Caso base:} $\strlen(\varepsilon) = 0$, onde $\varepsilon$ é a
    palavra vazia.

    \item \emph{Caso indutivo:} $\strlen(wx) = \strlen(w) + 1$, se $w \in
    \Sigma^{*}$ e $x \in \Sigma$.

  \end{enumerate}

\end{defn}


\begin{exmp}

  Sejam $\Sigma = \{a, b, c\}$ e $w_1 = babaca$. Usando a definição recursiva
  dada para o comprimento de palavras na Definição~\ref{def:string-length}
  podemos calcular o comprimento da palavra $w_1$.

  \begin{eqnarray*}
    \strlen(w_1)
      & = & \strlen(babaca)   \\
      & = & \strlen(babac) + 1  \\
      & = & \strlen(baba) + 1 + 1 \\
      & = & \strlen(bab) + 1 + 1 + 1 \\
      & = & \strlen(ba) + 1 + 1 + 1 + 1 \\
      & = & \strlen(b) + 1 + 1 + 1 + 1 + 1 \\
      & = & \strlen(\varepsilon) + 1 + 1 + 1 + 1 + 1 + 1 \\
      & = & 0  + 1 + 1 + 1 + 1 + 1 + 1 \\
      & = & 6
  \end{eqnarray*}

\end{exmp}


\section{Indução Estrutural}

Para provar resultados sobre conjuntos recursivamente definidos, pode-se usar
indução matemática e também pode-se usar uma forma mais conveniente de indução
conhecida como indução estrutural.

Uma demonstração usando indução estrutural tem a seguinte forma:

\begin{enumerate}

  \item \emph{Caso base:} Mostrar que o resultado é válido para todos os
  elementos especificados no caso base da definição recursiva do conjunto
  em questão.

  \item \emph{Caso indutivo:} Mostrar que, se a afirmação é verdadeira para
  cada um dos elementos utilizados para a construção de novos elementos no
  caso intutivo da definição do conjunto, então o resultado é válido para
  estes novos elementos.

\end{enumerate}


\begin{exmp}

  Use indução estrutural para provar que $\strlen(v.w) = \strlen(v) +
  \strlen(w)$, onde $v, w \in \Sigma^{*}$ e $l: \Sigma^{*} \to \mathbb{N}$ é a
  função de comprimento de palavras definida na
  Definição~\ref{def:string-length}.

  \noindent\textbf{Solução:} Seja $P(w): \strlen(v.w) = \strlen(v) +
  \strlen(w)$, onde $v, w \in \Sigma^{*}$. Sabemos que $\strlen(\varepsilon) =
  0$, e que $\strlen(wx) = \strlen(w) + 1$ quando $w \in \Sigma^{*}$ e $x \in
  \Sigma$.

  \begin{enumerate}

    \item \emph{Caso base}: Para $w = \varepsilon$, temos que mostrar que
    $P(\varepsilon)$ é verdadeiro, ou seja, temos que mostrar que
    \[
      \strlen(v.\varepsilon) = \strlen(v) + \strlen(\varepsilon)
    \]
    Pela definição de concatenação $v.\varepsilon = v$, e pela definição de
    comprimento $\strlen(\varepsilon) = 0$, logo:
    \begin{eqnarray*}
      \strlen(v.\varepsilon) & = & \strlen(v) + \strlen(\varepsilon) \\
      \strlen(v) & = & \strlen(v) + 0 \\
      \therefore\;
      \strlen(v) & = & \strlen(v)
    \end{eqnarray*}
    Que demonstra que o caso base é verdadeiro.

    \item \emph{Caso indutivo}: Supomos que $P(u)$ é verdadeiro para uma palavra
    $u \in \Sigma^{*}$ arbitrária. Devemos mostrar que esta suposição implica
    que $P(ux)$ é verdadeiro para qualquer $x \in \Sigma$. Ou seja, supomos que
    a equação $\strlen(v.u) = \strlen(v) + \strlen(u)$ é verdadeira
    (\emph{hipótese de indução}), e devemos mostrar que $\strlen(v.ux) =
    \strlen(v) + \strlen(ux)$ é verdadeira.
    \begin{eqnarray*}
      \strlen(v.ux) & = & \strlen(v) + \strlen(ux)
      \\
      \strlen(v.u) + 1 & = & \strlen(v) + \strlen(u) + 1
      \qquad\triangleright\text{pela definição de $l$}
      \\
      \strlen(v) + \strlen(u) + 1 & = & \strlen(v) + \strlen(u) + 1
      \qquad\triangleright\text{pela hipótese de indução}
    \end{eqnarray*}
    O que demonstra que, de fato, $\strlen(v.ux) = \strlen(v) + \strlen(ux)$.

  \end{enumerate}
  \qed

\end{exmp}


\newcommand{\revstr}{\,\mathrm{r}}

A operação \emph{reverso} aplicada a uma palavra (cadeia) mapeia esta cadeia em
uma outra palavra, sobre o mesmo alfabeto, que possui exatamente os mesmos
símbolos da cadeia original, mas na ordem inversa. Por exemplo,
$\revstr(babaca) = acabab$. Onde $\revstr(w)$ denota a operação reverso
aplicada à palavra $w$.


\begin{defn}[Reverso de Palavras]
\label{def:string-reverse}

  Seja $\Sigma$ um conjunto de símbolos e $\Sigma^{*}$ o conjunto de palavras
  formadas por símbolos $\Sigma$. Podemos definir $\revstr(w)$, o reverso de
  uma palavra $w$, como se segue.

  \begin{enumerate}

    \item Caso base: $\revstr(\varepsilon) = \varepsilon$.

    \item Caso indutivo: $\revstr(wx) = x \revstr(w)$, onde $x \in \Sigma$ e $w
    \in \Sigma^{*}$.

  \end{enumerate}

\end{defn}


\begin{exmp}

  Mostre, passo-a-passo, como é obtido o reverso da palavra $abacca$ de acordo
  com a operação para a operação reverso dada na Definição
  \ref{def:string-reverse}.

  \noindent\textbf{Solução:}
  \[
  \begin{array}{lclclcl}
    \revstr(abacca)
    & = & a \revstr(abacc)
    & = & ac \revstr(abac)
    & = & acc \revstr(aba)
    \\
    & = & acca \revstr(ab)
    & = & accab \revstr(a)
    & = & accaba \revstr(\varepsilon)
    \\
    & = & accaba
  \end{array}
  \]

\end{exmp}


\begin{exmp}

  Use indução estrutural para provar que \(\revstr(u.v) =
  \revstr(v).\revstr(u)\)

  \noindent\textbf{Solução:} É possível efetuar a demonstração de duas formas
  diferentes:
  \begin{enumerate}
    \item Considerar $u$ como uma palavra arbitrária e usar indução sobre $v$.
    \item Considerar $v$ como uma palavra arbitrária e usar indução sobre $u$.
  \end{enumerate}

  Optaremos por seguir a primeira forma, i.e., usar indução sobre $v$. O aluno
  pode tentar refazer o exemplo, como exercício, usando indução sobre $u$.

  \begin{enumerate}

    \item \emph{Caso base:} \(
    \revstr(u.\varepsilon) = \revstr(\varepsilon).\revstr(u)
    \Longrightarrow
    \revstr(u) = \varepsilon.\revstr(u)
    \Longrightarrow\ 
    \therefore\ \revstr(u) = \revstr(u)
    \)

    \item \emph{Caso indutivo:} Supondo que $\revstr(u.v) =
    \revstr(v).\revstr(u)$ seja verdadeiro, desejamos mostrar que $\revstr(u.vx)
    = \revstr(vx).\revstr(u)$.
    \[
      \begin{array}{rcll}
      \revstr(u.vx)
      & =
      & x\revstr(u.v)
      & \triangleright\;\text{pela definição de $\revstr(wx)$}
      \\
      & =
      & x\revstr(v).\revstr(u)
      & \triangleright\;\text{pela hipótese de indução}
      \\
      & =
      & \revstr(vx).\revstr(u)
      & \triangleright\;\text{pela definição de $\revstr(wx)$}
    \end{array}
    \]
  \end{enumerate}
  \qed
\end{exmp}


\section{Exercícios}

\begin{exercise}

  Escreva os cinco primeiros valores para as sequências (definidas por
  recursão) abaixo.
  \begin{enumerate}

    \item
    \(A(1) = 5\) \\
    \(A(n) = A(n - 1) + 5\), para $n > 1$

    \item
    \(B(1) = 1\) \\
    \(B(n) = \frac{1}{B(n - 1)}\), para $n > 1$

    \item
    \(C(1) = 1\) \\
    \(C(n) = C(n - 1) + n^2\), para $n > 1$

    \item
    \(S(1) = 1\) \\
    \(S(n) = S(n - 1) + \frac{1}{n}\), para $n > 1$

    \item
    \(T(1) = 1\) \\
    \(T(n) = n\cdot T(n - 1)\), para $n > 1$

    \item
    \(P(1) = 1\) \\
    \(P(n) = n^2 \cdot P(n - 1) + (n - 1)\), para $n > 1$

    \item
    \(M(1) = 2\) \\
    \(M(2) = 2\) \\
    \(M(n) = 2 M(n - 1) + M(n - 2)\), para $n > 2$

    \item
    \(D(1) = 3\) \\
    \(D(2) = 5\) \\
    \(D(n) = (n-1)D(n-1) + (n-2)D(n-2)\), para $n > 2$

    \item
    \(W(1) = 2\) \\
    \(W(2) = 3\) \\
    \(W(n) = W(n-1)\cdot W(n-2)\), para $n > 2$

    \item
    \(R(1) = 1\) \\
    \(R(2) = 2\) \\
    \(R(3) = 3\) \\
    \(R(n) = R(n-1) + 2R(n-2) + 3R(n-3)\), para $n > 3$

  \end{enumerate}

\end{exercise}


\begin{exercise}

  Suponha que a exponenciação é definida pela equação
  \[
    x^j \cdot x = x^{j+1}
  \]
  para qualquer $j \in \mathbb{Z}^+$. Use indução para provar que $x^n
  \cdot x^m = x^{n+m}$ para $m, n \in \mathbb{Z}^+$

\end{exercise}

\begin{exercise}

  Uma cadeia de $0$s e $1$s deve ser processada e convertida para uma
  cadeia de paridade par acrescentando-se um bit de paridade no final da
  cadeia. O bit de paridade é um bit que faça com que o número total de
  bits $1$ da cadeia final seja par. Por exemplo:
  \begin{itemize}

  \item Para a cadeia $0010101$ o bit de paridade seira $1$, e a cadeia
    final seria $0010101\mathbf{1}$, com 4 bits $1$.

  \item Para a cadeia $0010010$ o bit de paridade seria $0$, e a cadeia
    final seria $0010010\mathbf{0}$, com 2 bits $1$.

  \end{itemize}
  O bit de paridade é inicialmente $0$. Quando um caracter $0$ é
  processado, o bit de paridade permanece inalterado. Quando um caracter
  $1$ é processado, o bit de paridade muda de $0$ para $1$ ou de $1$
  para $0$.

  Prove que o número de $1$s numa cadeia final, incluindo o bit de
  paridade, é
  sempre par.\\
  (\emph{Sugestão:} considere várias possibilidades.)

\end{exercise}


\begin{exercise}
  \label{exer:recur:1}

  Sejam o consjunto $T = \{a, b\}$, e a linguagem $T_1$ sobre $T$
  definida do seguinte modo:
  \begin{enumerate}[label={\arabic*}.]

    \item $a \in T_1$

    \item Se $u,v \in T_1$, então $ubv \in T_1$.

  \end{enumerate}

  Considerando as definições acima, responda os itens abaixo:

  \begin{enumerate}

    \item Escreva 5 cadeias que pertencem a $T_1$.

    \item Se $w \in T_1$ é possível ter dois $a$'s consecutivos em $w$?
    Demonstre a sua resposta usando indução.

    \item Demonstre que se $w \in T_1$ o número de $a$'s em $w$ é igual ao
    número de $b$'s mais 1.

  \end{enumerate}

\end{exercise}


\begin{exercise}

  Sejam o consjunto $T = \{a, b\}$, e a linguagem $T_2$ sobre $T$ definida do
  seguinte modo:
  \begin{enumerate}[label={\arabic*}.]

    \item $a \in T_2$

    \item Se $w \in T_2$, então $wbw \in T_2$.

  \end{enumerate}

  Considerando as definições acima, responda os itens abaixo:

  \begin{enumerate}

    \item Escreva 4 cadeias que pertencem a $T_2$.

    \item Dê 3 exemplos de cadeias que pertencem a $T_1$
    (Exercício~\ref{exer:recur:1}) mas não pertencem a $T_2$.

    \item Se $w \in T_2$ é possível ter dois $b$'s consecutivos em $w$?
    Demonstre a sua resposta usando indução.

    \item Demonstre que, se $w \in T_2$, o número de $a$'s em $w$ é igual ao
    número de $b$'s mais 1.

    \item Demonstre que, se $w \in T_2$, então o número de $a$'s em $w$ é igual
    a $2^n$ para algum número natural $n$.

  \end{enumerate}

\end{exercise}


%%% Local Variables:
%%% coding: utf-8
%%% mode: latex
%%% TeX-master: "../ilm-book"
%%% ispell-local-dictionary: "brasileiro"
%%% End:
