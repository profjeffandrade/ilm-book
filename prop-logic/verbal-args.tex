\section{Argumentos Verbais}
\label{sec:verbal-arguments}

%%% Texto adaptado de:
%%% http://web.utk.edu/~nolt/courses/logic.html

Na Seção~\ref{sec:informal-logic} falamos um pouco sobre argumentos informais e
dissemos que uma das formas de se trabalhar com argumentos é traduzi-los para
uma linguagem formal. Sendo assim, podemos verificar a validade de um argumento
em um processo de duas etapas:

\begin{enumerate}
\item Traduzir o argumento para a forma simbólica utilizando lógica
  proposicional, como visto na Seção~\ref{sec:proplog-prop-calc}.
\item Provar que o argumento é válido construindo uma sequência de demonstração
  para ele usando o sistema de dedução natural.
\end{enumerate}

Veja que, como foi dito na Seção~\ref{sec:informal-logic}, o processo de
preparação de um argumento informal, para que ele possa ser transformado em um
argumento formal pode exigir a introdução de \emph{premissas implícitas}, assim
como o descarte de ``ruído'' no argumento original. É uma tarefa que exige
prática e atenção.

Muitas vezes, ao tentar preparar um argumento, assumimos premissas implícitas
que não são adequadas. Essa é uma prática perigosa, pois pode nos levar a
concluir erroneamente que um argumento é válido, quando na verdade ele é
inválido. Veremos abaixo alguns exemplos de argumentos verbais inválidos.

\begin{exmp}
  Considere o argumento:
  
  \begin{quote}
    ``Sandy não é um homem. Portanto, Sandy é uma mulher.''
  \end{quote}

  À primeira vista, pode parecer que o argumento é válido, pois costuma-se
  assumir a premissa implícita de que ``Sandy é humano/a''. Entretanto, nada nos
  garante que esta premissa implícita seja válida.

  Assim, o argumento é \textbf{inválido}. Um contraexemplo para justificar a
  invalidade do argumento seria: ``Sandy é um hamster.''
\end{exmp}

\begin{exmp}
  \begin{quote}
    ``Se a TV está desconectada (da tomada), ela não funciona. A TV não está
    funcionando. Logo, a TV está desconectada.''
  \end{quote}

  O erro deste argumento é interpretar o condicional (se\ldots então\ldots) como
  se fosse um bicondicional (se e somente se\ldots). 

  \emph{Contraexemplo:} A TV está conectada, mas não funciona porque está com
  defeito.
\end{exmp}


\subsection{Indicadores de Argumentos}
\label{sec:args-indicators}

Definimos um argumento como uma sequencia de frases declarativas, i.e.,
proposições. Uma das quais deve ser uma conclusão; e as demais, as premissas, se
propõem a sustentar a conclusão. Consideramos agora as pistas gramaticais pelas
quais as pessoas comunicam tais intenções. As pistas mais importantes são os
indicadores de argumento, palavras ou frases que sinalizam a presença e indicam
a estrutura dos argumentos. Os indicadores se dividem em duas classes:
indicadores de premissa e indicadores de conclusão. Um indicador de premissa é
uma expressão como ``para'', ``desde'' e ``porque'', que conecta duas
afirmações, significando que uma é a premissa da qual a outra é inferida como
uma conclusão.Assim, por exemplo, na frase:

\begin{quote}
  ``A alma é indestrutível porque é indivisível.''
\end{quote}

O indicador de premissa ``porque'' indica que a afirmação ``é indivisível'' é
uma premissa que sustenta a conclusão de que ``a alma é indestrutível''.
Indicadores de premissa também podem ocorrer no início das frases, mas a regra
ainda se mantém: a declaração à qual o indicador de premissa está anexado é a
premissa; a outra é a conclusão. Assim, por exemplo, na frase:

\begin{quote}
  ``Como os números são não físicos, existem objetos não físicos.''
\end{quote}

A palavra ``como'' mostra que a afirmação ``números não são físicos'' é uma
premissa que leva à conclusão de que ``existem objetos não físicos''.

Os indicadores de conclusão são palavras ou frases que significam que a
afirmação a que estão ligadas é uma conclusão que se segue de premissas
previamente estabelecidas. Inglês é rico em indicadores de conclusão. Alguns dos
mais comuns são ``portanto'', ``assim'', ``assim'', ``por isso'', ``então'',
``segue-se que'', ``em conclusão'', ``em conformidade'' e ``consequentemente''.
No seguinte argumento, por exemplo, ``consequentemente'' indica que a terceira
afirmação ``a consciência existe'' é uma conclusão a partir dos dois primeiros:

\begin{quote}
  ``Sem consciência, não pode haver moralidade. Contudo a moralidade existe.
  Consequentemente a consciência existe.''
\end{quote}

Argumentos também podem ser declarados sem indicadores, nesses casos devemos
confiar em pistas mais sutis de contexto, entonação ou ordem para discernir sua
estrutura. Na maioria das vezes quando faltam indicadores de argumento, a
conclusão é dada em primeiro lugar, seguida pelas premissas. Aqui está um
exemplo:

\begin{quote}
  ``Não há verdade sem pensamento. A verdade é uma correspondência entre pensamento
  e realidade. E uma correspondência entre duas coisas não pode existir, a menos
  que as próprias coisas existam.''
\end{quote}

Aqui a primeira afirmação é uma conclusão das duas restantes.


\subsection{Tradução e Prova}
\label{sec:traducao-e-prova}

Ao traduzir um argumento verbal para a forma simbólica, o primeiro passo é
preparar o argumento e identificando as premissas, premissas implícitas e
conclusões. Em seguida fazemos a simbolização de cada um destes elementos,
criando uma chave de simbolização e à partir daí, escrevemos o argumento formal.
Em seguida, cria-se a sequência de demonstração para provar a validade do
argumentos.

\begin{exmp}
  Considere o seguinte argumento:
  \begin{quote}
    ``Se as taxas de juros caírem, o mercado imobiliário vai melhorar. %
    Ou a taxa federal de descontos vai cair, ou então o mercado imobiliário não
    vai melhorar. %
    As taxas de juros vão cair. %
    Portanto, a taxa federal de descontos vai cair.''
  \end{quote}

  Vamos considerar o argumento tal como foi dados, ou seja, sem premissas
  implícitas. Construímos a seguinte chave de simbolização:

  \begin{itemize}
  \item[J:] As taxas de juros vão cair.
  \item[I:] O mercado imobiliário vai melhorar.
  \item[F:] A taxa federal de descontos vai cair.
  \end{itemize}

  O argumento formal é:
  \[
    J \to I, F \lor \neg I, J \vdash F
  \]

  A sequência de demonstração para estabelecer a validade do argumento é:

  \begin{math}
    \begin{nd}
      \hypo {1} {J \to I} \by{hip}{}
      \hypo {2} {F \lor \neg I} \by{hip}{}
      \hypo {3} {J} \by{hip}{}
      \have {4} {\neg I \lor F} \by{comut}{2}
      \have {5} {I \to F} \by{cond}{3}
      \have {6} {J \to F} \by{sh}{1,5}
      \have {7} {F} \by{mp}{3,6}
    \end{nd}
  \end{math}\\
  $\qed$
\end{exmp}


\subsection{Exercícios}
\label{sec:exercicios-verbal-args}


\begin{exercise}
  Converta os argumentos verbais dados abaixo para argumentos formais, ou seja,
  converta as sentenças para a forma simbólica e identifique a conclusão
  (consequência lógica) indicada no argumento.

  \begin{enumerate}

  \item Ou esta pintura é de Rembrandt ou é de Vermeer. Não é de Rembrandt.
    Então, deve ser de Vermeer.

  \item Café e chá contêm cafeína. Então, o chá contém cafeína.

  \item Se o prêmio foi pago, o seguro está em vigor. Mas o prêmio não foi pago.
    Então, o seguro não está em vigor.

  \item Se as pessoas podem viver em Vênus, então elas podem viver em Marte. Se
    eles podem viver em Marte, então eles podem viver em Júpiter. Portanto, se
    as pessoas podem viver em Vênus, elas podem viver em Júpiter.

  \item A casa será vendida até agosto, ou não será vendida este ano. Não será
    vendido até agosto. Então, não será vendido este ano.

  \item Se George não estiver atrasado para a reunião, ele apresentará o orador.
    Mas George estava atrasado para a reunião. Então, ele não apresentará o
    orador.

  \item Rotterdam está na Holanda ou na Europa. Rotterdam está na Holanda.
    Então, Roterdã não está na Europa.

  \item O cão não vai latir, se a criança não o assustar. A criança não vai
    assustar. Então, o cachorro não vai latir.

  \item Se chover, então as ruas estão molhadas. Se congelar, as ruas ficam
    escorregadias. Está chovendo ou congelando. Então, as ruas estão molhadas ou
    escorregadias.

  \item Se chover e congelar, as ruas estarão molhadas e escorregadias. Está
    chovendo ou congelando. Então, as ruas estão molhadas ou escorregadias.

  \end{enumerate}
\end{exercise}


\begin{exercise}
  Converta os argumentos verbais dados abaixo para argumentos formais, ou seja,
  converta as sentenças para a forma simbólica e identifique a conclusão
  (consequência lógica) indicada no argumento.

  \begin{enumerate}

  \item Se George ou Liz foram à festa, Tom e Susan ficaram chateados. Liz, como
    se viu, não foi, mas Tom e Susan ainda estavam chateados. Portanto, George
    de fato foi à festa.

  \item Se Al não está cantando, Bo não está dançando. Ou Bo ou Clyde está
    dançando. Então, se Clyde não está dançando, então Al está cantando.

  \item A orquestra não tocará ambos Stravinski e Mozart hoje à noite. Eles vão,
    como sabemos, tocar Mozart hoje à noite. Devemos concluir, portanto, que
    eles não vão tocar Stravinski hoje à noite.

  \item Não é verdade que tanto você não pode ir nos passeios de crianças quanto
    você não pode ir nos passeios de adultos. Você, naturalmente, não pode ir
    nos passeios infantis. Portanto, você pode ir nos passeios de adultos.

  \item Sua carteira de motorista não será revogada se ele não violar a lei. Mas
    ele deve ter violado a lei, porque sua licença foi revogada.

  \item Se esta escola irá sobreviver, deve aumentar sua anuidade (a fim de
    compensar as despesas). Mas, se esta escola quiser sobreviver, não pode
    aumentar sua anuidade (para se manter competitiva). Então, essa escola
    definitivamente não vai sobreviver.

  \item Se essa criatura não tiver dentes, ela não morderá. Ai! Bem, não é o
    caso que não morde. Então, não é o caso que não tem dentes.

  \item Eles venceram a batalha e é falso que eles não venceram a guerra. Então,
    eles venceram a guerra, e não é verdade que eles não venceram a batalha.

  \item Se algum número $N$ for o maior número possível, então $N$ é o maior
    número possível (por hipótese) e $N$ não é o maior número possível (desde
    que você possa adicionar 1 a ele). Então, é falso que algum número $N$ seja
    o maior número possível.

  \item Paris, Londres ou Roma sediarão a Convenção do Vinho este ano. Se Paris
    o fizer, os vinhos franceses vencerão. Se Londres, então, os vinhos
    britânicos vão ganhar. Se Roma o fizer, os vinhos italianos vencerão. Os
    vinhos britânicos não vão ganhar este ano. Então, vinhos franceses ou
    italianos ganharão este ano.
    
  \end{enumerate}
\end{exercise}


\begin{exercise}
  Converta os argumentos verbais dados abaixo para argumentos formais, ou seja,
  converta as sentenças para a forma simbólica e identifique a conclusão
  (consequência lógica) indicada no argumento.

  \begin{enumerate}

  \item Robert conhece o latim se e somente se ele não sabe grego. Ele sabe
    grego. Então, Robert não sabe latim.

  \item Beth irá, se James pedir a ela. Se Mateus pedir a ela, Beth também irá.
    Se James não pedir a ela, Mateus pedirá. Então, Beth irá.

  \item A música não é de Vivaldi, a menos que o estilo seja barroco. Se o
    estilo é romântico, então não é barroco. Então, se a música é de Vivaldi,
    então não é romântico.

  \item Mateus terá comido, se Beth cozinhou. Mateus não comeu a menos que James
    também tenha comido. Então, James comeu apenas se Beth cozinhou.

  \item Laura dará a palestra a menos que ninguém apareça. Felizmente, as
    pessoas apareceram. Então, ela deu a palestra.

  \item Se o anfitrião conhece o senador, então o senador será convidado. Se a
    anfitriã gostar do senador, ele também será convidado. Mas nem o anfitrião o
    conhece nem a anfitriã gosta dele. Então, o senador não será convidado.

  \item Eles não venderão a casa somente se puderem pagar a hipoteca. Mas eles
    estão vendendo a casa. Então, eles não podem pagar a hipoteca.

  \item Samantha não correrá a menos que o tempo não esteja quente. Assim,
    ela correr quando o tempo está quente não vai acontecer.

  \item Este cilindro é quadrado apenas se não for redondo. Mas, mesmo que o
    cilindro seja quadrado, ele ainda precisa ser redondo. Mas qualquer tolo
    sabe que não pode ser tanto redondo quanto não redondo. Então, esse cilindro
    não pode ser quadrado.

  \item Se a demanda por esses produtos aumentar, o preço subirá. Além disso, a
    demanda por esses produtos aumentará somente se o emprego subir. No entanto,
    a demanda não aumentou. Então, o preço ou o emprego não está subindo.

  \item Alguns funcionários devem ser demitidos, se o orçamento for reduzido. Não
    haverá um aumento salarial a menos que o orçamento não seja congelado. O
    orçamento será reduzido ou congelado. Então, alguns funcionários devem ser
    demitidos ou não haverá um aumento salarial.

    %%% ATENÇÃO
  % \item A menos que os clubes não sejam liderados, eu posso pegar esse truque.
  %   Eu tenho uma boa mão, se as espadas são trunfo. Ambos os clubes foram
  %   conduzidos ou espadas são trunfo. Então, ou eu posso pegar esse truque, ou
  %   eu tenho uma boa mão.

  \item Se a pressão for muito baixa, o motor não funcionará. E, se a pressão
    for muito alta, o motor não funcionará. Então, se o motor funcionar, a
    pressão não é nem muito baixa nem muito alta.

  \item O proprietário pode despejar o inquilino apenas se o inquilino não tiver
    satisfeito os termos do contrato de arrendamento. O inquilino não satisfez
    os termos do contrato, a menos que o aluguel seja pago. Então, se o aluguel
    não for pago, o senhorio pode despejar o inquilino.

  \item Desde que Albert saiba cálculo, ele conhece álgebra. Mas ele não conhece
    cálculo se não conhece a trigonometria. Então, ele não sabe trigonometria a
    menos que ele não saiba álgebra.

  \item Joe não conhece cálculo, mas conhece álgebra. Mas ele não conhece a
    trigonometria sem conhecer o cálculo. Então, Joe sabe álgebra, mas ele não
    sabe trigonometria.
    
  \end{enumerate}
  
\end{exercise}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../ilm-book"
%%% ispell-local-dictionary: "brasileiro"
%%% End:
