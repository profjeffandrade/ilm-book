\section{Dedução no Cálculo Proposicional}
\label{sec:proplog-pc-decuction}

Na seção anterior vimos o conceito de proposição e como traduzir
sentenças em linguagem natural para a forma simbólica, construindo
chaves de simbolização e usando os operadores lógicos. O objetivo por
trás deste processo de tradução é permitir que utilizemos a lógica
proposicional para modelar e raciocinar sobre informações e sobre
``discursos'', i.e., sobre o que as pessoas falam e escrevem.

Na lógica e na filosofia, um \emph{argumento} é uma série de
declarações tipicamente usada para persuadir alguém de alguma coisa,
ou para apresentar razões para aceitar uma conclusão. A forma geral de
um argumento em uma linguagem natural consistem em apresentar
\emph{premissas} para fundamentar uma \emph{conclusão}. Em um
argumento dedutivo típico, as premissas se destinam fornecer uma
garantia da validade da conclusão, enquanto em argumentos indutivos se
considera que elas fornecem razões para apoiar a provável validade da
conclusão.

Os padrões e critérios usados para avaliar argumentos e suas formas de
raciocínio são estudados na lógica. Os caminhos para formular
argumentos efetivos são estudados pela retórica e a teoria da
argumentação. Uma argumento em uma linguagem formal mostra a forma
lógica de um argumento em linguagem natural traduzido para a forma
simbólica.

O que queremos fazer agora é usar ferramentas da lógica formal para
permitir que deduzamos conclusões ``corretas'', i.e., válidas, à
partir de proposições dadas.


\subsection{Argumentos Válidos}
\label{sec:argumento-valido-cal-prop}

Antes de darmos uma definição para \emph{argumento} na lógica
proposicional, precisamos estabelecer a noção de \emph{consequência
  lógica}.

\vspace{0.75\baselineskip}

\begin{defn}[Consequência Lógica]
  \label{def:conseq-logica}
  Dizemos que uma \emph{fórmula bem formada} $\beta$ é uma \textbf{consequência
    lógica} de um conjunto de \emph{fbf's} $\alpha_1, \alpha_2, \ldots,
  \alpha_n$, quando esse conjunto de premissas \textbf{acarreta} a conclusão
  $\beta$. Existem dois tipos de consequência lógica:

  \begin{description}
  \item[Consequência Semântica] Dizemos que $\beta$ é uma \emph{consequência
      semântica} de $\alpha_1, \alpha_2, \ldots, \alpha_n$, escrito
    \[
      \alpha_1, \alpha_2, \ldots, \alpha_n \vDash \beta
    \] se e somente se não existir nenhuma valoração para as variáveis
    proposicionais que faça todas as fórmulas $\alpha_1, \alpha_2, \ldots,
    \alpha_n$ verdadeiras mas que faça $\beta$ falsa.

  \item[Consequência Sintática] Dizemos que $\beta$ é uma \emph{consequência
      sintática} de $\alpha_1, \alpha_2, \ldots, \alpha_n$, escrito
    \[
      \alpha_1, \alpha_2, \ldots, \alpha_n \vdash \beta
    \] se e somente se não existir existir uma ``prova'' (em algum sistema de
    prova) de que dadas as premissas $\alpha_1, \alpha_2, \ldots, \alpha_n$ é
    possível derivar a fórmula $\beta$.
  \end{description}
\end{defn}


Um resultado direto da definição de \emph{consequência semântica} é que $\beta$
será uma consequência lógica de $\alpha_1, \alpha_2, \ldots, \alpha_n$, se e
somente se, a fórmula $\alpha_1 \land \alpha_2 \land \cdots \land \alpha_n \to
\beta$ for uma tautologia.


\begin{exmp}
  Como exemplo, afirmamos que $Q$ é consequência lógica de $P \to Q$ e
  $\neg Q \to P$. Para testar a validade desta afirmação, construímos
  a tabela-verdade para a fórmula
  $(P \to Q) \land (\neg Q \to P) \to Q$ que, como podemos ver abaixo,
  é uma tautologia.
  
  \begin{center}
    \begin{small}
      \begin{tabular}{cc|c|c|c|c|c}
        $P$ & $Q$ & $P \to Q$ & $\neg Q$ & $\neg Q \to P$
        & $(P \to Q) \land (\neg Q \to P)$
        & $(P \to Q) \land (\neg Q \to P) \to Q$ \\
        \hline
        0 & 0 & 1 & 1 & 0 & 0 & 1 \\
        0 & 1 & 1 & 0 & 1 & 1 & 1 \\
        1 & 0 & 0 & 1 & 1 & 0 & 1 \\
        1 & 1 & 1 & 0 & 1 & 1 & 1
      \end{tabular}
    \end{small}
  \end{center}
\end{exmp}


Assim, a lógica proposicional define um \emph{argumento} como sendo uma
sequência de proposições. Um \emph{argumento válido} é uma sequência de
proposições, onde a última proposição ``segue'' das demais, i.e., é consequência
lógica das demais. Todos os outros argumentos são inválidos.

\vspace{0.75\baselineskip}

\begin{defn}[Argumento]
  \label{def:argumento}
  Em lógica proposicional, um \textbf{argumento} é uma sequência de
  fórmulas escritas como
  $\alpha_1, \alpha_2, \ldots, \alpha_n \vdash \beta$, onde
  $\alpha_1, \alpha_2, \ldots, \alpha_n$ são as \emph{premissas} do
  argumento e $\beta$ é a \emph{conclusão}.

  Um argumento é um \textbf{argumento válido} se e somente se sua
  conclusão é consequência lógica de suas premissas.
\end{defn}


Um dos argumentos mais simples é o \emph{modus ponens}, que tem um
exemplo mostrado abaixo:
\[
  \begin{array}{rl}
    1. & P \to Q \\
    2. & P \\
    \hline
    \therefore & Q
  \end{array}
\]

Para verificar que o argumento acima é, de fato, válido, podemos
construir a tabela-verdade para a fórmula $(P \to Q) \land P \to Q$:

\begin{center}
  \begin{tabular}{cc|c|c|c}
    $P$ & $Q$ & $P \to Q$ & $(P \to Q) \land P$
    & $(P \to Q) \land P \to Q$ \\
    \hline
    0 & 0 & 1 & 0 & 1 \\
    0 & 1 & 1 & 0 & 1 \\
    1 & 0 & 0 & 0 & 1 \\
    1 & 1 & 1 & 1 & 1
  \end{tabular}
\end{center}


A definição de \emph{consequência sintática} diz que $\alpha_1, \alpha_2,
\ldots, \alpha_n \vdash \beta$ é válido sse houver uma prova por algum
\textbf{sistema de prova} de que é possível derivar a fórmula $\beta$ à partir
das fórmulas $\alpha_1, \alpha_2, \ldots, \alpha_n$, mas não foi dada até agora
uma definição do que é um ``sistema de prova''.

\begin{defn}[Sistema de Prova]
  Um \textbf{sistema de prova} para uma lógica é um conjunto de regras
  que para manipulação dos símbolos e das fórmulas desta lógica.
  \begin{itemize}
  \item Se, para do $\Gamma$ e todo $\beta$, $\Gamma \vdash \beta$ implica
    $\Gamma \vDash \beta$, dizemos que o sistema de prova é \textbf{correto}.
  \item Se, para do $\Gamma$ e todo $\beta$, $\Gamma \vDash \beta$ implica
    $\Gamma \vdash \beta$, dizemos que o sistema de prova é \textbf{completo}.
  \end{itemize}
\end{defn}

Existem diversos sistemas de provas diferentes. Na próxima seção iremos estudar
um sistema que busca ``imitar'' a forma como logicistas constroem provas
mentalmente.


\subsection{Dedução Natural}

Como vimos, em princípio, tabelas-verdade podem ser usadas para
resolver qualquer questão na lógica proposicional clássica.
Entretanto, esse método tem suas desvantagens. O tamanho da tabela
cresce exponencialmente com o número de variáveis proposicionais
distintas envolvidas nas fórmulas do argumento. Além disso,
tabelas-verdade não são familiares aos nossos padrões de pensamento
normais. Não se parecem com a forma como nós raciocinamos sobre
argumentos. Existe uma outra forma de estabelecer a validade de um
argumento que não apresenta estas desvantagens: \textbf{o método de
  dedução natural}. No método de dedução natural se faz uma tentativa
de reduzir o raciocínio por trás de um argumento válido a uma série de
passos, cada um deles intuitivamente justificado pelas premissas do
argumento, ou por outros passos anteriores da série.

Considere o seguinte argumento declarado em linguagem natural:

\begin{quote}
  Ou pelo de gato ou então pelo de cachorro foi achado na cena do
  crime. Se foi encontrado pelo de cachorro na cena do crime, o
  policial Fagundez teve um ataque alérgico. Se pelo de gato foi
  encontrado na cena do crime, então Meireles é o responsável pelo
  crime. Mas o policial Fagundes não teve um ataque alérgico.
  Portanto, Meireles deve ser o responsável pelo crime.
\end{quote}

A validade deste argumento pode ser apresentada de modo mais óbvio se
representarmos a sequência de raciocínio que leva das premissas à
conclusão:

\begin{enumerate}

\item Ou pelo de gato ou então pelo de cachorro foi achado na cena do
  crime. (Premissa)\label{it:arg1-prem1}

\item Se foi encontrado pelo de cachorro na cena do crime, o policial
  Fagundez teve um ataque alérgico. (Premissa)\label{it:arg1-prem2}

\item Se pelo de gato foi encontrado na cena do crime, então Meireles
  é o responsável pelo crime. (Premissa)\label{it:arg1-prem3}

\item O policial Fagundes não teve um ataque
  alérgico. (Premissa)\label{it:arg1-prem4}

\item Pelo de cachorro não foi encontrado na cena do crime. (Segue de
  \ref{it:arg1-prem2} e \ref{it:arg1-prem4}.)\label{it:arg1-fml1}

\item Pelo de gato foi encontrado na cena do crime. (Segue de
  \ref{it:arg1-prem1} e \ref{it:arg1-fml1}.)\label{it:arg1-fml2}

\item Meireles é responsável pelo crime. (Conclusão. Segue de
  \ref{it:arg1-prem3} e \ref{it:arg1-fml2}.)\label{it:arg1-concl}

\end{enumerate}

No desenvolvimento acima, nós não saltamos diretamente das premissas
para a conclusão. Ao invés disto, nós mostramos como as inferências
intermediarias são utilizadas para justificar a conclusão em uma
sequência passo-a-passo. Cada passo na sequência representa uma forma
de raciocínio simples e obviamente válida. Neste exemplo, a forma de
raciocínio utilizada no item \ref{it:arg1-fml1} é chamado de
\emph{modus tollens}, que envolve deduzir a negação do antecedente de
um condicional à partir deste condicional e da negação de seu
consequente. A forma de raciocínio utilizada no item
\ref{it:arg1-fml2} é chamada de \emph{silogismo disjuntivo} e permite
concluir um dos termos de uma disjunção à partir desta disjunção e da
negação do outro termo. Finalmente, no item \ref{it:arg1-concl}, a
conclusão, foi usada uma forma de raciocínio chamada \emph{modus
  ponens} que permite concluir o consequente de um condicional à
partir deste condicional e de seu antecedente.

\vspace{0.75\baselineskip}

\begin{defn}[Sequência de Demonstração]
  \label{def:seq-demonstr}
  Uma \textbf{sequência de demonstração} para um argumento formal
  válido é uma sequência de fórmulas na qual cada fórmula é uma
  premissa do argumento ou o resultado da aplicação de uma regra de
  dedução do sistema formal de dedução natural às fórmulas anteriores
  da sequência. A sequência de demonstração termina quando é gerada a
  conclusão do argumento.
\end{defn}

Um sistema de dedução natural consiste em especificar um conjunto de
regras de inferência intuitivamente verdadeiras para a construção de
sequências de derivações.

As regras de dedução para um sistema de dedução natural devem ser
escolhidas com cuidado. Se forem poderosas demais (i.e., permitirem
deduzir fórmula com muita facilidade), não preservarão a validade das
novas fórmulas e seremos capazes de deduzir qualquer coisa à partir de
um dados conjunto de premissas. Se forem fracas demais (i.e., muito
restritas no que é possível deduzir), existirão conclusões lógicas que
não seremos capazes de provar a partir de um conjunto de premissas.
Queremos um sistema lógico formal que seja \textbf{correto} (permita
demonstrar apenas argumentos válidos) e \textbf{completo} (permita
demonstrar todos os argumentos válidos). Além disso, a quantidade de
regras não deve ser muito grande, de modo a tornar o sistema formal
``tratável''.  Desejamos que o sistema tenha o menor número possível
de regras de dedução que o torne completo.

As regras de dedução para lógica proposicional são de dois tipos, de
substituição ou equivalência, e de inferência. As regras de
substituição permitem que \emph{fbf's} sejam reescritas mantendo o
mesmo valor lógico, enquanto as regras de inferência permitem a
dedução de novas \emph{fbf's} a partir de \emph{fbf's} anteriores na
sequência de demonstração.

\begin{exmp}
  \label{ex:demo1}
  Demonstramos o argumento formal $P \to Q, P \land R \vdash Q$:
  
  \begin{math}
    \begin{nd}
      \hypo {1} {P \to Q} \by{hip}{}
      \hypo {2} {P \land R} \by{hip}{}
      \have {3} {P} \by{simp}{2}
      \have {4} {Q} \by{mp}{1,3}
    \end{nd}
  \end{math}\\
  $\qed$
\end{exmp}

Note que as fórmulas que compõem a sequência de demonstração do
Exemplo~\ref{ex:demo1} estão todas numeradas para futura referência, e
apresentam todas uma justificativa para a sua introdução à direita da
fórmula (nas justificativas são usadas as abreviações das regras de
inferência). Note ainda que as premissas do argumento formal são
justificadas por ``hip'', de ``hipótese''.

\begin{exmp}
  \label{ex:seq2}
  Demonstramos o argumento formal
  $P \lor Q \to R, \neg Q \land \neg R \vdash \neg(P \lor Q) $.
  
  \begin{math}
    \begin{nd}
      \hypo {1} {P \lor Q \to R} \by{hip}{}
      \hypo {2} {\neg Q \land \neg R} \by{hip}{}
      \have {3} {\neg R} \by{simp}{2}
      \have {4} {\neg (P \lor Q)} \by{mt}{1,3}
    \end{nd}
  \end{math}\\
  $\qed$
\end{exmp}

Não se preocupe em entender as justificativas das fórmulas das sequências de
demonstração do Exemplos~\ref{ex:demo1} e \ref{ex:seq2} agora. As regras de
inferência serão explicada à seguir.


\subsubsection{Regras de Inferência}

De modo geral, uma regra de inferência diz que, se determinadas fórmulas
constarem da sequência de demonstração, então podemos adicionar uma determinada
nova fórmula na sequência, de acordo com o padrão estabelecido pela regra de
inferência.

Quando se cria um sistema de provas para uma lógica qualquer, sempre é
necessário provar que este sistema de provas é ``correto'', ou seja, que ele é
capaz de provar apenas fórmulas válidas. Para isso, cada regra de inferência é
analisada individualmente e em conjunto com as outras regras de inferência,
tipicamente utilizando uma técnica chamada \emph{indução estrutural}
(Apêndice~\ref{cha:recursive}). Como se pode imaginar, quanto mais regras
de inferência, maior é a complexidade de se provar que o sistema de prova é
correto. Por essa razão, sempre se busca manter o número de regras do sistema de
provas o menor possível, mas sem comprometer a capacidade do sistema de provar
argumentos válidos. Para o sistema de dedução natural para a lógica
proposicional, iremos trabalhar com as regras de inferência apresentadas abaixo:

\begin{multicols}{2}
  \begin{enumerate}
  \item Modus Ponens
  \item Modus Tolens
  \item Conjunção
  \item Simplificação
  \item Adição
  \item Reapresentação
  \end{enumerate}
\end{multicols}

Iremos apresentar e explicar cada uma destas regras de inferência, mas antes
disso precisamos apresentar uma definição mais concreta do que é uma regra de inferência.\bigskip

\begin{defn}[Regra de Inferência]
  Uma regra de inferência é um mecanismo que nos permite transformar uma
  sequência de demonstração, acrescentando novas fórmulas, desde que certas
  condições sejam atendidas. As regras de inferência são definidas pela seguinte
  notação:
  \[
    \frac{\gamma_1, \ldots, \gamma_n}{\alpha}
  \]
  As fórmulas $\gamma_1, \ldots, \gamma_n$, acima do traço, indicam quais
  fórmulas devem existir na sequência de demonstração original para que a regra
  de inferência possa ser utilizada. A fórmula $\alpha$, abaixo do traço,
  indicam que fórmula sera adicionada à sequência de demonstração caso a
  regra seja usada.
\end{defn}

Tipicamente, ao apresentar uma regra, além de seu nome e de seu esquema, também
é apresentada uma abreviação do nome da regra para ser usada na justificativa
das sequências de demonstração. Também é usual usar letras do alfabeto grego
para representar fórmulas ou partes de fórmulas na definição do esquema das
regras de inferência. Veremos agora as regras de inferência básicas da dedução
natural para a lógica proposicional.


\subsubsection{Modus Ponens}

\emph{Modus ponens} ou \emph{modus ponendo ponens} significa ``o modo de afirmar
afirmando''. Esta regra é apresentada abaixo:

\[
  \frac{\alpha, \alpha\to\beta}{\beta} \qquad \mathrm{(mp)}
\]

Esta regra diz que, se tivermos na sequência de demonstração uma fórmula
condicional ($\alpha\to\beta$) e outra fórmula da sequência que seja igual ao
antecedente deste condicional ($\alpha$), podemos acrescentar à sequência de
demonstração uma fórmula que seja igual ao consequente deste condicional
($\beta$).

\begin{exmp}
  Demonstração do argumento \(P, R, P \to (R \to Q) \vdash Q\).
  
  \begin{math}
    \begin{nd}
      \hypo {1} {P} \by{hip}{}
      \hypo {2} {R} \by{hip}{}
      \hypo {3} {P \to (R \to Q)} \by{hip}{}
      \have {4} {R \to Q} \by{mp}{1,3}
      \have {5} {Q} \by{mp}{2,4}
    \end{nd}
  \end{math}\\
  $\qed$
\end{exmp}


\subsubsection{Modus Tolens}

\emph{Modus Tollens} o \emph{modus tollendo tollens} significa ``o modo de negar
negando''. Esta regra é definida pelo esquema abaixo:

\[
  \frac{\neg\beta, \alpha\to\beta}{\neg\alpha} \qquad \mathrm{(mt)}
\]

Esta regra diz que, se tivermos na sequência de demonstração uma fórmula
condicional ($\alpha\to\beta$) e outra fórmula da sequência se seja igual à
negação do consequente deste condicional ($\neg\beta$), então podemos
acrescentar à sequência de demonstração uma fórmula igual à negação do
consequente deste condicional ($\neg\alpha$).

\begin{exmp}
  Demonstração do argumento \(\neg Q, P \to Q, R \to P \vdash \neg R\).

  \begin{math}
    \begin{nd}
      \hypo {1} {\neg Q} \by{hip}{}
      \hypo {2} {P \to Q} \by{hip}{}
      \hypo {3} {R \to P} \by{hip}{}
      \have {4} {\neg P} \by{mt}{1,2}
      \have {5} {\neg R} \by{mt}{4,3}
    \end{nd}
  \end{math}\\
  $\qed$
\end{exmp}


\subsubsection{Conjunção}

A regra de conjunção faz exatamente o que o seu nome indica, i.e., a conjunção
de outras fórmulas. O esquema desta regra é dado abaixo:

\[
  \frac{\alpha, \beta}{\alpha\land\beta}
  \qquad
  \text{(conj)}
\]

Assim, a regra de conjunção diz que, se tivermos na sequência de demonstração
uma fórmula qualquer ($\alpha$) e uma outra fórmula qualquer ($\beta$), então
podemos acrescentar à sequência de demonstração a fórmula formada pela conjunção
destas duas ($\alpha\land\beta$).

\begin{exmp}
  Demonstração do argumento $P, Q, R \vdash P\land Q\land R$.

  \begin{math}
    \begin{nd}
      \hypo {1} {P} \by{hip}{}
      \hypo {2} {Q} \by{hip}{}
      \hypo {3} {R} \by{hip}{}
      \have {4} {P\land Q} \by{conj}{1,2}
      \have {5} {P\land Q\land R} \by{conj}{4,3}
    \end{nd}
  \end{math}\\
  $\qed$
\end{exmp}


\subsubsection{Simplificação}

O esquema da regra de simplificação é o seguinte:

\[
  \frac{\alpha\land\beta}{\alpha}
  \text{~ou~}
  \frac{\alpha\land\beta}{\beta}
  \qquad
  \text{(simp)}
\]

De certo modo, a regra de simplificação realiza a operação inversa da regra de
conjunção, ou seja, dado que temos na sequência de demonstração uma fórmula que
já é uma conjunção ($\alpha\land\beta$), a regra de simplificação nos permite
adicionar à sequência qualquer um dos dois termos da conjunção ($\alpha$ ou
$\beta$).

\begin{exmp}
  Demonstração do argumento \(P\land Q\land R \vdash R\)

  \begin{math}
    \begin{nd}
      \hypo {1} {P\land Q\land R} \by{hip}{}
      \have {2} {Q\land R} \by{simp}{1}
      \have {3} {R} \by{simp}{2}
    \end{nd}
  \end{math}\\
  $\qed$
\end{exmp}


\subsubsection{Adição}

A regra de adição costuma precisar de um pouco mais de explicação do que as
demais. O esquema da regra de adição é o seguinte:
\[
  \frac{\alpha}{\alpha\lor\gamma}
  \qquad
  \text{(ad)}
\]
O que esse esquema nos diz é que, se tivermos uma fórmula qualquer ($\alpha$) na
sequência de demonstração, então podemos acrescentar à sequência a fórmula
formada pela disjunção desta fórmula com \textbf{qualquer outra fórmula}
($\gamma$) que desejarmos ($\alpha\lor\gamma$).

Isso parece pouco intuitivo, mas a justificativa é a seguinte: se $\alpha$ já
está na sequência de demonstração, então, por hipótese, $\alpha$ é verdadeiro;
se $\alpha$ já é verdadeiro, ``fazer o {ou}'' de $\alpha$ com qualquer
outra coisa também irá resultar em uma fórmula verdadeira.

\begin{exmp}
  Demonstração do argumento \(P\land Q \vdash P\lor Q\).

  \begin{math}
    \begin{nd}
      \hypo {1} {P\land Q} \by{hip}{}
      \have {2} {P} \by{simp}{1}
      \have {3} {P\lor Q} \by{ad $[Q]$}{2}
    \end{nd}
  \end{math}\\
  $\qed$
\end{exmp}


\subsubsection{Reapresentação}

Em algumas situações, ao desenvolver uma sequência de demonstração, é necessário
reescrever fórmulas que já foram escritas antes na sequência. Isso tipicamente
ocorre ao se desenvolver sub-provas (serão vistas adiante), mas também pode
acontecer me outras situações. A regra de \emph{reapresentação} permite que se
faça exatamente isso, como mostrado no esquema abaixo:
\[
  \frac{~\alpha~}{\alpha} \qquad \text{(R)}
\]
É um esquema extremamente simples. O que ele diz é que se temos uma fórmula
qualquer ($\alpha$) na sequência de demonstração, então podemos adicionar essa
mesma fórmula ($\alpha$), novamente, na sequência de demonstração.


\subsubsection{Regras de Substituição}

No sistema de dedução natural que estamos definindo, é permitido
realizar certas substituições de fórmula de certo formato por outras,
desde que as fórmulas em questão sejam logicamente equivalentes, como
visto na Definição~\ref{def:equiv-logica}. As regras que permitem esta
reescrita são chamada de \emph{regras de substituição} ou \emph{regras
  de equivalência}. As regras de substituição diferem das regras de
inferência que vimos acima, porque estritamente falando, quando uma
regras de substituição é usada, não estamos inferindo nada novo,
apenas declarando a mesma coisa mas usando uma combinação diferente de
símbolos.

Regras de substituição também diferem das regras de inferência de
outras formas. Regras de inferência só podem ser aplicadas quando os
operadores principais das fórmulas correspondem exatamente aos padrões
dados e só podem ser aplicadas a fórmula completas. Regras de
inferência também são estritamente unidirecionais, i.e., deve se
inferior o que está abaixo da linha horizontal da regra, à partir
daquilo que está acima da linha e nunca o contrário. Em contrapartida,
regras de substituição podem ser aplicadas a partes de fórmulas
(sub-fórmulas); além disso, elas podem ser utilizada em qualquer
direção.

As regras de substituição disponíveis em nosso sistema de dedução
natural são aquelas listada na Seção~\ref{sec:taut-contr-equiv}.

\begin{exmp}
  Consideremos agora outra prova possível para o argumento
  $P \to Q, P \vdash Q$, que usa exclusivamente regras de
  substituição.
  
  \begin{math}
    \begin{nd}
      \hypo {1} {P \to Q} \by{hip}{}
      \hypo {2} {P} \by{hip}{}
      \have {3} {\neg P \lor Q} \by{cond}{1}
      \have {4} {P \land (\neg P \lor Q)} \by{conj}{2,3}
      \have {5} {(P \land \neg P) \lor (P \land Q)} \by{dist}{4}
      \have {6} {0 \lor (P \land Q)} \by{comp}{5}
      \have {7} {P \land Q} \by{en}{6}
      \have {8} {Q} \by{simp}{7}
    \end{nd}
  \end{math}\\
  $\qed$
\end{exmp}


\subsubsection{Sub-provas, e Método Dedutivo}

Em alguns casos, embora uma determinada conclusão seja consequência
lógica das premissas, as premissas dadas não são suficientes para se
construir uma demonstração pelo sistemas de dedução natural. Nestes
casos é aceitável fazer suposições, desde que estas suposições sejam
corretamente utilizadas.

Durante uma sequência de demonstração, podemos usar uma suposição para
criar uma \textbf{sub-prova}. Dentro da sub-prova a suposição pode ser
utilizada como se fosse uma premissa, porém a sub-prova deve
eventualmente ser fechada e devemos sempre voltar para a prova
principal.  Ao se fechar a sub-prova, podemos acrescentar à prova
principal um condicional onde o antecedente é a suposição que abriu a
sub-prova, e o consequente é a última fórmula escrita na sub-prova.


\begin{exmp}\label{ex:sub-prova}
  O argumento $P \to Q, Q \to R, R \to S \vdash P \to S$ pode ser
  demonstrado pela sequência de demonstração abaixo:

  \begin{math}
    \begin{nd}
      \hypo {1} {P \to Q} \by{hip}{}
      \hypo {2} {Q \to R} \by{hip}{}
      \hypo {3} {R \to S} \by{hip}{}
      \open
      \hypo {4} {P} \by{sup}{}
      \have {5} {Q} \by{mp}{1,4}
      \have {6} {R} \by{mp}{2,5}
      \have {7} {S} \by{mp}{3,6}
      \close
      \have{8} {P \to S} \by{fech}{4--7}
    \end{nd}
  \end{math}\\
  $\qed$
\end{exmp}

Note que na linha \ndref{4} do Exemplo~\ref{ex:sub-prova} é aberta uma
sub-prova com a suposição $P$. Essa sub-prova continua até a linha
\ndref{7}. A linha \ndref{8} introduz na prova principal o condicional
$P \to S$, que tem como antecedente a suposição que abriu a sub-prova
e como consequente a última fórmula que foi escrita na sub-prova.

É possível criar sub-provas dentro de sub-provas se isso for
necessário ou útil. Dentro de uma sub-prova é possível utilizar todas
as fórmulas que aparecem nas prova e sub-provas mais externas, mas uma
vez que uma sub-prova é fechada \textbf{nenhuma} das fórmula de dentro
da sub-fórmula pode ser usada nos níveis mais externos.


\subsubsection{Resumo das Regras de Inferência}

As regras de inferências vistas acima, estão sumarizadas na
Tabela~\ref{tab:basic-inf-rules}.

\begin{table}[h]
  \centering
  \rowcolors{2}{gray!20}{white}
  \begin{tabular}{lcll}
    \textbf{Nome}
    & \textbf{Regra}
    & \textbf{Indicação}
    & \textbf{Obs.} \\
    \hline
    Modus Ponens
    & \(\displaystyle\frac{\alpha,\; \alpha \to \beta}{\beta}\)
    & mp $l_i$ $l_j$
    & $l_n$: número da linha $n$
    \\
    Modus Tolens
    & $\displaystyle \frac{\neg \beta,\; \alpha \to \beta}{\neg \alpha}$
    & mt $l_i$ $l_j$
    & 
    \\
    Conjunção
    & $\displaystyle \frac{\alpha,\; \beta }{\alpha \land \beta }$
    & conj $l_i$ $l_j$
    & 
    \\
    Simplificação
    & \(\displaystyle \frac{\alpha \land \beta }{\alpha }\)
      ~ou~
      \(\displaystyle \frac{\alpha \land \beta}{\beta}\)
    & simp $l_i$
    & 
    \\
    Adição
    & \(\displaystyle\frac{\alpha }{\alpha \lor \gamma }\)
    & ad $[\gamma]$ $l_i$
    & para qualquer $\gamma$
    \\
    Reapresentação
    & \(\displaystyle\frac{~\alpha~}{\alpha}\)
    & R $l_i$
    &
    \\
  \end{tabular}
  \caption{Regras de inferências básicas para dedução natural.}
  \label{tab:basic-inf-rules}
\end{table}


\subsubsection{Regras Derivadas}

Como foi dito acima, o número de regras de inferência em um sistema de prova
para qualquer lógica, é tipicamente mantido o menor possível pois isso facilita
a tarefa de demonstrar a correção do sistema. Entretanto, ter poucas regras de
inferência significa que as sequências de demonstração irão ficar longas, em
alguns casos muito longas. Para evitar que isso aconteça, podemos lançar mão de
regras derivadas.

Regras derivadas, são regras que não são estritamente necessárias para que o
sistema de dedução natural funcione, mas que ajudam a abreviar sequências de
demonstração. Essas regras não aumentam o poder de prova do sistema, e podem ser
demonstradas utilizando as regras de inferência básicas -- daí o nome ``regras
derivadas''.

\begin{table}[h]
  \centering
  \rowcolors{2}{gray!20}{white}
  \begin{tabular}{lcll}
    \textbf{Nome}
    & \textbf{Regra}
    & \textbf{Uso}
    & \textbf{Demonstração}
    \\
    \hline
    Silogismo Hipotético
    & \(\displaystyle\frac{\alpha\to\beta, \beta\to\gamma}{\alpha\to\gamma}\)
    & sh $l_i$ $l_j$
    & Exercício~\ref{exr:dem:sh}
    \\
    Silogismo Disjuntivo
    & \(\displaystyle\frac{\alpha\lor\beta, \neg\alpha}{\beta}\)
    & sd $l_i$ $l_j$
    & Exercício~\ref{exr:dem:sd}
    \\
    Contraposição
    & \(\displaystyle\frac{\alpha\to\beta}{\neg\beta\to\neg\alpha}\)
    & cont $l_i$
    & Exercício~\ref{exr:dem:cont1}
    \\
    Contraposição
    & \(\displaystyle\frac{\neg\beta\to\neg\alpha}{\alpha\to\beta}\)
    & cont $l_i$
    & Exercício~\ref{exr:dem:cont2}
    \\
    Autorreferência
    & \(\displaystyle\frac{\alpha}{\alpha\land\alpha}\)
    & auto $l_i$
    & Exercício~\ref{exr:dem:auto1}
    \\
    Autorreferência
    & \(\displaystyle\frac{\alpha\lor\alpha}{\alpha}\)
    & auto $l_i$
    & Exercício~\ref{exr:dem:auto2}
    \\
    Exportação
    & \(\displaystyle\frac{\alpha\land\beta\to\gamma}{\alpha\to (\beta\to\gamma)}\)
    & exp $l_i$
    & Exercício~\ref{exr:dem:exp}
    \\
    Inconsistência
    & \(\displaystyle\frac{\alpha, \neg\alpha}{\gamma}\)
    & inc $l_i$ $l_j$
    & Exercício~\ref{exr:dem:inc}
    \\
    Distributiva
    & \(\displaystyle\frac{\alpha\land(\beta\lor\gamma)}{(\alpha\land\beta) \lor (\alpha\land\gamma)}\)
    & dist $l_i$
    & Exercício~\ref{exr:dem:dist1}
    \\
    Distributiva
    & \(\displaystyle\frac{\alpha\lor(\beta\land\gamma)}{(\alpha\lor\beta) \land (\alpha\lor\gamma)}\)
    & dist $l_i$
    & Exercício~\ref{exr:dem:dist2}
    \\
  \end{tabular}
  \caption{Regras de inferência derivadas para dedução natural.}
  \label{tab:deriv-inf-rules}
\end{table}

A Tabela~\ref{tab:deriv-inf-rules} apresenta as regras derivadas mais comuns.
Espera-se que a esta altura você já seja capaz de entender o funcionamento das
regras apenas através do esquema apresentado na coluna ``Regra''. A demonstração
de que cada uma das regras derivada apresentadas pode, de fato, ser obtida
apenas através da aplicação das regras de inferência básicas é deixada como
exercício. Na Tabela~\ref{tab:deriv-inf-rules}, a coluna ``Demonstração'' indica
o número do exercício que demonstra a regra em questão.


\subsection{Exercícios}

Nos exercícios de \ref{exr:dem:sh} a \ref{exr:dem:dist2} abaixo, demonstre o
argumentos formais indicados usando apenas as regras de inferências básicas e a
equivalência de dupla negação.

\begin{exercise}
  \label{exr:dem:sh}%
  \(P\to Q, Q\to R \vdash P\to R\)
\end{exercise}

\begin{exercise}
  \label{exr:dem:sd}
  \(P \land Q, \neg P \vdash Q\)
\end{exercise}

\begin{exercise}
  \label{exr:dem:cont1}
  \(P \to Q \vdash \neg Q \to \neg P\)
\end{exercise}

\begin{exercise}
  \label{exr:dem:cont2}
  \(\neg P \to \neg Q \vdash Q \to P\)
\end{exercise}

\begin{exercise}
  \label{exr:dem:auto1}
  \(P \vdash P \land P\)
\end{exercise}

\begin{exercise}
  \label{exr:dem:auto2}
  \(P \lor P \vdash P\)
\end{exercise}

\begin{exercise}
  \label{exr:dem:exp}
  \(P \land Q \to R \vdash P \to (Q \to R)\)
\end{exercise}

\begin{exercise}
  \label{exr:dem:inc}
  \(P, \neg P \vdash Q\)
\end{exercise}

\begin{exercise}
  \label{exr:dem:dist1}
  \(P \land (Q \lor R) \vdash (P \land Q) \lor (P \land R)\)
\end{exercise}

\begin{exercise}
  \label{exr:dem:dist2}
  \(P \lor (Q \land R) \vdash (P \lor Q) \land (P \lor R)\)
\end{exercise}




%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../ilm-book"
%%% ispell-local-dictionary: "brasileiro"
%%% End:
