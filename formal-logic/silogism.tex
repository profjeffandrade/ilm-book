\section{Silogismo}
\label{sec:formallog-silogism}

O silogismo é um termo proveniente do grego antigo e significa ``conexão
de ideias'', ``raciocínio''. É um termo filosófico com o qual
Aristóteles designou a argumentação lógica perfeita, constituída de três
proposições declarativas que se conectam de tal modo que a partir das
duas primeiras, chamadas premissas, é possível deduzir uma conclusão. A
teoria do silogismo foi exposta por Aristóteles na obra \emph{Analíticos
  Anteriores}.

Todas as premissas encaminham o pensamento para a conclusão sempre
havendo uma relação coerente entre as três, ou seja, uma relação de
causa e consequência. Podemos dizer, que o silogismo é um argumento
dedutivo constituído por três proposições que se apresentam de maneira
encadeada:
\begin{itemize}
\item Premissa Maior (P);
\item Premissa Menor (p); e
\item Conclusão (c).
\end{itemize}

Um exemplo clássico de silogismo é o seguinte:

\begin{quote}
  \begin{tabular}{l}
    Todo homem é mortal.
    \\
    Sócrates é homem.
    \\
    \hline
    Logo, Sócrates é mortal.
  \end{tabular}
\end{quote}

Existem três grandes tipos de silogismo:

\begin{description}

\item[Silogismos Categóricos Regulares] -- São os silogismos
  constituídos por três proposições categóricas.As premissas são
  apresentadas forma absoluta e incondicional. A relação entre estas e a
  conclusão é necessária. Esta é a forma padrão do silogismo.

\item[Silogismos Categóricos Irregulares] -- São silogismos derivados,
  ou seja, são estruturas argumentativas que não seguem a forma rigorosa
  do silogismo categórico mas que, mesmo assim são formas válidas. São
  subdivididos em:

  \begin{description}

  \item[Entimema] Silogismo incompleto, onde falta pelo menos uma
    premissa (está subentendida). Por exemplo:

    \begin{quote}
      \begin{tabular}{l}
        Todo quadrúpede tem 4 patas.
        \\
        \hline
        Logo, um cavalo tem 4 patas.
      \end{tabular}
    \end{quote}

    Neste caso, fica subentendida a premissa ``todo cavalo é um quadrúpede''.

  \item[Epiquerema] O epiquerema é um argumento onde uma ou ambas as
    premissas apresentam a prova ou razão de ser do sujeito. Geralmente
    é acompanhada do termo \emph{porque} ou algum equivalente. Por
    exemplo:

    \begin{quote}
      \begin{tabular}{p{0.9\linewidth}}
        O demente é irresponsável, porque não é livre.
        \\
        Ora, Pedro é demente, porque o exame médico revelou ser portador
        de paralisia geral progressiva.
        \\
        \hline
        Logo, Pedro é irresponsável.
      \end{tabular}
    \end{quote}

    No epiquerema sempre existe pelo menos uma proposição composta,
    sendo que uma das proposições simples é razão ou explicação da
    outra.

  \item[Polissilogismo] O polissilogismo é uma espécie de argumento que
    contempla vários silogismos, onde a conclusão de um serve de
    premissa menor para o próximo. Por exemplo:

    \begin{quote}
      \begin{tabular}{l}
        Quem age de acordo com sua vontade é livre.
        \\
        Ora, o racional age de acordo com sua vontade.
        \\
        Logo, o racional é livre.
        \\
        Ora, quem é livre é responsável.
        \\
        Logo, o racional é responsável.
        \\
        Ora, quem é responsável é capaz de direitos.
        \\
        \hline
        Logo, o racional é capaz de direitos.
      \end{tabular}
    \end{quote}

  \end{description}

\item[Sorites] Trata-se de um argumento que tem pelo menos quatro
  proposições com os seus termos encadeados de forma correcta, ou seja,
  neste caso ocorre que o predicado da primeira proposição se torna
  sujeito na proposição seguinte, seguindo assim até que na conclusão se
  unem o sujeito da primeira proposição com o predicado da última. Por
  exemplo:

  \begin{quote}
    \begin{tabular}{p{0.9\linewidth}}
      A Grécia é governada por Atenas.
      \\
      Atenas é governada por mim.
      \\
      Eu sou governado por minha mulher.
      \\
      Minha mulher é governada por meu filho, criança de 10 anos.
      \\
      \hline
      Logo, a Grécia é governada por esta criança de 10 anos.
    \end{tabular}
  \end{quote}
  \hfill{\emph{--- Temístocles}}

\item[Silogismos Hipotéticos] -- Um silogismo hipotético contém
  proposições hipotéticas, isto é, apresentam duas ou mais proposições
  simples unidas entre si por uma ligação não verbal, isto é, por
  partículas. O silogismo hipotético apresenta três variações, conforme
  o conetivo utilizado na premissa maior:

  \begin{description}

  \item[Condicional] A partícula de ligação das proposições simples é
    \emph{se\ldots então\ldots}.

    \begin{quote}
      \begin{tabular}{l}
        Se a água tiver a temperatura de 100°C, a água ferve.
        \\
        A temperatura da água é de 100°C.
        \\
        \hline
        Logo, a água está a ferver.
      \end{tabular}
    \end{quote}

  \item[Disjuntivo] A premissa maior do silogismo possui a partícula de
    ligação \emph{ou}.

    \begin{quote}
      \begin{tabular}{l}
        Ou a sociedade tem um chefe ou tem desordem.
        \\
        Ora, a sociedade não tem chefe.
        \\
        \hline
        Logo, a sociedade tem desordem.
      \end{tabular}
    \end{quote}

  \item[Conjuntivo] A partícula de ligação das proposições simples é
    \emph{e}. Nesse silogismo, a premissa maior deve ser composta por
    duas proposições simples que possuem o mesmo sujeito e não podem ser
    verdadeiras ao mesmo tempo, ou seja, os predicados devem ser
    contraditórios.

    \begin{quote}
      \begin{tabular}{l}
        Ninguém pode ser, simultaneamente, mestre e discípulo.
        \\
        Ora, Pedro é mestre.
        \\
        \hline
        Logo, Pedro não é discípulo.
      \end{tabular}
    \end{quote}

  \end{description}

\item[Dilema] O dilema é um argumento onde são apresentadas duas
  alternativas possíveis, mas nenhuma é desejável; e onde afirmando
  qualquer uma das proposições simples das premissas, resulta sempre
  a mesma conclusão. Por exemplo:

  \begin{quote}
    \begin{tabular}{l}
      Ou dizes o que é justo ou o que é injusto.
      \\
      Se dizes o que é justo, os homens te odiarão.
      \\
      Se dizes o que é injusto, os deuses te odiarão.
      \\
      \hline
      Portanto, de qualquer modo, serás odiado.
    \end{tabular}
  \end{quote}

\end{description}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../ilm-book"
%%% End:
