\section{Verdade e Validade}
\label{sec:formallog-truth-vs-validity}

A \emph{verdade} e a \emph{validade} são diferentes. A verdade (tal como
a falsidade) é uma característica das proposições. A validade (tal como
a invalidade) é uma característica dos argumentos. Por isso, é
incorrecto dizer que uma proposição é válida ou inválida, tal como é
incorrecto dizer que um argumento é verdadeiro ou falso.

A validade diz respeito à relação entre o valor lógico (verdadeiro ou
falso) das premissas e o valor lógico da conclusão. Um argumento válido
é um argumento em que as premissas justificam a conclusão, pois esta é
uma consequência lógica daquelas. Isso significa que a verdade das
premissas assegura a verdade da conclusão.

Deste modo, podemos dizer que a verdade e a falsidade são
características possíveis das diferentes partes de um argumento:
premissas e conclusão. A validade e a invalidade são características da
ligação dessas partes -- ou seja, do próprio argumento.

A lógica formal distingue raciocínios válidos de raciocínios inválidos
(ou formalmente incorrectos).

A validade é independente do conteúdo das proposições; a validade é
função unicamente da forma, ou da estrutura das relações que são
estabelecidas entre as proposições (que são tomados como ponto de
partida -- premissas) e a conclusão. Vejamos um exemplo:

``Se todos os $A$ são $B$'' e ``se todos os $B$ são $C$'', daí resulta
necessariamente que ``todos os $A$ são $C$''. Esta inferência (ou
raciocínio dedutivo) é válida, seja qual for a verdade ou a falsidade
das premissas; trata-se de um raciocínio formal, pois é válido seja qual
for o conteúdo dos termos $A$, $B$ ou $C$.

Outro exemplo: ``Se todos os homens são mortais'' e ``Pedro é homem'',
então ``Pedro é mortal''. Se substituirmos Pedro por João ou por Maria,
ou se se substituir os termos por símbolos (``Se todos os $X$ são $Y$''
e ``$Z$ é $X$'', então ``$Z$ é $Y$''), mantendo a mesma forma, ainda
assim o raciocínio continua válido. A validade lógica do raciocínio
depende deste encadeamento formal do raciocínio, independentemente dos
conteúdos atribuídos aos símbolos utilizados.


Consederemos agora:

\begin{quote}
  \begin{tabular}{l}
    Todos os homens têm asas
    \\
    Pedro é homem
    \\
    \hline
    Logo, Pedro tem asas
  \end{tabular}
\end{quote}

Podemos verificar que:
\begin{enumerate}[label=({\alph*})]
\item O raciocínio obedece à mesma estrutura formal anterior; por isso,
  do ponto de vista da validade formal, é válido.
\item A conclusão é \emph{materialmente} falsa, ou seja, no confronto com a
  realidade, verificamos que a proposição ``Pedro tem asas'' é falsa. Isto
  aconteceu porque, apesar da conclusão decorrer necessariamente das
  premissas, parte da premissa era falsa: ``Todos os homens têm asas''.
\end{enumerate}

Portanto, é importante distinguir entre \emph{validade formal} e
\emph{verdade material} de um raciocínio. Esta última avalia-se no
confronto entre o que é afirmado ou negado e a realidade. A conformidade
com as regras lógicas é uma condição necessária, mas não suficiente,
para garantir que se atinja conclusões verdadeiros. É preciso ainda
que se parta de premissas verdadeiras.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../ilm-book"
%%% End:
