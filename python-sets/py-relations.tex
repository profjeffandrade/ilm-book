\section{Relações em Python}
\label{sec:py-relations}

A linguagem Python não possui mecanismos específicos para a definição
de relações, mas há formas de conseguir criar essas definições. Uma
dessas formas é a utilização de conjuntos, que já vimos, outra é a
utilização de \emph{dicionários}, que serão apresentados mais adiante.


\subsection{Definição de Relações em Python}

Como foi dito, relações podem ser definidas em Python como conjuntos
de pares ordenados (tuplas em Python), exatamente da forma como vimos
a definição de relações na Seção~\ref{sec:relations}.

\begin{exmp}[Relação como Conjunto de Pares Ordenados]
  \label{exm:py-rel-as-set}
  Sejam $A = \{1,2,3,4,5\}$ e $B = \{\mbox{one}, \mbox{two},
  \mbox{three}, \mbox{four}, \mbox{five}\}$. Desejamos definir em
  Python a relação $R$ que associa cada número em $A$ com a palavra
  correspondente em $B$.

  \begin{center}
    \begin{minipage}{0.95\linewidth}
\begin{minted}{python}
>>> R = frozenset({(1,'one'), (2,'two'), (3,'three'), (4,'four'),
... (5,'five')})
>>> R
frozenset({(5, 'five'), (3, 'three'), (1, 'one'), (2, 'two'),
(4, 'four')})
\end{minted}
    \end{minipage}
  \end{center}
\end{exmp}

Note que no Exemplo~\ref{exm:py-rel-as-set}, assim como nos outros
exemplos em que definimos conjuntos, ao consultarmos o valor da
variável \texttt{R}, a ordem dos elementos é diferente da ordem em que
eles foram especificados na definição de \texttt{R}. É importante
relembrar: a ordem interna em que os elementos de um conjunto são
armazenados em Python não é determinada pela especificação da
linguagem e pode variar de uma execução do código para outra.

Uma vez que definimos nossa relação como conjunto, podemos executar
qualquer operação válida para conjuntos em Python. Como, por exemplo,
as operações de pertinência, e de subconjunto.

\begin{exmp}
  Continuando com a relação definida no
  Exemplo~\ref{exm:py-rel-as-set}.

  \begin{center}
    \begin{minipage}{0.95\linewidth}
\begin{minted}{python}
>>> (2,'two') in R
True
>>> ('one',1) in R
False
>>> frozenset({(3,'three'), (5,'five')}) < R
True
\end{minted}
    \end{minipage}
  \end{center}
\end{exmp}

Uma desvantagem de definir relações como conjuntos em Python, é o fato
de que não é trivial consultar se um elemento do conjunto de origem ou
do conjunto de destino fazem parte da relação. Não é possível
simplesmente perguntar, por exemplo, se
\mintinline{python}{1 in R}, pois o resultado será falso,
embora o 1 apareça na relação. Para realizar o teste desejado
precisaremos usar a operação \texttt{any}, como mostrado no exemplo
abaixo.

\begin{exmp}
  Teste para determinar se um elemento da origem, ou do destino, faz
  parte da relação.

  \begin{center}
    \begin{minipage}{0.95\linewidth}
\begin{minted}{python}
>>> any(a == 1 for (a,b) in R)
True
>>> any(b == 'four' for (a,b) in R)
True
\end{minted}
    \end{minipage}
  \end{center}
\end{exmp}

Uma outra forma de definir relações é Python é utilizando
\textbf{dicionários}, também chamados de tabelas
associativas. Entretanto os dicionários são menos gerais do que os
conjunto. Um dicionário em Python associa uma \emph{chave} a um
\emph{valor}, e posteriormente permite que se inquira o valor à partir
da chave.

\begin{exmp}
  Exemplo de uso de dicionários em Python.

  \begin{center}
    \begin{minipage}{0.95\linewidth}
\begin{minted}{python}
>>> D = {1:'one', 2:'two', 3:'three', 4:'four', 5:'five'}
>>> D
{1: 'one', 2: 'two', 3: 'three', 4: 'four', 5: 'five'}
\end{minted}
    \end{minipage}
  \end{center}
\end{exmp}

É possível testar se uma determinada chave pertence ao dicionário com
o operador \texttt{in}, mas não é possível perguntar diretamente se um
par ordenado pertence ao dicionário. Entretanto, esta restrição
desaparece se usarmos o método \texttt{items()}, que retorna os pares
ordenados do dicionário. Também existem os métodos \texttt{keys()} e
\texttt{values())} que retornam as listas de chaves e valores
respectivamente.

\begin{exmp}
  Testes para determinar se uma determinada chave ou par ordenado faz
  parte da relação.

  \begin{center}
    \begin{minipage}{0.95\linewidth}
\begin{minted}{python}
>>> 4 in D
True
>>> (4,'four') in D
False
>>> (4,'four') in D.items()
True
>>> D.items()
dict_items([(1, 'one'), (2, 'two'), (3, 'three'), (4, 'four'),
(5, 'five')])
>>> D.keys()
dict_keys([1, 2, 3, 4, 5])
>>> D.values()
dict_values(['one', 'two', 'three', 'four', 'five'])
\end{minted}
    \end{minipage}
  \end{center}
\end{exmp}

Outro ponto positivo dos dicionário é que, com eles, é possível ter
acesso ao valor de destino de modo bastante direto e fácil, à partir
do valor de origem (chave). Entretanto, a tentativa de acessar o valor
para ua chave que não consta do dicionário causará um erro. Como
mostrado no exemplo abaixo.

\begin{exmp}
  Acesso direto aos valores do dicionário.

  \begin{center}
    \begin{minipage}{0.95\linewidth}
\begin{minted}{python}
>>> D[4]
'four'
>>> D[7]
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
KeyError: 7
\end{minted}
    \end{minipage}
  \end{center}
\end{exmp}

Embora os dicionários tenham alguns recursos que facilitam o trabalho
com relações em alguns casos, eles têm duas limitações quando
comparados com o uso de conjuntos para definir relações:

\begin{enumerate}
\item Dicionários não aceitam dicionários como chaves. E não existe
  algo como um \emph{frozen}-dicionário em Python. Esta limitação tem
  consequências menores pois não é comum querermos ou precisarmos usar
  dicionários como conjunto de partida nas relações.
\item Dicionários só aceitam que uma determinada chave apareça uma
  vez. Ou seja, dicionários só podem ser usados para representar
  relações funcionais. Embora boa parte das situações em computação
  lide com relações funcionais, é relativamente comum aparecerem
  conceitos que são melhor modelados por relação
  não-funcionais. Nestes casos o uso de dicionários não é o mais
  indicado para representar estas relações.
\end{enumerate}

Uma vez que uma chave no dicionário pode estar associada a no máximo um
valor, os dicionários são mais próximos de relações funcionais parciais
do que de relações gerais como vimos na Seção~\ref{sec:relations}.

Considere agora, que ao invés de uma relação funcional, com aquela
definida no Exemplo~\ref{exm:py-rel-as-set}, tenhamos uma relação em
que cada número aparece relacionado a mais de uma palavra. Como
podemos manipular uma relação como esta? O Exemplo~\ref{exm:rel-nfunc}
mostra como essa relação pode ser definida, e como podemos inquirir
sobre elementos desta relação.

\begin{exmp}
  \label{exm:rel-nfunc}
  Definição de uma relação não-funcional em Python:

  \begin{center}
    \begin{minipage}{0.95\linewidth}
\begin{minted}{python}
>>> S = frozenset({
...     (1,'one'), (2,'two'), (3,'three'), (4,'four'), (5,'five'),
...     (1,'un'), (2,'deux'), (3,'trois'), (4,'quatre'), (5,'cinq')
... })
>>> S
frozenset({(1, 'one'), (3, 'trois'), (2, 'two'), (1, 'un'),
(5, 'five'), (3, 'three'), (2, 'deux'), (4, 'four'), (4, 'quatre'),
(5, 'cinq')})
\end{minted}
    \end{minipage}
  \end{center}

  Teste se um determinado elemento de origem, ou de destino, pertence
  à relação.

  \begin{center}
    \begin{minipage}{0.95\linewidth}
\begin{minted}{python}
>>> any(a == 3 for (a,b) in S)
True
>>> any(a == 7 for (a,b) in S)
False
>>> any(b == 'cinq' for (a,b) in S)
True
>>> any(b == 'neuf' for (a,b) in S)
False
\end{minted}
    \end{minipage}
  \end{center}

  Operação pra obter os elementos de destino associados com um
  determinado elementos de origem, e vice-versa.

  \begin{center}
    \begin{minipage}{0.95\linewidth}
\begin{minted}{python}
>>> {y for (x,y) in S if x == 1}
{'one', 'un'}
>>> {y for (x,y) in S if x == 3}
{'three', 'trois'}
>>> {x for (x,y) in S if y == 'cinq'}
{5}
>>> {x for (x,y) in S if y == 'five'}
{5}
\end{minted}
    \end{minipage}
  \end{center}
\end{exmp}


\subsection{Operações sobre Relações em Python}

Na Seção~\ref{sec:oper-rel-fn} vimos três operações que podem ser
aplicadas a relações e funções:

\begin{enumerate}
\item Relação identidade.
\item Relação inversa.
\item Composição de relações.
\end{enumerate}

Infelizmente, a linguagem Python, não possui formas específicas,
pré-definidas, de executar estas operações. Mas não é difícil
implementá-las com os recursos existentes na linguagem. Para facilitar
a explicação, vamos considerar que as relações foram definidas como
conjuntos.


\subsubsection{Relação Identidade}

Dado um conjunto \texttt{A}, é bastante simples definir uma relação
identidade sobre este conjunto através de uma expressão de compreensão
de conjuntos, como demonstrado no exemplo abaixo:

\begin{exmp}
  O código abaixo define um conjunto \texttt{A} e, em seguida, define
  uma relação identidade sobre \texttt{A}, chamada \texttt{idA}.

  \begin{center}
    \begin{minipage}{0.95\linewidth}
\begin{minted}{python}
>>> A = frozenset({1, 2, 4, 8, 16, 32, 64})
>>> idA = {(x,x) for x in A}
>>> idA
{(16, 16), (64, 64), (32, 32), (4, 4), (8, 8), (2, 2), (1, 1)}
\end{minted}
    \end{minipage}
  \end{center}
\end{exmp}


\subsubsection{Relação Inversa}

Uma vez que estamos usando conjuntos de pares ordenados para
representar relações em Python, é bastante simples construir a relação
inversa de qualquer relação dada.

\begin{exmp}
  O código abaixo define uma relação \texttt{R} e, em seguida, define
  a relação \texttt{invR} que é a relação inversa de \texttt{R}.

  \begin{center}
    \begin{minipage}{0.95\linewidth}
\begin{minted}{python}
>>> R = frozenset({(1,'a'), (2,'b'), (3,'c'), (4,'d'), (5,'e')})
>>> invR = {(y,x) for (x,y) in R}
>>> invR
{('a', 1), ('d', 4), ('b', 2), ('e', 5), ('c', 3)}
\end{minted}
    \end{minipage}
  \end{center}
\end{exmp}


\subsubsection{Composição de Relações}

Novamente, a representação de relações através de conjuntos em Python
torna bastante simples a construção de relações compostas através de
compreensão de conjuntos.

\begin{exmp}
  O código abaixo define duas relações, \texttt{R}, de inteiro para
  caracter, e \texttt{S}, de caracter para string, e em seguida cria a
  relação composta $S\circ R$, de inteiro para string.

  \begin{center}
    \begin{minipage}{0.95\linewidth}
\begin{minted}{python}
>>> R = frozenset({(1, 'a'), (1,'b'), (2,'b'), (2,'c'), (3,'d'),
... (4,'e'), (4,'f'), (5,'g'), (6,'h'), (6,'i'), (7,'j')})
>>> S = frozenset({('a','alpha'), ('b','beta'), ('d', 'delta'),
... ('e', 'epsilon'), ('g', 'gamma'), ('i', 'iota')})
>>> SoR = {(x,v) for (x,y) in R for (u,v) in S if y == u}
>>> SoR
{(5, 'gamma'), (1, 'alpha'), (1, 'beta'), (3, 'delta'), (6, 'iota'),
(4, 'epsilon'), (2, 'beta')}
\end{minted}
    \end{minipage}
  \end{center}
\end{exmp}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../ilm-book"
%%% End:
