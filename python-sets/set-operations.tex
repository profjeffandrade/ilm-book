\section{Operações sobre Conjuntos}
\label{sec:py-set-operations}

Python define praticamente todas as operações sobre conjuntos que foram
vistas anteriormente neste capítulo, para a maioria das operações de
conjunto a linguagem Python define duas sintaxes diferentes, uma
utilizando operadores e uma utilizando \emph{métodos}. Apresentaremos as
duas sintaxes, mas daremos preferência ao uso da sintaxe com operadores.


% ----------------------------------------------------------------------
\subsection{Cardinalidade}

\begin{defn}[Cardinalidade]
  A quantidade de um conjunto $S$, ou seja, a quantidade de elementos de
  $S$, escrita como $|S|$, é implementada em Python pela a função
  \mintinline{python}{len(S)}.
\end{defn}

\begin{exmp}
  Sejam os conjuntos $X = \{1, 3, 5, 7, 9\}$, $Y = \{y \mid y = x^2
  \quad\text{e}\quad x \in X\}$ e $Z = \{\{x,y\} \mid x \in X
  \quad\text{e}\quad y \in Y\}$, use a operação
  \mintinline{python}{len} do Python para determinar a
  cardinalidade de cada um dos conjuntos.
  
  \begin{center}
    \begin{minipage}{0.95\linewidth}
\begin{minted}{python}
>>> X = frozenset({1, 3, 5, 7, 9})
>>> len(X)
5
>>> Y = frozenset({x**2 for x in X})
>>> len(Y)
5
>>> Z = frozenset({frozenset({x,y}) for x in X for y in Y})
>>> len(Z)
24
\end{minted}
    \end{minipage}
  \end{center}

  Como exercício, tente entender porque a cardinalidade do conjunto
  $Z$ foi 24 e não 25. Liste os elementos do conjunto $Z$.
\end{exmp}


% ----------------------------------------------------------------------
\subsection{União}

\begin{defn}[União]
  A união de dois conjuntos $A$ e $B$, $A \union B$, é implementada em
  Python pelo operador ``\texttt{|}''. Assim a união dos conjuntos $A$ e
  $B$ é escrita como \texttt{A | B} em Python. Alternativamente, também
  é possível escrever \texttt{A.union(B)} para a união.
\end{defn}

\begin{exmp}
  Sejam os conjuntos $F = \{1, 2, 3, 5, 8, 13\}$ e $P = \{2, 3, 5, 7,
  11, 13\}$. Escreva os comandos em Python que calculam a união de $F$ e
  $P$, i.e., $F \cup P$.
  
  \begin{center}
    \begin{minipage}{0.95\linewidth}
\begin{minted}{python}
>>> F = frozenset({1, 2, 3, 5, 8, 13})
>>> P = frozenset({2, 3, 5, 7, 11, 13})
>>> F | P
frozenset({1, 2, 3, 5, 7, 8, 11, 13})
\end{minted}
    \end{minipage}
  \end{center}
\end{exmp}


% ----------------------------------------------------------------------
\subsection{Interseção}

\begin{defn}[Interseção]
  A interseção de dois conjuntos $A$ e $B$, $A \intersect B$, é
  implementada em Python pelo operador ``\texttt{\&}''. Assim a união
  dos conjuntos $A$ e $B$ é escrita como \texttt{A \& B} em
  Python. Alternativamente, também é possível escrever
  \texttt{A.intersection(B)} para a interseção.
\end{defn}

\begin{exmp}
  Sejam os conjuntos $F = \{1, 2, 3, 5, 8, 13\}$ e $P = \{2, 3, 5, 7,
  11, 13\}$. Escreva os comandos em Python que calculam a interseção de $F$ e
  $P$, i.e., $F \cap P$.
  
  \begin{center}
    \begin{minipage}{0.95\linewidth}
\begin{minted}{python}
>>> F = frozenset({1, 2, 3, 5, 8, 13})
>>> P = frozenset({2, 3, 5, 7, 11, 13})
>>> F & P
frozenset({2, 3, 5, 13})
\end{minted}
    \end{minipage}
  \end{center}
\end{exmp}

% ----------------------------------------------------------------------
\subsection{Diferença}

\begin{defn}[Diferença]
  A diferença do conjunto $A$ em relação ao conjunto $B$, ou
  simplesmente, diferença entre $A$ e $B$, $A\setminus B$, é
  implementada em Python pelo operador ``\texttt{-}''. Assim a
  diferenção entre $A$ e $B$, é escrita em Python como \texttt{A - B}.
  Alternativamente, também é possível escrever \texttt{A.difference(B)}
  para a differença.
\end{defn}

\begin{exmp}
  Sejam os conjuntos $F = \{1, 2, 3, 5, 8, 13\}$ e
  $P = \{2, 3, 5, 7, 11, 13\}$. Escreva os comandos em Python que
  calculem a diferença entre $F$ e $P$, i.e., $F\setminus P$, e a
  diferença entre $P$ e $F$, i.e., $P\setminus F$.
  
  \begin{center}
    \begin{minipage}{0.95\linewidth}
\begin{minted}{python}
>>> F = frozenset({1, 2, 3, 5, 8, 13})
>>> P = frozenset({2, 3, 5, 7, 11, 13})
>>> F - P
frozenset({8, 1})
>>> P - F
frozenset({11, 7})
\end{minted}
    \end{minipage}
  \end{center}
\end{exmp}


% ----------------------------------------------------------------------
\subsection{Diferença Simétrica}

\begin{defn}[Diferença Simétrica]
  A diferença simétrica entre os conjuntos $A$ e $B$, $A\Delta B$, é
  implementada em Python pelo operador
  ``\texttt{\textasciicircum}''. Assim a diferenção simétrica entre $A$
  e $B$, é escrita em Python como \texttt{A \textasciicircum\ B}.
  Alternativamente, também é possível escrever
  \texttt{A.symmetric\_difference(B)} para a differença simétrica.
\end{defn}

\begin{exmp}
  Sejam os conjuntos $F = \{1, 2, 3, 5, 8, 13\}$ e $P = \{2, 3, 5, 7,
  11, 13\}$. Escreva os comandos em Python que calculem a diferença
  simétrica entre $F$ e $P$, i.e., $F\Delta P$.
  
  \begin{center}
    \begin{minipage}{0.95\linewidth}
\begin{minted}{python}
>>> F = frozenset({1, 2, 3, 5, 8, 13})
>>> P = frozenset({2, 3, 5, 7, 11, 13})
>>> F ^ P
frozenset({1, 7, 8, 11})
\end{minted}
    \end{minipage}
  \end{center}
\end{exmp}

Estas operações funcionam tanto para conjuntos mutáveis (\texttt{set}s)
quanto para conjuntos imutáveis (\texttt{frozenset}s). Para o caso dos
conjuntos mutáveis, há ainda algumas outras operações que alteram o
conjunto, mas nós não discutiremos essas funções neste texto.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../ilm-book"
%%% End:
