\section{Compreensão de Conjuntos}
\label{sec:set-comprehension}

Na subseção anterior vimos como definir conjuntos por extensão, ou seja,
como definir conjuntos litando todos os seus elementos. Entretanto, como
dito anteriormente neste capítulo, frequentemente é mais interessante
definir os elementos de um conjunto por intensão. Python possuir um
mecanismo muito conveniente para suprir essa necessidade, trata-se da
\emph{compreensão de conjuntos}.

Talvez a melhor forma de explicar compreensão de conjuntos seja começar
dando um exemplo de como ela é utilizada:

\begin{exmp}
  \label{exmp:set-comprehension1}

  Seja $A = \{x \mid x \in \mathbb{N} \quad\text{e}\quad 1 \leq x \leq 100\}$,
  desejamos definir o conjunto $B$ que contenha os elementos de $A$ que
  são múltiplos de 3.  A definição matemática de $B$ é dada por
  \[
  B = \{x \mid x \in A \quad\text{e}\quad x\bmod 3 = 0\}
  \]

  Em Python, as definições equivalentes de A e B seriam:

  \begin{center}
    \begin{minipage}{0.95\linewidth}
\begin{minted}{python}
>>> A = frozenset([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 
... 15, 16, 17, 18, 19])
>>> B = frozenset({x for x in A if x % 3 == 0})
>>> B
frozenset({0, 18, 3, 6, 9, 12, 15})
\end{minted}
    \end{minipage}
  \end{center}
\end{exmp}

A segunda linha do Exemplo~\ref{exmp:set-comprehension1} define o
conjunto $B$ utilizando compreensão de conjuntos. A compreensão de
conjuntos é uma expressão formada por 3 partes, que gera um conjunto.
As três partes de uma compreensão de conjuntos são mostradas no esquema
abaixo:
\[
\{\mathit{expr} ~ \mathit{fonte} ~ \mathit{filtro}\}
\]
\begin{description}
\item[expr] Parte obrigatória. Descreve a forma geral dos termos que
  irão compor o conjunto. Tipicamente envolve ao menos uma variável que
  será descrita na parte \emph{fonte}, mas o uso dessa variável não é
  obrigatório. No Exemplo~\ref{exmp:set-comprehension1} a expressão é
  $x$, o que significa que cada elemento do conjunto $B$ será formado
  pelo valor de $x$.
\item[fonte] Parte obrigatória. Uma ou mais ocorrências de uma expressão
  do tipo ``\texttt{for} \emph{var} \texttt{in} \emph{coleção}''. Onde
  \emph{var} é o nome de uma variável e \emph{coleção} é uma coleção
  (conjunto, lista, tupla, dicionário, etc.) de dados, de onde será
  obtidos os dados para a variável. No
  Exemplo~\ref{exmp:set-comprehension1} a fonte é ``\textbf{for} $x$
  \textbf{in} $A$''. O que significa que o valor de $x$ virá do conjunto
  $A$, ou seja, que a expressão será avaliada para cada um dos elementos
  do conjunto $A$.
\item[filtro] Parte opcional. Uma expressão do tipo ``\textbf{if}
  \emph{condição}''. Para cada grupo de valores gerados pela
  \emph{fonte} a condição é avaliada. Apenas se a condição for
  verdadeira é que o elementos correspondente do conjunto é gerado.  No
  Exemplo~\ref{exmp:set-comprehension1} o filtro é ``\textbf{if} $x \% 3
  == 0$'', o que significa que o item só será gerado se o resto da
  divisão de $x$ por 3 for igual a zero, ou seja, se $x$ for divisível
  por 3.
\end{description}

É muito comum, se utilizar compreensões em Python conjuntamente com o
contrutor \texttt{range}. Este construtor gera uma sequência de números
inteiros e pode ser usado de três formas diferentes:
\begin{enumerate}
\item \textbf{range($n$)} --- gera a sequência de números de $0$ até
  $n-1$, i.e., $0, 1, 2, \ldots, n-1$.
\item \textbf{range($i$, $j$)} --- gera a sequência de números de $i$
  até $j-1$, i.e., $i, i+1, \ldots, j-1$.
\item \textbf{range($i$, $j$, $d$)} --- gera a sequência de números de
  $i$ até $j-1$, com incrementos de $d$, i.e., $i, i+d, i+2d, \ldots,
  j-1$. Se o número $j-1$ não fizer parte da sequência, para no maior
  número da sequência que seja anterior a $j-1$.
\end{enumerate}

\begin{exmp}
  Crie o conjunto com os números naturais menores que 10.

  \begin{center}
    \begin{minipage}{0.95\linewidth}
\begin{minted}{python}
>>> frozenset(range(10))
frozenset({0, 1, 2, 3, 4, 5, 6, 7, 8, 9})
\end{minted}
    \end{minipage}
  \end{center}
\end{exmp}

\begin{exmp}
  Crie o conjunto com os números naturais menores ou iguais a 10.
  \begin{center}
    \begin{minipage}{0.95\linewidth}
\begin{minted}{python}
>>> frozenset(range(11))
frozenset({0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10})
\end{minted}
    \end{minipage}
  \end{center}
\end{exmp}

\begin{exmp}
  Crie o conjunto com os números inteiro entre -20 e 10, inclusive.
  \begin{center}
    \begin{minipage}{0.95\linewidth}
\begin{minted}{python}
>>> frozenset(range(-20,11))
frozenset({0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, -20, -19, -18,
-17, -16, -15, -14, -13, -12, -11, -10, -9, -8, -7, -6, -5,
-4, -3, -2, -1})
\end{minted}
    \end{minipage}
  \end{center}
\end{exmp}

\begin{exmp}
  Crie o conjunto com os primeiros 10 múltiplos de 7 (use compreensão).

  \textbf{Solução 1:}\vspace{-1ex}
  \begin{center}
    \begin{minipage}{0.95\linewidth}
\begin{minted}{python}
>>> frozenset({x*7 for x in range(10)})
frozenset({0, 35, 7, 42, 14, 49, 21, 56, 28, 63})
\end{minted}
    \end{minipage}
  \end{center}
  
  \textbf{Solução 2:}\vspace{-1ex}
  \begin{center}
    \begin{minipage}{0.95\linewidth}
\begin{minted}{python}
>>> frozenset({x for x in range(10*7) if x % 7 == 0})
frozenset({0, 35, 7, 42, 14, 49, 21, 56, 28, 63})
\end{minted}
    \end{minipage}
  \end{center}
\end{exmp}

\begin{exmp}
  Crie o conjunto contendo os quadrados dos números inteiros de 1 a 10.

  \begin{center}
    \begin{minipage}{0.95\linewidth}
\begin{minted}{python}
>>> frozenset({x*x for x in range(1,11)})
frozenset({64, 1, 4, 36, 100, 9, 16, 49, 81, 25})
\end{minted}
    \end{minipage}
  \end{center}
\end{exmp}

\begin{exmp}
  Sejam os conjuntos $A = \{10, 30, 50\}$ e $B = \{20, 40, 60\}$, crie o
  conjunto $C$ contendo elementos formados à partir da soma dos
  elementos de $A$ com os elementos de $B$, ou seja, $C = \{x+y \mid x
  \in A \quad\text{e}\quad y \in B\}$.

  \begin{center}
    \begin{minipage}{0.95\linewidth}
\begin{minted}{python}
>>> A = frozenset({10, 30, 50})
>>> B = frozenset({20, 40, 50})
>>> C = frozenset({x+y for x in A for y in B})
>>> C
frozenset({80, 50, 100, 70, 90, 60, 30})
\end{minted}
    \end{minipage}
  \end{center}
\end{exmp}

\begin{exmp}
  Dados os conjuntos $A = \{1, 2, 3\}$ e $B = \{a, b, c\}$, gere o
  conjunto $A\times B$.

  \begin{center}
    \begin{minipage}{0.95\linewidth}
\begin{minted}{python}
>>> A = frozenset({1, 2, 3})
>>> B = frozenset("abc")
>>> frozenset({(x,y) for x in A for y in B})
frozenset({(3, 'a'), (1, 'a'), (1, 'c'), (3, 'c'), (2, 'b'),
(3, 'b'), (2, 'c'), (2, 'a'), (1, 'b')})
\end{minted}
    \end{minipage}
  \end{center}
\end{exmp}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../ilm-book"
%%% End:
