\section{Relações entre Conjuntos}
\label{sec:relat-over-sets}

As relações básicas de conjuntos (pertinência, igualdade, subconjunto)
são todas definidas na linguagem Python através de operadores. Suas
sintaxes são explicadas abaixo.

% ----------------------------------------------------------------------
\subsection{Pertinência}

\begin{defn}[Pertinência]
  A relação de pertinêcia em Python é implementada pelos operadores
  \pyinline{in} e \pyinline{not in}. Deste modo, para testar se o valor de
  $x$ pertence a um conjunto $S$, i.e., $x \in S$, em Python
  escre\-vemos \pyinline{x in S}.  De modo similar, para testar se $x
  \notin S$, escrevemos \pyinline{x not in S}.
\end{defn}

\begin{exmp}
  Escreva os comandos em Python que testem se o elemento $3$ pertence ao
  conjunto $A = \{1, 3, 5, 7\}$.

\begin{minted}{python}
>>> A = frozenset({1, 3, 5, 7})
>>> 3 in A
True
\end{minted}

\end{exmp}

\begin{exmp}
  Escreva os comandos em Python que testem se o elemento $3$ pertence ao
  conjunto $B = \{x \mid x \in \mathbb{Z} \quad\text{e}\quad {-100} \leq
  x \leq 100\}$.
  
\begin{minted}{python}
>>> B = frozenset(range(-100,101))
>>> 3 in B
True
\end{minted}

\end{exmp}

\begin{exmp}
  Escreva os comandos em Python que testem se o elemento $7$ não
  pertence ao conjunto dos múltiplos de 3 entre 0 e 100.
  
\begin{minted}{python}
>>> multi3 = frozenset({x for x in range(101) if x % 3 == 0})
>>> 7 not in multi3
True
\end{minted}

\end{exmp}

\begin{exmp}
  Escreva os comandos em Python para verificar que o conjunto das
  soluções inteiras, entre 0 e 100, da equação $x^3-6x^2+3x+10=0$ é
  igual a $\{2,5\}$.
  
\begin{minted}{python}
>>> X = frozenset({x for x in range(101) if x**3 - 6*x**2 + 3*x + 10 == 0})
>>> X == frozenset({2, 5})
True
\end{minted}

\end{exmp}


\subsection{Igualdade}

\begin{defn}[Igualdade]
  Para testar se dois conjuntos $A$ e $B$ são iguais em Python,
  escrevemos \pyinline{A == B}. De modo semelhante, para testar se $A \neq
  B$ escrevemos \pyinline{A != B}.
\end{defn}

\begin{exmp}
  Escreva os comandos em Python que testem se os conjuntos $S = \{1, 3,
  5, 7, 9\}$ e $T = \{x \mid x \in \mathbb{N} \quad\text{e}\quad 0 < x <
  10 \quad\text{e}\quad x\bmod 2 = 0\}$ são iguais.
  
\begin{minted}{python}
>>> S = frozenset({1,3,5,7,9})
>>> T = frozenset({x for x in range(1,10) if x % 2 == 0})
>>> S == T
False
>>> S
frozenset({9, 1, 3, 5, 7})
>>> T
frozenset({8, 2, 4, 6})
\end{minted}

\end{exmp}

\begin{exmp}
  Escreva os comandos em Python que testem se os conjuntos $S = \{1, 3,
  5, 7, 9\}$ e $T = \{x \mid x \in \mathbb{N} \quad\text{e}\quad 0 < x <
  10 \quad\text{e}\quad x\bmod 2 \neq 0\}$ são iguais.
  
\begin{minted}{python}
>>> S = frozenset({1,3,5,7,9})
>>> T = frozenset({x for x in range(1,10) if x % 2 != 0})
>>> S == T
True
>>> S
frozenset({9, 1, 3, 5, 7})
>>> T
frozenset({1, 9, 3, 5, 7})
\end{minted}

\end{exmp}


% ----------------------------------------------------------------------
\subsection{Subconjunto}

\begin{defn}[Subconjunto]
  Para testar se um conjunto $A$ é subconjunto de $B$, i.e., $A \subseteq B$, em
  Python escrevemos \pyinline{A <= B}. Também é possível escrever
  \pyinline{A.issubset(B)}, com o mesmo significado.
\end{defn}

\begin{exmp}
  Sejam os conjuntos $F = \{1, 2, 3, 5, 8, 13, 21\}$, $P = \{2, 3, 5, 7,
  11, 13, 17\}$, $I = F\cap P$ e $J = F\cup P$. Escreva os comandos em
  Python que testem se $F\subseteq P$, $P\subseteq F$, $I\subseteq F$,
  $I\subseteq P$, $F\subseteq J$, $P\subseteq J$, $I\subseteq J$,
  $I\subseteq I$ e $J\subseteq J$.

\begin{minted}{python}
>>> F = frozenset({1, 2, 3, 5, 8, 13, 21})
>>> P = frozenset({2, 3, 5, 7, 11, 13, 17})
>>> I = F & P
>>> J = F | P
>>> F <= P
False
>>> P <= F
False
>>> I <= F
True
>>> I <= P
True
>>> F <= J
True
>>> P <= J
True
>>> I <= J
True
>>> I <= I
True
>>> J <= J
True
\end{minted}
\end{exmp}


% ----------------------------------------------------------------------
\subsection{Subconjunto Próprio}

\begin{defn}[Subconjunto Próprio]
  A expressão \pyinline{A < B}, em Python, significa que $A$ é
  subconjunto próprio de $B$, ou seja, \pyinline{A < B}, significa que
  \pyinline{A <= B} e \pyinline{A != B}.
\end{defn}

\begin{exmp}
  Sejam os conjuntos $F = \{1, 2, 3, 5, 8, 13, 21\}$, $P = \{2, 3, 5, 7,
  11, 13, 17\}$, $I = F\cap P$ e $J = F\cup P$. Escreva os comandos em
  Python que testem se $F\subset P$, $P\subset F$, $I\subset F$,
  $I\subset P$, $F\subset J$, $P\subset J$, $I\subset J$, $I\subset I$ e
  $J\subset J$.
  
\begin{minted}{python}
>>> F = frozenset({1, 2, 3, 5, 8, 13, 21})
>>> P = frozenset({2, 3, 5, 7, 11, 13, 17})
>>> I = F & P
>>> J = F | P
>>> F < P
False
>>> P < F
False
>>> I < F
True
>>> I < P
True
>>> F < J
True
>>> P < J
True
>>> I < I
False
>>> J < J
False
\end{minted}
\end{exmp}


% ----------------------------------------------------------------------
\subsection{Superconjunto e Superconjunto Próprio}

Os conceitos de \emph{superconjunto} e \emph{superconjunto próprio} são
simétricos aos conceitos de subconjunto e subconjunto próprio.

\begin{defn}[Superconjunto e Superconjunto Próprio]
  Dados dois conjuntos $A$ e $B$, dizemos que $A$ é superconjunto de
  $B$, escrito $A \supseteq B$, se todo elemento que pertence a $B$
  também pertence a $A$. De modo análogo, dizemos que $A$ é superconjunto
  próprio de $B$, escrito $A \supset B$, se todo elemento de $B$ pertence
  a $A$, e existe ao menos um elementos de $A$ que não pertence a $B$, ou seja,
  $A\supset B \equiv (A\supseteq B \;\text{e}\; A\neq B)$.
\end{defn}

Estritamente falando, os operadores $\supset$ e $\supseteq$ são
desnecessários pois sempre é o caso que $A\supseteq B = B\subseteq A$ e
$A\supset B = B\subset A$. Apesar desses operadores não serem
necessários, a linguagem Python define os operadores de superconjunto e
superconjunto próprio.

Em Python a expressão \pyinline{A >= B} significa que $A$ é superconjunto de
$B$. e \pyinline{A > B} significa que $A$ é superconjunto próprio de $B$.

\begin{exmp}

\begin{minted}{python}
>>> F = frozenset({1, 2, 3, 5, 8, 13})
>>> E = frozenset({1, 2, 3, 5, 8})
>>> F >= E
True
>>> F > E
True
>>> F >= F
True
>>> F > F
False
>>> E >= E
True
>>> E > E
False
\end{minted}

\end{exmp}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../ilm-book"
%%% ispell-local-dictionary: "brasileiro"
%%% End:
