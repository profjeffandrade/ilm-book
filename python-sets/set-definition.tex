\section{Definição de Conjuntos}
\label{sec:set-defn}

Desde a versão 2.6, Python suporta conjuntos como tipos de dados
pré-definidos, através dos tipos \texttt{set} e \texttt{frozenset}.
%
Um conjunto em Python é uma coleção não-ordenada de objetos. Isso
significa que, diferentemente de listas e tuplas, um conjunto não pode
ser indexado, ou seja, não é aceito pela linguagem Python que tente
acessar os elementos de um conjunto por um índice.
%
Conjuntos não podem ter elementos duplicados, ou seja, um objeto
qualquer aparece exatamente 0 ou 1 vez em um conjunto.
%
Além disso, todos os elementos de um conjunto precisam ser
imutáveis\footnote{Na verdade, a definição oficial diz que o objeto
  precsa ser \emph{hashable}, ou seja, tem que ser possível calcular um
  valor para a função de \emph{hashing} do objeto e este valor não pode
  mudar durante todo o tempo de vida do objeto. Na prática, um objeto
  \emph{hashable} é quase sempre um objeto imutável.} Números inteiros,
números em ponto flutuante, tuplas, e strings, por exemplo, são objetos
imutaveis. Por outro lado, listas, dicionários, e conjuntos
(\texttt{set}s) não são imutáveis e portanto não podem ser elementos de
um conjunto em Python.

A restrição de que um conjunto não pode ser elemento de outro conjunto
constitui um problema sério, pois há vários algoritmos que utilizam
conjuntos de conjuntos em seu processamento. Para contornar essa
dificuldade a linguagem Python suporta um tipo de dados chamado
\texttt{frozenset}, que representam conjutnos imutáveis, ou seja, uma
vez que o conjunto tenha sido criado, não é possível alterar que objetos
compõem o conjunto. Assim, um \texttt{frozenset} pode ser elementos de
um conjunto, e pode também ter outros \texttt{frozenset}s como seus
elementos.

Uma forma de construir conjuntos em Python é passar um objeto qualquer
que represente uma sequência para o construtor \texttt{set}. Por exemplo:

\begin{center}
  \begin{minipage}{0.95\linewidth}
\begin{minted}{python}
>>> set()
set()
>>> set("estudantes")
{'d', 't', 'e', 'n', 'u', 'a', 's'}
>>> set([0,1,2,3])
{0, 1, 2, 3}
>>> set((2,0,1,4))
{0, 1, 2, 4}
>>> set([('a',1), ('b',2), ('c',3), ('d',4)])
{('a', 1), ('c', 3), ('b', 2), ('d', 4)}
>>> set([0, set()])
Traceback (most recent call last):
  File "<pyshell#12>", line 1, in <module>
    set([0, set()])
TypeError: unhashable type: 'set'
\end{minted}
  \end{minipage}
\end{center}

É importante notar alguns pontos no exemplo acima:

\begin{enumerate}
\item A forma de criar um conjuntos vazio ($\emptyset$) é usando o
  comando \texttt{set()}.
\item Caso haja repetições de elementos na sequência passada ao
  construtor (como as letras repetidas ``e'' e ``s'' da palavra
  ``estudantes'') estas repetições serão eliminadas.
\item A forma padrão com Python representa conjuntos
  mutáveis é através do uso de chaves (`\{',`\}').
\item A tentativa de criar um conjunto, em que um dos elementos era um
  conjunto vazio, gerou um erro.
\end{enumerate}

Para construir conjuntos imutaveis pode-se fazer o mesmo, porém
trocando-se o contrutor \texttt{set} pelo construtor \texttt{frozenset}:

\begin{center}
  \begin{minipage}{0.95\linewidth}
\begin{minted}{python}
>>> frozenset()
frozenset()
>>> frozenset("estudantes")
frozenset({'d', 't', 'e', 'n', 'u', 'a', 's'})
>>> frozenset([0,1,2,3])
frozenset({0, 1, 2, 3})
>>> frozenset((2,0,1,4))
frozenset({0, 1, 2, 4})
>>> frozenset([('a',1), ('b',2), ('c',3), ('d',4)])
frozenset({('a', 1), ('c', 3), ('b', 2), ('d', 4)})
>>> frozenset([0, frozenset()])
frozenset({0, frozenset()})
\end{minted}
  \end{minipage}
\end{center}

Note que ao tentar criar um conjunto contendo como elementos o zero e o
conjunto imutável vazio, não houve erro e o conjunto foi criado
corretamente.

No caso de conjuntos mutáveis, ou seja, \texttt{set}s, em alguns casos é
possível usar uma sintaxe abreviada para a criação de conjuntos,
substituindo a chamada ao construtor \texttt{set(\ldots)} pelo uso de
chaves (`\{', `\}'). Por exemplo:

\begin{center}
  \begin{minipage}{0.95\linewidth}
\begin{minted}{python}
>>> {1, 2, 3, 4}
{1, 2, 3, 4}
>>> {('a', 1), ('c', 3), ('b', 2), ('d', 4)}
{('c', 3), ('a', 1), ('b', 2), ('d', 4)}
>>> {'d', 't', 'e', 'n', 'u', 'a', 's'}
{'d', 't', 'e', 'n', 'u', 'a', 's'}
\end{minted}
  \end{minipage}
\end{center}

Entretanto, esta sintaxe abreviada não pode ser utilizada em todos os
casos. A expressão \texttt{\{\}}, por exemplo, não retorna um conjunto
vazio, ao invés disso ela retorna um objeto do tipo \texttt{dict}, i.e.,
um dicionário. Também não é possível criar um conjunto com as letras de
uma palavra utilizando-se a sintaxe abreviada. Ao invés de obter um
conjunto com a letras, o que obteremos é um conjunto com a palavra
inteira como 1 elemento.

\begin{center}
  \begin{minipage}{0.95\linewidth}
\begin{minted}{python}
>>> {}
{}
>>> type({})
<class 'dict'>
>>> {"estudantes"}
{'estudantes'}
\end{minted}
  \end{minipage}
\end{center}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../ilm-book"
%%% Endo:
